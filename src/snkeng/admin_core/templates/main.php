<?php
$siteVars = $GLOBALS["siteVars"];

// Compatibility old method, should be added during group?
\snkeng\core\engine\nav::pageFileGroupAdd(['admin_full', 'admin_core_template', 'core_main']);
// \snkeng\core\engine\nav::pageFileGroupAdd(['admin_full', 'core_main']);

// SVG Icons
\snkeng\core\engine\nav::svgIconsAdd('font-awesome-4-7-0');

// Module JS version
\snkeng\core\engine\nav::pageFileModuleAdd('admin_core', '', '/index.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_core', '', '/templates/main.mjs');

// Load available apps
$apps = ( require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_apps.php' );

//<editor-fold desc="Menu lateral">
$menu = '';
// Administración
if ( \snkeng\core\engine\login::check_loginLevel('uadmin') ) {
	$menu .= '<a class="menuElem" se-nav="se_main" title="Super Administración" href="/admin/core/admin_uber/"><svg class="icon inline"><use xlink:href="#fa-gears"/></svg><span class="title">Uber Administración</span></a>'."\n";
}
if ( \snkeng\core\engine\login::check_loginLevel('sadmin') ) {
	// Super admin
	$menu .= '<a class="menuElem" se-nav="se_main" title="Administración" href="/admin/core/admin_super/"><svg class="icon inline"><use xlink:href="#fa-gear"/></svg><span class="title">Administración</span></a>'."\n";
	// Site Core
	$menu .= '<a class="menuElem" se-nav="se_main" title="Sitio" href="/admin/core/site/"><svg class="icon inline"><use xlink:href="#fa-sitemap"/></svg><span class="title">Sitio</span></a>'."\n";

	// Todas disponibles
	foreach ( $apps as $app ) {
		$menu .= '<a class="menuElem" se-nav="se_main" title="' . $app['title'] . '" href="/admin/site/' . $app['name'] . '/"><svg class="icon inline"><use xlink:href="#fa-' . $app['icon'] . '"/></svg><span class="title">' . $app['title'] . '</span></a>' . "\n";
	}
} else {
	// Verificar por permiso
	$validAppList = [];
	foreach (\snkeng\core\engine\login::$rulesCurrentPermits as $appName => $permLevel ) {
		$validAppList[] = $appName;
	}

	// Check for site content (only exception to other apps since its admin is contained win admin core, not in site modules)
	if ( in_array('site', $validAppList) ) {
		// Site Core
		$menu .= '<a class="menuElem" se-nav="se_main" title="Sitio" href="/admin/core/site/"><svg class="icon inline"><use xlink:href="#fa-sitemap"/></svg><span class="title">Sitio</span></a>'."\n";
	}

	// All other apps in list
	foreach ( $apps as $app ) {
		if ( in_array($app['name'], $validAppList) ) {
			$menu .= '<a class="menuElem" se-nav="se_main" title="' . $app['title'] . '" href="/admin/site/' . $app['name'] . '/"><svg class="icon inline"><use xlink:href="#fa-' . $app['icon'] . '"/></svg><span class="title">' . $app['title'] . '</span></a>' . "\n";
		}
	}
}

//</editor-fold>

//
$page['body'] = <<<HTML
<div id="se-admin-template">
<!-- INI: TopBar -->
<div class="template-top-bar">
	<div class="template-top-bar-menu-left">
		<a class="simple" onclick="$('#se_body').classList.toggle('menuShow')"><svg class="icon inline"><use xlink:href="#fa-navicon"/></svg></a>
		<a class="simple" href="/" target="_blank"><svg class="icon inline"><use xlink:href="#fa-sitemap"/></svg></a>
	</div>
	<div class="template-top-bar-menu-right">
		<div class="se_menu_parent se_menu_hover" id="se_user">
			<div class="se_menu_button">
				<picture data-site-user="image_square" data-width="40">
					<img class="img" src="/res/image/objects/w_40/{$siteVars['user']['data']['image_square']}.jpg">
				</picture>
				<span data-site-user="name_full" class="text">{$siteVars['user']['data']['image_square']}</span>
			</div>
			<div class="se_menu_hidden se_menu_simple">
				<a se-nav="se_main" href="/admin/core/user/settings/"><svg class="icon inline mr"><use xlink:href="#fa-user"/></svg>Cuenta</a>
				<a onclick="se.user.logOut('/admin');"><svg class="icon inline mr"><use xlink:href="#fa-sign-out"/></svg>Cerrar Sesión</a>
			</div>
		</div>
	</div>
</div>
<!-- END: TopBar -->\n\n
<!-- INI: BODY -->
<div class="template-body">
	<!-- INI: Vertical -->
	<div class="template-vertical-menu" aria-expanded="false">
		<!-- INI: Navigation -->
		<nav class="template-vertical-menu-nav">
{$menu}
		</nav>
		<!-- END: Navigation -->
	</div>
	<!-- END: Vertical -->\n
	<!-- INI: Content -->\n
	<div class="template-content">
		<!-- INI: Mid -->
		<div id="se_main">{$page['body']}</div>
		<!-- END: Mid -->\n\n
		<!-- INI: Foot -->
		<footer class="template-footer">
			<b>{$siteVars['site']['name']}</b><br />
			SNK Engine
		</footer>
		<!-- END: Foot -->
		</div>
	</div>
	<!-- END: CONTENT -->
</div>
<!-- END: Body -->
HTML;
//
