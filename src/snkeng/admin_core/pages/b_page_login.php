<?php
// Archivos a cargar

//
$page['title'] = "Login";

//
$page['body'] = <<<HTML
<style>
.container { display:grid; place-items:center; height:100vh; width:100vw; }
#login { background-color:var(--color-main-00-norm-n); color:var(--color-main-00-comp-n); border:thin solid #999; border-radius:5px; width:300px; box-shadow:0 3px 5px #e2e2e2; }
#login h2 { padding:10px; margin:0; background-color:var(--color-main-04-norm-n); color:var(--color-main-04-comp-n); font-size:1.8rem; font-weight:bold; }
#login > .content { padding:10px; }
#uLogin { visibility:hidden; }
label { display:block; margin-bottom:10px; }
form .title { display:block; font-weight:bold; margin-bottom:5px; }
form .title a { float:right; font-size:1.1rem; }
input { width:100%; border-radius:3px; padding:5px; border:1px solid #CCC; box-sizing:border-box; }
input[type="checkbox"] { width:auto; }
button { cursor:pointer; border:1px solid #4c7bff; border-radius:5px; padding:6px 10px; background-color:#5499ff; color:#FFF; font-weight:bold; margin:0 auto; display:block; }
/* */
@media only screen and (max-width:767px) {
	.container { display:block; }
	#login { display:block; margin:10px auto; width:calc(100% - 20px); }
}
/* */
svg.icon { display:block; vertical-align:sub; fill:currentColor; height:1em; width:1em; }
svg.icon.inline { display:inline-block; }
svg.icon.inline.mr { margin-right:0.5em; }
</style>\n\n
<div id="login">
	<h2>Login</h2>
	<div class="content">
		<form class="se_form" is="se-login-form" data-target="/admin/">
			<label class="separator required">
				<span class="title">Correo electrónico</span>
				<input type="email" class="elem" name="email" placeholder="Correo electrónico"  title="Correo electrónico" tabindex="1" autofocus required autocomplete="username" />
			</label>
			<label class="separator required">
				<span class="title">Contraseña <a se-nav="se_template_root" href="/admin/guest/recovery">Recuperar contraseña</a></span>
				<input type="password" class="elem" name="current-password" placeholder="Contraseña" title="Contraseña" tabindex="2" required autocomplete="current-password" />\n
			</label>
			<label class="checkbox"><input type="checkbox" se-elem="showPass" tabindex="3" /><span>Ver Contraseña</span></label>
			<label class="checkbox"><input type="checkbox" name="a_save" value="t" tabindex="4" checked />Recordar sesión</label>
			<button class="btn blue center big gOMB" type="submit" tabindex="3"><svg class="icon inline mr"><use xlink:href="#fa-sign-in"/></svg>Login</button>
			<output se-elem="response"></output>
		</form>
		<a href="/">Ir a página principal</a>
	</div>
</div>
HTML;
//
