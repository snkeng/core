<?php
$_SERVER['DOCUMENT_ROOT'] = substr($_SERVER['DOCUMENT_ROOT'], 0, -12);

// Start head (ENV, MYSQL, AUTOLOAD)
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/head.php';

// mysql upgrade
\snkeng\core\engine\mysql::changeConnection($_ENV['MYSQL_ADM_USR'], $_ENV['MYSQL_ADM_PWD']);


// Check if it has tables
if ( \snkeng\core\engine\mysql::nullQuery("SHOW TABLES;") ) {
	$sql_qry = file_get_contents(__DIR__ . '/db_structure.sql');
	\snkeng\core\engine\mysql::submitMultiQuery($sql_qry);
}


//
$htmlContent = '';
$formError = '';
$formErrors = [];
$hasUser = false;
$showForm = true;
$userData = [
	'email' => '',
	'fname' => '',
	'lname' => '',
	'passw' => '',
];

//
if ( \snkeng\core\engine\mysql::singleNumber("SELECT COUNT(*) FROM sb_objects_obj;") === 0 ) {
	$hasUser = true;

	// No users, add option?
	if ( !empty($_POST) ) {
		//
		$userData = filter_input_array(
			INPUT_POST,
			[
				'email' => FILTER_SANITIZE_EMAIL,
				'fname' =>
					[
						'filter'    => FILTER_UNSAFE_RAW,
						'flags'     => FILTER_FLAG_STRIP_LOW
					],
				'lname' =>
					[
						'filter'    => FILTER_UNSAFE_RAW,
						'flags'     => FILTER_FLAG_STRIP_LOW
					],
				'passw' =>
					[
						'filter'    => FILTER_UNSAFE_RAW,
						'flags'     => FILTER_FLAG_STRIP_LOW
					]
			],
			true
		);

		//
		array_map(
			function ($cEl) {
				return trim($cEl);
			},
			$userData
		);

		//
		if ( !filter_var($userData['email'],FILTER_VALIDATE_EMAIL) ){
			$formErrors[] = "Correo electrónico no válido.";
		}

		//
		if ( empty($userData['fname']) ){
			$formErrors[] = "Nombre no disponible.";
		}

		//
		if ( empty($userData['lname']) ){
			$formErrors[] = "Nombre no disponible.";
		}

		//
		if ( empty($userData['passw']) ){
			$formErrors[] = "Contraseña no disponible.";
		} else if ( strlen($userData['passw']) < 4 ) {
			$formErrors[] = "Contraseña demasiado corta.";
		}

		//
		if ( !empty($formErrors) ) {
			$formError = "<ul>\n<li>";
			$formError.= implode("</li>\n<li>", $formErrors);
			$formError.= "</li>\n</ul>";
		} else {
			//
			$response = [];

			// Crear usuario
			\snkeng\core\user\management::userCreate($response, $userData['fname'], $userData['lname'], 0, 'mail', [
				'eMail' => $userData['email'],
				'pass' => $userData['passw'],
				'mailStatus' => 1
			]);

			$showForm = false;
		}
	}
} else {
	$showForm = false;
	echo "Installer already run.";
	exit();
}

if ( $showForm ) {
	//
	$htmlContent = <<<HTML
<div class="formError">{$formError}</div>
<div>Database structure is ready.</div>
<div>Create super administrator.</div>
<form class="simpleForm" method="post">
	<label>
		<div class="name">First Name</div>
		<input type="text" name="fname" value="{$userData['fname']}" minlength="3" maxlength="75" required>
	</label>
	<label>
		<div class="name">Last Name</div>
		<input type="text" name="lname" value="{$userData['lname']}" minlength="3" maxlength="75" >
	</label>
	<label>
		<div class="name">E-Mail</div>
		<input type="text" name="email" value="{$userData['email']}" required>
	</label>
	<label>
		<div class="name">Password</div>
		<input type="password" name="passw" value="{$userData['passw']}" minlength="4" maxlength="40" required>
	</label>
	<button type="submit">Create Super Admin</button>
</form>
HTML;
	//
} else {
	//
	$htmlContent = <<<HTML
User created. Application is ready to use.
HTML;
	//
}


//
header('Content-type: text/html; charset=utf-8');

//
echo <<<HTML
<html lang="es">
<head>
<title>First Installation</title>
<style>
body { margin:0; padding:0; border:0; background-color:#a0eeca; min-height:100vh; font-family: Tahoma, sans-serif; }
.container { margin:0; padding:0; border:0; display:grid; place-items:center; background-color:#a0eeca; height:100vh; width:100vw; }
#floatWindow { margin:0; padding:20px; border:1px solid #000; border-radius:4px; box-shadow:5px 4px 5px 0 rgba(0,0,0,0.75); background-color:#FFF; color:#000; height:min-content; width: 30%; min-width: 300px; }
#floatWindow .title { font-size:2em; margin-bottom:1em; font-weight:bold; text-align:center; }
.formError { border:1px solid #000; padding:10px; color:#000; background-color:#ffd5d5; margin-bottom:20px; }
.formError li { padding-left:1em; }
.formError:empty { display:none; }
.simpleForm {}
.simpleForm label { padding:0; border:0; display:block; }
.simpleForm label .name { font-weight:bold; margin-bottom:10px; }
.simpleForm input { width:100%; border-radius:3px; border:1px solid #000; }
.simpleForm input:invalid { border-color:#900; }
.simpleForm button { width:100%; border-radius:3px; border:1px solid #000; padding:5px; }
.simpleForm > *:not(:first-child) { margin-top:15px; }
</style>
</head>

<body>
<div class="container">
	<div id="floatWindow">
		<div class="title">First installation</div>
		{$htmlContent}
	</div>
</div>
</body>

</html>
HTML;
//
