<?php
// TODO: cleanup

//
namespace snkeng\core\admin;

//
use snkeng\core\admin\Facebook;

class notifications
{
	// Variables
	public $mysql;
	public $siteVars;

	private $sql_add = "";                            // Query al cual se le asocian todas las notificaciones (hacer el update de las notificaciones en un sólo query)
	private $fbNotifications = array();                // Lista de usuarios a los cuales hay que enviar notificación

	// Manejadores de actualizaciones
	public $add_sql = "";
	public $upd_sql = "";
	public $add_sql_f = true;
	public $addCountersIds = array();

	public function __construct()
	{
		global $mysql, $siteVars;
		$this->mysql =& $mysql;
		$this->siteVars =& $siteVars;
		seLoad_engFunc('saveData');
	}

	// ===========================================
	//	Funciones de impresión de notificaciones
	// ===========================================
	// {

	//
	// INIT: notifAdd
	//	Elemento para la actualización de notificaciones
	public function notifRead()
	{
		$abort = false;
		$error = '';
		$response = array();
		$dataCount = 0;

		// Obtención variables
		$first = date("Y-m-d H:i:s", intval($this->vars['post']['f']));
		$last = date("Y-m-d H:i:s", intval($this->vars['post']['l']));
		switch ( $this->vars['post']['op'] ) {
			case 'all':
				$limit = 20;
				break;
			case 'new':
				$limit = 50;
				$where .= " AND notif_dtadd>'$last'";
				break;
			case 'old':
				$limit = 20;
				$where .= " AND notif_dtadd<'$first'";
				break;
			case 'five':
				$limit = 5;
				break;
			case 'fiveNew':
				$limit = 5;
				$where .= " AND notif_dtadd>'$last'";
				break;
			case 'fiveOld':
				$limit = 5;

				$where .= " AND notif_dtadd<'$first'";
				break;
			default:
				$error = "Caso no definido.";
				$abort = true;
				break;
		}
		if ( !$abort ) {
			$sql_qry = "SELECT
							notif_id, obj_id, notif_dtadd, notif_status, ntype_id, notif_text, notif_url
						FROM sb_notif_obj
						WHERE obj_id={$this->vars['obj']['id']} $where
						ORDER BY notif_status ASC, notif_dtadd DESC
						LIMIT $limit;";
			// die($sql_qry);
			if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
				$messagesR = array();
				$upd_qry = "";
				$url = (!empty($_SERVER['HTTPS'])) ? "https://" . $_SERVER['HTTP_HOST'] : "http://" . $_SERVER['HTTP_HOST'];
				$xUrl = (intval($this->vars['post']['nW']) === 1) ? $url : '';
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
					$temp = array();
					$temp['id'] = $datos['notif_id'];
					$temp['em'] = $datos['obj_id'];
					$temp['dt'] = $datos['notif_dtadd'];
					$temp['st'] = intval($datos['notif_status']);
					$temp['tp'] = $datos['ntype_id'];
					$temp['txt'] = $datos['notif_text'];
					$temp['url'] = $xUrl . $datos['notif_url'];
					$messagesR[] = $temp;
					// Crear update query
					if ( $temp['st'] === 1 ) {
						$upd_qry .= "UPDATE sb_notif_obj SET notif_status=2 WHERE notif_id={$temp['id']};\n";
					}

					// Actualizar contador
					$dataCount++;
				}

				// Determinación de cual es el primero y el último
				switch ( $this->vars['post']['op'] ) {
					case 'new':
					case 'fiveNew':
						$response['c']['l'] = strtotime($messagesR[0]['dt']);
						break;
					case 'old':
					case 'fiveOld':
						$response['c']['f'] = strtotime($messagesR[count($messagesR) - 1]['dt']);
						break;
					case 'all':
					case 'five':
						$response['c']['l'] = strtotime($messagesR[0]['dt']);
						$response['c']['f'] = strtotime($messagesR[count($messagesR) - 1]['dt']);
						break;
				}
				// Actualizar cambios
				if ( !empty($upd_qry) ) {
					if ( !\snkeng\core\engine\mysql::submitMultiQuery($upd_qry) ) {
						$error = "Actualizar leidos." . \snkeng\core\engine\mysql::error;
					}
					$sql_qry = "SELECT COUNT(*) FROM sb_notif_obj WHERE obj_id={$this->vars['obj']['id']} AND notif_status=1;";
					$countMsg = \snkeng\core\engine\mysql::singleNumber($sql_qry);
					$ins_qry = "UPDATE st_socnet_objects SET notif_count=$countMsg WHERE obj_id={$this->vars['obj']['id']};";
					if ( !\snkeng\core\engine\mysql::submitQuery($ins_qry) ) {
						$error = "Actualizar contador.";
					}
				} else {
					$countMsg = 0;
				}

				// Guardar
				$response['d'] = $messagesR;
				$response['c']['nm'] = $countMsg;
			} else {
				if ( \snkeng\core\engine\mysql::isError ) {
					$error = \snkeng\core\engine\mysql::error;
				}
			}
		}

		// Datos finales
		$response['c']['n'] = $dataCount;

		// Determinar el status del mensaje y regresar el array
		if ( empty($error) ) {
			$response['s']['t'] = 1;
		} else {
			$response['s']['t'] = 0;
			$response['s']['e'] = "ERROR (SISTEMA): Notif - " . $error;
		}
		return $response;
	}
	// END: createComment
	//

	// }
	// ===========================================
	//	Funciones básicas de notificación
	//

	// ===========================================
	//	Funciones generales
	// ===========================================
	// {

	//
	// INIT: createNotifications
	//	Elemento para la actualización de notificaciones
	public function createNotifications($appId, $type, $itemId, $authorId, $unotText, $unotURL, $allwaysNew, $forceAddCounter)
	{
		// Hacer operación de actualización por todos los objetos en la lista
		$unotId = 0;
		$addCounter = false;

		// Revisar notificación anterior
		if ( !$allwaysNew ) {
			$sql_qry = "SELECT notif_id, notif_count, notif_counttot, notif_status FROM sb_notif_obj WHERE notif_app_id={$appId} AND notif_act_id={$type} LIMIT 1;";
			$notifData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
			$unotId = intval($notifData['notif_id']);
			$nCountN = intval($notifData['notif_count']);
			$nCountTotN = intval($notifData['notif_counttot']);
			$nCountN = ($nCountN < 250) ? $nCountN + 1 : $nCountN;
			$nCountTotN = ($nCountTotN < 65500) ? $nCountTotN + 1 : $nCountTotN;
			$addCounter = (intval($notifData['notif_status']) === 0) ? true : false;
		}

		// Revisar el trato necesario
		if ( $allwaysNew || $unotId === 0 ) {
			// Nueva
			$add_qry = "INSERT INTO sb_notif_obj
						(notif_app_id, notif_act_id, notif_item_id, obj_id, notif_dtadd, notif_status, notif_text, notif_url)
						VALUES
						({$appId}, {$type}, {$itemId}, {$authorId}, now(), 1, '{$unotText}', '{$unotURL}');";
		} else {
			// Vieja - Actualizar
			$add_qry = "UPDATE sb_notif_obj
						SET notif_dtmod=now(), obj_id='{$authorId}', notif_text='{$unotText}', notif_url='{$unotURL}', notif_status=1,
							notif_count={$nCountN}, notif_counttot={$nCountTotN}
						WHERE notif_id={$unotId};";
		}

		// Crear notificación
		\snkeng\core\engine\mysql::submitMultiQuery($add_qry);
		\snkeng\core\engine\mysql::killIfError("ERROR (SISTEMA): Admin Notif - No se actualizaron las notificaciones.");

		// Actualizar contadores
		if ( $allwaysNew || $unotId === 0 || $forceAddCounter || $addCounter ) {
			$sql_upd = "UPDATE sa_notif_type SET ntype_count=ntype_count+1 WHERE ntype_id={$type} AND ntype_count<65500;";
			\snkeng\core\engine\mysql::submitQuery($sql_upd);
			\snkeng\core\engine\mysql::killIfError("ERROR (SISTEMA): Admin Notif - No se actualizaron los contadores.");
		}
	}
	// END: createNotifications
	//

	// }
	// ===========================================
	//	Funciones generales
	//

	// ===========================================
	//	Operaciones de Facebook
	// ===========================================
	// {

	//
	// INIT: fbNotification
	//
	private function fbNotification($ids, $content, $data)
	{
		$idsCount = count($ids);
		if ( $idsCount !== 0 && !$_ENV['SE_DEBUG'] ) {
			if ( !empty($this->siteVars['sn']['fb']['app']) ) {
				// Establecer conexión
				$token_url = "https://graph.facebook.com/oauth/access_token?client_id={$this->siteVars['sn']['fb']['app']['id']}&client_secret={$this->siteVars['sn']['fb']['app']['secret']}&grant_type=client_credentials";
				$app_access_token = file_get_contents($token_url);

				// Proteger texto
				$content = urlencode($content);
				$data = urlencode($data);

				// El request es diferente en caso de tratarse de diferente número de usuarios
				if ( $idsCount === 1 ) {
					$user_id = $ids[0];
					$apprequest_url = "https://graph.facebook.com/{$user_id}/apprequests?message={$content}&data={$data}&{$app_access_token}&method=post";
				} else {
					$idsString = "";
					$first = true;
					foreach ( $ids as $id ) {
						$idsString .= ($first) ? '' : ',';
						$idsString .= $id;
						$first = false;
					}
					$apprequest_url = "https://graph.facebook.com/apprequests?ids={$idsString}&message='{$content}'&data='{$data}'&access_token={$app_access_token}";
				}
				$result = file_get_contents($apprequest_url);
			}
		}
	}
	// END: fbNotification
	//

	//
	// INIT: fbNotificationsClear
	//
	public function fbNotificationsClear($userId)
	{
		if ( !$_ENV['SE_DEBUG'] ) {
			if ( !empty($this->siteVars['sn']['fb']) ) {
				$sql_qry = "SELECT a_sn_fb_id FROM sb_objects_obj WHERE a_id=$userId;";
				$fbId = \snkeng\core\engine\mysql::singleNumber($sql_qry);
				if ( $fbId !== 0 ) {
					/*
					// Establecer conexión
					$token_url = "https://graph.facebook.com/oauth/access_token?client_id={$this->siteVars['sn']['fb']['app']['id']}&client_secret={$this->siteVars['sn']['fb']['app']['secret']}&grant_type=client_credentials";
					$app_access_token = file_get_contents($token_url);
					// Hacer solicitud de los request actuales
					$request_url ="https://graph.facebook.com/{$fbId}/apprequests?{$app_access_token}";
					$requests = file_get_contents($request_url);
					$data = json_decode($requests);
					// Hacer operación por cada request
					foreach ( $data->data as $item )
					{
						$id = $item->id;
						$delete_url = "https://graph.facebook.com/{$id}?{$app_access_token}&method=delete";
						$result = file_get_contents($delete_url);
					}
					*/
					require($_SERVER['DOCUMENT_ROOT'] . "/se_core/external/fbsrc/facebook.php");
					$facebook = new Facebook(array('appId' => $this->siteVars['sn']['fb']['app']['id'], 'secret' => $this->siteVars['sn']['fb']['app']['secret'],));
					// reading the requests
					$requests = $facebook->api($fbId . "/apprequests/");

					foreach ( $requests['data'] as $request ) {
						// building the stack for the batch requests
						$stack[] = array("method" => 'DELETE', "relative_url" => $request['id']);

						// max 20 requests can be bundled in a batch request, if 20 => call fb
						if ( count($stack) == 20 ) {
							$batched_request = json_encode($stack); // json encode the stack array
							$facebook->api('/?batch=' . $batched_request, 'POST'); // call fb
							$stack = array(); // empty stack
						}
					}

					// if there are some requests left in the stack, call it again to empty
					if ( count($stack) > 0 ) {
						$batched_request = json_encode($stack);
						$facebook->api('/?batch=' . $batched_request, 'POST');
						$stack = array();
					}
				}
			}
		}
	}
	// END: fbNotificationsClear
	//

	//
	// INIT: getFBId
	//	Determinar si tiene FB
	public function getFBId($userId)
	{
		// Determinar información del comentario para actualizar notificaciones
		$sql_qry = "SELECT a_sn_fb_id FROM sb_objects_obj WHERE a_id={$userId};";
		return \snkeng\core\engine\mysql::singleNumber($sql_qry);
	}
	// END: getFBId
	//

	// }
	// ===========================================
	//	Operaciones de Facebook
	//
}

?>