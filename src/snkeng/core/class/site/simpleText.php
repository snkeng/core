<?php
//
namespace snkeng\core\site;


//
class simpleText
{
	public static function load($texts) {
		
		// Admin?

		$admin = false;
		if ( \snkeng\core\engine\login::check_loginLevel('sadmin') ) {
			$admin = true;
		}

		// Leer
		foreach ( $texts as $text ) {
			$sql_qry = "SELECT ste_id AS id, ste_content AS content FROM sc_site_textedit WHERE ste_title='{$text}' LIMIT 1;";
			$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
				[
					'errorKey' => 'siteCoreSimpleTextLoad',
					'errorDesc' => 'No fue posible leer el contenido.'
				]
			);

			//
			$struct = '';

			//
			if ( empty($data) ) {
				$data['id'] = \snkeng\core\engine\mysql::getNextId('sc_site_textedit');
				$data['content'] = '';
				$sql_qry = "INSERT INTO sc_site_textedit (ste_id, ste_type, ste_title) VALUES ({$data['id']}, 1, '{$text}');";
				\snkeng\core\engine\mysql::submitQuery($sql_qry,
					[
						'errorKey' => 'siteCoreSimpleTextCreate',
						'errorDesc' => 'No fue posible guardar el contenido.'
					]
				);
			}
			//

			if ( $admin ) {
				//
				$struct = <<<HTML
<div se-plugin="wysiwyg" data-saveurl="/api/module_adm/core/site/texts/updLive" data-id="{$data['id']}">
{$data['content']}
</div>
HTML;
				//
			} else {
				//
				$struct = <<<HTML
<div>
{$data['content']}
</div>
HTML;
				//
			}

			//
			$response['elems'][$text] = $struct;
		}

		return $response;
	}
}
//
