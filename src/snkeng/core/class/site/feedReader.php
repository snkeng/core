<?php
//
namespace snkeng\core\site;

//
class feedReader
{
	//
	// INIT: Variables

	private $feed;                        // Contiene el elemento xml del feed
	public $feedType;                    // Tipo de lectura que se tiene que realizar

	public $counter = 0;                // Contador de noticias que se tienen en el archivo

	public $properties = array();        // Propiedades del feed.
	public $items = array();            // El array que contiene toda la infomración del contenido de los items

	public $lastUpdate;                    // Fecha de última actualización
	public $dateLimit;                    // Límite de fecha que debe cumplir para ser nueva noticia

	// END: Variables
	//

	//
	// INIT: construct
	public function __construct()
	{
	}
	// END: construct
	//

	//
	// INIT: 
	//	
	public function getFeed($feed)
	{
		$this->feed = simplexml_load_file($feed, "SimpleXMLElement", LIBXML_NOCDATA);
		$type = $this->feed->getName();
		if ( $type == "rss" ) {
			$version = $this->feed['version'];
			if ( $version == "2.0" ) {
				$this->feedType = "RSS2";
				return true;
			} elseif ( $version == "0.91" ) {
				$this->feedType = "RSS091";
				return true;
			} elseif ( $version == "0.92" ) {
				$this->feedType = "RSS092";
				return true;
			} else {
				return false;
			}
		} elseif ( $type == "feed" ) {
			$this->feedType = "atom";
			return true;
		} else {
			return false;
		}
	}
	// END: 
	//

	//
	// INIT: 
	//	
	public function printXML()
	{
		print_r($this->properties);
		echo("\n<br/>\n<br/>\n<br/>\n");
		print_r($this->items);
	}
	// END: 
	//

	//
	// INIT: readFeed
	//	Basado en el tipo de feed manda a una función a interpretarlo
	public function readFeedProperties()
	{
		$type = $this->feedType;
		if ( $type == "RSS2" ) {
			$this->readRSS2Properties();
		} elseif ( $type == "atom" ) {
			$this->readATOMProperties();
		} else {
			echo("ERROR: Tipo no soportado\n<br/>\n" . $this->feedType . ".<br/>\n");
		}
	}

	public function readFeedItems()
	{

		$type = $this->feedType;
		if ( $type == "RSS2" ) {
			$this->readRSS2Items();
		} elseif ( $type == "atom" ) {
			$this->readATOMItems();
		} else {
			echo("ERROR: Tipo no soportado\n<br/>\n" . $this->feedType . ".<br/>\n");
		}
	}
	// END: readFeed
	//

	//
	// INIT: readRSS2Properties
	//	Interpreta el contenido de un rss2
	public function readRSS2Properties()
	{
		$channel = $this->feed->channel;
		$this->properties['title'] = $this->textUndecode(strval($channel->title));
		$this->properties['desc'] = $this->textUndecode(strval($channel->description));
		$this->properties['siteurl'] = strval($channel->link);

		$date = strtotime($channel->lastBuildDate);
		if ( $date == "" ) {
			$date = strtotime($channel->pubDate);
		}
		$this->properties['pubDate'] = $date;
	}
	// END: readRSS2Properties
	//

	//
	// INIT: readRSS2Items
	//	Interpreta el contenido de un rss2 en items
	public function readRSS2Items()
	{
		$counter = $this->counter;
		foreach ( $this->feed->channel->item as $item ) {
			$date = strtotime($item->pubDate);

			$dateLimit = $this->dateLimit;
			$updateDate = $this->lastUpdate;

			if ( ($updateDate == "" || $updateDate < $date) && $dateLimit < $date ) {
				$this->items[$counter]['title'] = $this->textUndecode(addslashes(strval($item->title)));
				$this->items[$counter]['link'] = strval($item->link);

				// Descripción
				$desc = strval($item->description);
				$desc = $this->simplifyText($desc);
				$this->items[$counter]['desc'] = $desc;
				$this->items[$counter]['pubDate'] = $date;
				$counter++;
			}
		}
		$this->counter = $counter;
	}
	// END: readRSS2Items
	//

	//
	// INIT: readATOMProperties
	//	Interpreta el contenido de un atom
	public function readATOMProperties()
	{
		$feed = $this->feed;
		$this->properties['titulo'] = $this->textUndecode(strval($feed->title));
		$this->properties['desc'] = $this->textUndecode(strval($feed->subtitle));
		$this->properties['pubDate'] = strtotime($feed->updated);
		$this->properties['siteurl'] = strval($feed->link['href']);
	}
	// END: readATOMProperties
	//

	//
	// INIT: readATOMItems
	//	Interpreta el contenido de un atom en items
	public function readATOMItems()
	{
		$counter = $this->counter;
		foreach ( $this->feed->entry as $item ) {
			$date = strtotime($item->updated);

			$dateLimit = $this->dateLimit;
			$updateDate = $this->lastUpdate;

			if ( ($updateDate == "" || $updateDate < $date) && $dateLimit < $date ) {
				$this->items[$counter]['title'] = $this->textUndecode(strval($item->title));
				$this->items[$counter]['link'] = strval($item->link['href']);
				$this->items[$counter]['pubDate'] = $date;

				// Descripción
				$desc = strval($item->summary);
				$desc = $this->simplifyText($desc);
				$this->items[$counter]['desc'] = $desc;

				$counter++;
			}
		}
		$this->counter = $counter;
	}
	// END: readATOMItems
	//

	//
	// INIT: simplifyText
	//	
	public function simplifyText($text)
	{
		// Objetos auxiliares
		$textJumps = array("\r\n", "\n", "\r");
		$espaces = array("</p>", "<br>", "<br />", "<br/>");

		// Operaciones de simplificación
		$text = str_replace($textJumps, "", $text);                // Quitar saltos
		$text = str_replace($espaces, "\n", $text);                // Quitar elementos salto html y poner salto normal
		$text = strip_tags($text);                                // Quitar elementos HTML
		$text = trim($text);                                    // Quitar bordes que podrían existir
		$text = $this->textUndecode($text);                        // Simplificar caracteres
		$text = str_replace("\n", "<br />\n", $text);            // Cambiar los saltos de página por saltos HTML
		return $text;
	}
	// END: simplifyText
	//

	//
	// INIT: textUndecode
	//	Remueve caracteres UTF8 y convierte todo a html
	public function textUndecode($text)
	{
		$text = htmlentities(html_entity_decode($text));        // Codificar los caracteres al formato convencional
		return $text;
	}
	// END: textUndecode
	//
}

?>