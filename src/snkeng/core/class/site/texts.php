<?php
//
namespace snkeng\core\site;

//
class texts
{
	//
	public static function add(string $appName, string $appMode, int $appId, string $lang) {
		$textData = self::getElement($appName, $appMode, $appId, $lang);

		//
		if ( !empty($textData) ) {
			\snkeng\core\engine\nav::killWithError('NO fue posible crear el registro.', 'Ya existe un registro con el mismo ID e idioma.');
		}

		//
		$qry_ins = <<<SQL
INSERT INTO sc_site_texts
(txt_app_name, txt_app_mode, txt_app_id, txt_lang, txt_type)
VALUES
('{$appName}', '{$appMode}', '{$appId}', '{$lang}', 'md');
SQL;
		\snkeng\core\engine\mysql::submitQuery($qry_ins,
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);
	}

	//
	public static function upd(
		string $appName, string $appMode, int $appId,
		string $lang, int $txtId,
		string $content,
		array $versions = [], array $return = []
	) {
		$textData = self::getElement($appName, $appMode, $appId, $lang);

		//
		if ( empty($textData) ) {
			\snkeng\core\engine\nav::killWithError('No fue posible modificar el registro.', 'Ya existe un registro con el mismo ID e idioma.');
		}
		//
		if ( $textData['id'] !== $txtId ) {
			\snkeng\core\engine\nav::killWithError('No fue posible modificar el registro.', "ID del texto no válido. ({$textData['id']} - {$txtId})");
		}

		
		$securedTxt = \snkeng\core\engine\mysql::real_escape_string($content);

		//
		$qry_ins = <<<SQL
UPDATE sc_site_texts
SET txt_content='{$securedTxt}'
WHERE txt_id='{$textData['id']}' 
SQL;
		\snkeng\core\engine\mysql::submitQuery(
			$qry_ins,
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);

		//
		if ( $versions ) {
			self::updateChildren($textData['id'], $appName, $appMode, $appId, $lang, $content, $versions);
		}

		//
		if ( $return ) {
			$returnArray = [];

			//
			$markDown = new \snkeng\core\site\markdown();

			//
			foreach ( $return AS $cReturn ) {
				// Operate
				switch ( $cReturn ) {
					//
					case 'html':
						$returnArray[$cReturn] = $markDown->text($content);
						break;
					//
					case 'amp':
						$returnArray[$cReturn] = $markDown->text($content, 'amp');
						break;
					//
					default:
						continue 2;
				}
			}

			//
			return $returnArray;
		}

		return null;
	}

	//
	private static function getElement(string $appName, string $appMode, int $appId, string $lang) {
				//
		$qry_sel = <<<SQL
SELECT
	txt.txt_id AS id, txt.txt_id_parent AS parentId,
	txt.txt_dt_add AS dtAdd, txt.txt_dt_mod AS dtMod,
	txt.txt_app_name AS appName, txt.txt_app_mode AS appMode, txt.txt_app_id AS appId,
	txt.txt_lang AS txtLang, txt.txt_type AS txtType
FROM sc_site_texts AS txt
WHERE txt.txt_app_name='{$appName}' AND txt.txt_app_mode='{$appMode}' AND txt.txt_app_id='{$appId}' AND txt.txt_lang='{$lang}' AND txt.txt_type='md'
LIMIT 1;
SQL;
		return \snkeng\core\engine\mysql::singleRowAssoc(
			$qry_sel,
			[
				'int' => ['id', 'parentId']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);
	}

	//
	private static function updateChildren(int $textId, string $appName, string $appMode, int $appId, string $lang, string $content, array $versions) {
		
		//
		$qry_sel = <<<SQL
SELECT
	txt.txt_id AS id, txt.txt_type AS txtType
FROM sc_site_texts AS txt
WHERE txt.txt_id_parent='{$textId}'
LIMIT 5;
SQL;
		$curVersions = \snkeng\core\engine\mysql::returnArray(
			$qry_sel,
			[
				'int' => ['id']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);

		//
		$markDown = new \snkeng\core\site\markdown();

		//
		foreach ( $versions AS $cVer ) {
			$nContent = '';
			// Operate
			switch ( $cVer )  {
				//
				case 'html':
					$nContent = $markDown->text($content);
					break;
				//
				case 'amp':
					$nContent = $markDown->text($content, 'amp');
					break;
				//
				default:
					continue 2;
			}

			// Save convert
			$nContent = \snkeng\core\engine\mysql::real_escape_string($nContent);

			// Save
			$found = false;
			foreach ( $curVersions AS $cDBVersion ) {
				if ( $cVer === $cDBVersion['txtType'] ) {
					$found = true;

					// Update
					$qry_ins = <<<SQL
UPDATE sc_site_texts
SET txt_content='{$nContent}'
WHERE txt_id='{$cDBVersion['id']}' 
SQL;
					\snkeng\core\engine\mysql::submitQuery(
						$qry_ins,
						[
							'errorKey' => '',
							'errorDesc' => '',
						]
					);
				}
			}

			//
			if ( !$found ) {
				//
				$qry_ins = <<<SQL
INSERT INTO sc_site_texts
(txt_id_parent, txt_app_name, txt_app_mode, txt_app_id, txt_lang, txt_type, txt_content)
VALUES
($textId, '{$appName}', '{$appMode}', '{$appId}', '{$lang}', '{$cVer}', '{$nContent}');
SQL;
				//
				\snkeng\core\engine\mysql::submitQuery(
					$qry_ins,
					[
						'errorKey' => '',
						'errorDesc' => '',
					]
				);
			}
		}
		//
	}
}
//
