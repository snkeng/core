<?php
//
namespace snkeng\core\external;

//
use snkeng\core\external\Google_Client;
use snkeng\core\external\Google_Service_Plus;

class google_se
{
	// Variables
	private $appData = ['id'=>'', 'secret'=>'', 'simple'=>''];
	private $siteVars;
	private $appAccessToken;
	private $userAccessToken;
	private $curl_opts = [
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_USERAGENT => 'snkeng_1.0',
	];

	public function __construct()
	{
		global $siteVars;
		$this->siteVars =& $siteVars;
		if ( !empty($siteVars['sn']['gp']['app']) ) {
			$this->appData['id'] = $siteVars['sn']['gp']['app']['id'];
			$this->appData['secret'] = $siteVars['sn']['gp']['app']['secret'];
			$this->appData['simple'] = $siteVars['sn']['gp']['app']['simple'];
		}
	}
	
	// ===========================================
	//	funciones públicas
	// ===========================================
	// {

	private function graphQry($path, $params) {
		$qry = http_build_query($params);
		$defParams = $this->curl_opts;
		$defParams[CURLOPT_URL] = 'https://graph.facebook.com'.$path.'?'.$qry;
		//open connection
		$ch = curl_init();
		curl_setopt_array($ch, $defParams);
		$result = curl_exec($ch);
		curl_close($ch);
		//
		if ( substr($result, 0, 1) === '{' ){
			$result = json_decode($result, true);
		} else {
			parse_str($result, $result);
		}

		return $result;
	}
	private function graphApi($path, $params=[]) {
		return $this->graphQry('/v2.2/'.$path, [
			'access_token' => $this->userAccessToken,
			'format' => 'json',
			'method' => 'get'
		]);
	}


	public function getUser($active) {
		$this->userAccessToken = null;
		require_once($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/external/google-api/src/Google/Client.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/external/google-api/src/Google/Service/Plus.php');
		$client = new Google_Client();
		$client->setApplicationName('Vive Paintball');
		$client->setClientId($this->appData['id']);
		$client->setClientSecret($this->appData['secret']);
		$client->setRedirectUri($this->siteVars['server']['url'].'/usuario/registrarse/?login=true&lType=gp');
		$client->setDeveloperKey($this->appData['simple']);
		$plus = new Google_Service_Plus($client);
		//
		if ( isset($_SESSION) && isset($_SESSION['user']['gp']['token']) ) {
			// Sessión, validar
			$this->userAccessToken = $_SESSION['user']['gp']['token'];
		} else {
			// Revisar redirección
			$code = strval($_GET['code']);
			$state = strval($_GET['state']);
			$uniqueCode = md5(session_id());
			if ( !empty($code) && $state === $uniqueCode ) {
				$client->authenticate($code);
				// Get your access and refresh tokens, which are both contained in the
				// following response, which is in a JSON structure:
				$jsonTokens = $client->getAccessToken();
				$_SESSION['user']['gp']['token'] = $jsonTokens;
				header('Location: '.$this->siteVars['server']['url'].'/usuario/registrarse/?login=true&lType=gp');
				exit();
			}
		}
		if ( !$this->userAccessToken && $active ) {
			// No hay session
			$qry = http_build_query([
				'scope' => 'https://www.googleapis.com/auth/plus.login',
				'state' => md5(session_id()),
				'redirect_uri' => $this->siteVars['server']['url'].'/usuario/registrarse/?login=true&lType=gp',
				'client_id' => $this->appData['id'],
				'response_type' => 'code',
				'access_type' => 'offline',
			]);
			header('Location: https://accounts.google.com/o/oauth2/auth?'.$qry);
			exit();
		} else {
			return $plus->people->get('me');
		}
	}

}

?>