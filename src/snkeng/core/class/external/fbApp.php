<?php
//
namespace snkeng\core\external;

//
class fbApp
{
	// Variables
	private $mysql;
	private $siteVars;
	private $accessToken;

	public function __construct()
	{
		global $mysql, $fbApiD, $siteVars;
		$this->mysql = $mysql;
		$this->siteVars = $siteVars;
		
		// Revisar si hay conexión con FB
		if (!empty($fbApiD))
		{
			$token_url = "https://graph.facebook.com/oauth/access_token".
						"?client_id=" . $fbApiD['id'] .
						"&client_secret=" . $fbApiD['secret'] .
						"&grant_type=client_credentials";
			
			$accessToken = file_get_contents($token_url);
		}
	}
	
	// ===========================================
	//	funciones públicas
	// ===========================================
	// {

	//
	// INIT: notifAdd
	//	Elemento para la actualización de notificaciones
	public function sendMultiNotification ($notifChain)
	{
		if ( !$this->siteVars['server']['test'] )
		{
			if ( !empty($notifChain) )
			{
				foreach ( $notifChain as $notif )
				{
					$first = true;
					$ids = "";
					foreach ($notif['uIds'] as $x)
					{
						$ids.= ($first)?"":",";
						$ids.= $x;
						$first = false;
					}
					$apprequest_url ="https://graph.facebook.com/apprequests".
									"?access_token=".$this->accessToken.
									"&ids=".$ids.
									"&message=".$notif['text'];
					$result = file_get_contents($apprequest_url);
				}
			} else { return true; }
		} else { return true; }
	}
	// END: createComment


	// }
	
}

?>