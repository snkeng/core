<?php
//
namespace snkeng\core\external;

//
class recaptcha
{
	public static function verify() {

		// If dev environment, ignore request.
		if ( $_ENV['SE_DEBUG'] || !$_ENV['GOOGLE_CAPTCHA_V2_KEY'] ) {
			return true;
		}

		// Test server auto pass
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s';
		$url = sprintf($url, $_ENV['GOOGLE_CAPTCHA_V2_SECRET'], $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
		$status = file_get_contents($url);
		$r = json_decode($status);
		return ( isset($r->success) && $r->success );
	}
}
//
