<?php
//
namespace snkeng\core\user;

/*
 * management_Class manages user/object operations
 *
 *
 * @access public
 */
class management
{
	/**
	 * Create a user for the platform. User creation filters must be applyed in site logic.
	 * @param array $response the returning array with information
	 * @param string $fname user first name
	 * @param string $lname user last name
	 * @param int $levels privilege level user starts with, default common user
	 * @param string $method method in which user must be addressed about its creation
	 * @param array $params additional parameters
	 */
	public static function userCreate(array &$response, string $fname, string $lname, int $levels, string $method, array $params)
	{
				//
		$userId = \snkeng\core\engine\mysql::getNextId('sb_objects_obj');
		$ins_qry = "";

		//
		$userData = [
			'id' => $userId,
			'iKey' => se_randString(32),
			'idPad' => str_pad($userId, 8, '0', STR_PAD_LEFT),
			'name_full' => $fname." ".$lname,
			'dtAdd' => date('Y-m-d'),
			'fName' => $fname,
			'lName' => $lname,
			'status' => 1
		];

		// Extra data for operations
		$userData['name_url'] = $userData['idPad'];
		$userData['url'] = "/@".$userData['idPad']."/";
		$userData['image_square'] = 'default_img.jpg';

		//
		$uPass = '';

		//
		switch ( $method ) {
			//
			case 'mail':
				// Revisar
				$sql_qry = "SELECT a_id FROM sb_users_mail WHERE am_mail='{$params['eMail']}' AND am_status!=0 LIMIT 1;";
				if ( \snkeng\core\engine\mysql::notNullQuery($sql_qry) ) {
					se_killWithError('El correo electrónico ya se encuentra en uso.');
				}
				$response['debug']['mail_sql'] = $sql_qry;

				//
				$userData['eMail'] = $params['eMail'];
				$userData['eMailStatus'] = 2;

				//
				if ( !$params['mailStatus'] ) {
					$userData['status'] = 0;
					$userData['eMailStatus'] = 1;
				}

				//
				$uPass = self::createPassword($params['pass']);

				//
				$response['mail']['id'] = \snkeng\core\engine\mysql::getNextId('sb_users_mail');

				// Query
				$ins_qry .= <<<SQL
INSERT INTO sb_users_mail
(a_id, am_mail, am_status)
VALUES
({$userId}, '{$params['eMail']}', {$userData['eMailStatus']});\n
SQL;
				//
				break;

			//
			case 'socnet':
				$userData['status'] = 1; // User is already validated
				// Query
				$ins_qry .= <<<SQL
INSERT INTO sb_users_socnet
(a_id, asn_sn_id, asn_type)
VALUES
({$userId}, '{$params['snId']}', '{$params['socnet']}');\n
SQL;
				break;

			//
			default:
				se_killWithError('User registration: Invalid option');
				break;
		}

		//
		$ins_qry.= <<<SQL
INSERT INTO sb_objects_obj (
	a_id, a_fname, a_lname, a_obj_name,
    a_type,
    a_url_name_full, a_url_name_simple,
	a_upass, a_status, a_adm_level, a_ikey
) VALUES (
	{$userData['id']}, '{$userData['fName']}', '{$userData['lName']}', '{$userData['name_full']}',
	'user',
	'{$userData['idPad']}', '{$userData['idPad']}',
	'{$uPass}',  {$userData['status']}, {$levels}, '{$userData['iKey']}'
);\n
SQL;

		// Revisar si se requiere alguna operación extra para el usuario
		$file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_user_new.php';
		if ( file_exists($file) ) {
			require $file;
		}

		// debugVariable($ins_qry);

		//
		\snkeng\core\engine\mysql::submitMultiQuery($ins_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'No pudo ser creado el usuario'
			]
		);

		//
		$response['d'] = $userData;
	}

	/**
	 * Create an object, only with name and type. Must be protected by system logic
	 * @param array $response the return object with object data.
	 * @param string $name The name to use in object.
	 * @param string $type The type of the object
	 */
	public static function objectCreate(array &$response, string $name, string $type, array $options = [])
	{
		$defaults = [
			'parentId' => 0,
			'extTable' => null
		];
		$settings = array_merge($defaults, $options);

		//
		$userId = \snkeng\core\engine\mysql::getNextId('sb_objects_obj');
		$ins_qry = "";

		//
		$userData = [
			'id' => $userId,
			'iKey' => se_randString(32),
			'idPad' => str_pad($userId, 8, '0', STR_PAD_LEFT),
			'name_full' => $name,
			'dtAdd' => date('Y-m-d'),
			'status' => 1
		];

		// Extra data for operations
		$userData['name_url'] = $userData['idPad'];
		$userData['url'] = "/@".$userData['idPad']."/";
		$userData['image_square'] = 'default_img.jpg';

		//
		$ins_qry.= <<<SQL
INSERT INTO sb_objects_obj (
	a_id, a_id_parent, a_obj_name, a_type,
	a_url_name_full, a_url_name_simple,
	a_status, a_adm_level, a_ikey
) VALUES (
	{$userData['id']}, '{$settings['parentId']}','{$userData['name_full']}', '{$type}',
	'{$userData['idPad']}', '{$userData['idPad']}',
	1, 4, '{$userData['iKey']}'
);\n
SQL;

		// Secondary db
		if ( $settings['extTable'] ) {
			$ins_qry.= <<<SQL
INSERT INTO {$settings['extTable']} (obj_id) VALUES ({$userData['id']});\n
SQL;
		}

		//
		\snkeng\core\engine\mysql::submitMultiQuery($ins_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'No pudo ser creado el objeto.'
			]
		);

		//
		$response['d'] = $userData;
	}

	//

	/**
	 * Update URL name of an object (doesn't matter what type)
	 * @param array $response the modifyable object for the response
	 * @param int $id ID of the object to update
	 * @param string $urlName The probable new URL name of object.
	 */
	public static function objectUrlUpdate(array &$response, int $id, string $urlName) {
				//
		$shortUrl = strtolower(preg_replace('/[^A-z]/', '', $urlName));
		//
		$qry_sel = <<<SQL
SELECT a_id FROM sb_objects_obj WHERE a_url_name_simple='{$shortUrl}' LIMIT 1;
SQL;
		if ( \snkeng\core\engine\mysql::notNullQuery($qry_sel) ) {
			se_killWithError('El URL seleccionado ya está en uso.');
		}

		//
		$qry_upd = <<<SQL
UPDATE sb_objects_obj SET a_url_name_full='{$urlName}', a_url_name_simple='{$shortUrl}' WHERE a_id={$id};
SQL;
		\snkeng\core\engine\mysql::submitQuery($qry_upd);

		//
		$response['d'] = 1;

	}

	//<editor-fold desc="Mail">

	// Validar un mail a través de su contraseña temporal (valida también cuentas)
	public static function mailStatus($mailId)
	{
		
		//
		$sql_qry = <<<SQL
SELECT
	admail.am_id AS mailId, admail.am_status AS mailStatus, admail.am_mail AS eMail,
    usr.a_status AS usrStatus, usr.a_id AS usrId, usr.a_obj_name AS usrName
FROM sb_users_mail AS admail
INNER JOIN sb_objects_obj AS usr ON usr.a_id=admail.a_id
WHERE admail.am_id='{$mailId}'
LIMIT 1;
SQL;
		$datos = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['mailId', 'mailStatus', 'usrId', 'usrStatus']
			],
			[
				'errorKey' => 'seCoreUsrMngmtMailValidate01',
				'errorDesc' => 'No fue posible consultar el estado del usuario.',
				'errorEmpty' => 'Correo no existe en el sistema.'
			]
		);

		//
		return $datos;
	}

	// Agregar un correo a la cuenta actual
	public static function mailAdd($email, $userId)
	{
		
		//
		$sql_qry = "SELECT COUNT(*) FROM sb_users_mail WHERE a_id={$userId} AND am_status!=0;";
		$count = \snkeng\core\engine\mysql::singleNumber($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible hacer el conteo de correos actuales'
			]
		);

		//
		if ( $count >= 3 ) {
			se_killWithError('Ya no es posible registrar más correos.', 'El limite de registrar correos distintos es de tres.');
		}

		//
		$sql_qry = "SELECT a_id AS userId, am_status AS status FROM sb_users_mail WHERE am_mail='{$email}' AND am_status!=0 LIMIT 1;";
		$mailData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['userId', 'status']
			],
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible hacer el conteo de correos actuales'
			]
		);

		//
		if ( !empty($mailData) ) {
			if ( $mailData['userId'] === $userId ) {
				se_killWithError('El correo ya se encuentra en uso.', 'Se encuentra a registrar.');
			} else {
				se_killWithError('El correo ya se encuentra en uso.', '');
			}
		}

		//
		$sql_qry = "SELECT a_fname, a_lname FROM sb_objects_obj AS adm WHERE a_id={$userId};";
		$userData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible obtener la información del usuario.'
			]
		);

		//
		$sql_ins = <<<SQL
INSERT INTO sb_users_mail (a_id, am_mail, am_status, am_type) VALUES ($userId, '{$email}', 1, 0);
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_ins,
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible obtener la información del usuario.'
			]
		);

		//
		$mailId = \snkeng\core\engine\mysql::getLastId();

		//
		return [
			'userId' => $userId,
			'userFName' => $userData['a_fname'],
			'userLName' => $userData['a_lname'],
			'email' => $email,
			'id' => $mailId,
			'isPrimary' => 0,
			'isValidated' => 0
		];
	}

	// Remover un mail del sistema
	public static function mailRemove($emailId, $userId)
	{
				//
		$sql_qry = "SELECT a_id AS userId, am_status AS status, am_type AS type FROM sb_users_mail WHERE am_id='{$emailId}';";
		$mailData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			['int' => ['userId', 'status', 'type']],
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible hacer el conteo de correos actuales',
			]
		);

		//
		if ( empty($mailData) ) {
			se_killWithError('No hay información del correo.', 'Se encuentra a registrar.');
		}

		//
		if ( $mailData['userId'] !== $userId ) {
			se_killWithError('El correo no corresponde al usuario.', '');
		}

		//
		if ( $mailData['type'] === 1 ) {
			se_killWithError('No es posible eliminar el correo primario.', '');
		}

		//
		$sql_upd = "UPDATE sb_users_mail SET am_status=0 WHERE am_id={$emailId};";
		\snkeng\core\engine\mysql::submitQuery($sql_upd,
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible eliminar el correo.'
			]
		);
	}

	// Cambiar el mail seleccionado a tipo principal
	public static function mailSetPrimary($mailId, $userId)
	{
				$sql_qry = "SELECT a_id AS userId, am_status AS status, am_type AS type FROM sb_users_mail WHERE am_id={$mailId} LIMIT 1;";
		$mailData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['userId', 'status']
			],
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible encontrar el email'
			]
		);

		//
		if ( empty($mailData) ) {
			se_killWithError('No existe el correo seleccionado.', '');
		}

		//
		if ( $mailData['userId'] !== $userId ) {
			se_killWithError('El correo seleccionado no corresponde al usuario.', '');
		}

		//
		if ( $mailData['status'] !== 2 ) {
			se_killWithError('El correo seleccionado no ha sido validado.', '');
		}

		//
		if ( $mailData['type'] === 1 ) {
			se_killWithError('El correo seleccionado ya es primario.', '');
		}

		//
		$sql_upd = <<<SQL
UPDATE sb_users_mail SET am_type=2 WHERE a_id={$userId} AND am_status!=0 LIMIT 5;
UPDATE sb_users_mail SET am_type=1 WHERE am_id={$mailId};
SQL;
		\snkeng\core\engine\mysql::submitMultiQuery($sql_upd,
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible actualizar email'
			]
		);
	}

	// Validar un mail a través de su contraseña temporal (valida también cuentas)
	public static function mailValidate($mailId, $userId)
	{
		
		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj SET a_status=1 WHERE a_id={$userId};
UPDATE sb_users_mail SET am_status=1 WHERE am_id={$mailId};
SQL;
		\snkeng\core\engine\mysql::submitMultiQuery($sql_upd,
			[
				'errorKey' => 'seCoreUsrMngmtMailValidate02',
				'errorDesc' => 'No fue posible actualizar el estado del usuario.'
			]
		);
	}

	//</editor-fold>


	//<editor-fold desc="Manejo contraseñas">

	// Cambiar la contraseña del login tradicional
	public static function changePassword($origPass, $newPass, $uId)
	{
		
		//
		$sql_qry = "SELECT a_upass AS uPass, a_type AS oType FROM sb_objects_obj WHERE a_id={$uId};";
		$objData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'Password update. User not valid.'
			]
		);

		//
		if ( $objData['oType'] !== 'user' ) {
			se_killWithError('No se posible cambiar la contraseña de un objeto.');
		}
		//
		if ( crypt($origPass, $objData['uPass']) !== $objData['uPass'] ) {
			se_killWithError("Contraseña actual no válida.");
		}
		// debugVariable($cPass);
		if ( $origPass === $newPass ) {
			se_killWithError('La contraseña actual es identica a la nueva.', '');
		}

		//
		$modPass = self::createPassword($newPass);
		//
		$sql_qry = "UPDATE sb_objects_obj SET a_upass='{$modPass}' WHERE a_id='{$uId}';";
		// debugVariable($sql_qry);
		\snkeng\core\engine\mysql::submitQuery($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => 'Usuario contraseña no guardada.'
			]
		);
	}

	//
	public static function passRecoveryUpdate($userId, $newPass, $closeActive = false) {
		
		//
		if ( !self::validNewPassword($newPass) ) {
			se_killWithError('Contraseña no aprobada por el sistema.');
		}

		//
		$modPass = self::createPassword($newPass);
		$sql_upd = "UPDATE sb_objects_obj SET a_upass='{$modPass}' WHERE a_id='{$userId}';";

		//
		if ( $closeActive ) {
			$cookiePw = se_randString(32);
			$sql_upd.= "UPDATE sb_users_logins SET ul_cookie='{$cookiePw}' WHERE a_id={$userId};";
		}

		\snkeng\core\engine\mysql::submitMultiQuery($sql_upd,
			[
				'errorKey' => 'seCoreUserPassUpdate',
				'errorDesc' => 'No fue posible actualizar la contraseña del usuario.'
			]
		);
	}

	//</editor-fold>

	//
	public static function createPassword($pass)
	{
		$randId = str_shuffle(preg_replace('/[^A-Za-z0-9]/', '', base64_encode(uniqid(rand(), true))));
		if ( CRYPT_SHA512 == 1 ) {
			$method = '$6$rounds=10331$'.substr($randId, 4, 16).'$';
		} elseif (CRYPT_SHA256 == 1) {
			$method = '$5$rounds=11331$'.substr($randId, 6, 16).'$';
		} elseif (CRYPT_BLOWFISH == 1) {
			$method = '$2a$12$'.substr($randId, 2, 22).'$';
		} elseif (CRYPT_MD5 == 1) {
			$method = '$1$'.substr($randId, 4, 12).'$';
		} elseif (CRYPT_EXT_DES == 1) {
			$method = '_J9..'.substr($randId, 10, 4);
		} else {
			$method = substr($randId, 2, 2);
		}
		$nPass = crypt($pass, $method);
		return $nPass;
	}

	//
	public static function validNewPassword($pass)
	{
		if ( !empty($pass) ) {
			if ( strlen($pass) >= 4 )
			{
				if ( !preg_match('/[^A-Za-z0-9.#\\-$]/', $pass) ) {
					return true;
				} else { self::$error = 'La contraseña tiene caracteres inválidos'; return false; }
			} else { self::$error = 'La contraseña debe tener al menos 6 caracteres.'; return false; }
		} else { self::$error = 'La contraseña esta vacía.'; return false; }
	}
}
