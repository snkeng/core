<?php
//
namespace snkeng\core\engine;


//
class saveDataToDataBase {
	//
	public static function addSingle(array $data, array $dbStructure, callable | null $onFail = null ) : array | bool {
		//
		$varNameList = "";
		$varItemList = "";
		$saveData = [];
		$returnData = $data;

		//
		$first = true;
		foreach ( $dbStructure['elems'] as $dbName => $varName ) {
			$varNameList .= ($first) ? '' : ', ';
			$varItemList .= ($first) ? '' : ', ';
			
			//
			$varNameList .= $dbName;
			$varItemList .= '?';

			//
			if ( gettype($data[$varName]) === 'array' ) {
				$saveData[] =\json_encode($data[$varName], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			} else {
				$saveData[] = $data[$varName] ?? 'NULL';
			}

			//
			$first = false;
		}

		// Get ID
		$nextId = \snkeng\core\engine\mysql::getNextId($dbStructure['tName']);

		// Add to query
		$saveData[] = $nextId;
		$varNameList.= ", " . $dbStructure['tId']['db'];
		$varItemList.= ", ?";

		// To results
		$returnData[$dbStructure['tId']['nm']] = $nextId;

		//
		$sql_ins = <<<SQL
INSERT INTO {$dbStructure['tName']} ($varNameList) VALUES ($varItemList);
SQL;

		//
		\snkeng\core\engine\mysql::submitPreparedQuery(
			$sql_ins,
			$saveData,
			[
				'errorKey' => 'saveDataAddRow',
				'errorDesc' => 'No fue posible actualizar la información.',
				'onError' => $onFail
			]
		);

		return $returnData;
	}

	//
	public static function addMany(array $data, array $dbStructure) : array {
		return [];
	}

	//
	public static function updateSingle(array $data, array $dbStructure, int $resourceId, callable | null $onFail = null) : void {
		// Actualizar tabla
		$elementsString = '';
		$saveData = [];
		$first = true;

		// Parse
		foreach ( $dbStructure['elems'] as $dbName => $varName ) {
			$elementsString.= ( $first ) ? '' : ', ';
			//
			$elementsString.= "{$dbName}=?";

			//
			if ( gettype($data[$varName]) === 'array' ) {
				$saveData[] =\json_encode($data[$varName], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			} else {
				$saveData[] = $data[$varName] ?? 'NULL';
			}

			//
			$first = false;
		}

		// Procesar
		$sql_qry = "UPDATE {$dbStructure['tName']} SET {$elementsString} WHERE {$dbStructure['tId']['db']}={$resourceId} LIMIT 1;";

		\snkeng\core\engine\mysql::submitPreparedQuery(
			$sql_qry,
			$saveData,
			[
				'errorKey' => 'saveDataAddRow',
				'errorDesc' => 'No fue posible actualizar la información.',
				'onError' => $onFail
			]
		);
	}

	//
	public static function updateMultiple(array $data, array $dbStructure) : void {
		$elementsString = '';
		$varOrder = [];
		// Build
		$first = true;
		foreach ( $dbStructure['elems'] as $dbName => $varName ) {
			$elementsString.= ( $first ) ? '' : ', ';
			//
			$elementsString.= "{$dbName}=?";

			$varOrder[] = ['d', $varName];

			//
			$first = false;
		}

		$varOrder[] = ['i'];

		// Resulting query
		$sql_qry = "UPDATE {$dbStructure['tName']} SET {$elementsString} WHERE {$dbStructure['tId']['db']}= ? LIMIT 1;";


		//
		\snkeng\core\engine\mysql::multiSubmitPreparedQuery(
			$sql_qry,
			$varOrder,
			$data,
			[
				'errorKey' => 'saveDataAddRow',
				'errorDesc' => 'Unable to save information'
			]
		);


	}

	//
	public static function deleteSingle(array $dbStructure, int $resourceId) : void {

		// Procesar
		$sql_qry = "DELETE FROM {$dbStructure['tName']} WHERE {$dbStructure['tId']['db']}={$resourceId} LIMIT 1;";

		\snkeng\core\engine\mysql::submitQuery(
			$sql_qry,
			[
				'errorKey' => 'saveDataAddRow',
				'errorDesc' => 'No fue posible actualizar la información.'
			]
		);
	}
}
