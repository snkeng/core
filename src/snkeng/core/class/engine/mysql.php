<?php
//
namespace snkeng\core\engine;

use mysql_xdevapi\Exception;

/**
 * Class mysqliMaster
 *
 * @package \snkeng\core
 */
class mysql
{
	//
	// VARIABLES

	private static string $con_server;
	private static string $con_database;

	//
	private static $mysqli;                    // Conexión con el server
	private static $debugMode = false;       // Avoid error loop

	// Resultados extraibles
	public static $result;                        // Resultados del query
	public static $numRows = '';                // Número de resultados
	public static $isError = false;            // Reporte de error mysql
	public static $isNull = false;                // Decir si el execQuery es vacio
	public static $moreResults = false;        // Multiquery next result

	// errores
	public static $se_error = '';                    // Error en caso de que un querry sea incorrecto.
	public static $ms_error = '';            // Error en caso de que un querry sea incorrecto.
	public static $lastQuery = '';                // Proviene de crear un error y tratarlo

	//<editor-fold desc="Class core">


	/**
	 * Stablish first connection to server. All variables are requiered.
	 *
	 * @param string $server  the server information (may include port)
	 * @param string $dbName  the name of the database
	 * @param string $usrName the user name to connect to
	 * @param string $usrPwd  the user password to connect to
	 */
	public static function firstConnect(string $server, string $dbName, string $usrName, string $usrPwd)
	{
		// These do not move
		self::$con_server = $server;
		self::$con_database = $dbName;

		// debugVariable($data, '', true);
		self::createConnection($usrName, $usrPwd);
	}

	//</editor-fold>

	//<editor-fold desc="Conexiones">

	/**
	 * Cierra la conexión anterior y solicita una nueva con los datos.
	 * Puede servir para cambiar permisos
	 *
	 * @param array $data Información de la conexión
	 */
	public static function changeConnection(string $usrName, string $usrPwd)
	{
		// Cerrar anterior
		self::$mysqli->close();
		// Crear nueva
		self::createConnection($usrName, $usrPwd);
	}

	/**
	 * Create connection to DB by assign the user name and password. other information must be already set.
	 *
	 * @param string $usrName DB user name
	 * @param string $usrPwd  DB user password
	 */
	public static function createConnection(string $usrName, string $usrPwd)
	{
		mysqli_report(MYSQLI_REPORT_OFF);
		// Abrira una nueva
		self::$mysqli = new \mysqli(self::$con_server, $usrName, $usrPwd, self::$con_database);

		/* check connection */
		if ( self::$mysqli->connect_error ) {
			$message = "DB startup error (initial setup).\n";
			if ( $_ENV['SE_DEBUG'] || self::$debugMode ) {
				$message .= "(" . self::$mysqli->connect_errno . ") {" . self::$mysqli->connect_error . "\n\nData:\n";
			}

			//
			trigger_error($message, E_USER_ERROR);
		}

		//
		self::$mysqli->set_charset("utf8mb4");
	}

	//</editor-fold>

	public function getDatabase()
	{
		return self::$con_database;
	}

	//
	// Limpiar resultado
	public static function clearResult()
	{
		//
		if ( is_a(self::$result, 'mysqli_result') ) {
			self::$result->free();
			self::$result = null;
		}
		self::$numRows = "";
		self::$se_error = "";
		self::$isError = false;
		self::$isNull = false;
	}
	//
	//

	//<editor-fold desc="Guardado">


	/**
	 * submitQuery - Execute a simple query to the db. Only single submits work.
	 *
	 * @param string $query   SQL Query to execute
	 * @param mixed  $options Aditional parameters (errorKey, errorDesc, onError)
	 *
	 * @return bool True if succesful?
	 */
	public static function submitQuery($query, $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'onError' => null,
			'securityKill' => false
		];
		$settings = array_merge($defaults, $options);

		if ( !self::$mysqli->query($query) ) {
			//
			if ( $settings['securityKill'] ) {
				debug_print_backtrace();
				se_killWithError('DB ERROR SECURITY KILL', 'PREVIOUS ERROR CAUSED OPERATIONS TO STOP.');
			}

			//
			if ( is_callable($settings['onError']) ) {
				$settings['onError']();
			}

			// Is allways error
			self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc']);
			return false;
		}

		return true;
	}

	/**
	 * preparedSubmitQuery - Execute a simple query to the db. This variant is made using prepared queries.
	 *
	 * @param string $query     SQL Query to execute
	 * @param array  $variables SQL Query to execute
	 * @param mixed  $options   Aditional parameters (errorKey, errorDesc, onError)
	 *
	 * @return bool True if succesful?
	 */
	public static function submitPreparedQuery(string $query, array $variables, array $options = []) : bool
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'onError' => null,
			'securityKill' => false
		];
		$settings = array_merge($defaults, $options);

		// debugVariable($variables);

		//
		$statement = self::$mysqli->prepare($query);
		$statement->execute($variables);

		// php 8.3>
		// $result = self::$mysqli->execute_query($query, $variables);

		//
		if ( self::$mysqli->error ) {
			//
			if ( $settings['securityKill'] ) {
				debug_print_backtrace();
				se_killWithError('DB ERROR SECURITY KILL', 'PREVIOUS ERROR CAUSED OPERATIONS TO STOP.');
			}

			//
			if ( is_callable($settings['onError']) ) {
				$settings['onError']();
			}

			// Is always error
			self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc'], $variables);
		}

		return $statement->affected_rows;
	}

	/**
	 * preparedSubmitQuery - Execute a simple query to the db. This variant is made using prepared queries.
	 *
	 * @param string $query     SQL Query to execute
	 * @param array  $data SQL Query to execute
	 * @param mixed  $options   Aditional parameters (errorKey, errorDesc, onError)
	 *
	 * @return bool True if succesful?
	 */
	public static function multiSubmitPreparedQuery(string $query, array $varOrder, array $data, array $options = []) : bool
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'onError' => null,
			'securityKill' => false
		];
		$settings = array_merge($defaults, $options);

		// debugVariable($data);

		//
		$statement = self::$mysqli->prepare($query);

		// For all data rows
		foreach ( $data as $id => $cData ) {
			// Build the current data array
			$saveData = [];

			foreach ( $varOrder as $cOrder ) {
				if ( $cOrder[0] === 'd' ) {
					//
					if ( gettype($cData[$cOrder[1]]) === 'array' ) {
						$saveData[] = \json_encode($cData[$cOrder[1]], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
					}
					else {
						$saveData[] = $cData[$cOrder[1]] ?? 'NULL';
					}
				} else {
					$saveData[] = $id;
				}
			}

			//
			$statement->execute($saveData);

			//
			if ( self::$mysqli->error ) {
				//
				if ( is_callable($settings['onError']) ) {
					$settings['onError']();
				}

				// Is always error
				self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc'], $data);
			}
		}

		return true;
	}


	/**
	 * submitMultiQuery - Execute a posibly multiple querys into db all types submit, not results expected.
	 *
	 * @param string $query   SQL Query to execute
	 * @param mixed  $options Aditional parameters (errorKey, errorDesc)
	 *
	 * @return bool
	 */
	public static function submitMultiQuery($query, $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'onError' => null
		];
		$settings = array_merge($defaults, $options);


		$query = \trim($query);
		// Limpiar cualqueir cosa anterior
		self::clearResult();

		// Realizar el query
		if ( self::$mysqli->multi_query($query) ) {
			// Limpiar Buffer y revisar errores
			do {
				// Revisar por errores
				if ( self::$mysqli->errno !== 0 ) {
					//
					if ( is_callable($settings['onError']) ) {
						$settings['onError']();
					}

					//
					self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc']);
					break;
				}
			} while ( self::$mysqli->more_results() && self::$mysqli->next_result() );
		}
		else {
			//
			if ( is_callable($settings['onError']) ) {
				$settings['onError']();
			}

			//
			self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc']);
		}

		//
		return true;
	}
	// End: submitMultiQuery
	//

	//</editor-fold>

	//<editor-fold desc="Multi query">
	//
	// Init: multiQuery
	// Multiple Query.
	public static function multiQuery(string $query, array $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'onError' => null
		];
		$settings = array_merge($defaults, $options);

		self::clearResult();

		if ( self::$mysqli->multi_query($query) ) {
			self::$result = self::$mysqli->store_result();
			//
			if ( \is_object(self::$result) ) {
				self::$numRows = self::$result->num_rows;
			}
			//
			self::$moreResults = self::$mysqli->more_results();
			return true;
		}
		else {
			//
			self::mysqliErrorProcess($query, $settings['errorKey'], $settings['errorDesc']);
			return false;
		}
	}
	// End: multiQuery
	//

	//
	// Init: mqNextResult
	// Siguiente resultado de un multiquery.
	public static function mqNextResult()
	{
		if ( self::$mysqli->more_results() ) {
			//
			if ( \is_object(self::$result) ) {
				self::$result->free();
			}

			//
			if ( self::$mysqli->next_result() ) {
				// Revisar por errores
				if ( self::$mysqli->errno !== 0 ) {
					//
					self::mysqliErrorProcess('asdf', '', '');
				}


				self::$result = self::$mysqli->store_result();
				//
				if ( \is_object(self::$result) ) {
					self::$numRows = self::$result->num_rows;
				}
				//
				self::$moreResults = self::$mysqli->more_results();
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	// End: mqNextResult
	//
	//</editor-fold>

	//<editor-fold desc="ID related">
	//
	// Init: getNextId
	// Obtiene el valor del autoincremento de una tabla específica.
	public static function getNextId(string $table) : int
	{
		$query = "ANALYZE TABLE `{$table}`; SELECT Auto_increment FROM information_schema.tables WHERE table_name='{$table}' AND table_schema='" . self::$con_database . "' LIMIT 1;";

		//
		self::multiQuery(
			$query,
			[
				'errorKey' => 'mysqliGetNextId',
				'errorDesc' => "No fue posible obtener el id de la tabla: '{$table}'.",
			]
		);

		// Ignore first result (analyze...) and get next result
		self::mqNextResult();

		// Extract data from result
		$data = self::$result->fetch_row();
		if ( empty($data) ) {
			// debugVariable($query);
			se_killWithError('Next Id not found', '');
		}
		$nextId = \intval($data[0]);

		// Clear
		self::clearResult();

		// Return result
		return $nextId;
	}
	// End: getNextId
	//

	//
	// Init: getLastId
	// Regresa el valor del id de la última operación.
	public static function getLastId()
	{
		$query = "SELECT LAST_INSERT_ID();";
		$errorKey = 'mysqliGetNextId';
		$errorDesc = "No fue posible obtener el último ID insertado.";

		//
		if ( self::execQuery($query, $errorKey, $errorDesc) ) {
			$datos = self::$result->fetch_row();
			$lastId = \intval($datos[0]);
			self::clearResult();
			return $lastId;
		}
		else {
			return false;
		}
	}
	// End: getLastId
	//
	//</editor-fold>

	//<editor-fold desc="Valores Simples">

	/**
	 * singleValue
	 * Obtain a single value of a query (the first). Better use singleRowAssoc with all the first row.
	 *
	 * @param string $query   query to execute
	 * @param array  $options extra parameters
	 *
	 * @return bool
	 */
	public static function singleValue(string $query, array $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.'
		];
		$settings = array_merge($defaults, $options);

		//
		if ( self::execQuery($query, $settings['errorKey'], $settings['errorDesc']) ) {
			$datos = self::$result->fetch_row();
			$sResult = (!empty($datos)) ? $datos[0] : '';
			self::clearResult();
			return $sResult;
		}
		else {
			return false;
		}
	}
	// END: singleValue
	//

	/**
	 * singleNumber
	 * Obtain only the first row, first field and turn it into a number with intval
	 * singleRowAssoc is still prefered
	 *
	 * @param string $query
	 * @param array  $options
	 *
	 * @return int
	 */
	public static function singleNumber(string $query, array $options = [])
	{
		return \intval(self::singleValue($query, $options));
	}
	// END: singleNumber
	//

	//
	// INIT: singleJSONGet
	// Obtiene un sólo valor según el query dado (el query debe ser asegurado que dará un sólo valor o si no dará el primero).
	/**
	 * singleJSONGet
	 * Obtain only the first row, first field and turn it into a json with json_encode (array = true)
	 * singleRowAssoc is still prefered
	 *
	 * @param string $query
	 * @param array  $options
	 *
	 * @return mixed
	 */
	public static function singleJSONGet(string $query, array $options = [])
	{
		// Extraer información
		$sqlValue = self::singleValue($query, $options);
		$sqlValue = \json_decode($sqlValue, true);

		$error = '';

		//
		switch ( \json_last_error() ) {
			case JSON_ERROR_NONE:
				break;
			//
			case JSON_ERROR_DEPTH:
				$error = ' - Excedido tamaño máximo de la pila';
				break;
			case JSON_ERROR_STATE_MISMATCH:
				$error = ' - Desbordamiento de buffer o los modos no coinciden';
				break;
			case JSON_ERROR_CTRL_CHAR:
				$error = ' - Encontrado carácter de control no esperado';
				break;
			case JSON_ERROR_SYNTAX:
				$error = ' - Error de sintaxis, JSON mal formado';
				break;
			case JSON_ERROR_UTF8:
				$error = ' - Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
				break;
			default:
				$error = ' - Error desconocido';
				break;
		}

		//
		if ( !empty($error) ) {
			se_killWithError('ERROR SISTEMA. MYSQL - JSON :' . $error);
		}

		//
		return $sqlValue;
	}
	// END: singleJSONGet
	//

	//
	// INIT: arrayToJSON
	// Convert PHP Array to mysql ready JSON
	public static function arrayToJSON($array)
	{
		return self::real_escape_string(\json_encode($array, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
	}
	// END: arrayToJSON
	//

	//
	// INIT: singleRow
	//	Regresa la primera línea de datos (Si hay múltiples datos serán descartados)
	public static function singleRow($query, array $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlSingleRowAssoc',
			'errorDesc' => 'No fue posible realizar el query.',
			'errorEmpty' => null,
			'debug' => false
		];
		$settings = array_merge($defaults, $options);

		//
		if ( self::execQuery($query, $settings['errorKey'], $settings['errorDesc']) ) {
			$datos = self::$result->fetch_row();
			self::clearResult();

			//
			if ( $settings['debug'] ) {
				debugVariable($datos);
			}

			return $datos;
		}
		else {
			// No error, but empty, optional kill
			if ( !empty($settings['errorEmpty']) ) {
				se_killWithError($settings['errorEmpty'], '');
			}

			//
			return [];
		}
	}
	// END: singleRow
	//

	/**
	 * singleRowAssoc
	 * Obtain a single row of data as an assosiative array.
	 *
	 * @param string $query    query to database
	 * @param array  $toSimple convert row results to primitives
	 * @param array  $options  convert row results to primitives
	 *
	 * @return array return the data if successful false if empty
	 */
	public static function singleRowAssoc(string $query, array $toSimple = [], array $options = []) : array
	{
		$defaults = [
			'errorKey' => 'seMysqlSingleRowAssoc',
			'errorDesc' => 'No fue posible realizar el query.',
			'errorEmpty' => null,
			'debug' => false
		];
		$settings = array_merge($defaults, $options);

		// Pre process
		self::toSimpleParse($toSimple);

		//
		if ( $settings['debug'] ) {
			debugVariable($query, 'QUERY', false);
		}

		//
		if ( self::execQuery($query, $settings['errorKey'], $settings['errorDesc']) ) {
			$datos = self::$result->fetch_assoc();
			// Simplificar
			self::dataParse($toSimple, $datos);
			//
			self::clearResult();

			//
			if ( $settings['debug'] ) {
				debugVariable($datos, 'RESULTS');
			}

			//
			return $datos;
		}
		else {
			// No error, but empty, optional kill
			if ( !empty($settings['errorEmpty']) ) {
				se_killWithError($settings['errorEmpty'], $query);
			}

			//
			return [];
		}
	}

	/**
	 * Query to get data, counts rows
	 *
	 * @param string $query     Query of the operation
	 * @param string $errorKey  Short trackable string for finding error section
	 * @param string $errorDesc Description of error.
	 *
	 * @return bool true if successful submition.
	 */
	public static function execQuery($query, $errorKey = '', $errorDesc = '')
	{
		self::clearResult();
		// Analizar situación
		if ( self::$result = self::$mysqli->query($query) ) {
			self::$numRows = self::$result->num_rows;
			// Ver si el query es positivo y existen valores
			if ( self::$numRows === 0 ) {
				self::$isNull = true;
				return false;
			}
			else {
				return true;
			}
		}
		else {
			//
			if ( self::$mysqli->errno !== 0 ) {
				self::mysqliErrorProcess($query, $errorKey, $errorDesc);
			}

			// Posible error en la base de datos, por diseño se revisa en la misma clase
			return false;
		}
	}


	/**
	 * Get if there is data that meets query conditions
	 *
	 * @param string $query  Query of the operation
	 * @param array  $params Optional error parameters
	 *
	 * @return bool true if data exists
	 */
	public static function notNullQuery(string $query, array $options = [])
	{
		return !self::nullQuery($query, $options);
	}
	// END: notNullQuery
	//

	/**
	 * Get if there is no data with current query conditions
	 *
	 * @param string $query Query of the operation
	 *
	 * @return bool true if no data exists
	 */
	public static function nullQuery(string $query, array $options = [])
	{
		$defaults = [
			'errorKey' => 'seMysqlGetSingleRowAssoc',
			'errorDesc' => 'No fue posible realizar el query.',
			'errorEmpty' => null
		];
		$settings = array_merge($defaults, $options);


		self::clearResult();
		if ( self::execQuery($query, $settings['errorKey'], $settings['errorDesc']) ) {
			if ( self::$isNull ) {
				return true;
			}
			else {
				$datos = self::$result->fetch_row();
				$sResult = strlen($datos[0]);
				if ( $sResult === 0 ) {
					return true;
				}
				else {
					return false;
				}
			}
		}
		else {
			return true;
		}
	}
	// END: nullQuery
	//
	//</editor-fold>

	//<editor-fold desc="Description">


	/**
	 * returnArray
	 * Convert all the query operations into an assossiative array
	 *
	 * @param string $query    query to execute
	 * @param array  $toSimple to simple conversion options (process the data)
	 * @param array  $options  additional parameters
	 *
	 * @return array
	 */
	public static function returnArray(string $query, $toSimple = [], $options = [])
	{
		$defaults = [
			'toId' => null,
			'modifyData' => null,
			'errorKey' => 'seMysqlGetSingleRowAssoc',
			'errorDesc' => 'No fue posible realizar el query.',
			'errorEmpty' => null,
			'debug' => false
		];
		$settings = array_merge($defaults, $options);

		$result = [];
		// Pre process
		self::toSimpleParse($toSimple);
		//
		if ( self::execQuery($query, $settings['errorKey'], $settings['errorDesc']) ) {
			while ( $datos = self::$result->fetch_assoc() ) {
				//
				self::dataParse($toSimple, $datos);

				//
				if ( !empty($settings['modifyData']) && is_callable($settings['modifyData']) ) {
					$settings['modifyData']($datos);
				}

				//
				if ( $settings['toId'] && !empty($datos[$settings['toId']]) ) {
					$result[$datos[$settings['toId']]] = $datos;
				}
				else {
					$result[] = $datos;
				}
			}

			//
			self::clearResult();
		}
		else {
			// No error, but empty, optional kill
			if ( !empty($settings['errorEmpty']) ) {
				se_killWithError($settings['errorEmpty'], '');
			}

			//
			return [];
		}

		//
		if ( $settings['debug'] ) {
			debugVariable($result);
		}

		//
		return $result;
	}
	// END:returnArray
	//

	//
	// INI: returnCSV
	//
	public static function returnCSV($sql_qry, $toSimple = [], $fileName = 'defName', $headers = [], $test = false)
	{
		$result = [];
		// Pre process
		self::toSimpleParse($toSimple);
		$toID = (isset($toSimple['id']) && !empty($toSimple['id'])) ? $toSimple['id'] : null;

		// Headers

		// Obtener cabecera (evitar traslape querys)
		if ( !$test ) {
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="' . $fileName . '.csv"');
		}

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fputcsv($output, $headers);

		//
		if ( self::execQuery($sql_qry) ) {
			while ( $datos = self::$result->fetch_assoc() ) {
				//
				self::dataParse($toSimple, $datos);

				fputcsv($output, $datos);
			}
			self::clearResult();
		}
		else {
			self::killIfError('DB Array return not possible.', '');
		}

		// Close
		fclose($output);
		exit();
	}
	// END:returnCSV
	//


	/**
	 * printSimpleQuery
	 * Permite imprimir directamente un objeto aplicando los datos del query.
	 *
	 * @param string  $sql_qry  the query to execute
	 * @param string  $template the template to apply the query to
	 * @param mixed[] $options  associative array with multiple static functions!
	 *
	 * @return string the result of the query applying the data
	 */
	public static function printSimpleQuery(string $sql_qry, string $template, array $options = [])
	{
		$defaults = [
			'toSimple' => [],
			'modifyData' => null,
			'modifyTemplate' => null,
			'mergeChar' => null,
			'errorKey' => 'seMysqlPrintQuery',
			'errorDesc' => 'No fue posible realizar el query.',
			'debug' => false
		];
		$settings = array_merge($defaults, $options);


		$result = "";
		$first = true;

		// Pre process for row data convertion
		$toSimple = null;
		if ( !empty($settings['toSimple']) ) {
			self::toSimpleParse($settings['toSimple']);
			$toSimple = $settings['toSimple'];
		}

		//
		if ( $settings['debug'] ) {
			debugVariable($sql_qry);
		}

		//
		if ( self::execQuery($sql_qry, $settings['errorKey'], $settings['errorDesc']) ) {
			// Parse data
			while ( $cData = self::$result->fetch_assoc() ) {
				//
				if ( $toSimple ) {
					self::dataParse($toSimple, $cData);
				}

				//
				if ( !empty($settings['modifyData']) && is_callable($settings['modifyData']) ) {
					$settings['modifyData']($cData);
				}

				//
				if ( !empty($settings['modifyTemplate']) && is_callable($settings['modifyTemplate']) ) {
					$cTemplate = $settings['modifyTemplate']($cData, $template);
				}
				else {
					$cTemplate = $template;
				}

				//
				if ( isset($settings['mergeChar']) && !$first ) {
					$result .= $settings['mergeChar'];
				}

				//
				$result .= stringPopulate($cTemplate, $cData);

				//
				$first = false;
			}

			//
			self::clearResult();
		}

		//
		return $result;
	}
	// END:printSimpleQuery
	//

	//
	// INI: printSimpleQueryRandom
	//
	public static function printSimpleQueryRandom($sql_qry, $template, $maxNumber)
	{
		$count = 0;
		$data = [];
		$result = '';
		if ( self::execQuery($sql_qry) ) {
			while ( $cData = self::$result->fetch_array() ) {
				$data[] = $cData;
				$count++;
			}
			self::clearResult();
		}
		else {
			self::killIfError('DB Random print not possible.', '');
		}
		// Randomize
		shuffle($data);
		$to = min($count, $maxNumber);
		if ( $to > 0 ) {
			for ( $i = 0; $i < $to; $i++ ) {
				$result .= stringPopulate($template, $data[$i]);
			}
		}
		return $result;
	}
	// END:printSimpleQueryRandom
	//

	//</editor-fold>


	//<editor-fold desc="Operaciones">
	//
	private static function toSimpleParse(&$toSimple)
	{
		$data = ['int', 'float', 'replace', 'string', 'moneyInt', 'moneyIntTxt', 'timeUnix', 'json', 'stripCSlashes'];
		//
		foreach ( $data as $el ) {
			$toSimple[$el] = (isset($el) && !empty($toSimple[$el])) ? $toSimple[$el] : [];
		}
	}

	/**
	 * Convierte los datos del query a datos procesables directo para php
	 *
	 * @param array $toSimple estructura para simplificar
	 * @param array $datos    datos a procesar
	 */
	private static function dataParse($toSimple, &$datos)
	{
		// Simplificar
		foreach ( $toSimple['int'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? \intval($datos[$target]) : 0;
		}
		//
		foreach ( $toSimple['float'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? \floatval($datos[$target]) : 0.0;
		}
		//
		foreach ( $toSimple['replace'] as $target => $values ) {
			$datos[$target] = (isset($datos[$target]) && isset($values[$datos[$target]])) ? $values[$datos[$target]] : '';
		}
		//
		foreach ( $toSimple['string'] as $target => $ops ) {
			if ( isset($datos[$target]) ) {
				foreach ( $ops as $cOp ) {
					$datos[$target] = $cOp($datos[$target]);
				}
			}
		}
		//
		foreach ( $toSimple['timeUnix'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? \strtotime($datos[$target]) : 0;
		}
		//
		foreach ( $toSimple['json'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? \json_decode($datos[$target], true) : [];
		}
		//
		foreach ( $toSimple['stripCSlashes'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? \stripCSlashes($datos[$target]) : '';
		}
		//
		foreach ( $toSimple['moneyInt'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? (\intval($datos[$target]) / 100) : 0.00;
		}
		//
		foreach ( $toSimple['moneyIntTxt'] as $target ) {
			$datos[$target] = (isset($datos[$target])) ? '$' . \number_format((intval($datos[$target]) / 100), 2, '.', ',') : "\$0.00";
		}
	}

	//</editor-fold>


	//<editor-fold desc="Debugging">

	//
	// Init: createError
	// Genera un string para imprimir que tiene las características de toda la db;
	private static function createError($query)
	{
		// Determinar si es un verdadero error
		if ( strlen(self::$mysqli->error) === 0 ) {
			self::$se_error = "No existen registros.";
		}
		else {
			// Procesar el query
			$search = ["/[ \t]+/", "/\r+/", "/[\n]+/", "/[ ]*\n[ ]*/"];
			$replace = [' ', '', "\n", "\n"];

			// Manipular query
			self::$lastQuery = preg_replace($search, $replace, $query);

			// Guardar error
			self::$ms_error = "(" . self::$mysqli->errno . ") ";
			self::$ms_error .= preg_replace($search, $replace, self::$mysqli->error);

			//
			self::$isError = true;
			self::$se_error = "SQL tiene errores.";
		}
	}
	// End: createError
	//

	//
	// Init: createAdvancedError
	// Genera el error avanzado
	public static function createAdvancedError($extraInfo = '', $errorKey = '')
	{
		// Seleccionar error a mostrar
		global $siteVars;

		$debug = [
			'key' => $errorKey,
			'msg' => $extraInfo,
			'seError' => self::$se_error,
			'msError' => self::$ms_error,
			'query' => '',
			'querySimple' => self::$lastQuery,
			'trace' => ''
		];

		// Simplificar Query
		$strings = explode("\n", self::$lastQuery);
		$count = 1;
		foreach ( $strings as $str ) {
			$countMod = str_pad($count, 2, '0', STR_PAD_LEFT);
			$debug['query'] .= $countMod . ' | ' . $str . "\n";
			$count++;
		}

		//
		$e = new \Exception;
		$debug['trace'] = $e->getTraceAsString();

		// Sólo salvar si es un error... hay otro caso dónde se cree sin error?
		if ( self::$isError ) {

			//
			$savedReport = <<<EOD
KEY:\t{$debug['key']}
MSG:\t{$debug['msg']}
SE ERR:\t{$debug['seError']}
MS ERR:\t{$debug['msError']}
QUERY SIMPLE:
---
{$debug['querySimple']}
---
TRACE:
{$debug['trace']}
---
EOD;
			//

			//
			$savedReport = self::$mysqli->real_escape_string($savedReport);


			//
			$ins_qry = <<<SQL
INSERT INTO sb_errorlog
(elog_dtadd, elog_type, elog_key, elog_content)
VALUES
(NOW(), 1, '{$errorKey}', '{$savedReport}');
SQL;
			//
			self::submitQuery($ins_qry);
		}

		// Regresar
		return $debug;
	}
	// End: createAdvancedError
	//

	//
	// Init: killIfError2
	//	Mata todas las operaciones en caso de error si no regresa que es vació
	public static function killIfError2($errorKey = '', $extraInfo = '')
	{
		self::killIfError($errorKey, $extraInfo);
	}
	// End: killIfError2
	//

	//
	// Init: killIfError
	//	Mata todas las operaciones en caso de error si no regresa que es vació
	public static function killIfError($errorKey = '', $extraInfo = '')
	{
		if ( self::$isError ) {
			self::killProcess($errorKey, $extraInfo);
		}
	}
	// End: killIfError
	//

	//
	// Init: killIfVoid
	//	Revisa si hay error SQL o query vacio
	public static function killIfVoid($extraInfo, $errorKey = '')
	{
		if ( self::$isError || self::$isNull ) {
			self::killProcess($errorKey, $extraInfo);
		}
	}
	// End: killIfVoid
	//

	//
	// Init: killProcess
	//	detener sitio
	public static function killProcess($errorKey = '', $extraInfo = '')
	{
		//
		$debugInfo = self::createAdvancedError($extraInfo, $errorKey);

		//
		se_killWithError(
			'ERROR (SISTEMA) DB.',
			"[{$errorKey}] {$extraInfo}",
			500,
			['query' => $debugInfo]
		);
	}
	// End: killProcess
	//

	//
	private static function mysqliErrorProcess(
		string $query,
		string $errorKey = '',
		string $errorDesc = '',
		array|null $data = null
	) : void {
		// Procesar el query
		$search = ["/[ \t]+/", "/\r+/", "/[\n]+/", "/[ ]*\n[ ]*/"];
		$replace = [' ', '', "\n", "\n"];

		// Manipular query
		self::$lastQuery = preg_replace($search, $replace, $query);

		// Guardar error
		self::$ms_error = "(" . self::$mysqli->errno . ") ";
		self::$ms_error .= preg_replace($search, $replace, self::$mysqli->error);

		//
		self::$isError = true;
		self::$se_error = "SQL tiene errores.";

		//
		$debug = [
			'key' => $errorKey,
			'msg' => $errorDesc,
			'seError' => self::$se_error,
			'msError' => self::$ms_error,
			'query' => '',
			'querySimple' => self::$lastQuery,
			'data' => ($data) ? json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_SLASHES) : '',
			'trace' => ''
		];

		// Simplificar Query
		$strings = explode("\n", self::$lastQuery);
		$count = 1;
		foreach ( $strings as $str ) {
			$countMod = str_pad($count, 2, '0', STR_PAD_LEFT);
			$debug['query'] .= $countMod . ' | ' . $str . "\n";
			$count++;
		}

		//
		$e = new \Exception;
		$debug['trace'] = $e->getTraceAsString();

		//
		$savedReport = <<<EOD
KEY:\t{$debug['key']}
MSG:\t{$debug['msg']}
SE ERR:\t{$debug['seError']}
MS ERR:\t{$debug['msError']}
QUERY SIMPLE:
---
{$debug['querySimple']}
---
DATA (PROBABLY):
---
{$debug['querySimple']}
---
TRACE:
{$debug['trace']}
---
EOD;
		//

		//
		$savedReport = self::$mysqli->real_escape_string($savedReport);

		//
		$ins_qry = <<<SQL
INSERT INTO sb_errorlog
(elog_dtadd, elog_type, elog_key, elog_content)
VALUES
(NOW(), 1, '{$errorKey}', '{$savedReport}');
SQL;
		//
		self::submitQuery($ins_qry, ['securityKill' => true]);

		//
		se_killWithError(
			'DB Error',
			"[{$errorKey}] {$errorDesc}",
			500,
			$debug
		);
	}

	// Importadas directo de 
	public static function real_escape_string($string)
	{
		return self::$mysqli->real_escape_string($string);
	}

	//
	public static function server_info()
	{
		return self::$mysqli->server_info;
	}
	//</editor-fold>
}
// Fin de clase
