<?php
// Actualizar archivos
namespace snkeng\core\engine;

//
use function snkeng\core\engine\se_siteVar;

class fileUpd
{
	// Variables
	/**
	 * @var
	 */
	private $siteVars;
	/**
	 * @var string
	 */
	private $opLog = '';

	/**
	 * Load basic mysql and siteVars
	 */
	public function __construct()
	{
		global $siteVars;
		$this->siteVars =& $siteVars;
	}

	//

	/**
	 *
	 * @param $type
	 * @param $name
	 *
	 * @return void
	 */
	private function logAdd($type, $name)
	{
		$this->opLog .= $name;
		if ( $type === 2 ) {
			$this->opLog .= "\n";
		}
	}

	//

	/**
	 * @param $response
	 * @param $title
	 * @param $message
	 *
	 * @return void
	 */
	private function escapeOp($response, $title, $message = '')
	{
		$this->logAdd(2, 'FAIL');
		$response['s']['t'] = 0;
		$response['s']['date'] = date('h:i:s A');
		$response['s']['e'] = $title;
		$response['s']['ex'] = $this->opLog . "\n\n------\n" . $message;
		printJSONData($response);
	}

	//

	/**
	 * @param $response
	 * @param $domain
	 * @param $postData
	 * @param $file
	 *
	 * @return void
	 */
	private function request(&$response, $domain, $postData, $file = null)
	{
		// Variables
		$cUrl = $domain . '/se_core/php/updater/updater.php';
		$passwords = [
			'passA' => $_ENV['SE_SYNC_CODE'],
		];
		$iniPost = array_merge($passwords, $postData);
		// debugVariable($iniPost);
		// $iniPost = $passwords;

		$curlOps = [
			CURLOPT_URL => $cUrl,
			CURLOPT_USERAGENT => "MSIE 3.0",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_POST => true
		];

		//
		if ( $file ) {
			// Create a CURLFile object / procedural method
			$iniPost['file'] = $file;
		}
		$curlOps[CURLOPT_POSTFIELDS] = $iniPost;

		//
		$ch = curl_init();
		curl_setopt_array($ch, $curlOps);
		$url_response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		//
		$curl_errno = curl_errno($ch);
		curl_close($ch);

		if ( $curl_errno ) {
			// $response['s']['t'] = 0;
			// $response['s']['e'] = 'Curl error: '.curl_error($ch);
			$this->escapeOp($response, 'Conn Err type: ' . $status, 'URL: ' . $cUrl . "\nRESPONSE:\n" . $url_response);
		}

		//
		if ( $response['s']['t'] && $status !== 200 ) {
			$this->escapeOp($response, 'Conn Err type: ' . $status, 'URL: ' . $cUrl . "\nRESPONSE:\n" . $url_response);
		}

		//
		if ( !empty($url_response) ) {
			$response = json_decode($url_response, true);
			$jsonError = true;
			$eText = '';
			switch ( json_last_error() ) {
				case JSON_ERROR_NONE:
					$jsonError = false;
					break;
				case JSON_ERROR_DEPTH:
					$eText = ' - Maximum stack depth exceeded';
					break;
				case JSON_ERROR_STATE_MISMATCH:
					$eText = ' - Underflow or the modes mismatch';
					break;
				case JSON_ERROR_CTRL_CHAR:
					$eText = ' - Unexpected control character found';
					break;
				case JSON_ERROR_SYNTAX:
					$eText = ' - Syntax error, malformed JSON';
					break;
				case JSON_ERROR_UTF8:
					$eText = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
					break;
				default:
					$eText = ' - Unknown error';
					break;
			}
			if ( $jsonError ) {
				$this->escapeOp($response, 'Invalid JSON: ' . $eText, 'URL: ' . $cUrl . "\nRESPONSE:\n" . $url_response);
			}
		} else {
			$this->escapeOp($response, 'El servidor no responde.');
		}

		// Si regresa con error.... agregar a la lista y cerrar
		if ( !$response['s']['t'] ) {
			$this->escapeOp($response, 'Servidor repsonde con error', "MENSAJE:\n" . $response['s']['e'] . "\n" . $response['s']['ex']);
		}
	}

	//

	/**
	 * @param $response
	 * @param $data
	 *
	 * @return void
	 */
	public function first_install(&$response, $data)
	{
		$response['debug']['ftp_log'] = "INIT:\n";

		// Initial files
		$conn_id = ftp_connect($data['server']);
		$login_result = ftp_login($conn_id, $data['user'], $data['pass']);

		// check connection
		if ( (!$conn_id) || (!$login_result) ) {
			$response['debug']['ftp_log'] .= "- No connection. S:{$data['server']}, U:{$data['user']}, P:{$data['pass']}\n";
			return;
		} else {
			$response['debug']['ftp_log'] .= "- Connected.\n";
		}

		//
		$minFiles = [
			'/se_site_core/specs/s_server_config.php',
			'/se_site_core/specs/s_sitevars.php',
			'/se_core/priv/class/mysqliMaster.php',
			'/se_core/priv/library/eng_ini_head.php',
			'/se_core/priv/library/eng_ini_basic_library.php',
			'/se_core/res/eng_ajax/f_ie_upd.php',
			'/se_core/priv/class/engine_fileUpd.php',
		];

		foreach ( $minFiles as $file ) {
			//
			$lPos = strrpos($file, '/');
			$dir = substr($file, 1, $lPos - 1); // Remove first and last "/"
			$fileName = substr($file, $lPos + 1); // Just after last "/"
			//
			$localFile = realpath($_SERVER['DOCUMENT_ROOT'] . $file);

			// Create / Check directory

			// Reset to root
			if ( !empty($data['root']) ) {
				ftp_chdir($conn_id, $data['root']);
			} else {
				ftp_chdir($conn_id, '/');
			}
			// Get to folder
			$parts = explode('/', $dir);
			foreach ( $parts as $part ) {
				if ( !@ftp_chdir($conn_id, $part) ) {
					ftp_mkdir($conn_id, $part);
					ftp_chdir($conn_id, $part);
					//ftp_chmod($ftpcon, 0777, $part);
				}
			}
			// upload the file
			$upload = ftp_put($conn_id, $fileName, $localFile, FTP_BINARY);

			// check upload status
			$response['debug']['ftp_log'] .= ($upload) ? 'K' : 'F';
			$response['debug']['ftp_log'] .= " - {$dir} - {$fileName}\n";

			if ( !$upload ) {
				$response['s']['t'] = 0;
				$response['s']['e'] = "FTP ERROR";
				$response['s']['ex'] = "At: {$localFile}";
				return;
			}
		}

		// close the FTP stream
		ftp_close($conn_id);

		$progress = [];

		// INSTALACIÓN ARCHIVOS
		$fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/site_update.zip';
		//
		$this->file_systemUpdate_completeZipCreate($response, $fileName, strtotime('2000-01-01'));
		$progress[] = 'ZIP Create: Success';
		// Agregar archivo al request
		$file = new \CURLFile($fileName, 'application/zip');
		$this->request($response, $data['site'], ['op' => 'files', 'act' => 'upload', 'date' => date("Y-m-d H:i:s")], $file);
		$progress[] = 'FILES Update: Success';
		$response['d']['progress'] = $progress;

		// DATABASE
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->fullBackup();
		file_put_contents($fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/site_sql_all.txt', $alpha->getData());
		$file = new \CURLFile($fileName, 'text/plain');
		$this->request($response, $data['site'], ['op' => 'sql_file'], $file);
	}

	//<editor-fold desc="Files">

	//
	/**
	 * @param $response
	 * @param $domain
	 *
	 * @return void
	 */
	public function files_uploads(&$response, $domain)
	{
		//
		$this->logAdd(2, 'Crear archivo.');
		$fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/site_uploads.zip';
		//
		$this->uploadsZipCreate($response, $fileName);
		// Agregar archivo al request
		$file = new \CURLFile($fileName, 'application/zip');
		//
		$this->logAdd(2, 'Subir archivo.');
		$this->request($response, $domain, ['op' => 'files', 'act' => 'add', 'date' => date("Y-m-d H:i:s")], $file);
	}

	//

	/**
	 * @param $response
	 * @param $domain
	 *
	 * @return void
	 */
	public function files_systemUpdate(&$response, $domain)
	{
		// Solicitar última actualización
		$this->logAdd(2, 'Request information: ' . $domain);
		$this->request($response, $domain, ['op' => 'files', 'act' => 'read']);

		// Fecha límite
		$dtLimit = strtotime($response['d']['date']);

		// Create zip and send
		$this->file_systemUpdate_partialZipCreate($response, $domain, $dtLimit);
	}

	//

	/**
	 * @param $response
	 * @param $domain
	 *
	 * @return void
	 */
	public function files_systemReinstall(&$response, $domain)
	{
		$this->file_systemUpdate_partialZipCreate($response, $domain, 0);
		/*
		$progress = [];
		//
		$fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/site_update.zip';
		//
		if ( $response['s']['t'] ) {
			$this->file_systemUpdate_completeZipCreate($response, $fileName, strtotime('2000-01-01'));
			$progress[] = 'ZIP Create: Success';
			// Agregar archivo al request
			$file = new \CURLFile($fileName, 'application/zip');
			$this->request($response, $domain, ['op' => 'files', 'act' => 'upload', 'date'=> date("Y-m-d H:i:s")], $file);
			$progress[] = 'FILES Update: Success';
		}
		$response['d']['progress'] = $progress;
		*/
	}

	//

	/**
	 * Apply update by extracting files and deleting extras (from upload)
	 *
	 * @param $response
	 * @param $file
	 * @param $date
	 *
	 * @return void
	 */
	public function files_updateExec(&$response, $file, $date)
	{
		$root = $_SERVER['DOCUMENT_ROOT'];
		$rootFiles = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_processed/temp';

		// Zip extraction and closing
		$zip = new \ZipArchive;
		$fileRes = $zip->open($file);
		if ( $fileRes !== true ) {
			se_killWithError("El archivo no puede ser abierto.(FILE:'{$file}'.)", $fileRes);
		}
		//
		$zip->extractTo($root);
		$zip->close();

		// Files log
		$logFile = $rootFiles . DIRECTORY_SEPARATOR . 'update_log.txt';
		$fileFile = $rootFiles . DIRECTORY_SEPARATOR . 'update_files.txt';

		//
		if ( !file_exists($fileFile) ) {
			return;
		}

		// Read system current files
		$filesOrig = explode("\r\n", file_get_contents($fileFile));
		$filesCur = [];
		$len = strlen($root) + 1;
		$this->dirToArray($filesCur, $root . '/site_core', $len);
		$this->dirToArray($filesCur, $root . '/core', $len);
		$this->dirToArray($filesCur, $root . '/se_files/site_generated/bundles', $len);
		$this->dirToArray($filesCur, $root . '/se_files/site_generated/modules', $len);
		$this->dirToArray($filesCur, $root . '/se_files/site_generated/svg-icons/final', $len);

		// Compare expected files (from upload) with obtained files and return extra files
		$filesDel = array_diff($filesCur['files'], $filesOrig);

		// debugVariable($filesCur['files'], 'FILES Sistema', false);
		// debugVariable($filesOrig, 'FILES Update', false);
		// debugVariable($filesDel, 'FILES Diff', false);

		// Delete extra files
		foreach ( $filesDel as $cFile ) {
			unlink($root . DIRECTORY_SEPARATOR . $cFile);
		}
		// Delete log files?
		unlink($logFile);
		unlink($fileFile);

		// Actualizar fecha
		se_siteVar('engUpdate', $date);
	}

	//

	/**
	 * @param $response
	 * @param $file
	 *
	 * @return void
	 */
	public function files_addExec(&$response, $file)
	{
		$root = $_SERVER['DOCUMENT_ROOT'];

		// Extraer archivos
		$zip = new \ZipArchive;
		if ( $zip->open($file) !== true ) {
			se_killWithError("No pudo ser abierto el archivo", "");
		}

		$zip->extractTo($root);
		$zip->close();

		// Not a secondary update operation
	}

	//

	/**
	 * @param $dir
	 *
	 * @return void
	 */
	private function deleteAll($dir)
	{
		//
		foreach ( glob($dir . '/*') as $file ) {
			if ( is_dir($file) ) {
				$this->deleteAll($file);
			} else {
				unlink($file);
			}
		}
		//
		rmdir($dir);
	}

	//

	/**
	 * Update core from main site download (from system)
	 *
	 * @param $response
	 * @param $file
	 *
	 * @return void
	 */
	public function file_core_update(&$response, $file)
	{
		$coreRoot = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core';

		// Delete all files
		// $this->deleteAll($coreRoot);

		// Extraer archivos
		$zip = new \ZipArchive;
		if ( $zip->open($file) !== true ) {
			se_killWithError("No pudo ser abierto el archivo", "");
		}

		$zip->extractTo($coreRoot);
		$zip->close();

		// Update version
		if ( !file_exists($coreRoot . '/info.json') ) {
			se_killWithError('file uploaded, but update not executed...');
		}

		// Update information in system
		$fileContents = file_get_contents($coreRoot . '/info.json');
		$jsonInformation = \json_decode($fileContents, true);
		//
		$response['debug']['inf'] = $jsonInformation;
		//
		$sql_upd = <<<SQL
UPDATE sa_sitevars SET sv_value='{$jsonInformation['version']}' WHERE sv_var='engVer' LIMIT 1;
UPDATE sa_sitevars SET sv_value='{$jsonInformation['date']}' WHERE sv_var='engUpdate' LIMIT 1;
SQL;
		//
		\snkeng\core\engine\mysql::submitMultiQuery($sql_upd);
	}

	//

	/**
	 * Get files for update (read current files in expected folders, compare last update date, add if needed)
	 *
	 * @param $response
	 * @param $domain
	 * @param $dtLimit
	 *
	 * @return void
	 */
	private function file_systemUpdate_partialZipCreate(&$response, $domain, $dtLimit = 0)
	{
		$complete = false;
		$maxFileSize = 19 * 1024 * 1024;
		$totalFileSize = 0;
		$updFileCount = 0;
		$updfiles = [];
		$cFile = '';
		$newFile = true;


		// Get all the files from expected folders

		$files = [];
		$len = strlen($_SERVER['DOCUMENT_ROOT']) + 1;
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/modules', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final', $len);

		// Default files
		$files['files'][] = 'se_index.php';
		$files['files'][] = '.htaccess';

		// Archivos
		$date_create = date("Y-m-d H:i:s");
		$fileData = '';
		$filesSystem = '';
		$c_fileTotal = 0;
		$c_fileAdd = 0;
		$c_fileFirst = true;

		// Zip obj create
		$zip = new \ZipArchive();

		$this->logAdd(2, 'Creando archivos');

		// Compare all files
		foreach ( $files['files'] as $file ) {
			//
			if ( $newFile ) {
				$curFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_processed/temp/site_update_' . $updFileCount . '.zip';
				$updfiles[] = $curFile;

				$this->logAdd(2, 'Archivo nuevo:' . $updFileCount);

				$updFileCount++;

				// Remove if exists
				if ( file_exists($curFile) ) {
					unlink($curFile);
				}

				// Crear
				if ( $zip->open($curFile, \ZipArchive::CREATE) !== true ) {
					$this->escapeOp($response, 'No pudo crearse el archivo:', $curFile);
				}

				//
				$newFile = false;
				$totalFileSize = 0;
			}

			$cFile = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $file;

			if ( !file_exists($cFile) ) {
				die("maximum failure, file not exist during zip:'{$cFile}'.");
			}
			$fDate = max(filemtime($cFile), filectime($cFile));
			$fileAdded = 'F';
			if ( $dtLimit < $fDate ) {
				$fileAdded = 'T';
				// Windows (backslash) -> Unix (slash)
				$zipFile = str_replace("\\", "/", $file);
				$zip->addFile($cFile, $zipFile);
				//
				$c_fileAdd++;
				//
				$totalFileSize += filesize($cFile);
			}

			// Log
			$file = str_replace("\\", "/", $file);
			// Archivos (todos)
			$filesSystem .= ($c_fileFirst) ? '' : "\r\n";
			$filesSystem .= $file;
			$c_fileFirst = false;
			// Simple
			$fileData .= "{$fileAdded},{$fDate},'{$file}'\r\n";
			//
			$c_fileTotal++;

			//
			if ( $totalFileSize >= $maxFileSize ) {
				$zip->close();
				$newFile = true;
			}
		}

		// Create new file if needed
		if ( $newFile ) {
			$curFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_processed/temp/site_update_' . $updFileCount . '.zip';
			$updfiles[] = $curFile;

			$this->logAdd(2, 'Archivo nuevo:' . $updFileCount);

			// Remove if exists
			if ( file_exists($curFile) ) {
				unlink($curFile);
			}

			// Crear
			if ( $zip->open($curFile, \ZipArchive::CREATE) !== true ) {
				$this->escapeOp($response, 'No pudo crearse el archivo:', $curFile);
			}
		}

		$fileData = <<<TXT
Nombre: {$curFile}.
Creado: '{$date_create}'.
---
ARCHIVOS:
Agregados: {$c_fileAdd}
Totales: {$c_fileTotal}
ZIP: {$zip->numFiles}.
LOG:
------
{$fileData}
------
TXT;

		// Log
		$zip->addFromString("se_files/site_processed/temp/update_log.txt", $fileData);
		// Archivo de archivos
		$zip->addFromString("se_files/site_processed/temp/update_files.txt", $filesSystem);
		$zip->close();

		$this->logAdd(2, 'Subiendo archivos');

		// Agregar archivo al request
		foreach ( $updfiles as $updCFile ) {
			$file = new \CURLFile($updCFile, 'application/zip');
			$this->logAdd(2, '- ' . $updCFile);
			$this->request($response, $domain, ['op' => 'files', 'act' => 'upload', 'date' => date("Y-m-d H:i:s")], $file);
		}
	}

	//

	/**
	 * Creates a full backup, no filters (same as manual update)
	 *
	 * @param $response
	 * @param $fileName
	 * @param $dtLimit
	 *
	 * @return void
	 */
	private function file_systemUpdate_completeZipCreate(&$response, $fileName, $dtLimit)
	{
		$files = [];
		$len = strlen($_SERVER['DOCUMENT_ROOT']) + 1;
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/modules', $len);
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final', $len);

		// Archivos externo a los directorios
		$files['files'][] = 'se_index.php';
		$files['files'][] = '.htaccess';

		// Archivos
		$date_create = date("Y-m-d H:i:s");
		$fileData = '';
		$filesSystem = '';
		$c_fileTotal = 0;
		$c_fileAdd = 0;
		$c_fileFirst = true;

		// Remove if exists
		if ( file_exists($fileName) ) {
			unlink($fileName);
		}
		// Zip create
		$zip = new \ZipArchive();

		// Crear
		if ( $zip->open($fileName, \ZipArchive::CREATE) !== true ) {
			$this->escapeOp($response, 'No pudo crearse el archivo:', $fileName);
		}

		// Archivos
		foreach ( $files['files'] as $file ) {
			$cFile = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $file;

			//
			$fDate = max(filemtime($cFile), filectime($cFile));
			$fileAdded = 'F';
			if ( $dtLimit < $fDate ) {
				$fileAdded = 'T';
				// Windows (backslash) -> Unix (slash)
				$zipFile = str_replace("\\", "/", $file);
				$zip->addFile($cFile, $zipFile);
				//
				$c_fileAdd++;
			}
			// Log
			$file = str_replace("\\", "/", $file);
			// Archivos (todos)
			$filesSystem .= ($c_fileFirst) ? '' : "\r\n";
			$filesSystem .= $file;
			$c_fileFirst = false;
			// Simple
			$fileData .= "{$fileAdded},{$fDate},'{$file}'\r\n";
			//
			$c_fileTotal++;
		}

		//
		$fileData = <<<TXT
Nombre: {$fileName}.
Creado: '{$date_create}'.
---
ARCHIVOS:
Agregados: {$c_fileAdd}
Totales: {$c_fileTotal}
ZIP: {$zip->numFiles}.
LOG:
------
{$fileData}
------
TXT;

		// Log
		$zip->addFromString("update_log.txt", $fileData);

		// Archivo de archivos
		$zip->addFromString("update_files.txt", $filesSystem);
		$zip->close();
	}

	//

	/**
	 * Creates zip file of uploaded content (/res folder)
	 *
	 * @param $response
	 * @param $fileName
	 *
	 * @return void
	 */
	private function uploadsZipCreate(&$response, $fileName)
	{
		$files = [];

		// Logs
		$date_create = date("Y-m-d H:i:s");
		$fileData = '';
		$filesSystem = '';
		$c_fileTotal = 0;
		$c_fileAdd = 0;
		$c_fileFirst = true;

		// Obtener archivos
		$len = strlen($_SERVER['DOCUMENT_ROOT']) + 1;
		$this->dirToArray($files, $_SERVER['DOCUMENT_ROOT'] . '/res', $len);

		// Remove if exists
		if ( file_exists($fileName) ) {
			unlink($fileName);
		}

		// Zip create
		$zip = new \ZipArchive();

		// Crear
		if ( $zip->open($fileName, \ZipArchive::CREATE) !== true ) {
			exit("cannot open <$fileName>\n");
		}

		// ALL files will be updloaded
		foreach ( $files['files'] as $file ) {
			$cFile = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $file;

			//
			$fDate = filemtime($cFile);
			//
			$fileAdded = 'T';


			// Windows (backslash) -> Unix (slash)
			$zipFile = str_replace("\\", "/", $file);
			$zip->addFile($cFile, $zipFile);
			//
			$c_fileAdd++;


			// Log
			$file = str_replace("\\", "/", $file);
			// Archivos (todos)
			$filesSystem .= ($c_fileFirst) ? '' : "\r\n";
			$filesSystem .= $file;
			$c_fileFirst = false;
			// Simple
			$fileData .= "{$fileAdded},{$fDate},'{$file}'\r\n";
			//
			$c_fileTotal++;
		}


		$fileData = <<<TXT
Nombre: {$fileName}.
Creado: '{$date_create}'.
---
ARCHIVOS:
Agregados: {$c_fileAdd}
Totales: {$c_fileTotal}
ZIP: {$zip->numFiles}.
LOG:
------
{$fileData}
------
TXT;

		// Log
		$zip->addFromString("uploads_log.txt", $fileData);
		// Archivo de archivos
		$zip->addFromString("uploads_files.txt", $filesSystem);
		$zip->close();
	}

	//

	/**
	 * Converts a folder directory to an array file list
	 *
	 * @param $result
	 * @param $dir
	 * @param $len
	 *
	 * @return void
	 */
	private function dirToArray(&$result, $dir, $len)
	{
		$cdir = array_diff(scandir($dir), ['..', '.', 'temp']);
		if ( !empty($cdir) ) {
			$result['folder'][] = substr($dir . DIRECTORY_SEPARATOR, $len);
			foreach ( $cdir as $key => $value ) {
				if ( is_dir($dir . DIRECTORY_SEPARATOR . $value) ) {
					$this->dirToArray($result, $dir . DIRECTORY_SEPARATOR . $value, $len);
				} else {
					$result['files'][] = substr($dir . DIRECTORY_SEPARATOR . $value, $len);
				}
			}
		} else {
			// Borrar si vacío
			@rmdir($dir);
		}
	}

	//</editor-fold>

	//<editor-fold desc="SQL">

	/**
	 * @param $response
	 * @param $domain
	 *
	 * @return void
	 */
	public function sql_allUpdate(&$response, $domain)
	{
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->fullBackup();
		file_put_contents($fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/site_sql_all.txt', $alpha->getData());
		$file = new \CURLFile($fileName, 'text/plain');
		$this->request($response, $domain, ['op' => 'sql_file'], $file);
	}

	/**
	 * @param $response
	 * @param $domain
	 *
	 * @return void
	 */
	public function sql_structureCreate(&$response, $domain)
	{
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->structure();
		file_put_contents($fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_processed/temp/site_sql_structure.txt', $alpha->getData());
		$file = new \CURLFile($fileName, 'text/plain');
		$this->request($response, $domain, ['op' => 'sql_file'], $file);
	}

	/**
	 * @param $response
	 * @param $sql
	 *
	 * @return void
	 */
	public function sql_exec(&$response, $sql)
	{
		if ( \snkeng\core\engine\mysql::submitMultiQuery($sql) ) {
			$response['s']['d'] = "La base de datos ha sido cargada con éxito.<br />\n";
		} else {
			$response['s']['e'] = "ERROR: No se pudo subir la base de datos al sistema.";
			if ( \snkeng\core\engine\mysql::isError ) {
				$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', 'full');
			} else {
				$response['s']['ex'] = "Contenido Vacío.<br />\n";
			}
		}
	}

	//

	/**
	 * @param $response
	 * @param $domain
	 * @param $sql
	 *
	 * @return void
	 */
	public function sql_send(&$response, $domain, $sql)
	{
		$this->request($response, $domain, ['op' => 'sql_exec', 'sql' => $sql]);
		$response['debug']['send'] = $sql;
	}

	//

	/**
	 * @param $response
	 * @param $sql
	 *
	 * @return void
	 */
	public function sql_direct(&$response, $sql)
	{
		if ( \snkeng\core\engine\mysql::multiQuery($sql) ) {
			$queryCounter = 1;
			$result = '';
			do {
				$result .= "<h4>Query $queryCounter</h4>\n";
				if ( !empty(\snkeng\core\engine\mysql::$result) ) {
					$result .= "<table class=\"qTable\">\n";
					$result .= "<thead>\n";
					$finfo = \snkeng\core\engine\mysql::$result->fetch_fields();
					foreach ( $finfo as $x ) {
						$result .= "\t<th>" . $x->name . "</th>\n";
					}
					$result .= "</thead>\n<tbody>\n";
					while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
						$ammount = sizeof($data) / 2;
						$result .= "<tr>\n";
						for ( $i = 0; $i < $ammount; $i++ ) {
							$result .= "\t<td>" . wordwrap($data[$i], 80, "<br />") . "</td>\n";
						}
						$result .= "</tr>\n";
					}
					$result .= "</table>\n</tbody>\n";
					/* print divider */
					if ( \snkeng\core\engine\mysql::$moreResults ) {
						$result .= "-----------------<br />\n";
					}
				} else {
					$result .= "Comando ejecutado.";
				}
				$queryCounter++;
			} while ( \snkeng\core\engine\mysql::mqNextResult() );

			// Reportar
			$response['s']['d'] = 'Query ejecutado';
			$response['s']['ex'] = $result;
		} else {
			$response['s']['t'] = 0;
			$response['s']['e'] = 'Query no válido.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('ERROR Query no válido', 'full');
		}
	}

	/**
	 * @param $response
	 * @param $file
	 *
	 * @return void
	 */
	public function sql_file(&$response, $file)
	{
		$sql = file_get_contents($file);
		if ( \snkeng\core\engine\mysql::multiQuery($sql) ) {
			$queryCounter = 1;
			$result = '';
			do {
				$result .= "<h4>Query $queryCounter</h4>\n";
				if ( !empty(\snkeng\core\engine\mysql::$result) ) {
					$result .= "<table class=\"qTable\">\n";
					$result .= "<thead>\n";
					$finfo = \snkeng\core\engine\mysql::$result->fetch_fields();
					foreach ( $finfo as $x ) {
						$result .= "\t<th>" . $x->name . "</th>\n";
					}
					$result .= "</thead>\n<tbody>\n";
					while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
						$ammount = sizeof($data) / 2;
						$result .= "<tr>\n";
						for ( $i = 0; $i < $ammount; $i++ ) {
							$result .= "\t<td>" . wordwrap($data[$i], 80, "<br />") . "</td>\n";
						}
						$result .= "</tr>\n";
					}
					$result .= "</table>\n</tbody>\n";
					/* print divider */
					if ( \snkeng\core\engine\mysql::$moreResults ) {
						$result .= "-----------------<br />\n";
					}
				} else {
					$result .= "Comando ejecutado.";
				}
				$queryCounter++;
			} while ( \snkeng\core\engine\mysql::mqNextResult() );
			// Reportar
			$response['s']['d'] = 'Query ejecutado';
			$response['s']['ex'] = $result;
		} else {
			$response['s']['t'] = 0;
			$response['s']['e'] = 'Query no válido.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('ERROR Query no válido', 'full');
		}
	}

	//</editor-fold>
}
//
