<?php
//
namespace snkeng\core\engine;


class processUserInput
{
	public static string $errorMessage = '';
	public static array $errorList = [];
	public static int $errorCount = 0;

	//

	public static array $exampleArray = [
		'varName' => [
			'type' => 'int',
			'required' => true,
			'validate' => true,
			'name' => 'Test Int',
			'properties' => [
				'min' => 0,
				'max' => 10,
			]
		]
	];

	/**
	 * Processing of a single element (value) with a descriptor
	 *
	 * @param mixed $value       The inserted element to check
	 * @param array $description The array descriptor of the element
	 *
	 * @return void
	 */
	public static function processElement(mixed $value, string $varName, array $description) : mixed
	{
		//
		switch ( $description['type'] ) {
			//
			case 'int':
			case 'float':
			case 'moneyInt':
				if ( $description['required'] && $value === null ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				//
				switch ( $description['type'] ) {
					case 'int':
						$value = intval($value);
						break;
					case 'float':
						$value = floatval($value);
						break;
					case 'moneyInt':
						$value = intval(floatval($value) * 100);
						break;
				}


				//
				$valMax = $description['validation']['max'] ?? null;
				$valMin = $description['validation']['min'] ?? null;
				$notNull = $description['validation']['notNull'] ?? null;

				//
				if ( $notNull && $value === 0 ) {
					self::buildProcessError($varName, $description['name'], "is empty.");
					return null;
				} elseif ( $valMax && $value > $valMax ) {
					self::buildProcessError($varName, $description['name'], "is grater than {$valMax}");
					return null;
				} elseif ( $valMin && $value < $valMin ) {
					self::buildProcessError($varName, $description['name'], "is lower than {$valMin}");
					return null;
				}
				break;

			//
			case 'bool':
				$value = (isset($value) && $value !== 'false' && boolval($value)) ? 1 : 0;
				break;

			//
			case 'hexcolor':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				//
				$value = strtoupper(preg_replace("/[^A-Fa-f0-9 ]/", '', $value));
				$len = strlen($value);

				//
				if ( $len !== 3 && $len !== 6 ) {
					self::buildProcessError($varName, $description['name'], "format is not valid");
					return null;
				}
				break;

			//
			case 'url':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}


				//
				if ( !filter_var($value, FILTER_VALIDATE_URL) ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}
				break;

			//
			case 'email':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				//
				if ( !filter_var($value, FILTER_VALIDATE_EMAIL) ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}
				break;

			//
			case 'datetime':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				$dt = \DateTime::createFromFormat("Y-m-d H:i:s", $value);
				if ( !($dt !== false && !array_sum($dt->getLastErrors())) ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}

				//
				$value = $dt->format('Y-m-d H:i:s');
				break;

			//
			case 'date':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}


				$dt = \DateTime::createFromFormat("Y-m-d", $value);
				if ( !($dt !== false && !array_sum($dt->getLastErrors())) ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}

				//
				$value = $dt->format('Y-m-d');
				break;

			//
			case 'time':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				if ( !preg_match('/^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/', $val) ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}
				break;

			//
			case 'json':
				$value = \json_decode($value, true);

				if ( $value === null ) {
					self::buildProcessError($varName, $description['name'], "not valid format");
					return null;
				}
				break;

			//
			case 'array':
				$value = (isset($value)) ? $value : [];
				break;

			//
			case 'numberList':
				$value = (isset($value)) ? trim($value) : '';
				if ( $description['required'] && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				//
				$value = explode(',', $value);
				if ( $value ) {
					for ( $i = 0; $i < sizeof($value); $i++ ) {
						$value[$i] = intval($value[$i]);
					}
				}
				break;

			//
			case 'file':
				if ( $description['required'] && !isset($_FILES[$varName]) ) {
					$errors[] = $description['name'] . ' no subido.';
					self::$errorCount++;
				}

				//
				$value = $_FILES[$varName];
				$value['fName'] = strtolower(pathinfo($value['name'], PATHINFO_FILENAME));
				$value['fType'] = strtolower(pathinfo($value['name'], PATHINFO_EXTENSION));
				$value['fSize'] = round($value['size'] / 1024, 2);

				//
				if ( !empty($description['fType']) && !in_array($value['fType'], $description['fType']) ) {
					$fileFormats = implode(',', $description['fType']);
					self::buildProcessError($varName, $description['name'], "Formato de archivo no válido. Soportados: {$fileFormats}. Recibido:{$value['fType']}.");
					return null;
				} elseif ( !empty($description['mSize']) && $description['mSize'] < $value['fSize'] ) {
					self::buildProcessError($varName, $description['name'], "Archivo excede el tamaño máximo. (Máximo: {$description['mSize']} KBs, Recibido: {$value['fSize']} KBs.)");
					return null;
				} elseif ( empty($value['tmp_name']) ) {
					self::buildProcessError($varName, $description['name'], "Parece haber un problema con el archivo y no fue posible subirlo, por favor intente de nuevo.");
					return null;
				}
				break;

			//
			case 'files':
				$totalFiles = count($_FILES[$varName]['name']);

				// Re arreglo del array
				$value = [];
				$file_keys = array_keys($_FILES[$varName]);
				for ( $i = 0; $i < $totalFiles; $i++ ) {
					foreach ( $file_keys as $key ) {
						$value[$i][$key] = $_FILES[$varName][$key][$i];
					}
				}

				//
				for ( $i = 0; $i < $totalFiles; $i++ ) {
					$cFile = $value[$i];
					$cFile['fName'] = strtolower(pathinfo($cFile['name'], PATHINFO_FILENAME));
					$cFile['fType'] = strtolower(pathinfo($cFile['name'], PATHINFO_EXTENSION));
					$cFile['fSize'] = round($cFile['size'] / 1024, 2);

					//
					if ( empty($cFile['fName']) ) {
						$errors[] = $description['name'] . ' no subido.';
						self::$errorCount++;
						$cError = true;
					}
					if ( !$cError && !empty($description['fType']) && !in_array($cFile['fType'], $description['fType']) ) {
						$fileFormats = implode(',', $description['fType']);
						self::buildProcessError($varName, $description['name'], "Invalid format. Suported: {$fileFormats}. Received:{$value['fType']}.");
						return null;
					} elseif ( !$cError && !empty($description['mSize']) && $description['mSize'] < $cFile['fSize'] ) {
						self::buildProcessError($varName, $description['name'], "File to big. (Maximum Size: {$description['mSize']} KBs, Received: {$value['fSize']} KBs.)");
						return null;
					} elseif ( empty($value['tmp_name']) ) {
						self::buildProcessError($varName, $description['name'], "Problema al subir un archivo.");
						return null;
					}
				}
				break;

			// (Asume text type)
			default:
				$value = (isset($value)) ? trim($value) : '';
				if ( !empty($description['required'])  && empty($value) ) {
					self::buildProcessError($varName, $description['name'], "is empty");
					return null;
				}

				// Ajustes
				$maxLen = $description['validation']['maxlength'] ??  0;
				$minLen = $description['validation']['minlength'] ??  0;
				$filter = $description['validation']['filter'] ??  0;

				//
				if ( isset($description['process']) && $description['process'] ) {
					$value = self::textOpSimple($value, $description['lMax'], $filter);
				}

				// Validation
				$strLen = strlen($value);
				if ( $maxLen && $maxLen < $strLen ) {
					self::buildProcessError($varName, $description['name'], "String is too large. Max: {$maxLen}. Received: {$strLen}.");
					return null;
				}

				if ( $minLen && $minLen > $strLen ) {
					self::buildProcessError($varName, $description['name'], "String is too small. Max: {$minLen}. Received: {$strLen}.");
					return null;
				}
				break;
		}

		//
		return $value;
	}

	//
	public static function processRow(array $variableArray, array $descriptionList) : array
	{
		$results = [];

		//
		foreach ( $descriptionList as $varName => $description ) {
			$cVal = $variableArray[$varName] ?? null;
			$results[$varName] = self::processElement($cVal, $varName, $description);
		}

		return $results;
	}

	//
	public static function processMultiple(array $variableArray, array $descriptionList) : array {
		$results = [];

		//
		foreach ( $variableArray as $id => $row ) {
			$results[$id] = self::processRow($variableArray[$id], $descriptionList);
		}

		return $results;
	}

	//
	public static function getContents() : array | false {
		//
		if ( $_SERVER['CONTENT_TYPE'] === 'application/json' ) {
			$tempArray = \json_decode(file_get_contents('php://input'), true);
		} else {
			// Historic fallback
			$tempArray = $_POST;
		}

		//
		if ( empty($tempArray) ) {
			return false;
		}

		return $tempArray;
	}


	//
	public static function processRowRequest(array $descriptionList) : array | false {
		$originalData = self::getContents();

		$data = self::processRow($originalData, $descriptionList);

		//
		if ( self::$errorCount ) {
			nav::killWithError("Invalid Fields", "Please check the fields", 400, ['error' => self::$errorList]);
		}

		return $data;
	}

	//
	public static function processMultiRequest(array $descriptionList) : array | false {
		$originalData = self::getContents();

		$data = self::processMultiple($originalData, $descriptionList);

		//
		if ( self::$errorCount ) {
			nav::killWithError("Invalid Fields", "Please check the fields", 400, [], ['error' => self::$errorList]);
		}

		return $data;
	}



	//

	/**
	 * @param string $variableName
	 * @param string $errorDescription
	 *
	 * @return void
	 */
	private static function buildProcessError(string $variableName, string $variableTitle, string $errorDescription) : void
	{
		self::$errorList[] = [
			'name' => $variableName,
			'title' => $variableTitle,
			'message' => $errorDescription
		];
		self::$errorCount++;
	}



	// Operaciones de forma etendida para hacer las operaciones
	public static function textOperations($txt, $len, $strictCut, $delim, $reduceSpaces, $utfDecode, $htmlProtect, $sqlProtect, $regexCut = '', $addSlashes = false)
	{
		//
		$txt = trim($txt);
		$debug = false;

		// Debug
		if ( $debug ) {
			echo("-\nINI Debug:\n" . $txt . "\n");
		}

		// Quotes
		if ( function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc() ) {
			$txt = stripslashes($txt);
			if ( $debug ) {
				echo("-\nQuotes:\n" . $txt . "\n");
			}
		}

		// Remover caracteres no válidos
		if ( !empty($regexCut) ) {
			$txt = self::textCleaning($txt, $regexCut);
			if ( $debug ) {
				echo("-\nRegex:\n" . $txt . "\n");
			}
		}

		// Remover más de dos espaciados
		if ( $reduceSpaces ) {
			$search = array("/\r*/", "/\t+/", "/ +/", "/\n{2,}/");
			$replace = array("", " ", " ", "\n\n");

			$txt = preg_replace($search, $replace, $txt);
			if ( $debug ) {
				echo("-\nReduceSpaces:\n:" . $txt . "\n");
			}
		}

		// Quitar caracteres HTML
		if ( $htmlProtect ) {
			$txt = htmlspecialchars($txt, (1 | 2), 'UTF-8');
			if ( $debug ) {
				echo("-\nHTMLSpecialChars:\n" . $txt . "\n");
			}
		}

		// Soporte UTF8
		if ( $utfDecode ) {
			// $txt = $txt;
			if ( $debug ) {
				echo("-\nUTF8:\n" . $txt . "\n");
			}
		}

		// Protección SQL
		if ( $addSlashes ) {
			$txt = addslashes($txt);
			if ( $debug ) {
				echo("-\nAddSlashes:\n" . $txt . "\n");
			}
		}

		// Protección SQL
		if ( $sqlProtect ) {
			$txt = \snkeng\core\engine\mysql::real_escape_string($txt);
			if ( $debug ) {
				echo("-\nSQL - Escape:\n" . $txt . "\n");
			}
		}
		// Hacer funciones delim
		if ( $len !== 0 ) {
			$txtLen = strlen($txt);
			if ( $txtLen > $len ) {
				if ( $strictCut ) {
					$txt = substr($txt, 0, $len);
				} else {
					$rLen = 0;
					if ( empty($delim) ) {
						$delim = '...';
					} else {
						$rLen = strlen($delim);
					}
					preg_match('/(.{' . ($len - $rLen) . '}.*?)\b/', $txt, $matches);
					$txt = processUserInput . phprtrim($matches[1]) . $delim;
				}
			}
			if ( $debug ) {
				echo("-\nLim Cut:\n" . $txt . "\n");
			}
		}

		// Revisar que el último caracter no sea "\"
		$txt = rtrim($txt, "\\");

		return $txt;
	}

	// Función reducida para aplicar las operaciones de texto
	// Por lo general ya sufrieron el SQL Protect
	public static function textOpSimple($text, $len = 500, $type = 'ajaxFormText')
	{
		switch ( $type ) {
			case 'html':
				$text = self::textOperations($text, $len, true, '', false, false, false, true, '', false);
				break;
			case 'simpleTitle':
				$text = self::textOperations($text, $len, false, '...', true, true, true, true, 'simpleText', false);
				break;
			case 'simpleText':
				$text = self::textOperations($text, $len, true, '...', true, true, true, true, 'simpleText', false);
				break;
			case 'urlTitle':
				$text = self::textOperations($text, $len, true, '', false, false, true, true, 'urlTitle', false);
				break;
			case 'email':
				$text = self::textOperations($text, $len, true, '', false, false, true, true, 'email', false);
				break;
			case 'ajaxFormText':
				$text = self::textOperations($text, $len, false, '...', true, true, true, true, '', false);
				break;
			default:
				$text = self::textOperations($text, $len, false, '...', true, true, false, true, '', false);
				break;
		}
		return $text;
	}

}
//
