<?php
//
namespace snkeng\core\engine;

//
class imgResizer
{
	public $image;
	public $image_type;
	public $imageForm;            // 0 = cuadrada, 1 = ancha, 2 = alta
	public $imageSize;
	public $imageRatio;
	public $imageFileSize;

	//<editor-fold desc="Batch">

	//
	function batchJPG($image, $baseFile, $sizes)
	{
		if ( empty($image) ) {
			se_killWithError('No file', '');
		}
		if ( empty($baseFile) ) {
			se_killWithError('Base File', '');
		}
		if ( empty($sizes) ) {
			se_killWithError('Sizes not available', '');
		}

		$results = [];
		$fLoc = $_SERVER['DOCUMENT_ROOT'] . $baseFile;
		//
		foreach ( $sizes as $cSize ) {
			// Ajustar variables
			$newFullName = $fLoc . $cSize['suffix'] . '.jpg';
			$cFunc = (empty($cSize['op'])) ? 'none' : $cSize['op'];
			// No modification
			if ( $cFunc === 'orig' ) {
				if ( !copy($image, $newFullName) ) {
					die("Error copying the image");
				}
				$cDim = getimagesize($image);
				$fileSize = round(filesize($image) / 1024, 2);

				$results[] = [
					'fSize' => $fileSize,
					'w' => $cDim[0],
					'h' => $cDim[1]
				];
			} else {
				// quality adjustment
				$cQuality = (empty($cSize['quality'])) ? 65 : intval($cSize['quality']);

				// Cargar original
				$this->load($image);
				if ( $cFunc !== 'none' ) {
					$this->$cFunc($cSize['width'], $cSize['heigth']);
				}
				$this->save($newFullName, IMAGETYPE_JPEG, $cQuality);
				//
				$results[] = [
					'fSize' => $this->imageFileSize,
					'w' => $this->imageSize[0],
					'h' => $this->imageSize[1]
				];
			}


		}
		//
		return $results;
	}

	//
	function batchProcess($file, $folder, $fileName, $sizes)
	{
		// Nombre original
		$baseName = substr($fileName, 0, strrpos($fileName, '.'));
		if ( !file_exists($file) ) {
			se_killWithError('Archivo no existente');
		}
		foreach ( $sizes as $cSize ) {
			// Ajustar variables
			$newFullName = $_SERVER['DOCUMENT_ROOT'] . $folder . $baseName . $cSize['name'] . '.jpg';
			$cSize['width'] = intval($cSize['width']);
			$cSize['heigth'] = intval($cSize['heigth']);
			$cFunc = (empty($cSize['func'])) ? 'maxResize' : $cSize['func'];
			// Cargar original
			$this->load($file);
			$this->$cFunc($cSize['width'], $cSize['heigth']);
			$this->save($newFullName, IMAGETYPE_JPEG, 60);
		}
	}

	//
	function batchDelete($baseFile)
	{
		$delFiles = '';
		$count = 0;
		$cFiles = glob($_SERVER['DOCUMENT_ROOT'] . $baseFile . "*");
		foreach ( $cFiles as $key => $file ) {
			unlink($file);
			$delFiles .= $file . "\n";
			$count++;
		}

	}
	//</editor-fold>

	//<editor-fold desc="File Ops">
	//
	function load($fileName)
	{
		if ( empty($fileName) ) {
			die("imgResizer - ERRROR: Nombre no recibido.");
		}

		if ( !file_exists($fileName) ) {
			die("imgResizer - ERRROR:\n\nArchivo no encontrado: '{$fileName}'");
		}

		$image_info = getimagesize($fileName);
		$this->image_type = $image_info[2];
		if ( $this->image_type == IMAGETYPE_JPEG ) {
			$this->image = imagecreatefromjpeg($fileName);
		} elseif ( $this->image_type == IMAGETYPE_GIF ) {
			$this->image = imagecreatefromgif($fileName);
		} elseif ( $this->image_type == IMAGETYPE_PNG ) {
			$this->image = imagecreatefrompng($fileName);
		} elseif ( $this->image_type == IMAGETYPE_WEBP ) {
			$this->image = imagecreatefromwebp($fileName);
		}

		// Ver proporción
		$this->getImageSize();
	}

	//
	function loadOnline($url)
	{
		$tmpfname = tempnam("/tmp", "UL_IMAGE");
		$img = file_get_contents($url);
		file_put_contents($tmpfname, $img);

		$image_info = getimagesize($tmpfname);
		$this->image_type = $image_info[2];
		if ( $this->image_type == IMAGETYPE_JPEG ) {
			$this->image = imagecreatefromjpeg($tmpfname);
		} elseif ( $this->image_type == IMAGETYPE_GIF ) {
			$this->image = imagecreatefromgif($tmpfname);
		} elseif ( $this->image_type == IMAGETYPE_PNG ) {
			$this->image = imagecreatefrompng($tmpfname);
		} elseif ( $this->image_type == IMAGETYPE_WEBP ) {
			$this->image = imagecreatefromwebp($tmpfname);
		}
		// Ver proporción
		$this->getImageSize();
	}

	//

	/**
	 * Save internal image
	 *
	 * @param string $fileName The file name
	 * @param int $image_type Image type
	 * @param int|null $quality Image quality, null gives de default per format
	 * @param string|null $permissions
	 *
	 * @return never
	 */
	function save(string $fileName, int $image_type = IMAGETYPE_JPEG, int $quality = null, string $permissions = null)
	{
		if ( $image_type == IMAGETYPE_JPEG ) {
			// Progressive JPEG
			imageinterlace($this->image, true);
			imagejpeg($this->image, $fileName, $quality);
		} elseif ( $image_type == IMAGETYPE_GIF ) {
			imagegif($this->image, $fileName);
		} elseif ( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->image, $fileName);
		} elseif ( $image_type == IMAGETYPE_WEBP ) {
			imagewebp($this->image, $fileName, $quality);
		} elseif ( $image_type == IMAGETYPE_AVIF ) {
			imageavif($this->image, $fileName, $quality);
		} else {
			die("image format not supported.");
		}
		// Guardar propiedades
		$this->imageFileSize = round(filesize($fileName) / 1024, 2);
		$this->getImageSize();
		// 
		if ( $permissions != null ) {
			chmod($fileName, $permissions);
		}
	}

	//
	function output($image_type = IMAGETYPE_JPEG)
	{
		if ( $image_type == IMAGETYPE_JPEG ) {
			imagejpeg($this->image);
		} elseif ( $image_type == IMAGETYPE_GIF ) {
			imagegif($this->image);
		} elseif ( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->image);
		} elseif ( $image_type == IMAGETYPE_WEBP ) {
			imagewebp($this->image);
		} elseif ( $image_type == IMAGETYPE_AVIF ) {
			imageavif($this->image);
		} else {
			die("image format not supported.");
		}
	}
	//</editor-fold>

	//<editor-fold desc="Properties">
	//
	function getWidth()
	{
		return imagesx($this->image);
	}

	//
	function getHeight()
	{
		return imagesy($this->image);
	}

	// 0 = x, 1 = y
	function getImageSize()
	{
		$fDim = [
			0 => imagesx($this->image),
			1 => imagesy($this->image),
		];

		//
		if ( $fDim[0] === $fDim[1] ) {
			$this->imageForm = 0;
		} else {
			$this->imageForm = ($fDim[0] > $fDim[1]) ? 1 : 2;
		}
		$this->imageSize = $fDim;
		$this->imageRatio = $this->imageSize[0] / $this->imageSize[1];
	}
	//</editor-fold>

	//<editor-fold desc="Basic Ops">
	//
	function resizeToHeight($height)
	{
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width, $height);
	}

	//
	function resizeToWidth($width)
	{
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width, $height);
	}

	//
	function scale($scale)
	{
		$width = $this->getWidth() * $scale / 100;
		$height = $this->getheight() * $scale / 100;
		$this->resize($width, $height);
	}

	//
	function resize($width, $height)
	{
		$width = intval($width);
		$height = intval($height);
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
		$this->getImageSize();
	}

	// Corta una porción de la imagen y crea una imagen de dicho tamaño
	function crop($oWidth, $oHeight, $oX, $oY, $dWidth, $dHeight)
	{
		$new_image = imagecreatetruecolor($dWidth, $dHeight);
		imagecopyresampled($new_image, $this->image, 0, 0, $oX, $oY, $dWidth, $dHeight, $oWidth, $oHeight);
		$this->image = $new_image;
		$this->getImageSize();
	}
	//</editor-fold>

	//<editor-fold desc="Complex Ops">
	// Limita el tamaño de la imágen según los máximos impuestos (no superara ningún valor)
	public function maxResize($width = 0, $height = 0)
	{
		if ( $width !== 0 && $height !== 0 ) {
			// Ver si es necesario reducir tamaño por los coefs
			$xCoef = $this->imageSize[0] / $width;
			$yCoef = $this->imageSize[1] / $height;
			if ( $xCoef > 1 && $yCoef > 1 ) {
				if ( $xCoef > $yCoef ) {
					$this->resizeToWidth($width);
				} else {
					$this->resizeToHeight($height);
				}
			} elseif ( $xCoef > 1 && $yCoef <= 1 ) {
				$this->resizeToWidth($width);
			} elseif ( $xCoef <= 1 && $yCoef > 1 ) {
				$this->resizeToHeight($height);
			}
			// Si los coeficientes no son positivos no hacer operación
		} elseif ( $width !== 0 && $height === 0 ) {
			$xCoef = $this->imageSize[0] / $width;
			if ( $xCoef > 1 ) {
				$this->resizeToWidth($width);
			}
		} elseif ( $width === 0 && $height !== 0 ) {
			$yCoef = $this->imageSize[1] / $height;
			if ( $yCoef > 1 ) {
				$this->resizeToHeight($height);
			}
		}
	}

	// Reduce el lado más pequeño, sirve para hacer recortes especiales
	public function minResize($width, $height)
	{
		// Ver si es necesario reducir tamaño por los coefs
		$xCoef = $this->imageSize[0] / $width;
		$yCoef = $this->imageSize[1] / $height;
		if ( $xCoef > 1 && $yCoef > 1 ) {
			if ( $xCoef > $yCoef ) {
				$this->resizeToHeight($height);
			} else {
				$this->resizeToWidth($width);
			}
		}
		// Si los coeficientes no son positivos no hacer operación
	}

	// Primero reduce el tamaño al tamaño que mejor se ajuste y luego lo corta si la proporción no es la adecuada
	public function resizeCrop($width, $height, $color = '000')
	{
		// Ajuste para que redusca en proporción al tamaño más grande
		$this->minResize($width, $height);

		// Asignación de variables
		$oImg_x_printIni = 0;
		$oImg_y_printIni = 0;
		$oImg_x_readIni = 0;
		$oImg_y_readIni = 0;
		$oImg_x_scale = $this->imageSize[0];
		$oImg_y_scale = $this->imageSize[1];

		$xSize = $this->imageSize[0];
		$ySize = $this->imageSize[1];
		// Revisar ancho
		/*
		if ( $width < $xSize )
		{
			$oImg_x_printIni = ($width - $xSize) / 2;
		} elseif ( $width < $xSize ) {
			$oImg_x_readIni = ($width - $xSize) / 2;
		}
		if ( $height > $ySize )
		{
			$oImg_y_printIni = ($height - $ySize) / 2;
		} elseif ( $height < $ySize ) {
			$oImg_y_readIni = -(($height - $ySize) / 2);
		}
		*/
		if ( $width < $xSize ) {
			$oImg_x_printIni = -(($xSize - $width) / 2);
		}
		if ( $height < $ySize ) {
			$oImg_y_printIni = -(($ySize - $height) / 2);
		}

		// Test mod
		// $oImg_x_printIni = 10;
		// $oImg_y_printIni = 10;
		// $oImg_x_readIni = 20;
		// $oImg_y_readIni = 20;
		// $oImg_x_scale = $width;
		// $oImg_y_scale = $height;

		$this->imageOp($width, $height, $oImg_x_printIni, $oImg_y_printIni, $oImg_x_readIni, $oImg_y_readIni, $oImg_x_scale, $oImg_y_scale, $color);
	}

	// Reduce imagen y asegura de que siempre exista contenido (no hay bordes)
	public function smartThumb($width, $height, $ratio = '')
	{
		//
		$maxResize = true;
		$minResize = false;

		//
		if ( !empty($ratio) ) {
			//
			$tempRatio = explode(",", $ratio, 2);
			$ratio = intval($tempRatio[0]) / intval($tempRatio[1]);
			$maxResize = false;

			//
			if ( $width === 0 ) {
				$width = round($height * $ratio);
				$minResize = true;
			} elseif ( $height === 0 ) {
				$height = round($width / $ratio);
				$minResize = true;
			} else {
				$maxResize = true;
			}

			//
			if ( $minResize ) {
				$this->minResize($width, $height);
			}
		}


		//
		if ( $maxResize ) {
			// Reduce para que el tamaño máximo de la imagen corresponda con el máximo permitido
			$this->maxResize($width, $height);
		}

		// variables iniciales
		$oImg_x_printIni = 0;
		$oImg_y_printIni = 0;
		$oImg_x_readIni = 0;
		$oImg_y_readIni = 0;
		$oImg_x_scale = $this->imageSize[0];
		$oImg_y_scale = $this->imageSize[1];

		$xSize = $this->imageSize[0];
		$ySize = $this->imageSize[1];

		// Calculo del centrado
		if ( $width > $xSize ) {
			$oImg_x_printIni = -(($xSize - $width) / 2);
		}
		if ( $height > $ySize ) {
			$oImg_y_printIni = -(($ySize - $height) / 2);
		}
		// Hacer la operación
		$this->imageOp($width, $height, $oImg_x_printIni, $oImg_y_printIni, $oImg_x_readIni, $oImg_y_readIni, $oImg_x_scale, $oImg_y_scale, '000');
	}

	// Reduce imagen y asegura de que siempre exista contenido (no hay bordes)
	public function smarterThumb($width, $height, $ratio = '')
	{
		// Maths
		$target = [
			'w' => $width,
			'h' => $height,
			'r' => $ratio
		];
		// Temporal
		$temporal = [
			'w' => 0,
			'h' => 0,
		];
		//
		$finalCrop = [
			'oX' => 0,
			'oY' => 0,
			'oW' => 0,
			'oH' => 0,
			'nW' => 0,
			'nH' => 0
		];
		// Largest
		$smartCrop = false;
		$smartCropMode = 0;
		//
		$new_image = null;

		if ( $ratio ) {
			//
			$tempRatio = explode(",", $ratio, 2);
			$target['r'] = intval($tempRatio[0]) / intval($tempRatio[1]);

			//
			if ( !$width && $height ) {
				$target['h'] = $height;
				$target['w'] = round($target['h'] * $target['r']);
				//
				$temporal['w'] = $target['w'];
				$temporal['h'] = $target['h'];

				//
				$finalCrop['oW'] = $this->imageSize[0];
				$finalCrop['oH'] = $this->imageSize[1];
				$finalCrop['nW'] = $target['w'];
				$finalCrop['nH'] = $target['h'];

			} elseif ( $width ) {
				$target['w'] = $width;
				$target['h'] = round($target['w'] / $target['r']);
				//
				$temporal['w'] = $target['w'];
				$temporal['h'] = $target['h'];

			} else {
				se_killWithError('IMG Resource, not valid.', '', 500);
			}
		} else {
			//
			if ( $width && $height ) {
				$target['r'] = $width / $height;
			} elseif ( !$width && $height ) {
				$target['r'] = $this->imageRatio;
				$target['w'] = round($target['h'] * $target['r']);
			} else {
				$target['r'] = $this->imageRatio;
				$target['h'] = round($target['w'] / $target['r']);
			}
		}

		// Ratio difference
		$ratioDiff = 2 * abs($target['r'] - $this->imageRatio) / ($target['r'] + $this->imageRatio);

		//
		if ( $ratioDiff < 0.03 ) {
			// Simple resize

			//
			$finalCrop['oX'] = 0;
			$finalCrop['oY'] = 0;
			$finalCrop['oW'] = $this->imageSize[0];
			$finalCrop['oH'] = $this->imageSize[1];
			$finalCrop['nX'] = 0;
			$finalCrop['nY'] = 0;
			$finalCrop['nW'] = $target['w'];
			$finalCrop['nH'] = $target['h'];

			//
			$new_image = imagecreatetruecolor($finalCrop['nW'], $finalCrop['nH']);
			imagecopyresampled($new_image, $this->image, $finalCrop['oX'], $finalCrop['oY'], $finalCrop['oX'], $finalCrop['oY'], $finalCrop['nW'], $finalCrop['nH'], $finalCrop['oW'], $finalCrop['oH']);
			// Reemplazar (temp)
			$this->image = $new_image;
			$this->getImageSize();
		} else {

		}

		// Determinar proporción a cortar
		if ( $smartCrop ) {
			$tempImage = imagecreatetruecolor($this->imageSize[0], $this->imageSize[1]);

			//
			imagecopy($tempImage, $this->image, 0, 0, 0, 0, $this->imageSize[0], $this->imageSize[1]);
			//
			imagefilter($tempImage, IMG_FILTER_SMOOTH, 7);
			imagefilter($tempImage, IMG_FILTER_EDGEDETECT);
		} else {

		}


	}

	//</editor-fold>

	//<editor-fold desc="Other Options">
	// Editor resumido de imagen
	public function imageOp($nImg_width, $nImg_height, $oImg_x_printIni, $oImg_y_printIni, $oImg_x_readIni, $oImg_y_readIni, $oImg_x_scale, $oImg_y_scale, $bkgColor)
	{
		// Crear imagen
		$new_image = imagecreatetruecolor($nImg_width, $nImg_height);
		// Definir Fondo
		$bkgC = $this->colorConvert($bkgColor);
		// debugVariable($bkgC);
		$bkg = imagecolorallocate($new_image, $bkgC[0], $bkgC[1], $bkgC[2]);
		imagefill($new_image, 0, 0, $bkg);
		// Copiar imagen
		imagecopyresampled($new_image, $this->image, $oImg_x_printIni, $oImg_y_printIni, $oImg_x_readIni, $oImg_y_readIni, $oImg_x_scale, $oImg_y_scale, $this->imageSize[0], $this->imageSize[1]);
		$this->image = $new_image;
		$this->getImageSize();
	}

	// Convertidor de colores
	private function colorConvert($color)
	{
		$len = strlen($color);
		$c = [];
		if ( $len === 6 ) {
			$c[0] = "0x" . substr($color, 0, 2);
			$c[1] = "0x" . substr($color, 2, 2);
			$c[2] = "0x" . substr($color, 4, 2);
		} elseif ( $len === 3 ) {
			$c[0] = "0x" . substr($color, 0, 1) . substr($color, 0, 1);
			$c[1] = "0x" . substr($color, 1, 1) . substr($color, 1, 1);
			$c[2] = "0x" . substr($color, 2, 1) . substr($color, 2, 1);
		} elseif ( $len === 1 ) {
			$c[0] = "0x$color$color";
			$c[1] = "0x$color$color";
			$c[2] = "0x$color$color";
		} else {
			die("Color no válido");
		}
		for ( $i = 0; $i < 3; $i++ ) {
			$c[$i] = hexdec($c[$i]);
		}
		return $c;
	}
	//</editor-fold>
}
