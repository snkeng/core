<?php
// Actualizar archivos
namespace snkeng\core\engine;

//
class debug
{
	//
	static function variable($var, $name='Not Defined', $kill=true, $saveToSQL = false) {
		if ( $kill ) {
			http_response_code(500);
			header('Content-type: text/plain; charset=utf-8');
		}

		$result = self::debugConvertVariable($var);

		//
		echo <<<TEXT
--- DEBUGVARIABLE ---
Name: {$name}.
Type: {$result['type']}.
Var:
--- INI ---
{$result['content']}
--- END ---
---\n\n
TEXT;
		//
		if ( $saveToSQL ) {
			//
			$report = <<<TEXT
Type: {$result['type']}.
Var:
--- INI ---
{$result['content']}
--- END ---
TEXT;

			$safeReport = \snkeng\core\engine\mysql::real_escape_string($report);

			//
			$ins_qry = <<<SQL
INSERT INTO sb_errorlog
(elog_dtadd, elog_type, elog_key, elog_content)
VALUES
(now(), 2, '{$name}', '{$safeReport}');
SQL;
			\snkeng\core\engine\mysql::submitQuery($ins_qry);
		}

		// Terminar operación?
		if ( $kill ) { exit(); }
	}

	//
	static function debugConvertVariable($var) {
		$pVar = '';
		$pType = gettype($var);

		switch ( $pType ) {
			case 'boolean':
			case 'integer':
			case 'double':
			case 'string':
				$pVar = $var;
				break;
			case 'array':
			case 'object':
			case 'resource':
				$pVar = print_r($var, true);
				break;
			case 'null':
				break;
			default:
				$pType = 'Desconocido';
				$pVar = $var."\n\n---\n\n".print_r($var, true);
				break;
		}

		//
		return [
			'type' => $pType,
			'content' => $pVar
		];
	}
}
//
