<?php
//
namespace snkeng\core\engine;

//
class restfulOps
{
	//
	public static function resourceAddSingle(array $dataStructure, array $tableStructure, array $options = []) : array
	{
		//
		$defaults = [
			'setData' => [],
			'opsData' => null,
			'readData' => [],
			'onFail' => null
		];
		$settings = array_merge($defaults, $options);

		//
		$rData = processUserInput::processRowRequest($dataStructure);

		// Add defaults if any
		if ( !empty($settings['setData']) ) {
			$rData = array_merge($rData, $settings['setData']);
		}

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			$settings['opsData']($rData);
		}

		//
		return saveDataToDataBase::addSingle($rData, $tableStructure, $settings['onFail']);
	}

	//
	public static function resourceAddMultiple(array $dataStructure, array $tableStructure, array $options = []) : array
	{
		//
		$defaults = [
			'setData' => [],
			'opsData' => null,
			'readData' => []
		];
		$settings = array_merge($defaults, $options);


		//
		$rData = processUserInput::processMultiRequest($dataStructure);


		// Add defaults if any
		if ( !empty($settings['setData']) ) {
			$rData = array_merge($rData, $settings['setData']);
		}

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			$settings['opsData']($rData);
		}

		//
		$rData = saveDataToDataBase::addMany($rData, $tableStructure);

		//
		return $rData;

	}


	/**
	 * Combines validation and updating operation.
	 *
	 * @param array $postStructure Structure of elements to be filtered and processed
	 * @param array $sqlStructure  SQL Structure for update
	 * @param array $params        Aditional optinal parameters
	 */
	public static function resourceUpdateSingle(array $postStructure, array $sqlStructure, array $params = []) : array
	{
		//
		$defaults = [
			'setData' => [],
			'opsData' => null,
			'returnData' => null,
			'readData' => [],
		];
		$settings = array_merge($defaults, $params);

		//
		$resourceId = intval($sqlStructure['tId']['value']);
		if ( !$resourceId ) {
			trigger_error("Invalid ID", E_USER_ERROR);
		}

		// Post parsing
		$rData = processUserInput::processRowRequest($postStructure);

		// Add defaults
		if ( !empty($settings['setData']) ) {
			$rData = array_merge($rData, $settings['setData']);
		}

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			$settings['opsData']($rData);
		}

		// Copy final data
		$returnData = $rData;
		$returnData[$sqlStructure['tId']['nm']] = $resourceId;

		//
		saveDataToDataBase::updateSingle($rData, $sqlStructure, $resourceId);

		// Extra operations
		if ( is_callable($settings['returnData']) ) {
			$settings['returnData']($rData);
		}

		return $returnData;
	}

	// Actualizar múltiples
	public static function resourceUpdateMultiple(array $postStructure, array $sqlStructure, array $params = []) : string
	{
		//
		$defaults = [
			'opsData' => null,
			'returnData' => null,
			'readData' => [],
		];
		$settings = array_merge($defaults, $params);


		//
		$rData = processUserInput::processMultiRequest($postStructure);

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			foreach ( $rData as $id => &$cData )
			$settings['opsData']($cData);
		}

		//
		saveDataToDataBase::updateMultiple($rData, $sqlStructure);

		// Extra operations
		if ( is_callable($settings['returnData']) ) {
			$settings['returnData']($rData);
		}

		return 'ok';
	}

	// Borrar
	public static function resourceDeleteSingle(array $sqlStructure, callable | null $callback = null) : void
	{
		//
		$resourceId = intval($sqlStructure['tId']['value']);
		if ( !$resourceId ) {
			trigger_error("Invalid ID", E_USER_ERROR);
		}

		//
		saveDataToDataBase::deleteSingle($sqlStructure, $resourceId);

		// Callback
		if ( is_callable($callback) ) {
			$callback();
		}
	}

	// Leer uno
	public static function resourceReadSingle(array $sqlStructure) : array
	{
		$resourceId = intval($sqlStructure['tId']['value']);
		if ( !$resourceId ) {
			trigger_error("Invalid ID", E_USER_ERROR);
		}

		//
		if ( isset($sqlStructure['pId']) ) {
			$pId = intval($sqlStructure['pId']['value']);

			// First validation
			if ( empty($pId) ) {
				trigger_error("", E_USER_ERROR);
			}
		}

		//
		//
		$sql_qry = "SELECT\n{$sqlStructure['tId']['db']} AS {$sqlStructure['tId']['nm']}";
		foreach ( $sqlStructure['elems'] as $dbName => $vName ) {
			$sql_qry .= ", {$dbName} AS {$vName}";
		}
		$sql_qry .= "\nFROM {$sqlStructure['tName']}\nWHERE {$sqlStructure['tId']['db']}='{$resourceId}'\nLIMIT 1;";

		//
		$toSimple = (isset($sqlStructure['toSimple'])) ? $sqlStructure['toSimple'] : [];

		//
		$data = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			$toSimple,
			[
				'errorKey' => 'tableReadRow',
				'errorDesc' => 'No fue posible leer la información.'
			]
		);

		//
		if ( isset($sqlStructure['pId']) ) {
			$data['pId'] = 1;
		}

		return $data;
	}

}
