<?php
namespace snkeng\core\general;
//
class form
{
	public static function create($formData, $values = []) {
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/variable/regexp.php';

		//
		$defaults = [
			'method'=>'post',
			'action' => '',
			'class' => 'se_form',
			'enctype' => 'multipart/form-data',
			'actName' => 'Guardar',
			'actIcon' => '#fa-save',
			'se_plugin' => 'simpleForm'
		];
		$response = ['html' => ''];
		$elements = '';

		$settings = array_merge($defaults, $formData['settings']);

		// Procesar
		foreach ( $formData['elems'] as $elem )
		{
			// Obtener valores
			if ( !empty($values) ) {
				$elem['value'] = ( isset($values[$elem['name']]) ) ? $values[$elem['name']] : '';
			} else {
				$elem['value'] = '';
			}

			// crear elementos
			$cElem = '';
			if ( $elem['type'] === 'hidden' )
			{
				$cElem.= "<input type='hidden' name='{$elem['name']}' value='{$elem['value']}' />\n";
			} else {
				$isRequired = ($elem['required']) ? ' required' : '';
				$dataBox = "<div class='cont'><span class='title'>{$elem['title']}</span><span class='desc'>{$elem['desc']}</span></div>";
				//
				$xClass = ( isset($elem['xClass']) ) ? ' '.$elem['xClass'] : '';
				$cElem.= "<div class='separator{$isRequired}$xClass'>\n";
				//
				$minLength = ( isset($elem['min']) ) ? " minlength='{$elem['min']}'" : '';
				$maxLength = ( isset($elem['max']) ) ? " maxlength='{$elem['max']}'" : '';
				//
				switch ( $elem['type'] ) {
					case 'text':
						$pattern = '';
						if ( !empty($elem['regexp']) )
						{
							$pattern.= ' pattern="'.$regexpVarsSimple[$elem['regexp']][0];
							if ( $elem['min'] )
							{
								$pattern.= '{'.$elem['min'].',';
								$pattern.= ($elem['max']) ? $elem['max'] : '';
								$pattern.= '}';
							} else {
								$pattern.= '*';
							}
							$pattern.= '"';
							$minLength = '';
							$maxLength = '';
						}
						$cElem.= "<label>\n{$dataBox}\n<input type='text' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$pattern}{$minLength}{$maxLength} />\n</label>";
						break;
					case 'password':
						$cElem.= "<label>\n{$dataBox}\n<input type='password' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$minLength}{$maxLength} />\n</label>";
						break;
					case 'search':
						$cElem.= "<label>\n{$dataBox}\n<input type='search' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$minLength}{$maxLength} />\n</label>";
						break;
					case 'tel':
						$cElem.= "<label>\n{$dataBox}\n<input type='tel' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$minLength}{$maxLength} />\n</label>";
						break;
					case 'email':
						$cElem.= "<label>\n{$dataBox}\n<input type='email' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$minLength}{$maxLength} pattern='{$regexpVarsSimple['email'][0]}' />\n</label>";
						break;
					//
					case 'number':
						$minText = ( isset($elem['vMin']) && strlen($elem['vMin']) > 0 ) ? " min='{$elem['vMin']}'" : '';
						$maxText = ( isset($elem['vMax']) && strlen($elem['vMax']) > 0 ) ? " max='{$elem['vMax']}'" : '';
						$numStep = ( isset($elem['step']) && strlen($elem['step']) > 0 ) ? " step='{$elem['step']}'" : '';
						$cElem.= "<label>\n{$dataBox}\n<input type='number' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} pattern='{$regexpVarsSimple['number'][0]}'{$minText}{$maxText}{$numStep} />\n</label>";
						break;
					//
					case 'datetime':
						$cElem.= "<label>\n{$dataBox}\n<div class='modInput'><input type='datetime' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} pattern='{$regexpVarsSimple['datetime'][0]}' /><button type='button' se-plugin='calendar'><i class='fa fa-calendar'></i></button></div>\n</label>";
						break;
					case 'datetime-local':
						$cElem.= "<label>\n{$dataBox}\n<div class='modInput'><input type='datetime-local' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} pattern='{$regexpVarsSimple['datetime-local'][0]}' /><button type='button' se-plugin='calendar'><i class='fa fa-calendar'></i></button></div>\n</label>";
						break;
					//
					case 'color':
						$cElem.= "<label>\n{$dataBox}\n<input type='color' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} pattern='{$regexpVarsSimple['color'][0]}' />\n</label>";
						break;
					case 'range':
						$cElem.= "<label>\n{$dataBox}\n<input type='range' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} min='{$elem['min']}' max='{$elem['max']}' step='{$elem['step']}' />\n</label>";
						break;
					case 'url':
						$cElem.= "<label>\n{$dataBox}\n<input type='url' name='{$elem['name']}' value='{$elem['value']}'{$isRequired}{$minLength}{$maxLength} pattern='{$regexpVarsSimple['url'][0]}' />\n</label>";
						break;
					//
					case 'file':
						$cElem.= "<label>\n{$dataBox}\n<input type='file' name='{$elem['name']}' value='{$elem['value']}'{$isRequired} pattern='{$regexpVarsSimple['color'][0]}' />\n</label>";
						break;
					//
					case 'textarea':
						$cElem.= "<label>\n{$dataBox}\n<textarea se-plugin='autoSize' name='{$elem['name']}'{$isRequired}{$minLength}{$maxLength}>{$elem['value']}</textarea>\n</label>";
						break;
					case 'textareaesp':
						$cElem.= "<label>\n{$dataBox}\n<textarea se-plugin='wysiwyg' name='{$elem['name']}'>{$elem['value']}</textarea>\n</label>";
						break;
					//
					case 'checkbox':
						$isChecked = ( $elem['value'] === '1' ) ? 'checked' : '';
						$cElem.= "<label class='checkbox'>\n<input type='checkbox' name='{$elem['name']}' value='1' {$isChecked}/>\n{$dataBox}\n</label>";
						break;
					case 'radio':
						$options = '';
						foreach ( $elem['opts'] as $value => $name )
						{
							$isChecked = ( $elem['value'] == $value ) ? " checked='checked'" : '';
							$options.= "<label class='radio'><input type='radio' name='{$elem['name']}' value='{$value}'{$isRequired}{$isChecked} /> {$name}</label>";
						}
						$cElem.= "<div class='cont'>{$dataBox}<div>{$options}</div></div>";
						break;
					//
					case 'list':
					case 'menu':
						$options = '';
						$size = '';
						if ( $elem['type'] === 'list' )
						{
							$options.= '<option value="">- -</option>';
						} else {
							$size = ' multiple="5"';
						}
						foreach ( $elem['opts'] as $value => $name )
						{
							$options.= "<option value=\"{$value}\" ";
							if ( $elem['value'] == $value ) { $options.= 'selected="selected" '; }
							$options.= ">{$name}</option>";
						}
						$cElem.= "<label>\n{$dataBox}\n<select name='{$elem['name']}'{$size}>\n{$options}</select>\n</label>";
						break;
				}
				$cElem.= "\n</div>\n";

			}

			$elements.= $cElem;
		}

		// Plugin check
		$sePlugin = ( !empty($settings['se_plugin']) ) ? 'se-plugin="'.$settings['se_plugin'].'"' : '';

		// debugVariable($settings);
		$icon = ( isset($settings['actIcon']) ) ? "<svg class=\"icon inline mr\"><use xlink:href=\"{$settings['actIcon']}\" /></svg>" : '';

		$response['html'] = <<<HTML
<!-- INI:Form -->
<form class="{$settings['class']}" method="{$settings['method']}" action="{$settings['action']}" enctype="{$settings['enctype']}" {$sePlugin}>
{$elements}
<button type="submit">{$icon}{$settings['actName']}</button>
<output se-elem="response"></output>
</form>
<!-- END:Form -->\n
HTML;

		return $response;
	}
}
//
