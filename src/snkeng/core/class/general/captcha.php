<?php
//
namespace snkeng\core\general;


//
class captcha
{
	//
	// Variables
	private $image;                     // Nombre y dirección del archivo
	public $captchaText;                // Texto a mostrar en el captcha
	private $keyText;                   // String origen del captcha
	//

	//
	// INIT: Construct
	public function __construct() {}

	//
	// Funciones
	//

	//
	public function load() {
		global $page, $siteVars;

		// Select method
		if ( !$_ENV['SE_DEBUG'] && !empty($_ENV['GOOGLE_CAPTCHA_KEY']) ) {
			// Google

			//
			$page['files']['js'] = ['https://www.google.com/recaptcha/api.js?render=explicit&hl=es-419&onload=captchaLoad'];
			//
			$page['js'] .= <<<JS
function captchaLoad() { grecaptcha.render('g_captcha', {'sitekey':'{$_ENV['GOOGLE_CAPTCHA_KEY']}'}); }\n
JS;
			//
			$page['mt'] .= <<<JS
if( window.grecaptcha ) { captchaLoad(); }
JS;
			//
		}
		else {
			\snkeng\core\engine\nav::pageFileGroupAdd(['google_captcha']);
			//
			$page['js'] .= <<<JS
function captchaLoad() { se.plugin.captcha(); }\n
JS;
			//
			$page['mt'] .= <<<JS
if ( window.se.plugin.captcha ) { let temp = se.plugin.captcha(); }
JS;
			//
		}
		//
	}

	//
	public function request() {
		$this->keyCreate();
		$this->keyHashing();
		$this->createImage();
		exit();
	}

	//
	public function verify() {
		global $siteVars;

		// Select method
		if ( !$_ENV['SE_DEBUG'] && !empty($_ENV['GOOGLE_CAPTCHA_KEY']) ) {
			// Google
			$url = 'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s';
			$url = sprintf($url, $_ENV['GOOGLE_CAPTCHA_SECRET'], $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			$status = file_get_contents($url);
			$r = \json_decode($status);
			return ( isset($r->success) && $r->success );
		}
		else {
			// Local
			$eval = preg_replace('/[^A-Za-z0-9]/', '', $_POST['se-captcha-response']);

			//
			if ( !isset($_SESSION['captchakey']) || empty($eval) ) {
				return false;
			}

			// Revisar si ya se ha generado llave
			$this->keyText = $_SESSION['captchakey'];
			$this->keyHashing();
			return $this->keyCheck($eval);
		}
	}

	//
	public function keyCreate()
	{
		$this->keyText = md5(uniqid(rand(), true));
		$this->keyText = strtoupper(substr(str_shuffle(preg_replace('/[^A-Za-z0-9]/', '', base64_encode(uniqid(rand(), true)))), 0, 32));
		$_SESSION['captchakey'] = $this->keyText;
	}
	//

	//
	//
	public function keyCheck($text)
	{
		$text = strtoupper($text);
		return ( $text === $this->captchaText );
	}
	//

	//
	//
	public function keyHashing()
	{
		$coded = sha1("AyKlu2m2q" . $this->keyText . "Pp1k3mvqbb");
		$this->captchaText = strtoupper(substr(preg_replace('/[^A-Za-z0-9]/', '', base64_encode($coded)), 0, 6));
	}
	//

	//
	//
	public function createImage()
	{
		$xsize = 180;
		$ysize = 60;
		$im = @imagecreate($xsize, $ysize) or die("Cannot Initialize new GD image stream");

		// Definir opciones
		$a = rand(150, 255);
		$b = rand(0, 255);
		$c = rand(0, 150);

		// Definir colores
		$color1 = imagecolorallocate($im, $a, $b, $c);
		$color2 = imagecolorallocate($im, $b, $c, $a);
		$color3 = imagecolorallocate($im, $c, $a, $b);

		// Imprimir texto
		$font = $_SERVER['DOCUMENT_ROOT'] . "/snkeng/core/res/fonts/Gluten-Regular.ttf";
		$letters = str_split($this->captchaText);
		$xLet = 10;
		$yLet = 40;
		if ( !file_exists($font) ) {
			echo("ERROR: font no encontrado. <br />\n$font<br />\n");
			exit();
		}

		//
		foreach ( $letters as $letter ) {
			$xRand = rand(-3, 3);
			$yRand = rand(-3, 3);
			imagettftext($im, 30, rand(-35, 35), $xLet + $xRand, $yLet + $yRand, $color3, $font, $letter);
			$xLet += 28;
		}

		// Hacer Grid Vertical
		for ( $y = 5; $y < $ysize; $y = $y + 20 ) {
			imageline($im, 0, $y, $xsize, $y, $color2);
		}
		// Hacer Grid Horizontal
		for ( $x = 5; $x < $xsize; $x = $x + 20 ) {
			imageline($im, $x, 0, $x, $ysize, $color2);
		}
		// Hacer Líneas random
		for ( $x = 0; $x < 20; $x++ ) {
			imageline($im, rand(0, $xsize), rand(0, $ysize), rand(0, $xsize), rand(0, $ysize), $color3);
		}

		// Generar imagen y eliminar
		// imagepng($im, $_SERVER['DOCUMENT_ROOT'] . "/image.png");
		// imagedestroy($im);


		header('Content-type: image/png');
		imagepng($im);
		imagedestroy($im);
	}
	//
}
// Fin clase
