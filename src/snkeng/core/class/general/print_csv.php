<?php
namespace snkeng\core\general;
//
class print_csv
{
	public static function createCSV($fileName, $headers, $sql_qry, $operation) {
				// método manual ya que no es completamente automatizado
		//	Definir archivo
		if ( isset($_REQUEST['debug']) && $_REQUEST['debug'] )
		{
			header('Content-Type: text/plain; charset=utf-8');
			echo("Nombre del archivo: {$fileName}\n");
		} else {
			header('Content-Encoding: UTF-8');
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="'.$fileName.'.csv"');
			// Excel BOM Patch
			echo("\xEF\xBB\xBF");
		}

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// Headers
		fputcsv($output, $headers);

		// Hacer el query de datos
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			// Cargar los elementos del array
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
				fputcsv($output, $operation($datos));
			}
		}
		\snkeng\core\engine\mysql::killIfError('classCSVPrintQry', 'Unable to obtain information for table.');

		fclose($output);
		exit;
	}
}
//
