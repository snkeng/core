<?php
namespace snkeng\core\general;

//
class mail
{
	//
	public static function sendText(string $to, string $title, string $message, array $extraInfo = []) {
		$headers = [];

		$headers[] = "Content-type: text/plain; charset=UTF-8";

		//
		$message = strip_tags($message);
		$message = wordwrap($message, 70);

		// Debug message is same as message
		$debugMessage = $message;

		//
		$headersTxt = self::headerProcess($headers, $extraInfo);

		// Edit Message
		$debugMessage = <<<EOD
LOS DATOS A ENVIAR SON LOS SIGUIENTES:
Para:	{$to}
Título:	{$title}
Headers
--
{$headersTxt}
--

Mensaje:
---
{$debugMessage}
---
EOD;
		//

		//
		$response = self::mailSend($to, $title, $message, $headersTxt, $debugMessage);

		//
		return $response;
	}

	//
	public static function sendHTML(string $to, string $title, string $message, string $template = '', array $extraInfo = []) {
		$headers = [];

		// Tipo de mail
		if ( empty($template) || !file_exists($_SERVER['DOCUMENT_ROOT'] . $template) ) {
			$headers[] = "Content-type: text/plain; charset=UTF-8";
			$message = strip_tags($message);
			$message = wordwrap($message, 70);
			// Debug message is same as message
			$debugMessage = $message;
		} else {
			$headers[] = "MIME-Version: 1.0";
			$headers[] = "Content-type: text/html; charset=UTF-8";

			// Debug message is before appending
			$debugMessage = $message;

			$htmlText = ( require $_SERVER['DOCUMENT_ROOT'] . $template );

			// Append message to tempalte
			$message = stringPopulate(
				$htmlText,
				[
					'message' => $message,
					'prehead' => ( isset($extraInfo['preHead']) ) ? $extraInfo['preHead'] : ''
				]
			);
		}

		//
		$headersTxt = self::headerProcess($headers, $extraInfo);

		// Edit Message
		$debugMessage = <<<EOD
LOS DATOS A ENVIAR SON LOS SIGUIENTES:
Para:	{$to}
Título:	{$title}
Template: {$template}
--
Headers
--
{$headersTxt}
--

Mensaje:
---
{$debugMessage}
---
EOD;
		//

		//
		$response = self::mailSend($to, $title, $message, $headersTxt, $debugMessage);

		//
		return $response;
	}

	//
	private static function headerProcess($headers, $extraInfo) {
		global $siteVars;

		if ( !$siteVars['server']['local'] ) {
			//
			$headers[] = "From: {$siteVars['site']['name']} <no-reply@{$siteVars['site']['domain']}>";
			$headers[] = "Reply-To: {$siteVars['site']['name']} <no-reply@{$siteVars['site']['domain']}>";
		} else {
			$headers[] = "From: {$siteVars['site']['name']} <no-reply@{$_SERVER['SERVER_NAME']}>";
			$headers[] = "Reply-To: {$siteVars['site']['name']} <no-reply@{$_SERVER['SERVER_NAME']}>";
		}
		$headers[] = 'X-Mailer: PHP/'.phpversion();

		//
		if ( isset($extraInfo['replyTo']) ) {
			$headers[] = "Reply-To: {$extraInfo['replyTo']}";
		}

		//
		return implode("\r\n", $headers);
	}

	//
	private static function mailSend($to, $title, $message, $headersTxt, $debugMessage)
	{
		global $siteVars;

		$response = [
			'debug' => [
				'mail' => $debugMessage
			],
			's' => [
				't' => 1
			]
		];

		// Analizar si es posible enviar correo electrónico
		if ( !$siteVars['server']['local'] ) {
			if ( !mail($to, $title, $message, $headersTxt) ) {
				$response['s']['t'] = 0;
				$response['s']['e'] = 'No fue posible enviar correo electrónico.';
				return $response;
			}
		} else {
			mail($to, $title, $message, $headersTxt);
			// Save Message
			$fileName = 'mail_'.time();
			$myFile = fopen($_SERVER['DOCUMENT_ROOT']."/se_files/user_upload/mails/{$fileName}.txt", "w");
			fwrite($myFile, $debugMessage);
			fclose($myFile);
		}

		return $response;
	}
}
//
