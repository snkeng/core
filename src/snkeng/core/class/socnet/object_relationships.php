<?php
//
namespace snkeng\core\socnet;

// Descargas
class object_relationships
{
	//
	public static function getObjData($objIdTarget)
	{
		
		$sql_qry = <<<SQL
SELECT
	obj.a_count_members AS members, obj.a_count_subscribers AS subscribers, obj.a_count_commends AS commends
FROM sb_objects_obj AS obj
WHERE obj.a_id='{$objIdTarget}'
LIMIT 1;
SQL;
		$objData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['members', 'subscribers', 'commends']
			],
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible obtener la información del objeto.'
			]
		);

		return $objData;
	}

	//
	public static function getRelationship($objIdTarget, $objIdFrom)
	{
		
		//
		$sql_qry = <<<SQL
SELECT
    rel.orel_id AS id,
	rel.orel_status AS status, rel.orel_blocked AS blocked, rel.orel_commend AS commended,
    rel.orel_subscribed AS subscribed,
    rel.orel_admin_level AS admLevel, rel.orel_admin_type AS admType
FROM sb_objects_relationships AS rel
WHERE rel.obj_id_target='{$objIdTarget}' AND rel.obj_id_from='{$objIdFrom}'
LIMIT 1;
SQL;
		$response = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id', 'status', 'blocked', 'commended', 'subscribed', 'admLevel']
		]);

		//
		if ( empty($response) === 0 )
		{
			//
			$response =	[
				'id' => 0,
				'status' => 0,
				'blocked' => 0,
				'commended' => 0,
				'subscribed' => 0,
				'admLevel' => 0,
				'admType' => ''
			];

			//
			$ins_qry = <<<SQL
INSERT sb_objects_relationships (
	obj_id_target, obj_id_from,
	orel_status, orel_blocked, orel_commend, orel_subscribed, orel_admin_level, orel_admin_type
) VALUES (
    {$objIdTarget}, {$objIdFrom},
    0, 0, 0, 0, 0, ''
);
SQL;
			//
			\snkeng\core\engine\mysql::submitQuery($ins_qry, [
				'errorKey' => 'classObjRelAddRel'
			]);

			//
			$response['id'] = \snkeng\core\engine\mysql::getLastId();
		}

		//
		return $response;
	}

	//
	public static function setRelationship($userId, $objId, $relType)
	{
		
		//
		$response = [];
		$error = '';
		$upd_qry = "";
		$curRel = self::getRelationship($objId, $userId);
		//
		$setRel = 0;
		$setSubscription = 0;
		$setBlocked = 0;
		//
		switch ( $relType )
		{
			// Algún nivel
			case 1:
			case 2:
			case 3:
				$setRel = $relType;
				$setSubscription = 1;
				if ( $curRel['status'] === 0 )
				{
					$upd_qry.= "UPDATE sb_objects_obj SET a_count_members=a_count_members+1 WHERE a_id={$objId} LIMIT 1;";
				}
				if ( $curRel['subscribed'] === 0 )
				{
					$upd_qry.= "UPDATE sb_objects_obj SET a_count_subscribers=a_count_subscribers+1 WHERE a_id={$objId} LIMIT 1;";
				}
				break;
			// Remover
			case 0:
				if ( $curRel['status'] !== 0 ) {
					$upd_qry.= "UPDATE sb_objects_obj SET a_count_members=a_count_members-1 WHERE a_id={$objId} LIMIT 1;";
				}
				if ( $curRel['subscribed'] !== 0 ) {
					$upd_qry .= "UPDATE sb_objects_obj SET a_count_subscribers=a_count_subscribers-1 WHERE a_id={$objId} LIMIT 1;";
				}
				break;
			case 100:
				$setBlocked = 1;
				if ( $curRel['status'] !== 0 ) {
					$upd_qry .= "UPDATE sb_objects_obj SET a_count_members=a_count_members-1 WHERE a_id={$objId} LIMIT 1;";
				}
				if ( $curRel['subscribed'] !== 0 ) {
					$upd_qry .= "UPDATE sb_objects_obj SET a_count_subscribers=a_count_subscribers-1 WHERE a_id={$objId} LIMIT 1;";
				}
				break;
		}

		$upd_qry.= "UPDATE sb_objects_relationships
					SET orel_status={$setRel}, orel_subscribed={$setSubscription}, orel_blocked={$setBlocked}
					WHERE orel_id={$curRel['id']} LIMIT 1;";
		\snkeng\core\engine\mysql::submitMultiQuery($upd_qry);

		if ( empty($error) ) {
			$response['s'] = ['t' => 1];
		} else {
			$response['s'] = ['t' => 0, 'e' => $error];
		}

		return $response;
	}

	//
	public static function setSuscription($objIdTarget, $objIdFrom, $subActivate)
	{
		
		// Obtener ID Relación
		$rel = self::getRelationship($objIdTarget, $objIdFrom);
		
		if ( ( $rel['subscribed'] && !$subActivate ) || ( !$rel['subscribed'] && $subActivate ) )
		{
			if ( $subActivate )
			{
				$ins_qry = "UPDATE sb_objects_relationships SET orel_subscribed=1 WHERE orel_id={$rel['id']} LIMIT 1;
							UPDATE sb_objects_obj SET a_count_subscribers=a_count_subscribers+1 WHERE a_id={$objIdTarget} LIMIT 1;";
				$rel['subscribed'] = 1;
			} else {
				$ins_qry = "UPDATE sb_objects_relationships SET orel_subscribed=0 WHERE orel_id={$rel['id']} LIMIT 1;
							UPDATE sb_objects_obj SET a_count_subscribers=a_count_subscribers-1 WHERE a_id={$objIdTarget} LIMIT 1;";
				$rel['subscribed'] = 0;
			}
			\snkeng\core\engine\mysql::submitMultiQuery($ins_qry);
			\snkeng\core\engine\mysql::killIfError('', '');
		}
		return $rel;
	}

	//
	public static function setCommend($objIdTarget, $objIdFrom)
	{
		
		// Obtener ID Relación
		$rel = self::getRelationship($objIdTarget, $objIdFrom);

		// debugVariable($rel);
		if ( $rel['commended'] === 0 )
		{
			$ins_qry = "UPDATE sb_objects_relationships SET orel_commend=1 WHERE orel_id={$rel['id']} LIMIT 1;
						UPDATE sb_objects_obj SET a_count_commends=a_count_commends+1 WHERE a_id={$objIdTarget} LIMIT 1;";
			$rel['commended'] = 1;
		} else {
			$ins_qry = "UPDATE sb_objects_relationships SET orel_commend=0 WHERE orel_id={$rel['id']} LIMIT 1;
						UPDATE sb_objects_obj SET a_count_commends=a_count_commends-1 WHERE a_id={$objIdTarget} LIMIT 1;";
			$rel['commended'] = 0;
		}
		//
		\snkeng\core\engine\mysql::submitMultiQuery($ins_qry);
		\snkeng\core\engine\mysql::killIfError('', '');

		//
		return [
			'obj' => self::getObjData($objIdTarget),
			'rel' => $rel
		];
	}

	//
	public static function getRecommendId($objId, $userId)
	{
		
		$recData =	[
					'id'=>0,
					'rec'=>0,
					];
		//
		$sql_qry = <<<SQL
SELECT
    orel_id, orel_commend
FROM sb_objects_relationships
WHERE obj_id_target={$objId} AND obj_id_from={$userId}
LIMIT 1;
SQL;

		//
		$recSQL = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		if ( !empty($recSQL) ) {
			$recData =	[
						'id'	=> intval($recSQL['orec_id']),
						'rec'	=> intval($recSQL['orec_status']),
						];
		} else {
			$ins_qry = "INSERT INTO st_socnet_obj_rec_rel
						(a_id, a_id, orec_dtadd, orec_status)
						VALUES
						({$userId}, {$objId}, now(), 0);";
			\snkeng\core\engine\mysql::submitQuery($ins_qry);
			$recData['id'] = \snkeng\core\engine\mysql::getLastId();
		}
		return $recData;
	}

	//
	public static function getRequestId($objIdTarget, $objIdFrom)
	{
		
		$recData =	[
					'id'=>0,
					'status'=>0,
					];
		//
		$sql_qry = "SELECT ureq_id, ureq_status FROM st_socnet_requests WHERE a_id_1='$objIdTarget' AND a_id_2='$objIdFrom' LIMIT 1;";
		//
		$reqSQL = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		if ( !empty($recSQL) ) {
			$recData =	[
						'id'	=> intval($reqSQL['ureq_id']),
						'rec'	=> intval($reqSQL['ureq_status']),
						];
		} else {
			$ins_qry = "INSERT INTO st_socnet_requests
						(a_id_1, a_id_2, ureq_dtshow, ureq_status)
						VALUES
						({$objIdTarget}, {$objIdFrom}, now(), 0);";
			\snkeng\core\engine\mysql::submitQuery($ins_qry);
			\snkeng\core\engine\mysql::killIfError('', '');
			$recData['id'] = \snkeng\core\engine\mysql::getLastId();
		}
		return $recData;
	}

	//
	public static function setRequest($obj1, $obj2)
	{

	}
}
//
