<?php
//
namespace snkeng\core\socnet;

//
class object_picture
{
	//
	private static $imgFolder = '/se_files/user_upload/img/objects/';
	private static $imgFolderProc = '/se_files/site_processed/img/objects/';

	//
	private static function image_delete($fileStart) {
		// Borrar anteriores
		$mask = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder.$fileStart."*.*";
		array_map('unlink', glob($mask));
		//
		$mask = $_SERVER['DOCUMENT_ROOT'].self::$imgFolderProc.$fileStart."*.*";
		array_map('unlink', glob($mask));
	}


	//
	public static function picture_from_internet($userId, $url) {
		
		//
		$imgFolderFull = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder;


		//
		$userIdPad = str_pad($userId, 8, '0', STR_PAD_LEFT);
		$timeString = se_timeString();
		$fileNameFull = $userIdPad.'_img_f_'.$timeString;
		$fileNameCrop = $userIdPad.'_img_c_'.$timeString;

		// Mover el archivo
		if ( !copy($url, $imgFolderFull.$fileNameFull) ) {
			se_killWithError('Unable to copy file (for main)');
		}

		//
		if ( !copy($imgFolderFull.$fileNameFull, $imgFolderFull.$fileNameCrop) ) {
			se_killWithError('Unable to copy file (for crop)');
		}

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_img_full='{$fileNameFull}', a_img_crop='{$fileNameCrop}'
WHERE a_id={$userId}
LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_upd);
	}


	//
	public static function picture_upload(&$response, int $userId)
	{
		
		//
		$imgFolderFull = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder;

		// Validar
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'image' => ['name' => 'Image', 'type' => 'file', 'fType'=>['jpg', 'jpeg', 'webp'], 'mSize'=>512, 'validate' => true],
			]
		);

		//
		$userIdPad = str_pad($userId, 8, '0', STR_PAD_LEFT);
		$timeString = se_timeString();
		$fileNameFull = $userIdPad.'_img_f_'.$timeString;
		$fileNameCrop = $userIdPad.'_img_c_'.$timeString;
		$fileTypeEnd = ".".$rData['image']['fType'];

		// debugVariable($rData);

		//
		self::image_delete($userIdPad."_img_");

		// Mover el archivo
		if ( !move_uploaded_file($rData['image']["tmp_name"], $imgFolderFull.$fileNameFull.$fileTypeEnd) ) {
			se_killWithError('Unable to copy file (for main)');
		}

		//
		if ( !copy($imgFolderFull.$fileNameFull.$fileTypeEnd, $imgFolderFull.$fileNameCrop.$fileTypeEnd) ) {
			se_killWithError('Unable to copy file (for crop)');
		}

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_img_full='{$fileNameFull}', a_img_crop='{$fileNameCrop}'
WHERE a_id={$userId}
LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_upd);

		//
		$response['d']['img_full'] = $fileNameFull;
		$response['d']['img_crop'] = $fileNameCrop;
	}

	//
	public static function picture_crop(&$response, $userId) {
		
		//
		$imgFolderFull = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder;

		//
		$pData = [
			'img_x' => ['name' => 'Image X', 'type' => 'int', 'validate' => false],
			'img_y' => ['name' => 'Image Y', 'type' => 'int', 'validate' => false],
			'img_w' => ['name' => 'Image W', 'type' => 'int', 'validate' => true],
			'img_h' => ['name' => 'Image H', 'type' => 'int', 'validate' => true],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		//
		$sql_qry = "SELECT a_img_full AS imgFull FROM sb_objects_obj WHERE a_id='{$userId}';";
		$userData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

		//
		$userIdPad = str_pad($userId, 8, '0', STR_PAD_LEFT);
		$timeString = se_timeString();
		$fileNameCrop = $userIdPad.'_img_c_'.$timeString;

		// Borrar anteriores (sólo crop)
		self::image_delete($userIdPad.'_img_c_');

		// Preparar imagen
		$image = new \snkeng\core\engine\imgResizer();
		$image->load($imgFolderFull.$userData['imgFull']);
		$image->crop($rData['img_w'], $rData['img_h'], $rData['img_x'], $rData['img_y'], $rData['img_w'], $rData['img_h']);
		$image->save($imgFolderFull.$fileNameCrop, IMAGETYPE_JPEG, 90);

		// Actualizar
		$sql_upd = "UPDATE sb_objects_obj SET a_img_crop='{$fileNameCrop}' WHERE a_id={$userId} LIMIT 1;";
		\snkeng\core\engine\mysql::submitQuery($sql_upd);

		//
		$response['d']['img_crop'] = $fileNameCrop;
	}

	//
	public static function banner_upload(&$response, $userId)
	{
		
		//
		$imgFolderFull = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder;

		//
		$pData = [
			'image' => ['name' => 'Image', 'type' => 'file', 'fType'=>['jpg', 'jpeg'], 'mSize'=>512, 'validate' => true],
		];
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		//
		$userIdPad = str_pad($userId, 8, '0', STR_PAD_LEFT);
		$timeString = se_timeString();
		$fileNameFull = $userIdPad.'_ban_f_'.$timeString;
		$fileNameCrop = $userIdPad.'_ban_c_'.$timeString;

		// debugVariable($rData);

		// Borrar anteriores (sólo crop)
		self::image_delete($userIdPad.'_ban_');

		// Mover el archivo
		if ( !move_uploaded_file($rData['image']["tmp_name"], $imgFolderFull.$fileNameFull) ) {
			se_killWithError('Unable to copy file (for main)');
		}

		//
		if ( !copy($imgFolderFull.$fileNameFull, $imgFolderFull.$fileNameCrop) ) {
			se_killWithError('Unable to copy file (for crop)');
		}

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_banner_full='{$fileNameFull}', a_banner_crop='{$fileNameCrop}'
WHERE a_id={$userId}
LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_upd);

		$response['d']['img_full'] = $fileNameFull;
		$response['d']['img_crop'] = $fileNameCrop;
	}

	//
	public static function banner_crop(&$response, $userId) {
		
		//
		$imgFolderFull = $_SERVER['DOCUMENT_ROOT'].self::$imgFolder;

		//
		$pData = [
			'img_x' => ['name' => 'Image X', 'type' => 'int', 'validate' => false],
			'img_y' => ['name' => 'Image Y', 'type' => 'int', 'validate' => false],
			'img_w' => ['name' => 'Image W', 'type' => 'int', 'validate' => true],
			'img_h' => ['name' => 'Image H', 'type' => 'int', 'validate' => true],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		//
		$sql_qry = "SELECT a_banner_full AS imgFull FROM sb_objects_obj WHERE a_id='{$userId}';";
		$userData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

		//
		$userIdPad = str_pad($userId, 8, '0', STR_PAD_LEFT);
		$timeString = se_timeString();
		$fileNameCrop = $userIdPad.'_ban_c_'.$timeString;

		// Borrar anteriores (sólo crop)
		self::image_delete($userIdPad.'_ban_c_');

		// Preparar imagen
		$image = new \snkeng\core\engine\imgResizer();
		$image->load($imgFolderFull.$userData['imgFull']);
		$image->crop($rData['img_w'], $rData['img_h'], $rData['img_x'], $rData['img_y'], $rData['img_w'], $rData['img_h']);
		$image->save($imgFolderFull.$fileNameCrop, IMAGETYPE_JPEG, 90);

		// Actualizar
		$sql_upd = "UPDATE sb_objects_obj SET a_banner_crop='{$fileNameCrop}' WHERE a_id={$userId} LIMIT 1;";
		\snkeng\core\engine\mysql::submitQuery($sql_upd);

		//
		$response['d']['img_crop'] = $fileNameCrop;
	}
}
//
