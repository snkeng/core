"use strict";

//
se.plugin.wysiwyg = function(plugElem, plugOptions) {
	let defaults = {
			imgDir:'/api/module_adm/core/site/images/win_images_sel',
			saveUrl:'',
			saveId:0,
			savePost:{},
			menuMode:null,
			textWrap:null
		},
		plugin = this,
		parent, editable, menuObj, miniMenuObj, htmlPre,
		selectedElement = null;
	//

	plugin.settings = se.object.merge(defaults, plugOptions);
	plugin.properties = { floatMode: false, dragInstance: {}, floatWindow: '', htmlMode: false };

	//
	function init() {
		// Try alt defaults
		if ( plugin.settings.saveUrl === '' && plugElem.se_data('saveurl') ) {
			plugin.settings.saveUrl = plugElem.se_data('saveurl');
		}
		//
		if ( plugin.settings.saveId === 0 && plugElem.se_data('id') ) {
			plugin.settings.saveId = plugElem.se_data('id');
		}

		// console.log(plugin.settings);

		//
		let vForm = [
				{
					title:'Formato', type:1, format:1, name:'format',
					submenus:[
						{
							title:'Fuente',
							elems:[
								{
									type:'but',
									title:'Negritas',
									icon:'bold',
									action:'simple',
									vars:'bold'
								},
								{
									type:'but',
									title:'Itálicas',
									icon:'italic',
									action:'simple',
									vars:'italic'
								},
								{
									type:'but',
									title:'Subrayado',
									icon:'underline',
									action:'simple',
									vars:'underline'
								},
								{
									type:'but',
									title:'Tachado',
									icon:'strikethrough',
									action:'simple',
									vars:'strikeThrough'
								},
								{
									type:'but',
									title:'Subíndice',
									icon:'subscript',
									action:'simple',
									vars:'subscript'
								},
								{
									type:'but',
									title:'Superíndice',
									icon:'superscript',
									action:'simple',
									vars:'superscript'
								},
								{ type:'sep' },
								{
									type:'but',
									title:'Color de fondo',
									icon:'bitbucket',
									action:'formOpen',
									vars:'bkgcol'
								},
								{
									type:'but',
									title:'Color de la fuente',
									icon:'pencil',
									action:'formOpen',
									vars:'txtcol'
								},
								{
									type:'but',
									title:'Remover formato',
									icon:'ban',
									action:'simple',
									vars:'removeFormat'
								},
								{
									type:'but',
									title:'Cita',
									icon:'quote-right',
									action:'simple',
									vars:'quoteSimple'
								}
							]
						},
						{
							title:'Parrafo',
							elems:[
								{
									type:'but',
									title:'Lista ordenada',
									icon:'list-ol',
									action:'simple',
									vars:'insertOrderedList'
								},
								{
									type:'but',
									title:'Lista no ordenada',
									icon:'list-ul',
									action:'simple',
									vars:'insertUnorderedList'
								},
								{
									type:'but',
									title:'Outdent',
									icon:'dedent',
									action:'simple',
									vars:'outdent'
								},
								{
									type:'but',
									title:'Indent',
									icon:'indent',
									action:'simple',
									vars:'indent'
								},
								{ type:'sep' },
								{
									type:'but',
									title:'Alinear izquierda',
									icon:'align-left',
									action:'simple',
									vars:'justifyLeft'
								},
								{
									type:'but',
									title:'Alinear centro',
									icon:'align-center',
									action:'simple',
									vars:'justifyCenter'
								},
								{
									type:'but',
									title:'Alinear derecha',
									icon:'align-right',
									action:'simple',
									vars:'justifyRight'
								},
								{
									type:'but',
									title:'Justificado',
									icon:'align-justify',
									action:'simple',
									vars:'justifyFull'
								}
							]
						},
						{
							title:'Formatos',
							elems:[
								{
									type:'but',
									title:'H1',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H1'
								},
								{
									type:'but',
									title:'H2',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H2'
								},
								{
									type:'but',
									title:'H3',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H3'
								},
								{
									type:'but',
									title:'H4',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H4'
								},
								{
									type:'but',
									title:'H5',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H5'
								},
								{
									type:'but',
									title:'H6',
									icon:'',
									action:'simple2',
									vars:'formatBlock,H6'
								},
								{ type:'sep' },
								{
									type:'but',
									title:'Parrafo',
									icon:'paragraph',
									action:'simple2',
									vars:'formatBlock,P'
								},
								{
									type:'but',
									title:'Pre',
									icon:'',
									action:'simple2',
									vars:'formatBlock,PRE'
								},
								{
									type:'but',
									title:'Div',
									icon:'',
									action:'simple2',
									vars:'formatBlock,DIV'
								},

							]
						},
						{
							title:'Insertar',
							elems:[
								{
									type:'but',
									title:'Vínculo',
									icon:'chain',
									action:'formOpen',
									vars:'url'
								},
								{
									type:'but',
									title:'Imágen',
									icon:'photo',
									action:'formOpen',
									vars:'img'
								},
								{
									type:'but',
									title:'Video',
									icon:'video-camera',
									action:'formOpen',
									vars:'vid'
								},
								{
									type:'but',
									title:'Tabla',
									icon:'table',
									action:'formOpen',
									vars:'table'
								},
								{ type:'sep' },
								{
									type:'but',
									title:'Separador',
									icon:'minus',
									action:'simple',
									vars:'insertHorizontalRule'
								},
								{
									type:'but',
									title:'Abrebiatura',
									icon:'info',
									action:'formOpen',
									vars:'abbre'
								},
								{
									type:'but',
									title:'Cita',
									icon:'quote-right',
									action:'formOpen',
									vars:'quote'
								},
								{
									type:'but',
									title:'Estructura',
									icon:'columns',
									action:'formOpen',
									vars:'elements'
								},
								{
									type:'but',
									title:'Botón',
									icon:'square',
									action:'formOpen',
									vars:'button'
								}
							]
						},
						{
							title:'Operaciones',
							elems:[
								{
									type:'but',
									title:'Deshacer',
									icon:'rotate-left',
									action:'simple',
									vars:'undo'
								},
								{
									type:'but',
									title:'Re Hacer',
									icon:'rotate-right',
									action:'simple',
									vars:'redo'
								},
								{ type:'but', title:'HTML', icon:'code', action:'html', vars:'open' },
								{
									type:'but',
									title:'Remover formato',
									icon:'ban',
									action:'removeFormatAll'
								},
							]
						}
					]
				},
				{
					title:'Color de la fuente', type:2, format:2, name:'txtcol',
					forms:[
						{
							title:'Fuente',
							name:'txtcol',
							actName:'Aceptar',
							elems:[
								{ type:'color', title:'Color', name:'color', requiered:true }
							]
						}
					]
				},
				{
					title:'Color del fondo', type:2, format:2, name:'bkgcol',
					forms:[
						{
							title:'Fuente',
							name:'bkgcol',
							actName:'Aceptar',
							elems:[
								{ type:'color', title:'Color', name:'color', requiered:true }
							]
						}
					]
				},
				{
					title:'Insertar URL', type:2, format:2, name:'url',
					forms:[
						{
							title:'Insertar URL',
							name:'url',
							actName:'Insertar',
							elems:[
								{ type:'url', title:'URL Externo', name:'urlext', requiered:false },
								{ type:'text', title:'URL Interno', name:'urlint', requiered:false },
							]
						}
					]
				},
				{
					title:'Imágen', type:2, format:2, name:'img',
					forms:[
						{
							title:'Insertar Imágen',
							name:'img',
							actName:'Insertar',
							elems:[
								{ type:'text', title:'URL Imágen', name:'url', requiered:true, complex:true,
									rbut:{ content:'<svg class="icon"><use xlink:href="#fa-search"></use></svg>', action:'window', vars:'img' }

								},
								{ type:'text', title:'Texto alternativo', name:'alttext', requiered:true },
								{
									type:'select', title:'Clase', name:'class', requiered:true,
									options:[
										{ val:'gOImg', name:'&#9635;&#9635;&#9635;' },
										{ val:'gOImg center', name:'_&#9635;_' },
										{ val:'gOImg left', name:'&#9635;__' },
										{ val:'gOImg right', name:'__&#9635;' }
									]
								},
								{ type:'checkbox', title:'Completo', name:'fullImage' },
								{ type:'text', title:'Pie de foto', name:'foot', requiered:false }
							]
						}
					]
				},
				{
					title:'Insertar Video', type:2, format:2, name:'vid',
					forms:[
						{
							title:'Insertar Video',
							name:'vid',
							actName:'Insertar',
							elems:[
								{ type:'url', title:'URL Video', name:'url', requiered:true }
							]
						}
					]
				},
				{
					title:'Insertar Tabla', type:2, format:2, name:'table',
					forms:[
						{
							title:'Insertar Tabla',
							name:'table',
							actName:'Insertar',
							elems:[
								{ type:'number', title:'Columnas', name:'cols', value:2, min:1, max:100, requiered:true },
								{ type:'number', title:'Filas', name:'rows', value:2, min:1, max:100, requiered:true },
								{ type:'checkbox', title:'Cabecera', name:'head' },
								{ type:'checkbox', title:'Pie', name:'foot' },
								{
									type:'select', title:'Clase Original', name:'style_class', requiered:true,
									options:[
										{ val:'se_table', name:'SE Table' },
										{ val:'', name:'Ninguna' },
									]
								},
								{ type:'checkbox', title:'Alternative', name:'style_alternate' },
								{ type:'checkbox', title:'Wide', name:'style_wide' },
								{ type:'checkbox', title:'Border', name:'style_border' },
								{ type:'checkbox', title:'Fixed', name:'style_fixed' },
								{ type:'checkbox', title:'Comparación', name:'style_comparison' },
							]
						}
					]
				},
				{
					title:'Insertar Abrebiación', type:2, format:2, name:'abbre',
					forms:[
						{
							title:'Insertar Abrebiación',
							name:'abbre',
							actName:'Insertar',
							elems:[
								{ type:'url', title:'URL Imágen', name:'url', requiered:true },
								{ type:'text', title:'Texto alternativo', name:'url', requiered:true },
								{ type:'text', title:'Clase', name:'url', requiered:true }
							]
						}
					]
				},
				{
					title:'Insertar Cita', type:2, format:2, name:'quote',
					forms:[
						{
							title:'Insertar Cita',
							name:'quote',
							actName:'Insertar',
							elems:[
								{ type:'textarea', title:'Cita', name:'quote', requiered:true },
								{ type:'text', title:'Autor', name:'author', requiered:true },
								{ type:'url', title:'URL referencia', name:'url', requiered:false },
							]
						}
					]
				},
				{
					title:'Insertar Estructura', type:2, format:2, name:'elements',
					forms:[
						{
							title:'Insertar Estructura',
							name:'elements',
							actName:'Insertar',
							elems:[
								{ type:'list', title:'Objeto', name:'object', requiered:true },
							]
						}
					]
				},
				{
					title:'Botón', type:2, format:2, name:'button',
					forms:[
						{
							title:'Insertar Botón',
							name:'button',
							actName:'Insertar',
							elems:[
								{ type:'text', title:'URL', name:'href', requiered:true },
								{ type:'text', title:'Contenido', name:'content', requiered:true },
								{
									type:'select', title:'Abrir en', name:'target', requiered:true,
									options:[
										{ val:'_blank', name:'Nueva página' },
										{ val:'', name:'Misma página' }
									]
								},
								{
									type:'select', title:'Clase', name:'class', requiered:true,
									options:[
										{ val:'btnLink', name:'Botón general' },
										{ val:'', name:'Sin estilo' }
									]
								},
								{ type:'checkbox', title:'Link interno', name:'internalLink' }
							]
						}
					]
				},
			],
			// Fin formulario
			tabs = '',
			contents = '',
			tabsL = vForm.length,
			menu_html,
			isTextArea = false,
			cTab, cContent, cElem, tabSel, cSel,
			testOverride;

		// Overrides
		testOverride = plugElem.se_data('textwrap');
		if ( testOverride ) {
			plugin.settings.textWrap = testOverride;
		}


		// Opción de salvar
		if ( plugin.settings.saveUrl !== '' ) {
			vForm[0]['submenus'][4]['elems'].push({
				type:'but',
				title:'Guardar',
				icon:'save',
				action:'save',
				vars:''
			});
		}

		// ----------------------------
		// Interpretar operaciones
		for ( let i = 0; i < tabsL; i++ ) {
			cTab = '';
			cContent = '';
			cElem = vForm[i];
			// Tab
			tabSel = ( cElem['type'] === 1 ) ? 'true' : 'false';
			cTab = '<div role="tab" aria-controls="%s" aria-selected="%s" data-type="%s">%s</div>'.sprintf([cElem['name'], tabSel, cElem['type'], cElem['title']]);
			// Panel
			cSel = ( cElem['type'] === 1 ) ? 'false' : 'true';
			cContent = '<div role="tabpanel" aria-labelledby="%s" aria-hidden="%s" data-type="%s">'.sprintf([cElem['name'], cSel, cElem['type']]);
			if ( cElem['format'] === 1 ) {
				// Método de subelementos
				let subElL = cElem['submenus'].length;
				for ( let j = 0; j < subElL; j++ ) {
					let cSubEl = cElem['submenus'][j];
					//
					cContent += '<div class="subMenu"><div class="actions">';
					// Botones
					let cButsL = cSubEl['elems'].length;
					for ( let k = 0; k < cButsL; k++ ) {
						let cBut = cSubEl['elems'][k];
						switch ( cBut['type'] ) {
							case 'but':
								let innerTxt = ( cBut['icon'] !== '' ) ? '<svg class="icon"><use xlink:href="#fa-' + cBut['icon'] + '"></use></svg>' : cBut['title'];
								cContent += '<div class="but" data-wwtype="button" data-action="%s" data-vars="%s" title="%s">%s</div>'.sprintf([cBut['action'], cBut['vars'], cBut['title'], innerTxt]);
								break;
							case 'sep':
								cContent += '<div class="sep"></div>';
								break;
						}
					}
					cContent += '</div><div class="subElTitle">' + cSubEl['title'] + '</div></div>';
				}
			} else if ( cElem['format'] === 2 ) {
				// Método de subelementos
				let formsL = cElem['forms'].length;
				for ( let j = 0; j < formsL; j++ ) {
					let cForm = cElem['forms'][j];
					//
					cContent += '<form data-wwtype="form" data-name="%s">'.sprintf(cForm['name']);
					// Elementos
					let fElemL = cForm['elems'].length;
					for ( let k = 0; k < fElemL; k++ ) {
						let fElem = cForm['elems'][k],
							isReq = ( fElem.requiered ) ? 'requiered' : '';
						cContent += '<label class="' + isReq + '"><span class="el_title">' + fElem.title + '</span>';
						//
						if ( fElem.complex ) {
							cContent+= '<div class="c_input">';
						}
						// Inputs
						switch ( fElem.type ) {
							//	Textos simples
							case 'text':
								let cPattern = (true) ? '' : '',
									cTitle = (true) ? '' : '',
									cPlaceholder = (true) ? '' : '';
								cContent += '<input type="text" name="%s" />'.sprintf([fElem.name]);
								break;
							case 'password':
								cContent += '<input type="password" name="%s" />'.sprintf([fElem.name]);
								break;
							case 'email':
								cContent += '<input type="email" name="%s" />'.sprintf([fElem.name]);
								break;
							case 'url':
								cContent += '<input type="url" name="%s" />'.sprintf([fElem.name]);
								break;
							//
							case 'number':
								cContent += '<input type="number" name="%s" value="%s" min="%s" max="%s" />'.sprintf([
									fElem.name,
									( 'value' in fElem ) ? fElem.value : '',
									( 'min' in fElem ) ? fElem.min : '',
									( 'max' in fElem ) ? fElem.max : '',
								]);
								break;
							//
							case 'datetime-local':
								cContent += '<input type="datetime-local" name="%s" />'.sprintf([fElem.name]);
								break;
							case 'time':
								cContent += '<input type="time" name="%s" />'.sprintf([fElem.name]);
								break;
							case 'color':
								cContent += '<input type="color" name="%s" />'.sprintf([fElem.name]);
								break;
							//
							case 'textarea':
								cContent += '<textarea name="%s"></textarea>'.sprintf([fElem.name]);
								break;
							case 'checkbox':
								cContent += '<input type="checkbox" name="%s" value="1" />'.sprintf([fElem.name]);
								break;
							case 'select':
								let opts = '', cOpt;
								for ( let oI = 0, oLen = fElem.options.length; oI < oLen; oI++) {
									cOpt = fElem.options[oI];
									opts+= '<option value="%s">%s</option>'.sprintf(cOpt.val, cOpt.name);
								}
								cContent+= '<select name="%s">%s</select>'.sprintf(fElem.name, opts);
								break;
							case 'button':

								break;
							default:
								console.log('wysiwyg - elems - opnodef', fElem);
								break;
						}
						//
						if ( fElem.complex ) {
							if ( fElem.rbut ) {
								cContent += '<button type="button" data-wwtype="button" data-action="%s" data-vars="%s">%s</button>'.sprintf(fElem.rbut.action, fElem.rbut.vars, fElem.rbut.content);
							}
							cContent += '</div>';
						}
						cContent += '</label>';
					}
					cContent += '<input type="submit" value="%s" />'.sprintf(cForm['actName']);
					cContent += '<output se-elem="response"></output></form>';
				}
			}
			cContent += '</div>';
			// Guardar
			tabs += cTab;
			contents += cContent;
		}


		// ----------------------------
		// Generar objeto
		menu_html = '<div class="seWW_tabs" role="tablist">' + tabs + '\
<div class="seWW_mOps"><div class="seWW_menuReturn" data-wwtype="button" data-action="removeFloat"><svg class="icon"><use xlink:href="#fa-remove"></use></div></div>\
</div>\
<div class="seWW_panels">' + contents + '</div>';
		//

		let f_html = '<div class="but" data-wwtype="button" data-action="html">Salir HTML</div>';
		//
		// Construcción
		//

		// Definir comportamiento
		plugElem.se_wrap('<div class="seWW_container"></div>');
		parent = plugElem.parentNode;
		if ( plugElem.tagName === 'TEXTAREA' ) {
			parent.insertAdjacentHTML('afterbegin', '<div class="html5Wysiwyg" contenteditable="true"></div>');
			editable = parent.querySelector('div.html5Wysiwyg');
			editable.se_html(plugElem.value);
			// Copiar detalles
			let taStyle = window.getComputedStyle(plugElem);
			editable.style.background = taStyle['background-color'];
			editable.style.paddingTop = taStyle['padding-top'];
			editable.style.paddingRight = taStyle['padding-right'];
			editable.style.paddingBottom = taStyle['padding-bottom'];
			editable.style.paddingLeft = taStyle['padding-left'];
			editable.style.borderRadius = taStyle.getPropertyValue('borderRadius');
			// Actualizar TextArea
			editable.addEventListener('blur', function(e) {
				plugElem.value = htmlPurify(editable.se_html());
			});
			//
			plugElem.se_hide();
			plugElem.se_data('mode', 'textareaesp');
			// Permitir updates
			plugElem.se_on('change', function(e) {
				e.preventDefault();
				editable.se_html(e.target.value);
			});
		} else {
			editable = plugElem;
			editable.se_attr('contenteditable', 'true');
			editable.se_classAdd('html5Wysiwyg');
		}

		//
		if ( plugin.settings.textWrap ) {
			editable.style.whiteSpace = plugin.settings.textWrap;
		}

		// pre
		parent.insertAdjacentHTML('afterbegin', '<textarea class="seWW_form_pre"></textarea>');
		htmlPre = parent.querySelector('textarea.seWW_form_pre');
		// menu small
		parent.insertAdjacentHTML('afterbegin', '<div class="seWW_form_simple" style="display:none;">' + f_html + '</div>');
		miniMenuObj = parent.querySelector('div.seWW_form_simple');
		// menu
		parent.insertAdjacentHTML('beforeend', '<div class="seWW_form" unselectable="on" draggable="true" style="display:none;">' + menu_html + '</div>');
		menuObj = parent.querySelector('div.seWW_form');

		// parent.innerHTML+= '<div class="seWW_form" unselectable="on" draggable="true" style="display:none;">' + menu_html + '</div>';
		// menuObj.style.transition = 'top 0.3s ease, left 0.3s ease';

		//
		// LISTENERS
		// Botones
		menuObj.se_on('click', '[data-wwtype="button"]', (e, cElem) => {
			htmlAction(cElem);
		});
		menuObj.se_on('submit', 'form', (e, cElem) => {
			e.preventDefault();
			formSubitAction(cElem);
		});
		menuObj.se_on('click', 'div[role="tab"]', (e, cElem) => {
			e.preventDefault();
			formOpen(cElem.se_attr('aria-controls'), false);
		});
		miniMenuObj.se_on('click', '[data-wwtype="button"]', (e, cElem) => {
			htmlAction(cElem);
		});
		// Objects listeners
		editable.addEventListener('focus', function setPos(e) {
			e.preventDefault();
			e.stopPropagation();
			menuObj.se_css('display', 'block');
			setPositionForce();
		});
		// Tab
		editable.addEventListener("keydown", (event) => {
			if ( event.isComposing || event.keyCode === 229 ) {
				return;
			}
			// do something

			let cEl;

			//
			switch ( event.code ) {
				//
				case 'Tab':
					cEl = getSelectionElement();
					//
					switch ( cEl.tagName ) {
						//
						case 'LI':
							event.preventDefault();
							if ( event.shiftKey ) {
								document.execCommand('outdent', false, null);
							} else {
								document.execCommand('indent', false, null);
							}
							break;
						//
						case 'TH':
						case 'TD':
							event.preventDefault();
							let targetElement;
							if ( event.shiftKey ) {
								// go to prev
								targetElement = cEl.se_prev(cEl.tagName);
								if ( targetElement ) {
									targetElement.focus();
									targetElement.select();
								}
							} else {
								// go to next
								targetElement = cEl.se_next(cEl.tagName);
								if ( targetElement ) {
									targetElement.focus();
									targetElement.select();
								}
							}
							break;
						//
						default:
							console.log("IGNORING TAB ACTION", event, cEl);
							break;
					}
					break;
				//
				case 'Enter':
					// cEl = getSelectionElement();
					break;
			}
		});
		
		
		// Imagenes
		editable.se_on('click', 'img', editImage);
		

		function getSelectionElement() {
			var selection = window.getSelection();
			var container = selection.anchorNode;
			if ( container.nodeType !== 3 ) {
				return container;
			} else {         // return parent if text node
				return container.parentNode;
			}
		}

		//
		se.windowEvent(editable, 'click', function withinObj(e) {
			let elem = e.target;
			if ( !elem.se_hasParent(parent) ) {
				menuObj.se_css('display', 'none');
				// formsClose();
			}
		});
		se.windowEvent(editable, 'scroll', se.funcs.debounce(setPosition, 20));
		// Float element
		menuObj.addEventListener('dragstart', (event) => {
			let style = window.getComputedStyle(menuObj, null),
				dragOver = function(cEv) {
					cEv.preventDefault();
					return false;
				},
				drop = function(event){
					let offset = event.dataTransfer.getData("offset").split(',');
					event.preventDefault();
					menuObj.se_css({
						left:(event.clientX + parseInt(offset[0], 10)) + 'px',
						top:(event.clientY + parseInt(offset[1], 10)) + 'px'
					});
					document.body.removeEventListener('dragover', dragOver);
					document.body.removeEventListener('drop', drop);
					plugin.properties['floatMode'] = true;
					menuObj.querySelector('.seWW_menuReturn').se_css('display', 'inline-flex');
					return false;
				};
			event.dataTransfer.dropEffect = 'move';
			event.dataTransfer.setData("offset",
				(parseInt(style.getPropertyValue("left"), 10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"), 10) - event.clientY));
			// Bindings
			document.body.addEventListener('dragover', dragOver);
			document.body.addEventListener('drop', drop);
		});


	}

	//<editor-fold desc="Interacción">

	//
	function removeFloat(event) {
		if ( plugin.properties['floatMode'] ) {
			plugin.properties['floatMode'] = false;
			setPositionForce();
			menuObj.querySelector('.seWW_menuReturn').se_css('display', 'none');
		}
	}

	//
	function setPosition() {
		if ( !plugin.properties['floatMode'] ) {
			let rect = editable.getBoundingClientRect(),
				yCriteria = rect.y - menuObj.clientHeight,
				vTop = ( yCriteria > 0 ) ? rect.y - menuObj.clientHeight : 0;
			menuObj.se_css({ top:vTop + 'px' });
		}
	}

	//
	function setPositionForce() {
		if ( !plugin.properties['floatMode'] ) {
			let rect = editable.getBoundingClientRect(),
				yCriteria = rect.y - menuObj.clientHeight,
				vTop = ( yCriteria > 0 ) ? rect.y - menuObj.clientHeight : 0,
				vLeft = rect.x;
			vLeft = ( vLeft > window.scrollX ) ? vLeft : 0;
			menuObj.se_css({ top:vTop + 'px', left:vLeft + 'px'});
			menuObj.se_css('display', 'block');
		}
	}

	//</editor-fold>

	//<editor-fold desc="Acciones">

	//
	function editImage(e, cImg) {
		e.preventDefault();
		selectedElement = cImg;

		formOpen('img', true);

		console.log("INITIALIZE IMAGE EDITION", e, cImg);
	}

	//
	function htmlAction(cBut) {
		let op, cVar;
		switch ( cBut.se_data('action') ) {
			case 'simple':
				op = cBut.se_data('vars');
				document.execCommand(op, false, null);
				console.log('simple: ' + op);
				break;
			case 'simple2':
				cVar = cBut.se_data('vars').split(',');
				document.execCommand(cVar[0], false, cVar[1]);
				console.log('simple2: ' + cVar[0] + ' - ' + cVar[1]);
				break;
			case 'removeFormatAll':
				if ( confirm('¿Remover todo el formato?') ) {
					removeFormatAll();
				}
				break;
			case 'html':
				let content;
				if ( plugin.properties.htmlMode ) {
					plugin.properties.htmlMode = false;
					htmlPre.querySelectorAll('br').se_remove();
					content = htmlPre.value;
					content = content.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
					editable.se_html(content);
					editable.se_css('display', 'block');
					menuObj.se_css('display', 'block');
					miniMenuObj.se_css('display', 'none');
					htmlPre.se_css('display', 'none');
				} else {
					plugin.properties.htmlMode = true;
					htmlPre.value = htmlPurify(editable.se_html());
					editable.se_css('display', 'none');
					menuObj.se_css('display', 'none');
					miniMenuObj.se_css('display', 'flex');
					htmlPre.se_css('display', 'block');
				}
				break;
			case 'formOpen':
				op = cBut.se_data('vars');
				formOpen(op, true);
				break;
			case 'formSel':
				op = cBut.se_data('vars');
				formOpen(op, false);
				break;
			case 'formClose':
				formsClose();
				break;
			case 'window':
				op = cBut.se_data('vars');
				switch ( op ) {
					case 'img':
						windowImg();
						break;
					default:
						console.error("Caso no programado de ventan externa.");
						break;
				}
				break;
			case 'removeFloat':
				removeFloat();
				break;
			case 'save':
				saveData();
				break;
			default:
				console.log('htmlOp No definida ('+ cBut.se_data('action')+')');
				console.log(cBut);
				break;
		}
	}

	//
	function formLoadAction(formName) {
		switch ( formName ) {
			case 'img':
				formImgLoad();
				break;
		}
	}

	//
	function formSubitAction(cForm) {
		//
		switch ( cForm.se_data('name') ) {
			case 'bkgcol':
				formBkgCol(cForm);
				break;
			case 'txtcol':
				formTxtCol(cForm);
				break;
			case 'url':
				formUrl(cForm);
				break;
			case 'img':
				formImgSubmit(cForm);
				break;
			case 'vid':
				formVid(cForm);
				break;
			case 'table':
				formTable(cForm);
				break;
			case 'abbre':
				formAbbre(cForm);
				break;
			case 'quote':
				formQuote(cForm);
				break;
			case 'elements':
				formElements(cForm);
				break;
			case 'button':
				formButton(cForm);
				break;
			default:
				console.error("No hay forma asociada.", cForm);
				break;
		}
	}

	//
	function formOpen(name, newTab) {
		formsCloseOp(newTab);
		console.log("TEXT EDITOR - OPENING FORM: ", name);
		menuObj.querySelector('div[role="tab"][aria-controls="' + name + '"]').se_attr('aria-selected', 'true').se_classAdd('active');
		menuObj.querySelector('div[role="tabpanel"][aria-labelledby="' + name + '"]').se_attr('aria-hidden', 'false');

		formLoadAction(name);

		setPositionForce();
	}

	//
	function formsCloseOp(hide) {
		menuObj.querySelectorAll('div[role="tab"]').se_attr('aria-selected', 'false');
		menuObj.querySelectorAll('div[role="tabpanel"]').se_attr('aria-hidden', 'true');
		if ( hide ) {
			menuObj.querySelectorAll('div[role="tab"][data-seWW-tabType="2"]').se_classDel('active');
		}
	}

	//
	function formsClose() {
		formsCloseOp(true);
		menuObj.querySelectorAll('div[role="tab"][data-type="1"]').se_attr('aria-selected', 'true');
		menuObj.querySelectorAll('div[role="tabpanel"][data-type="1"]').se_attr('aria-hidden', 'false');
		setPositionForce();
	}

	//</editor-fold>

	//<editor-fold desc="Unassigned">

	//
	function windowImg() {
		if ( plugin.settings.imgDir ) {
			se.addon.floatWin({
				iniUrl:plugin.settings.imgDir,
				fullScreen:true,
				id:'admin_photoSel',
				onLoad:() => {
					let photoNav = $('#photoNav');
					photoNav.adm_imageEditor.setup({
						onSelect:(imgData) => {
							let form = menuObj.querySelector('form[data-name="img"]');
							form.querySelector('input[name="url"]').se_val('' + imgData.floc);
							form.querySelector('input[name="alttext"]').se_val(imgData.title);
							$('#admin_photoSel').se_remove();
						}
					});
				}
			});
		}
	}

	//
	function windowImgRes(event) {
		console.log(event);
	}

	//</editor-fold>

	//<editor-fold desc="Unassigned">

	//
	function removeFormatAll() {
		let str = editable.se_html();
		str = str
			.replace(/<(font|span)[^>]*>/gi, "")
			.replace(/<\/(font|span)>/gi, "")
			.replace(/<br[^>]*>/gi, "")
			.replace(/<([A-z]+)([^>]*)style="[^"]*"([^>]*)>/gi, "<$1$2$3>")
			.replace(/<(table|thead|tbody|tfoot|tr|th|td)([^>]*)>/gi, "<$1>")
			.replace(/lang="[^"]*"/gi, "")
			.replace(/<((p|ul|ol|li|b|i|s|u)[^>]+)>\s*<\/\1>/gi, "")
			.replace(/\n+/gi, "\n")
			.trim();
		editable.se_html(str);
	}

	//
	function htmlPurify(ih) {
		//
		function clearTags(str) {
			str = str
				.replace(/style="\s*"/gi, "") //styles vacios
				.replace(/lang="[^"]*"/gi, ""); //lang stuf
			return str;
		}
		function lc(str) {
			return str.toLowerCase()
		}
		function qa(str) {
			return str.replace(/(\s+\w+=)([^"][^>\s]*)/gi, '$1"$2"');
		}
		function sa(str) {
			return str.replace(/("|;)\s*[A-Z-]+\s*:/g, lc);
		}

		// Simplificar espacios
		ih = ih.replace(/[\s]+/gi, " ").replace(/&nbsp;/gi, " ").replace(/[ ]{2,}/gi, " ");
		// Non empty bug
		let ihTest = ih.replace(/[ ]*/gi, "");
		if ( ihTest === "<br>" ) {
			ih = "";
		}
		// Limpieza
		ih = ih
			// Meta, link, styles y comments
			// .replace(/<(meta|link)([^>]*)>/gi, "")
			.trim()
			.replace(/<style>(\w|\W)+?<\/style>/gi, "")
			.replace(/<script>(\w|\W)+?<\/script>/gi, "")
			.replace(/<!--(\w|\W)+?-->/gi, "")
			// Objetos no válidos en el HTML
			.replace(/<\/?(COL|XML|ST1|SHAPE|V:|O:|F:|F |PATH|LOCK|IMAGEDATA|STROKE|FORMULAS|ISTYLE|SPANSTYLE)[^>]*>/gi, "")
			.replace(/\bCLASS="?(MSO\w*|Apple-style-span)"?/gi, "") // Datos provenientes de MSWord o editores de Mac

			// LIMPIAR ELEMENTOS
			.replace(/<([^>])+>/gi, clearTags) //quote all atts

			// Correcciones
			.replace(/(<\/?)FONT([^>]*)/gi, "$1span$2")		// Cambiar FONT por SPAN
			.replace(/[–]/g, '-') //long –
			.replace(/[']/g, "'") //single smartquotes ''
			.replace(/["]/g, '"') //double smartquotes ""
			.replace(/<(TABLE|TD|TH|COL)(.*)(WIDTH|HEIGHT)=["'0-9A-Z]*/gi, "<$1$2") //no fixed size tables (%OK) [^A-Za-z>]
			.replace(/<(IMG|INPUT|BR|HR)([^>]*)>/gi, "<$1$2 />")  //self-close tags
			// Correcciones por formato
			.replace(/<P[^>]*>\s*<BR \/>\s*<\/P>/gi, "")
			.replace(/<BR \/>\s*<\/P>/gi, "</P>")
			.replace(/<\/P> <P /gi, "</P><P ")
			.replace(location.href + '#', '#') //IE anchor bug
			// Correcciones HTML
			.replace(/(<\/?[A-Z]*)/g, lc) //lowercase tags...
			.replace(/style="[^"]*"/gi, sa) //and style atts
			.replace(/<[^>]+=[^>]+>/g, qa) //quote all atts
			// Eliminación de tags de texto vacio
			.replace(/<span[^>]*>\s*<\/span>/gi, "")
			.replace(/<p[^>]*>\s*<\/p>/gi, "")
			.replace(/<((p|ul|ol|li|b|i|s|u)[^>]+)>\s*<\/\1>/gi, "")

			// Remover last <br>
			.replace(/<br[^>]*>/gi, "<br>")
			.replace(/(<br>)+$/gi, "")
			.replace(/<br>/gi, "<br />\n") //espacio..

			// Poner saltos de linea y formato html no visible
			.replace(/<(table|tbody|tr|thead|ul|ol|div|pre|img|source|figure|figcaption|picture|blockquote)([^>]*)>/gi, "<$1$2>\n")
			.replace(/<\/(p|h1|h2|h3|h4|h5|h6|ol|ul|li|table|tbody|tr|td|th|thead|iframe|pre|div|figure|figcaption|picture|blockquote)>/gi, "</$1>\n")
			.replace(/<(option|tr|input|td|th|li)/gi, "\t<$1") //indent..
			.replace(/(\n[\s]*)+/gi, "\n"); //..more
		return ih;
	}

	//
	function quoteSimple(event) {
	}

	//</editor-fold>

	//<editor-fold desc="Formularios">

	//
	function formImgLoad() {
		//
		if ( selectedElement && selectedElement.tagName === 'IMG' ) {
			let cForm = menuObj.querySelector('div[role="tabpanel"][aria-labelledby="img"] form');
			cForm.se_formElVal('url', selectedElement.se_attr('src'));
			cForm.se_formElVal('alttext', selectedElement.se_attr('title'));
			cForm.se_formElVal('class', selectedElement.se_attr('class'));
		}
	}

	//
	function formImgSubmit(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			// Variables
			vUrl = cForm.querySelector('input[name="url"]').se_val(),
			vTA = cForm.querySelector('input[name="alttext"]').se_val(),
			vClass = cForm.querySelector('[name="class"]').se_val(),
			vFull = cForm.querySelector('input[name="fullImage"]').se_val(),
			vFoot = cForm.querySelector('[name="foot"]').se_val(),
			//
			isAdjustable = false,
			sectionWidth = editable.offsetWidth,
			adjImageFolder, adjImageName,
			imgObj = {
				'tag':'img',
				'attr':{
					'class':vClass,
					'alt':vTA,
					'src':vUrl
				}
			};

		//
		if ( !vUrl ) {
			response.se_text('No se introdujo ningún URL de imagen');
			console.log(vUrl);
			return;
		}

		// Check image format
		if ( vUrl.startsWith('/res/image/') ) {
			isAdjustable = true;
			let filePath = vUrl.split('/');
			adjImageFolder = '/' + filePath.slice(1, filePath.length - 2).join('/');
			adjImageName = filePath.pop();
			//
			imgObj.attr.src = adjImageFolder + '/w_' + sectionWidth + '/' + adjImageName;
		}

		//
		if ( selectedElement ) {
			//
			selectedElement.se_attr('class', vClass).se_attr('src', imgObj.attr.src).se_attr('title', vTA);
			if ( isAdjustable ) {
				selectedElement.se_attr('srcset', `${ adjImageFolder }/w_360/${ adjImageName } 360w, ${ adjImageFolder }/w_832/${ adjImageName }`);
				selectedElement.se_attr('sizes', "(max-width:768px) 100vw, 80vw");
			}
			selectedElement = null;
		} else {
			//
			editable.focus();
			if ( !document.queryCommandEnabled('insertHTML') ) {
				response.se_text('No hay texto seleccionado.');
				return;
			}

			//
			if ( isAdjustable ) {
				imgObj.attr['srcset'] = `${ adjImageFolder }/w_360/${ adjImageName } 360w, ${ adjImageFolder }/w_832/${ adjImageName }`;
				imgObj.attr['sizes'] = "(max-width:768px) 100vw, 80vw";
			}

			//
			if ( vFull ) {
				delete imgObj.attr.class;
			}

			//
			if ( vFoot ) {
				imgObj = {
					'tag':'figure',
					'children':[
						imgObj,
						{
							'tag':'figcaption',
							'content':vFoot
						}
					]
				};
			}

			// Insert HTML Operation
			document.execCommand('insertHTML', false, se.struct.htmlFromJson(imgObj));
		}
		formsClose();
	}

	//
	function formBkgCol(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm);

		// Validación
		if ( !cFormData['color'].match(/^#?([a-fA-F0-9]{8}|[a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/) ) {
			response.se_text('El color es vació o tiene un formato no válido.');
			console.log(vColor);
			return;
		}

		// Intentar dar select
		editable.focus();

		// Evaluar
		if ( !document.queryCommandEnabled('backColor') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		//
		document.execCommand('backColor', false, vColor);
		formsClose();
	}

	//
	function formTxtCol(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm);

		// Validación
		if ( !cFormData['color'].match(/^#?([a-fA-F0-9]{8}|[a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/) ) {
			response.se_text('El color es vació o tiene un formato no válido.');
			console.log(vColor);
			return;
		}

		// Intentar dar select
		editable.focus();

		if ( !document.queryCommandEnabled('foreColor') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		//
		document.execCommand('foreColor', false, vColor);
		formsClose();
	}

	//
	function formUrl(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm),
			//
			ext = true,
			rUrl;

		//
		if ( cFormData['urlext'] ) {
			rUrl = cFormData['urlext'];
		} else if ( cFormData['urlint'] ) {
			ext = false;
			rUrl = cFormData['urlint'];
		} else {
			response.se_text('No se introdujo ningún URL');
			return;
		}

		// Intentar dar select
		editable.focus();

		// Evaluar
		if ( !document.queryCommandEnabled('createLink') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		// Ejecutar
		document.execCommand('createLink', false, rUrl);

		formsClose();
	}

	//
	function formVid(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm),
			//
			vid_id = [],
			video,
			embedObject;

		//
		if ( !cFormData['url'] ) {
			response.se_text('No se introdujo ningún URL del video.');
			console.log(cFormData['url']);
			return;
		}

		editable.focus();
		if ( !document.queryCommandEnabled('insertHTML') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}


		// Procesar
		if ( cFormData['url'].indexOf('youtube.com/watch') !== -1 ) {
			vid_id = cFormData['url'].split('v=')[1];
			let ampPos = vid_id.indexOf('&');
			if ( ampPos !== -1 ) {
				vid_id = vid_id.substring(0, ampPos);
			}
			embedObject = '<iframe src="https://www.youtube.com/embed/' + vid_id + '?rel=0" allowFullScreen></iframe>';
		} else if ( cFormData['url'].indexOf('youtu.be/') !== -1 ) {
			vid_id = cFormData['url'].split('.be/')[1];
			embedObject = '<iframe src="https://www.youtube.com/embed/' + vid_id + '?rel=0" allowFullScreen></iframe>';
		} else if ( cFormData['url'].indexOf('vimeo.com') !== -1 ) {
			vid_id = cFormData['url'].split('.com/')[1];
			embedObject = '<iframe src="https://player.vimeo.com/video/' + vid_id + '" allowFullScreen></iframe>';
		} else if ( cFormData['url'].indexOf('facebook.com') !== -1 ) {
			vid_id = encodeURI(cFormData['url']);
			embedObject = '<iframe src="https://www.facebook.com/plugins/video.php?href=' + vid_id + '&show_text=0&width=400&height=225" allowFullScreen></iframe>';
		} else {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		// Ejecutar
		video = '<div class="videoWrapper">' + embedObject + '</div>';
		document.execCommand('insertHTML', false, video);
		formsClose();
	}

	//
	function formTable(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm, false, {'int':['cols', 'rows']}),
			//
			tabClass = [],
			table = '',
			tabClassTxt = '',
			i;

		// Evaluar
		if ( cFormData['cols'] === 0 ) {
			response.se_text('Número de filas no válido.');
			return;
		}
		if ( cFormData['rows'] === 0 ) {
			response.se_text('Número de columnas no válido.');
			return;
		}

		//
		editable.focus();
		if ( !document.queryCommandEnabled('insertHTML') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		// Ejecutar
		let colBody = '', colHead = '', colFoot = '',
			rowBody = '';
		//
		for ( i = 0; i < cFormData['cols']; i++ ) {
			colBody += '<td>body</td>';
			colHead += '<th>title</th>';
			colFoot += '<td>foot</td>';
		}

		//
		for ( i = 0; i < cFormData['rows']; i++ ) {
			rowBody += '<tr>' + colBody + '</tr>';
		}

		//
		tabClass.push(cFormData['style_class']);

		//
		if ( cFormData['style_comparison'] ) {
			tabClass.push(cFormData['comparison']);
		}

		//
		if ( cFormData['style_alternate'] ) {
			tabClass.push('alternate');
		}
		//
		if ( cFormData['style_wide']) {
			tabClass.push('wide');
		}
		//
		if ( cFormData['style_border'] ) {
			tabClass.push('border');
		}
		//
		if ( cFormData['style_fixed'] ) {
			tabClass.push('fixed');
		}

		//
		tabClassTxt = tabClass.join(' ');

		//
		table = '<table class="' + tabClassTxt + '">';
		if ( cFormData['head'] ) {
			table += '<thead><tr>' + colHead + '</tr></thead>';
		}
		table += '<tbody>' + rowBody + '</tbody>';
		if ( cFormData['foot']) {
			table += '<tfoot><tr>' + colFoot + '</tr></tfoot>';
		}
		table += '</table>';

		//
		document.execCommand('insertHTML', false, table);
		formsClose();
	}

	//
	function formAbbre(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm);

		//
		editable.focus();
		if ( !document.queryCommandEnabled('insertHTML') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		// Ejecutar
		document.execCommand('foreColor', false, cFormData['color']);
		formsClose();
	}

	//
	function formQuote(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm),
			elStruct = {
				'tag':'figure',
				'attr':{
					'class':'quote'
				},
				'children':[
					{
						'tag':'blockquote',
						'attr':{
							'cite':cFormData['url']
						},
						'content':cFormData['quote']
					},
					{
						'tag':'figcaption',
						'attr':{
							'class':'quote'
						},
						'content':cFormData['author']
					}
				]
			};

		//
		if ( !cFormData['quote'] ) {
			response.se_text('No se insertó la cita.');
			return;
		}
		//
		if ( !cFormData['author'] ) {
			response.se_text('No se insertó el autor.');
			return;
		}

		//
		editable.focus();
		if ( !document.queryCommandEnabled('insertHTML') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		//
		document.execCommand('insertHTML', false, se.struct.htmlFromJson(elStruct));
		formsClose();
	}

	//
	function formElements(event) {
		console.log(event, "ejecutando formulario de elementos");
	}

	//
	function formButton(cForm) {
		let response = cForm.querySelector('output[se-elem="response"]'),
			cFormData = se.form.serializeToObj(cForm),
			elStruct = {
				'tag':'a',
				'attr':{
					'target':cFormData['target'],
					'href':cFormData['href'],
					'rel':'noopener',
					'class':cFormData['class']
				},
				'content':cFormData['content']
			};

		//
		editable.focus();
		if ( !document.queryCommandEnabled('insertHTML') ) {
			response.se_text('No hay texto seleccionado.');
			return;
		}

		// Mods


		// Ejecutar
		document.execCommand('insertHTML', false, se.struct.htmlFromJson(elStruct));
		formsClose();
	}

	//</editor-fold>

	//
	function getSelectedText(event) {
		let html = '';
		if ( typeof window.getSelection !== "undefined" ) {
			let sel = window.getSelection();
			if ( sel.rangeCount ) {
				let container = document.createElement("div");
				for ( let i = 0, len = sel.rangeCount; i < len; ++i ) {
					container.appendChild(sel.getRangeAt(i).cloneContents());
				}
				html = container.innerHTML;
			}
		} else if ( typeof document.selection !== "undefined" ) {
			if ( document.selection.type === "Text" ) {
				html = document.selection.createRange().htmlText;
			}
		}
		return html;
	}

	//
	function htmlNodeClean(node) {
		//
		for ( let n = 0; n < node.childNodes.length; n++ ) {
			let child = node.childNodes[n];

			//
			switch ( child.nodeType ) {
				// General tag
				case 1:
					console.log("NODE INFO ", child);
					htmlNodeClean(child);
					break;
				// Text Tag
				case 3:
					if ( !/\S/.test(child.nodeValue) ) {
						node.removeChild(child);
						n--;
					} else if ( ['P', 'LI', 'TH', 'TD'].includes(child.parentNode.nodeName) ) {
						if ( !child.previousSibling ) {
							child.nodeValue = child.nodeValue.trimStart();
						}
						if ( !child.nextSibling ) {
							child.nodeValue = child.nodeValue.trimEnd();
						}
						console.log("NODE INFO has parent", child);
					}
					break;
				// Comment tag
				case 8:
					node.removeChild(child);
					n--;
					break;
			}
		}
	}

	//
	function returnHTML() {
		// Cleaning
		htmlNodeClean(editable);
		console.log("editable: ", editable.se_html());
		let value =  htmlPurify(editable.se_html());
		console.log("subiendo: ", value);
		return value;
	}

	//
	function returnNodeData() {
		return editable;
	}

	//
	function saveData() {
		//
		se.ajax.json(plugin.settings.saveUrl,
			se.object.merge({id:plugin.settings.saveId, content:returnHTML()}, plugin.settings.savePost), {
				onSuccess:function(msg) {
					alert("Guardada");
				}
			}
		);
	}

	// 
	init();
	return {
		getNode:returnNodeData,
		getHTML:returnHTML
	};
};

//
se.plugin.editableObject = function(plugElem, plugOptions) {
	let pDefaults = {
			target:null,
			barTitle: 'Editar',
			saveUrl: '',
			onSuccess: null,
			onFail: null,
			hasFields: true,
			forms:{},
			actions:{},
			act_defaults:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions),
		//
		pStatus = {
			open:true,
		},
		//
		bMenu_1,
		floatMenu, plugResponse;
	//

	//
	function init() {
		// Crear barra de herramientas
		if ( pSettings.target ) {
			floatMenu = bMenu_1 = $(pSettings.target);
		}
		else {
			floatMenu = plugElem.se_prepend('<div class="se_editableObject show"><div class="menu"></div><div class="tab"><button data-action="menuToggle"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button></div></div>');
			bMenu_1 = floatMenu.querySelector('.menu');
		}

		// Botones
		if ( pSettings.actions ) {
			for ( const [action, cAct] of Object.entries(pSettings.actions) ) {
				if ( 'menu' in cAct ) {
					createBtn(cAct['menu']['title'], action, cAct['menu']['icon'], bMenu_1);
				}
			}
		}

		//
		if ( pSettings.saveUrl !== '' ) {
			// Buton de gaurdar
			createBtn('Guardar', 'save', 'fa-save', bMenu_1);
			// Response
			plugResponse = bMenu_1.se_site_coreend('<output></output>');

			// Naturaleza plugElemos
			plugElem.querySelectorAll('[data-edelem="obj"]').se_each(
					(index, cElem) => {
					//
					switch ( cElem.se_data('type') ) {
						//
						case 'hidden':
							break;
						//
						case 'simple':
							cElem.se_classAdd('html5Wysiwyg').se_attr('contenteditable', true);
							break;
						//
						case 'complex':
							let settings = cElem.se_attr('se-settings');
							if ( settings ) {
								try {
									settings = JSON.parse(settings.replace(/'/ig, '"'));
								} catch(e) {
									console.log('Plugin Load - Not valid', e);
									settings = {};
								}
							} else {
								settings = {};
							}
							cElem.se_plugin('wysiwyg', settings);
							break;
						//
						default:
							console.warn('Elemento: ' + cElem.se_data('type'));
							break;
					}
				}
			);
			//
		} else { console.log("Vínculo para guardar no definido."); }

		// Bindings
		floatMenu.se_on('click', 'button[data-action]', btnClick);
	}

	//<editor-fold desc="Private functions">

	//
	function btnClick(ev, target) {
		ev.stopPropagation();
		let actionName = target.se_data('action');
		switch ( target.se_data('action') ) {
			// Hide
			case 'menuToggle':
				if ( pStatus.open ) {
					pStatus.open = false;
					floatMenu.se_classDel('show');
					floatMenu.querySelector('.tab use').se_attr('xlink:href', '#fa-pencil');
				} else {
					pStatus.open = true;
					floatMenu.se_classAdd('show');
					floatMenu.querySelector('.tab use').se_attr('xlink:href', '#fa-remove');
				}
				break;
			// Guardar
			case 'save':
				saveAll();
				break;
			//
			default:
				let shortElem = pSettings.actions[actionName];
				switch ( shortElem['action'] ) {
					// Formulario
					case 'form':
						if ( 'form' in shortElem ) {
							let cForm = shortElem['form'],
								formLoadData = ( 'load_data' in cForm ) ? cForm['load_data'] : null,
								formSaveData = ( 'save_data' in cForm ) ? cForm['save_data'] : null;
							//
							formLoadData = se.object.merge(pSettings.act_defaults, formLoadData);
							formSaveData = se.object.merge(pSettings.act_defaults, formSaveData);

							// Llamado
							se.addon.windowForm({
								title:cForm['title'],
								load_url:( 'load_url' in cForm ) ? cForm['load_url'] : null,
								load_data:formLoadData,
								save_url:( 'save_url' in cForm ) ? cForm['save_url'] : null,
								save_data:formSaveData,
								elems:cForm['elems'],
								actName:( 'actName' in cForm ) ? cForm['actName'] : null,
								//
								onLoad:( 'onLoad' in cForm ) ? cForm['onLoad'] : null,
								onRequest:( 'onRequest' in cForm ) ? cForm['onRequest'] : null,
								onComplete:( 'onComplete' in cForm ) ? cForm['onComplete'] : null,
								onSuccess:( 'onSuccess' in cForm ) ? cForm['onSuccess'] : null,
								onFail:( 'onFail' in cForm ) ? cForm['onFail'] : null,
							});
						} else { console.log('Formulario no definido'); }
						break;
					// Función
					case 'function':
						let selected;
						if ( typeof shortElem['function'] === 'function' ) {
							shortElem['function'](pSettings.act_defaults);
						} else {
							console.log("Operación no válida.");
						}
						break;
					//
					default:
						console.log("asdf");
						break;
				}
				break;
		}
	}

	//
	function textEnable() {}

	//
	function textDisable() {}

	//
	function createBtn(name, action, icon, curMenu) {
		let btnTxt = '<button class="menuBtn" title="' + name + '" data-action="'+action+'">';
		btnTxt+= ( typeof icon !== 'undefined' && icon !== '' ) ? '<svg class="icon inline mr"><use xlink:href="#' + icon + '"></use></svg>' : '';
		btnTxt+= name + '</button>';
		curMenu.insertAdjacentHTML('beforeend', btnTxt);
	}

	//
	function saveAll() {
		let curData = se.object.merge(pSettings.act_defaults, getData());
		se.ajax.json(pSettings.saveUrl, curData, {
			response:plugResponse
		});
	}

	//
	function getData() {
		let curData = {};
		plugElem.querySelectorAll('[data-edelem="obj"]').se_each((index, cElem) => {
			let name = cElem.se_data('name');
			switch ( cElem.se_data('type') ) {
				case 'hidden':
					curData[name] = cElem.se_data('value');
					break;
				case 'simple':
					curData[name] = cElem.se_html().replace(/<(div|p|br)/gi, '\n<$1').replace(/(<([^>]+)>)/ig, "").trim();
					break;
				case 'complex':
					curData[name] = cElem.wysiwyg.getHTML();
					break;
				default:
					console.log('Error data: ' + cElem.se_data('type'));
					break;
			}
		});
		return curData;
	}

	//
	function menuClick(event, target) {
		event.stopPropagation();
		// Buscar action relacionada
		let actionName = target.se_data('butop');
		if ( actionName === 'save' ) {
			//
			saveAll();
		} else {
			// Otra acción
			let shortElem = pSettings.actions[actionName];
			switch ( shortElem['action'] ) {
				case 'form':
					if ( 'form' in shortElem ) {
						let cForm = shortElem['form'],
							formLoadUrl = ( 'load_url' in cForm ) ? cForm['load_url'] : null,
							formLoadData = ( 'load_data' in cForm ) ? cForm['load_data'] : null,
							formSaveUrl = ( 'save_url' in cForm ) ? cForm['save_url'] : null,
							formSaveData = ( 'save_data' in cForm ) ? cForm['save_data'] : null,
							actName = ( 'actName' in cForm ) ? cForm['actName'] : null,
							onSuccess = ( 'onSuccess' in cForm ) ? cForm['onSuccess'] : null;
						//
						formLoadData = se.object.merge(pSettings.act_defaults, formLoadData);
						formSaveData = se.object.merge(pSettings.act_defaults, formSaveData);
						// Llamado
						se.addon.windowForm({
							title:cForm['title'],
							load_url:formLoadUrl,
							load_data:formLoadData,
							save_url:formSaveUrl,
							save_data:formSaveData,
							elems:cForm['elems'],
							actName:actName,
							onSuccess:onSuccess
						});
					} else { console.log('Formulario no definido'); }
					break;
				// Función
				case 'function':
					let selected;
					if ( typeof shortElem['function'] === 'function' ) {
						shortElem['function'](pSettings.act_defaults);
					} else {
						console.log("Operación no válida.");
					}
					break;
				default:
					console.log('menu error', cElem);
					break;
			}
		}
	}
	//</editor-fold

	//
	init();
	//
	return {};
};

//