"use strict";
/*
 * Change [data-animate] property when element enters screen, animation handled trough CSS by changing element from [data-animate="0"] to [data-animate="1"].
 */
customElements.define('se-animate-on-appear', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		const observer = new IntersectionObserver((entries) => {
			entries.forEach((cEl) => {
				if ( cEl.isIntersecting ) {
					cEl.target.dataset.animate = '1';
				} else if ( this.dataset.mode !== 'once' ) {
					cEl.target.dataset.animate = '0';
				}
			});
		});

		const animationElements = this.querySelectorAll('[data-animate]');
		animationElements.forEach((el) => { observer.observe(el); });
	}
});