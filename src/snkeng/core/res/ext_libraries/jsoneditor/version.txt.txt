v: 7.0.4
d: 2019-09-11
s: https://cdnjs.com/libraries/jsoneditor

Customizations for compatibility:
- change icons file location:
    img/jsoneditor-icons.svg
    /se_core/res/ext_libraries/jsoneditor/image/jsoneditor-icons.svg