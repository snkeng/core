//
export default class websockets {
	pDefaults = {
		'url':null,
		'autoStart':false,
		'reconnect':true,
		'reconnectMaxTime':16000, // time in milliseconds
		'onConnect':null,
		'onDisconnect':null,
		'onError':null,
		'onMessage':null
	};
	pSettings;
	//
	wsSocket;
	debugging = false;
	//
	status = {
		connected:false,
		askClose:false,
		reconnectTime:0,
		reconnectAttempts:0
	};

	constructor(cOptions) {
		this.pSettings = {...this.pDefaults, ...cOptions};

		//
		if ( this.pSettings.autoStart ) {
			this.ws_connect();
		}
	}

	//
	ws_connect() {
		if ( !this.pSettings.url ) {
			this.ws_log('error', 'Error: url not defined', this.pSettings);
			return;
		}

		//
		try {
			//
			this.wsSocket = new WebSocket(this.pSettings.url);
			this.wsSocket.onopen = this.ws_open.bind(this);
			this.wsSocket.onmessage = this.ws_messageIncomming.bind(this);
			this.wsSocket.onclose = this.ws_close.bind(this);
			this.wsSocket.onerror = this.ws_error.bind(this);
		}
		catch(ex) {
			console.error("No connection to server", this.pSettings.url, ex);
		}
	}

	//
	ws_messageIncomming(msg) {
		let msgData;
		if ( msg.data.length === 0 ) {
			console.warn("incomming message empty");
		}
		//
		try {
			msgData = JSON.parse(msg.data);
		}
		catch ( err ) {
			console.log(err, msg);
			return;
		}

		//
		this.ws_log('log', 'Message IN', msgData);

		if ( typeof this.pSettings.onMessage === "function" ) {
			this.pSettings.onMessage(msgData);
		}
	}

	//
	ws_messageSendByteArray(cMsg) {
		this.ws_log('log', 'Message OUT', cMsg);
		//
		this.wsSocket.send(cMsg);
	}
	//
	ws_messageSendJSON(cMsg) {
		this.ws_log('log', 'Message OUT', cMsg);
		//
		this.wsSocket.send(JSON.stringify(cMsg));
	}

	//
	ws_open(data) {
		this.ws_log('log', 'CONNECT:', data);
		//
		this.status.reconnectAttempts = 0;
		this.status.connected = true;

		//
		if ( typeof this.pSettings.onConnect === "function" ) {
			this.pSettings.onConnect(data);
		}
	}

	//
	ws_close(e) {
		this.status.connected = false;
		if ( typeof this.pSettings.onDisconnect === "function" ) {
			this.pSettings.onDisconnect(e);
		}

		//
		if ( !e.wasClean ) {
			this.ws_log('error', 'CONNECTION INTERRUPTED', e);
		}

		//
		if ( !e.wasClean && this.pSettings.reconnect ) {
			let cTime = ( ( this.status.reconnectAttempts * 3 ) + 2 ) * 1000;
			cTime = ( cTime > this.pSettings.reconnectMaxTime ) ? this.pSettings.reconnectMaxTime : cTime;

			//
			this.ws_log('log', 'RECONNECT ATTEMPT', this.status.reconnectAttempts, cTime);
			console.log('RECONNECT IN: %dms', cTime);

			//
			this.status.reconnectAttempts++;

			//
			setTimeout(this.ws_connect.bind(this), cTime);
		}
	}

	//
	ws_error(data) {
		this.ws_log('error', 'HARD ERROR', data);
		this.status.connected = false;
		if ( typeof this.pSettings.onError === "function" ) {
			this.pSettings.onError(data);
		}
	}

	//
	ws_forceClose() {
		this.pSettings.reconnect = false;
		this.wsSocket.onclose = null;
		this.wsSocket.close(1000);
	}

	//
	ws_log(type, title) {
		if ( !this.debugging && type !== 'error') {
			return;
		}

		let cTitle = 'WS - ' + title,
			argList = [];

		for ( let i = 2; i < arguments.length; i++ ) {
			argList.push(arguments[i]);
		}

		//
		switch (type) {
			//
			case 'log':
				console.log(cTitle, argList);
				break;
			//
			case 'error':
				console.error(cTitle, argList);
				break;
			//
			default:
				console.error('WS - INCORRECT DATA')
				break;
		}

	}
}
//
