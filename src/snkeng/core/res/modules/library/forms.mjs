//
export function getValue(cEl) {
	// Type
	switch ( cEl.tagName ) {
		//
		case 'INPUT':
			switch ( cEl.type ) {
				//
				case 'number':
					return parseFloat(cEl.value);
					break;
				//
				case 'radio':
					return cEl.form.elements.namedItem(cEl.name).value;
					break;
				//
				case 'checkbox':
					if ( cEl.value === '1' ) {
						return ( cEl.checked ) ? 1 : 0;
					} else {
						return cEl.value;
					}
					break;
				//
				default:
					return cEl.value;
					break;
			}
			break;
		//
		case 'SELECT':
			console.log("select", cEl);
			if ( cEl.multiple ) {
				console.log("multiple");
				let tResult = [],
					optList = cEl.options, cOpt, j,
					jLen = optList.length;
				//
				for ( j = 0; j < jLen; j++ ) {
					cOpt = optList[j];
					if ( cOpt.selected ) {
						tResult.push(cOpt.value || cOpt.text);
					}
				}
				return tResult;
			} else {
				return cEl.value;
			}
			break;
		//
		default:
			return cEl.value;
			break;
	}
}

//
export function setValue(elem, cVal) {
	if ( elem.tagName === 'INPUT' && elem.type === 'checkbox' ) {
		elem.checked = ( parseInt(cVal) === 1 || cVal === true );
	} else if ( elem.tagName === 'INPUT' && elem.type === 'number' && elem.dataset['type'] === 'moneyInt' ) {
		elem.value = parseInt(cVal) / 100;
	} else if ( elem.tagName === 'INPUT' && elem.type === 'number' ) {
		elem.value = parseInt(cVal);
	} else if ( elem.tagName === 'TEXTAREA' && elem.dataset['mode'] === 'textareaesp' ) {
		elem.value = cVal;
		let evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		elem.dispatchEvent(evt);
	} else if ( elem.tagName === 'SELECT' && elem.multiple && cVal && cVal instanceof Array ) {
		let i, iLen = cVal.length,
			options = elem.options, cOption,
			j, jLen = options.length;
		//
		for ( i = 0; i < iLen; i++ ) {
			for ( j = 0; j < jLen; j++ ) {
				cOption = elem.options[j];
				if ( cOption.value === cVal[i] ) {
					cOption.selected = true;
				}
			}
		}
	} else {
		elem.value = cVal;
	}
}

//
export function formSetData(cForm, cData, resetForm = false) {
	//
	if ( resetForm ) {
		cForm.reset();
	}

	//
	for ( let cName in cData ) {
		if ( !cData.hasOwnProperty(cName) ) { continue;	}
		//
		let cVal = cData[cName],
			elem = cForm.elements.namedItem(cName);
		//
		if ( !elem ) {
			continue;
		}

		//
		setValue(elem, cVal);
	}
}

//
export function serializeFormToJSON(cForm, excludeDisabled, variableProcessing) {
	let json = {},
		patterns = {
			"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)])*$/,
			"key": /[a-zA-Z0-9_]+|(?=\[])/g,
			"push": /^$/,
			"fixed": /^\d+$/,
			"named": /^[a-zA-Z0-9_]+$/
		},
		elements, len;

	if ( !( cForm instanceof HTMLFormElement ) ) {
		console.error("Inserted type is not form", cForm);
		return;
	}

	//
	if ( excludeDisabled ) {
		elements = cForm.querySelectorAll('input:enabled, select:enabled, textarea:enabled');
	} else {
		elements = cForm.elements;
	}
	len = elements.length

	//
	for ( let i = 0; i < len; i++ ) {
		let cEl = elements[i];
		// Only process valid elements
		if ( cEl.name && patterns.validate.test(cEl.name) ) {
			let k,
				keys = cEl.name.match(patterns.key),
				merge = getValue(cEl);

			// Loop building
			let cPos = json;
			while ( ( k = keys.shift() ) !== undefined ) {
				if ( !cPos.hasOwnProperty(k) ) {
					cPos[k] = {};
				}

				// Check if last
				if ( keys.length === 0 ) {
					// Asign value
					cPos[k] = merge;
				} else {
					// Advance pointer
					cPos = cPos[k];
				}
			}
		}
	}

	if ( variableProcessing ) {
		for ( let pType in variableProcessing ) {
			if ( !variableProcessing.hasOwnProperty(pType) ) {
				continue;
			}

			//
			for ( let cEl of variableProcessing[pType] ) {
				if ( !json.hasOwnProperty(cEl) ) {
					continue;
				}

				//
				switch ( pType ) {
					case 'int':
						json[cEl] = parseInt(json[cEl]) || 0;
						break;
					case 'float':
						json[cEl] = parseFloat(json[cEl]) || 0;
						break;
				}
			}
		}
	}

	//
	return json;
}

//
export function serializeFormToObject(cForm, excludeDisabled, extraOps = null) {
	let elements,
		data = {};

	if ( !( cForm instanceof HTMLFormElement ) ) {
		console.error("Inserted type is not form", cForm);
		return;
	}

	//
	excludeDisabled = ( typeof excludeDisabled === 'undefined' ) ? false : excludeDisabled;
	if ( excludeDisabled ) {
		elements = cForm.querySelectorAll('input:enabled, select:enabled, textarea:enabled');
	} else {
		elements = cForm.elements;
	}

	//
	if ( !elements ) {
		console.error('form.serializeToObj - Formulario vació');
		return data;
	}

	//
	for ( let i = 0; i < elements.length; i++ ) {
		let cEl = elements[i];
		if ( !cEl.name ) {
			console.warn('form.serializeToObj - Elemento del formulario sin nombre. Saltar', cEl);
			continue;
		}

		data[cEl.name] = getValue(cEl);
	}

	//
	return data;
}

//
export function convertFormToURLParams(form) {
	//
	const formData = new FormData(form);
	//
	const urlParams = new URLSearchParams();

	for ( const [key, value] of formData.entries() ) {
		urlParams.append(key, value);
	}

	return urlParams.toString();
}


//
export async function postRequest(targetUrl, postData, configuration) {
	const defaults = {
		method: 'POST',
		response: null,
		onRequest: null,
		onComplete: null,
		onSuccess: null,
		onFail: null,
		onError: null,
	};
	//
	let settings = Object.assign(defaults, configuration);

	// Actual parameters
	let fetchParams = {
		headers: {},
		method: settings.method,
		body: null
	};

	//
	if ( postData instanceof FormData ) {
		fetchParams.body = postData;
	} else if ( settings.method === 'get' ) {
		// Convert to json and send?
		targetUrl += '?' + new URLSearchParams(postData).toString();
	} else if ( typeof postData === 'object' ) {
		//
		fetchParams.headers["Content-type"] = "application/json";

		//
		if ( postData !== null ) {
			fetchParams.body = JSON.stringify(postData);
		} else {
			fetchParams.body = '{}';
		}
	} else {
		console.error("Type of post data not supported", typeof postData, postData);
		return;
	}

	//
	if ( typeof settings.onRequest === 'function' ) {
		settings.onRequest();
	}

	//
	let fResponse,
		notificationData = {
			title: '',
			content: '',
			style: 'notification ',
			icon: '',
			autoClear: null
		};

	//
	function endingRequest(notificationData) {
		//
		if ( typeof settings.onComplete === 'function' ) {
			settings.onComplete(msg);
		}

		//
		buildNotification(settings.response, notificationData);
	}

	//
	try {
		fResponse = await fetch(
			targetUrl,
			fetchParams
		);
	} catch ( e ) {
		console.error("Could not fetch request.", e);

		//
		if ( typeof settings.onError === 'function' ) {
			settings.onError();
		}

		//
		notificationData.title = "Could not fetch";
		notificationData.content = e;
		notificationData.style = "notification error";
		notificationData.icon = "fa-alert";

		//
		endingRequest(notificationData)

		return;
	}

	let msg;

	try {
		msg = await fResponse.json();
	} catch ( error ) {
		if ( error instanceof SyntaxError ) {
			// Unexpected token < in JSON
			console.error("There was a SyntaxError.\n", error);
		} else {
			console.error("There was an error.\n", error);
		}

		//
		if ( typeof settings.onError === 'function' ) {
			settings.onError();
		}

		//
		notificationData.title = "Parsing error";
		notificationData.content = error;
		notificationData.style = "notification error";
		notificationData.icon = "fa-alert";

		//
		endingRequest(notificationData)

		return;
	}

	//
	if ( !fResponse.ok ) {
		// Successful request.
		notificationData.title = ( 'title' in msg ) ? 'ERROR: ' + msg.title : 'ERROR: Caso no definido';
		notificationData.content = ( 'description' in msg ) ? msg.description : ( 'timestamp' in msg ) ? msg.timestampt : '';
		notificationData.style = 'notification error';
		notificationData.icon = 'fa-remove';

		//
		if ( typeof settings.onFail === 'function' ) {
			settings.onFail(msg);
		}

		//
		if ( 'errors' in msg ) {
			console.error("asdf");
		}

		//
		endingRequest(notificationData)

		return;
	}

	// Successful request.
	notificationData.title = msg.title ?? 'Enviado';
	notificationData.content = msg.description ?? msg.timestamp ?? '';
	notificationData.style = 'notification success';
	notificationData.icon = 'fa-check';
	notificationData.autoClear = 10000;

	//
	if ( typeof settings.onSuccess === 'function' ) {
		settings.onSuccess(msg);
	}

	endingRequest(notificationData);
}

//
export async function formSubmit(cForm, cParameters) {
	const defaults = {
		method: 'POST',
		response: null,
		onComplete: null,
		onSuccess: null,
		onFail: null,
		onError: null,
	};

	//
	let settings = Object.assign(defaults, cParameters);

	// Check if output exists or add the form one
	if ( !( settings.response instanceof HTMLElement ) ) {
		settings.response = cForm.querySelectorAll('output');
	}

	// Check submition method (if not default)
	if ( cForm.method ) {
		settings.method = cForm.method;
	}

	//
	if ( !cForm.action ) {
		console.error("Form could not be submited. No action.", cForm);
		return;
	}

	//
	postRequest(
		cForm.action,
		new FormData(cForm),
		settings
	);
}

//
export function setAsyncForm(cForm, cParameters) {
	// Add a binding and process as a form (avoid default)
	cForm.addEventListener('submit', (e) => {
		e.preventDefault();
		formSubmit(cForm, cParameters);
	});
}

//
export function buildNotification(cEl, cOptions) {
	let defaults = {
			icon: null,
			iconMod: null,
			title: null,
			content: '',
			contentHTML: false,
			style: null,
			autoClear: null
		},
		settings = {...defaults, ...cOptions},
		//
		html = '',
		clearInterval;

	//
	if ( !cEl ) {
		return;
	}

	//
	if ( settings.icon ) {
		html += '<div class="icon">';
		if ( settings.icon.indexOf('.') === -1 ) {
			settings.iconMod = ( typeof settings.iconMod === 'string' ) ? settings.iconMod : '';
			html += '<svg class="' + settings.iconMod + '"><use xlink:href="#' + settings.icon + '" /></svg>';
		} else {
			console.log("NOTIFICACION, IMAGEN NO PROGRAMADA");
		}
		html += '</div>';
	}
	// Contenido
	let htmlContent = ( settings.contentHTML ) ? ' html' : '';
	html += '<div class="text"><div class="title">' + settings.title + '</div><div class="content' + htmlContent + '">' + settings.content + '</div></div>';

	// Apply
	cEl.className = settings.style;
	cEl.innerHTML = html;

	//
	if ( typeof settings.autoClear === 'number' && settings.autoClear !== 0 ) {
		clearInterval = setTimeout(() => {
			cEl.innerHTML = '';
		}, settings.autoClear);
	} else {
		clearTimeout(clearInterval);
	}
}

//
HTMLFormElement.prototype.se_elementValue = function (elName, cVal) {
	let cEl = this.elements.namedItem(elName);
	//
	if ( !cEl ) {
		console.trace("Form El Val, undefined element:", elName);
		return;
	}

	//
	if ( typeof cVal !== 'undefined' ) {
		setValue(cEl, cVal);
	} else {
		return getValue(cEl);
	}
};

//
HTMLFormElement.prototype.se_serializeToJSON = (excludeDisabled, variableProcessing) => {
	return serializeFormToJSON(this, excludeDisabled, variableProcessing);
};

//
HTMLFormElement.prototype.se_setData = function (cData) {
	formSetData(this, cData);
};


/**
 * Get de element from a form
 * @param {string} elName Form element to search
 */
Node.prototype.se_formEl = function (elName) {
	return this.elements.namedItem(elName);
};

/**
 * Update the element in a form
 * @param {string} elName Form element to search
 * @param {string|number} cVal optional assign value
 */
Node.prototype.se_formElVal = function (elName, cVal) {
	let cEl = this.elements.namedItem(elName);
	if ( !cEl ) {
		console.error("Form element not found in :", elName, this);
	}

	if ( typeof cVal === 'undefined' ) {
		return getValue(cEl);
	} else {
		setValue(cEl, cVal);
	}
}