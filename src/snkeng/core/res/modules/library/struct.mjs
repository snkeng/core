//
export function addElementsFromTemplateString(domTarget, referenceStr, cPosition, cContent) {
	if ( Array.isArray(cContent) ) {
		for ( let cEl of cContent ) {
			domTarget.insertAdjacentHTML(cPosition, stringPopulate(referenceStr, cContent));
		}
	} else {
		domTarget.insertAdjacentHTML(cPosition, stringPopulate(referenceStr, cContent));
	}
}

export function stringPopulate(cString, cData) {
	let value, re;
	for ( let cName in cData ) {
		if ( !cData.hasOwnProperty(cName) ) { continue; }
		value = ( cData[cName] !== null ) ? cData[cName] : '';
		re = new RegExp('!' + cName + ';', "g");
		cString = cString.replace(re, value);
	}

	//
	return cString;
}

export function stringPopulateMany(cString, cData, modFunction = null) {
	let rString = "";

	// Loop each data row
	for ( let rowCur of cData ) {
		// Mod current row
		if ( typeof modFunction === 'function' ) {
			modFunction(rowCur);
		}

		rString+= stringPopulate(cString, rowCur);
	}

	//
	return rString;
}

export function fixInputValues(domTarget) {
	let elementsToFix = domTarget.querySelectorAll('[se-fix]');
	//
	if ( !elementsToFix ) {
		return;
	}

	//
	for ( let cEl of elementsToFix ) {
		let fixVal = cEl.getAttribute('se-fix');
		// Only select, radio and checkbox should be affected
		switch ( cEl.tagName.toLowerCase() ) {
			//
			case 'select':
				cEl.value = fixVal;
				break;
			//
			case 'input':
				switch ( cEl.type) {
					//
					case 'checkbox':
						fixVal = parseInt(fixVal);
						cEl.checked = ( fixVal === 1 || fixVal === cEl.value );
						break;
					//
					case 'radio':
						let cForm = cEl.closest('form'),
							nEl = cForm.elements.namedItem(cEl.name);
						//
						if ( !nEl ) {
							console.trace("Form El Val, undefined element:", cEl.name);
							return;
						}

						//
						if ( typeof fixVal !== 'undefined' ) {
							nEl.value = fixVal;
							//
							if ( nEl.length > 1 ) {
								for ( let i = 0; i < nEl.length; i++ ) {
									let curEl = nEl[i];
									if ( curEl.value === fixVal ) {
										curEl.checked = true;
										break;
									}
								}
							} else if ( nEl.type.toLowerCase() === 'checkbox' ) {
								nEl.checked = ( fixVal );
							}
							return this;
						}
						break;
					default:
						console.log("AJAXSIMPLE - ElementFix - Input no definido");
						break;
				}
				break;
			default:
				console.log("AJAXSIMPLE - ElementFix - caso no definido", nEl.tagName);
				break;
		}
	}
}

//
export function htmlFromJson(cObj) {
	let cStruct = '';

	if ( typeof cObj !== 'object' ) {
		console.error("struct from json, error. type not valid", cObj);
	}

	//
	if ( cObj instanceof Array ) {
		for ( const cEl of cObj ) {
			cStruct+= htmlFromJson(cEl);
		}
	} else {
		let iniTag = cObj.tag;

		// Set attributes
		if ( cObj.hasOwnProperty('attr') ) {
			for ( const aName in cObj.attr ) {
				if ( !cObj.attr.hasOwnProperty(aName) ) { continue; }
				iniTag+= ' ' + aName + '="' + cObj.attr[aName] + '"';
			}
		}

		if ( cObj.hasOwnProperty('children') ) {
			let cBody = '';
			for ( const cEl of cObj.children) {
				cBody+= htmlFromJson(cEl);
			}
			//
			cStruct+= `<${ iniTag }>${ cBody }<${ cObj.tag }>`
		} else if ( cObj.hasOwnProperty('content') ) {
			//
			cStruct+= `<${ iniTag }>${ cObj['content'] }<${ cObj.tag }>`
		} else {
			cStruct+= `<${ iniTag } />`
		}
	}

	//
	return cStruct;
}

//
