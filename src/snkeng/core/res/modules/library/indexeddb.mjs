//
class indexeddb {
	dbObject = null;
	//
	constructor(idbStructure) {
		return new Promise((resolve, reject) => {
			//
			if ( idbStructure.reset ) {
				window.indexedDB.deleteDatabase(idbStructure.name).onsuccess = () => {
					console.log("IDB - DELETED. STARTING FROM SCRATCH.");
				};
			}
			//
			let openRequest = window.indexedDB.open(idbStructure.name, idbStructure.version),
				dbCreate;

			//
			openRequest.onupgradeneeded = (e) => {
				let thisDB = e.target.result,
					transactionObj = e.currentTarget.transaction,
					cDBNames = thisDB.objectStoreNames,
					tDBNames = Object.getOwnPropertyNames(idbStructure.structure),
					curDBOName,
					tDBICur,
					index;

				//
				console.log("IDB Update. %d -> %d", e.oldVersion, e.newVersion);

				// Delete operations
				for ( index = 0; index < cDBNames.length; index++ ) {
					const curDBOName = cDBNames[index];
					//
					if ( tDBNames.indexOf(curDBOName) === -1 ) {
						thisDB.deleteObjectStore(curDBOName);
					}
				}

				// Read new list for add/update
				for ( curDBOName in idbStructure.structure ) {
					if ( !idbStructure.structure.hasOwnProperty(curDBOName) ) {
						console.warn("IDB - but how?");
						continue;
					}
					let curDBData = idbStructure.structure[curDBOName];
					//
					if ( cDBNames.contains(curDBOName) ) {
						console.log("IDB - DB OBJECT CHECK UPDATE", curDBOName);
						// It exists, check updates
						let idbObject = transactionObj.objectStore(curDBOName),
							nIndexes = Object.getOwnPropertyNames(curDBData.indexes),
							oIndexes = idbObject.indexNames,
							idbOIndex;
						// Check for delete
						for ( index = 0; index < oIndexes.length; index++ ) {
							const idbOIndex = oIndexes[index];
							if ( nIndexes.indexOf(idbOIndex) === -1 ) {
								idbObject.deleteIndex(idbOIndex);
							}
						}
						// Check for add
						for ( index = 0; index < oIndexes.length; index++ ) {
							const idbOIndex = oIndexes[index];
							if ( !oIndexes.contains(idbOIndex) ) {
								let nIndexData = curDBData.indexes[idbOIndex];
								idbObject.createIndex(idbOIndex, nIndexData.keyPath, nIndexData.params);
							}
						}
						// check for update...?
					} else {
						// Not exist, add
						dbCreate = thisDB.createObjectStore(curDBOName, curDBData.build);
						for ( tDBICur in curDBData.indexes ) {
							if ( !curDBData.indexes.hasOwnProperty(tDBICur) ) {
								console.warn("IDB - but how?");
								continue;
							}
							let nIndexData = curDBData.indexes[tDBICur];
							dbCreate.createIndex(tDBICur, nIndexData.keyPath, nIndexData.params);
						}
					}
				}
			};

			//
			openRequest.onsuccess = (e) => {
				console.log("IDB - Ready, version: %i", idbStructure.version);
				this.dbObject = e.target.result;
				//
				resolve(this);
			};

			//
			openRequest.onerror = (e) => {
				console.error("IDB - OPEN ERROR: ", e.target.error, e);
				reject(`IDB - OPEN ERROR: ${e.target.error}`);
			};
		});
	}

	//
	close() {
	}

	//
	clearTables(cb) {
		let cTabNames = this.dbObject.objectStoreNames;
		for ( const cTabName of cTabNames ) {
			this.dbObject.transaction( cTabName, "readwrite" ).objectStore( cTabName ).clear();
		}
	}

	//
	clearTable(tableName, cb) {
		let cTabNames = this.dbObject.objectStoreNames;
		this.dbObject.transaction( tableName, "readwrite" ).objectStore( tableName ).clear();

		//
		if ( typeof cb === 'function' ) {
			cb();
		}
	}

	//
	async query(table, options) {
		let defaults = {
				index: null,
				keyRange: null,
				queryDirection: null,
				sort: null,
				get: null,
				onSuccess: null,
				onEmpty: null,
				onComplete: null,
				onError: null
			},
			settings = {...defaults, ...options},
			idbTxn = this.dbObject.transaction(table),
			idbObject = idbTxn.objectStore(table),
			result = [],
			request;

		//
		console.log("IDB - QUERYING", table, options);

		//
		if ( settings.get ) {
			// Caso particular
			request = await idbObject.get(settings.get);
			//
			request.onsuccess = (e) => {
				let result = e.target.result;
				if ( result ) {
					if ( typeof settings.onSuccess === 'function' ) {
						settings.onSuccess(result);
					} else {
						console.log("IDB - Result success: ", result);
					}
				} else {
					if ( typeof settings.onEmpty === 'function' ) {
						settings.onEmpty();
					} else {
						console.log("IDB - Result is empty.", settings);
					}
				}
			};
		} else {
			// Con filtros o sin ellos
			//
			if ( settings.index ) {
				idbObject = idbObject.index(settings.index);
			}
			//
			if ( settings.keyRange ) {
				if ( settings.queryDirection ) {
					request = idbObject.openCursor(settings.keyRange, settings.queryDirection);
				} else {
					request = idbObject.openCursor(settings.keyRange);
				}
			} else {
				request = idbObject.openCursor();
			}
			//
			request.onsuccess = (e) => {
				let cursor = e.target.result;
				if ( cursor ) {
					// Juntar todos los resultados
					result.push(cursor.value);
					cursor.continue();
				} else {
					// console.log("IDB - end", result);
					// Determinar si hay datos
					if ( result.length ) {
						// Contiene resultados
						if ( typeof settings.onSuccess === 'function' ) {
							// Juntar todos los resultados
							if ( settings.sort ) {
								result = this.sort(result, settings.sort.name, settings.sort.direction);
							}
							// regresar resultado
							settings.onSuccess(result);
						} else {
							console.log("IDB - Result success: ", result);
						}
					} else {
						// Esta vacío
						if ( typeof settings.onEmpty === 'function' ) {
							settings.onEmpty();
						} else {
							console.log("IDB - Result is empty.", settings);
						}
					}
				}
			};
		}

		//
		request.onerror = (e) => {
			console.error("IDB - QUERY ERROR (request)", e.target.error, e);
		};
		//
		if ( typeof settings.onComplete === 'function' ) {
			idbTxn.oncomplete = settings.onComplete;
		}
		//
		idbTxn.onerror = (e) => {
			console.error("IDB - QUERY ERROR (txn)", e.target.error, e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError();
			}
		};
	}

	sort(data, key, way = 'ASC') {
		return data.sort(function(a, b) {
			let x = a[key], y = b[key];
			if ( way === 'ASC' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
			else { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
		});
	}

	//
	async queryBatch(queries) {
		return new Promise((mainResolve, mainReject) => {
			let promises = [];

			//
			for ( let cQuery of queries ) {
				promises.push(new Promise(
					(resolve, reject) => {
						// Modify default query operations for new multy query
						cQuery.params.onSuccess = (data) => {
							let newResult = {};
							newResult[cQuery.rName] = data;
							resolve(newResult);
						};
						cQuery.params.onEmpty = () => {
							let newResult = {};
							newResult[cQuery.rName] = null;
							resolve(newResult);
						};
						cQuery.params.onFail = (error) => {
							reject(error);
						};

						//
						this.query(cQuery.table, cQuery.params)
					}
				));
			}

			//
			Promise.all(promises).then(
				(values) => {
					// returned data is in arguments[0], arguments[1], ... arguments[n]
					let result = {};
					for ( let cVal of values ) {
						result = {...result, ...cVal};
					}

					mainResolve(result);
				},
				(err) => {
					console.log("IDB - Query Batch Error, no CB: ", err);
					mainReject("Failed");
				}
			);
		});
	}

	//
	add(table, data, options) {
		if ( typeof options === 'undefined' ) {
			options = {};
		}

		//
		let idbTransaction = this.dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);

		//
		idbTransaction.oncomplete = (event) => {
			if ( typeof options.onComplete === 'function' ) {
				options.onComplete(event);
			}
			console.log("IDB - ADD Complete.", event);
		};

		//
		idbTransaction.onerror = (event) => {
			if ( typeof options.onError === 'function' ) {
				options.onError(event);
			}
			//
			switch ( event.target.error.code ) {
				//
				case 20:
					console.log("IDB - Prev op aborted.");
					break;
				//
				default:
					if ( event.target.error.code !== 20 ) {
						console.error("IDB - ADD Error\nERROR: %s\nName: %s\nMessage: %s\nCode: %s", event.target.error, event.target.error.name, event.target.error.message, event.target.error.code);
					}
					console.log(event);
					break;
			}
		};

		//
		if ( typeof data === 'undefined' ) {
			console.error('IDB - DEL ERROR. Data is undefined');
		} else if ( Array.isArray(data) ) {
			for ( let index in data ) {
				if ( !data.hasOwnProperty(index) ) {
					continue;
				}
				objStore.add(data[index]);
			}
		} else {
			let cOp = objStore.add(data);
			//
			cOp.onsuccess = (event) => {
				if ( typeof options.onSuccess === 'function' ) {
					options.onSuccess(event);
				}
			};
		}
	}

	//
	put(table, data, options) {
		if ( typeof options === 'undefined' ) {
			options = {};
		}
		let idbTransaction = this.dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);
		//
		if ( typeof options.onSuccess === 'function' ) {
			idbTransaction.oncomplete = options.onSuccess;
		}
		if ( typeof data === 'undefined' ) {
			console.error('Data type undefined');
		} else if ( Array.isArray(data) ) {
			for ( let index in data ) {
				if ( !data.hasOwnProperty(index) ) {
					continue;
				}
				objStore.put(data[index]);
			}
		} else {
			objStore.put(data);
		}
	}

	//
	del(table, id, options) {
		let defaults = {
				onSuccess:null,
				onComplete:null,
				onError:null,
			},
			settings = {...defaults, ...options},
			//
			idbTransaction = this.dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);

		//
		if ( typeof settings.onComplete === 'function' ) {
			idbTransaction.oncomplete = settings.onComplete;
		}

		//
		idbTransaction.onerror = (e) => {
			console.error("IDB - DEL ERROR", e.target.error, e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError(e);
			}
		};

		let delOperation = objStore.delete(id);
		//
		if ( typeof settings.onSuccess === 'function' ) {
			delOperation.onsuccess = settings.onSuccess;
		}

	};

	//
	update(table, id, data, options) {
		let defaults = {
				onComplete: null,
				onError: null
			},
			settings = {...defaults, ...options},
			idbTxn = this.dbObject.transaction(table, "readwrite"),
			idbObject = idbTxn.objectStore(table);

		//
		if ( typeof settings.onSuccess === 'function' ) {
			idbTxn.oncomplete = settings.onSuccess;
		}
		//
		idbTxn.onerror = (e) => {
			console.error("IDB - UPDATE ERROR (TXN)", e.target.error, e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError();
			}
		};

		//
		if ( typeof data === 'undefined' ) {
			console.error('IDB - UPDATE ERROR. Incomming data type undefined', data);
		} else {
			let request = idbObject.openCursor(IDBKeyRange.only(id));
			request.onsuccess = (e) => {
				let cursor = e.target.result,
					newData;
				if ( cursor ) {
					newData = {...cursor.value, ...data};
					idbObject.put(newData);
				}
			};
		}
	}

	//
	updateMany(table, indexName, data, options) {
		let defaults = {
				onComplete:null,
				onError:null
			},
			settings = {...defaults, ...options},
			idbTxn = this.dbObject.transaction(table, "readwrite"),
			idbObject = idbTxn.objectStore(table),
			promises = [];

		//
		idbTxn.onerror = (e) => {
			console.log("IDB - Error en la consulta", e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError();
			}
		};

		//
		function updateQuery(id, cData) {
			id = parseInt(id);
			//
			return new Promise((resolve, reject) => {
				let request = idbObject.openCursor(IDBKeyRange.only(id));
				request.onsuccess = function(e) {
					let cursor = e.target.result,
						newData;
					if ( cursor ) {
						newData = {...cursor.value, ...cData};
						idbObject.put(newData);
					}
					resolve();
				};
				request.onerror = function(e) {
					reject(e);
				};
			});
		}

		console.log("IDB - MULTI UPDATE");

		//
		for ( let gsId in data ) {
			if ( !data.hasOwnProperty(gsId) ) { continue; }
			console.log("IDB - ADD (id, value): ", data[gsId][indexName], data[gsId]);
			promises.push(updateQuery(data[gsId][indexName], data[gsId]));
		}

		//
		Promise.all(promises).then(
			() => {
				console.log("IDB - all promised accomplished");
				if ( typeof settings.onSuccess === 'function' ) {
					idbTxn.oncomplete = settings.onSuccess;
				}
			},
			(err) => {
				//
				if ( typeof settings.onSuccess === 'function' ) {
					settings.onError(err);
				} else {
					console.log("IDB - Query Batch Error, no CB: ", err);
				}
			}
		);
	}

	//
	del(table, id, options) {
		let defaults = {
				onSuccess:null,
				onComplete:null,
				onError:null,
			},
			settings = {...defaults, ...options},
			//
			idbTransaction = this.dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);

		//
		if ( typeof settings.onComplete === 'function' ) {
			idbTransaction.oncomplete = settings.onComplete;
		}

		//
		idbTransaction.onerror = (e) => {
			console.log("IDB - Error en la eliminación", e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError(e);
			}
		};

		let delOperation = objStore.delete(id);
		//
		if ( typeof settings.onSuccess === 'function' ) {
			delOperation.onsuccess = settings.onSuccess;
		}

	};

	//
	rebuild(table, cData, options) {
		//
		if ( typeof cData === 'undefined' ) {
			console.error('Data type undefined');
			return;
		}
		//
		let idbTransaction = this.dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);

		// Informa sobre el status de la transacción
		idbTransaction.oncomplete = (event) => {
			if ( typeof options.onComplete === 'function' ) {
				options.onComplete(event);
			}
			console.log("IDB - Transacción se llevó a cabo con éxito.", event);
		};

		idbTransaction.onerror = (event) => {
			if ( typeof options.onError === 'function' ) {
				options.onError();
			}
			let error = event.target.error;
			console.log('IndexedDb.rebuild -> request -> onerror', error, error.name, event);
		};

		// Clear
		objStore.clear();
		// Agregar elementos
		for ( let j in cData ) {
			if ( !cData.hasOwnProperty(j) ) {
				continue;
			}
			//
			objStore.add(cData[j]);
			//
			objStore.onsuccess = (event) => {
				console.log("IDB - adding element: ", event);
			}
		}
	}

	//
	syncData(inData) {
		let dbTXN, dbStore, name, j, cData;
		console.log("IDB - DB Sync");
		//
		for ( name in inData ) {
			if ( !inData.hasOwnProperty(name) ) { continue; }
			cData = inData[name];
			if ( !cData ) { continue; }

			//
			console.log("IDB: ", name, cData);

			// Elemento de grabación
			dbTXN = this.dbObject.transaction(name, "readwrite");

			//
			dbTXN.onerror = (e) => {
				let error = e.target.error;
				//
				if ( error.name === 'AbortError' ) { return false; }
				//
				console.log("IDB - Error en la transacción...", error);
			};

			//
			dbStore = dbTXN.objectStore(name);
			dbStore.clear();

			// Agregar elementos
			for ( j in cData ) {
				if ( cData.hasOwnProperty(j) ) {
					dbStore.add(cData[j]);
				}
			}

		}
	}
}
//

//
export { indexeddb };