// Basic appending to object


/**
 * Insert dom elements from strings
 * @param {Node|string} mainObject the object that will be adding sub elements
 * @param {"afterbegin" | "afterend" | "beforebegin" | "beforeend"} position to add elements
 * @param {string} template
 * @param {Array} content
 * @param {function} modFunction
 */
export function insertContentFromTemplate(mainObject, position, template, content, modFunction = null) {
	try {
		mainObject.insertAdjacentHTML(position, stringPopulate(template, content, modFunction));
	} catch(e) {
		console.error(e);
	}
}

/**
 * String replace function to modify template to actual content
 * @param {string} template
 * @param {Array} content
 * @param {function} modFunction
 */
export function stringPopulate(template, content, modFunction) {
	//
	if ( !Array.isArray(content) ) {
		content = [content];
	}

	if ( template instanceof HTMLElement ) {
		template = template.innerHTML;
	}

	let rString = "", cMod = "",
		cElId;
	for ( let rowCur of content ) {
		//
		cMod = template;

		// Mod current row
		if ( typeof modFunction === 'function' ) {
			modFunction(rowCur);
		}
		// Convert all elements
		for ( cElId in rowCur ) {
			if ( !rowCur.hasOwnProperty(cElId) ) { continue; }
			//
			let cEl = ( rowCur[cElId] !== null ) ? rowCur[cElId] : '',
				re = new RegExp('!' + cElId + ';', "g");
			//
			cMod = cMod.replace(re, cEl);
		}
		rString += cMod;
	}

	//
	return rString;
}


export function buildFromObject(cObj){
	let html = '',
		attr = '',
		content = '';

	//
	for ( let cName in cObj.attr ) {
		let cValue = cObj.attr[cName];
		if ( !cValue ) { continue; }
		//
		attr+= ` ${cName}="${cValue}"`;
	}

	html = `<${cObj.type}${attr}>`;

	//
	if ( typeof cObj.content === "undefined" || !cObj.content || cObj.content === '' ) {

	} else if ( typeof cObj.content === 'string' ) {
		content = cObj.content;
	} else if ( Array.isArray(cObj.content) ) {
		for ( let cObjEl of cObj.content ) {
			content+= buildFromObject(cObjEl);
		}
	}

	//
	html+= `${content}</${cObj.type}>`;

	//
	return html;
}


//
function elementUpdate(cEl, cOp) {
	//
	switch ( cOp[0] ) {
		case 'data':
			if ( cOp[2] === null ) {
				delete cEl.dataset[cOp[1]];
			} else {
				cEl.dataset[cOp[1]] = cOp[2];
			}
			break;
		case 'attr':
		case 'attribute':
			if ( cOp[2] === null || cOp[2] === false ) {
				cEl.removeAttribute(cOp[1]);
			} else if( typeof cOp[2] === 'boolean' ) {
				if ( cOp[2] ) {
					cEl.setAttribute(cOp[1], cOp[1]);
				} else {
					cEl.removeAttribute(cOp[1]);
				}
			} else {
				cEl.setAttribute(cOp[1], cOp[2]);
			}
			break;
		case 'class':
			switch ( cOp[1] )
			{
				case 'set':
					cEl.className = cOp[2];
					break;
				case 'add':
					cEl.classList.add(cOp[2]);
					break;
				case 'del':
				case 'remove':
					cEl.classList.remove(cOp[2]);
					break;
				case 'replace':
					cEl.classList.replace(cOp[2], cOp[3]);
					break;
			}
			break;
		case 'style':
			switch ( typeof(cOp[1]) )
			{
				case 'object':
					for ( let cStyle in cOp[1] ) {
						if ( cOp[1].hasOwnProperty(cStyle) ) {
							cEl.style[cStyle] = ( typeof cOp[1][cStyle] !== 'undefined' ) ? cOp[1][cStyle] : null;
						}
					}
					break;
				case 'string':
					if ( typeof(cOp[2]) === 'undefined' ) {
						cEl.style[cOp[1]];
					} else {
						cEl.style[cOp[1]] = cOp[2];
					}
					break;
				default:
					break;
			}
			break;
		case 'text':
			cEl.textContent = cOp[1];
			break;
		case 'html':
			cEl.innerHTML = cOp[1];
			break;
		//
		case 'func':
			if ( typeof cOp[1] !== 'function' ) {
				console.error("function improperly defined");
				return;
			}

			cOp[1](cEl);
			break;

		//
		default:
			console.error("not valid operation", cOp)
			break;
	}
}

//
export function updateElement(cElement, cOperations) {
	if ( !(cElement instanceof Node ) ) {
		console.error("Invalid element.", cElement);
		return;
	}

	// Many operations
	if ( Array.isArray(cOperations[0]) ) {
		for ( let cOp of cOperations ) {
			elementUpdate(cElement, cOp);
		}
	}
	// Single operation, just process
	else {
		elementUpdate(cElement, cOperations);
	}
}

//
export function updateElementProperties(cElement, cOperations) {
	if ( cElement instanceof NodeList ) {
		cElement.foreach((cEl) => {
			updateElement(cEl, cOperations);
		});
	} else if ( cElement instanceof Node ) {
		updateElement(cElement, cOperations);
	} else {
		console.trace("Invalid cElement", cElement);
	}
}

//
export function updateChildren(mainObject, updateList) {
	if ( !(mainObject instanceof HTMLElement) ) { console.error("MAIN OBJECT NOT VALID", mainObject); return; }

	function updateRow(querySelector, updateProperties) {
		let cElement = mainObject.querySelectorAll(querySelector);

		//
		if ( !cElement.length ) {
			console.error("Update children error, Child not found: %s.", querySelector);
			console.error(mainObject);
			return;
		}

		//
		for ( let cEl of cElement ) {
			updateElement(cEl, updateProperties);
		}
	}

	//
	if ( Array.isArray(updateList) ) {
		for ( let cRow of updateList ) {
			if ( cRow.length !== 2 ) { continue; }
			updateRow(cRow[0], cRow[1]);
		}
	}  else {
		for ( const [cSelector, cOperations] of Object.entries(updateList) ) {
			updateRow(cSelector, cOperations);
		}
	}
}

//
Node.prototype.se_empty = function() {
	this.innerHTML = '';
	return this;
};

//
Node.prototype.se_insertContentFromTemplate = function(position, template, content, modFunction) {
	//
	insertContentFromTemplate(this, position, template, content, modFunction);

	//
	return this;
};

Node.prototype.se_index = function() {
	return Array.prototype.indexOf.call(this.parentNode.children, this);
};

//
Node.prototype.se_remove = function() {
	this.remove();
}

//
NodeList.prototype.se_remove = HTMLCollection.prototype.se_remove = function() {
	return se_listOperation(this, (cEl) => {
		cEl.remove();
	});
};

//
function se_listOperation(tEl, fDef) {
	if ( !tEl.length || typeof fDef !== 'function' ) { return tEl; }
	// Loop elements
	let i = 0;
	for ( let cEl of tEl ) {
		fDef(cEl, i);
		i++;
	}

	return tEl;
}

//
Node.prototype.se_updateElement = function(cOperations) {
	updateElement(this, cOperations);
};

//
NodeList.prototype.se_updateElement = HTMLCollection.prototype.se_updateElement = function(cOperations) {
	return se_listOperation(this, (cEl) => {
		updateElement(cEl, cOperations);
	});
};

//
Node.prototype.se_updateChild = function (selector, cOperations) {
	updateElement(this.querySelector(selector), cOperations);
};

//
Node.prototype.se_updateChildren = function(selector, cOperations) {
	if ( typeof selector === 'string' ) {
		// Selector is a selector
		let children = this.querySelectorAll(selector);

		if ( !children ) { console.error("empty query", selector, this); return this; }

		children.forEach((cEl) => {
			updateElement(cEl, cOperations);
		});
	} else if ( typeof selector === 'object' ) {
		updateChildren(this, selector)
	} else {
		console.error("Update children failed. Not recognized.");
	}

	return this;
};

// Add event listeners
export function addEventListenerDelegate(cElem, cEvent, cDelegate, cFunc, cContext) {
	cElem.addEventListener(cEvent,
		(e) => {
			let rTarget = e.target.closest(cDelegate, cElem);
			if ( rTarget !== null ) {
				cFunc.call(cContext, e, rTarget);
			}
		},
		false
	);
}

export function addEventListenersToCustomElement(cClass, eventList) {
	// Is not an array of arrays.
	if ( !Array.isArray(eventList[0]) ) {
		eventList = [eventList];
	}

	//
	for ( let cEvent of eventList ) {
		switch ( cEvent.length ) {
			case 2:
				if ( typeof cEvent[0] !== 'string' || typeof cEvent[1] !== 'function' ) {
					console.error("Invalid event attempt in: ", cClass, cEvent);
					continue;
				}
				cClass.addEventListener(cEvent[0], cEvent[1]).bind(cClass);
				break;
			case 3:
				if ( typeof cEvent[0] !== 'string' || typeof cEvent[1] !== 'string' || typeof cEvent[2] !== 'function' ) {
					console.error("Invalid event attempt in: ", cClass, cEvent);
					continue;
				}
				addEventListenerDelegate(cClass, cEvent[0], cEvent[1], cEvent[2], cClass);
				break;
			default:
				console.error("Invalid event attempt in ", cClass, cEvent);
				break;
		}
	}
}

//
Node.prototype.se_on = function(var1, var2, var3, var4 = null) {
	let direct = function (cElem, cEvent, cFunc) {
			cElem.addEventListener(cEvent, cFunc);
		},
		//
		typeVar1 = typeof var1,
		typeVar2 = typeof var2,
		typeVar3 = typeof var3,
		//
		eventList, cEvent,
		cSubEl;

	//
	if ( typeVar1 === 'object' ) {
		//
		if ( typeVar2 === 'object' ) {
			// Multiple events on multiple sub elements
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of var1 ) {
				for ( cSubEl of var2 ) {
					addEventListenerDelegate(this, cEvent, cSubEl, var3);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// Multiple events and funcs related with single sub element, posible subDelegation
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				if ( typeof cFunc !== "function" ) { console.error("invalid function added"); }
				let subDelegation = ( var3 );
				addEventListenerDelegate(this, cEvent, var2, cFunc, subDelegation);
			}
		} else if ( typeVar2 === 'undefined' ) {
			// Multiple events and funcs no sub element
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				direct(this, cEvent, cFunc);
			}
		} else {
			console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
		}
	}
	else if ( typeVar1 === 'string' ) {
		// Variable is a string, probably action

		eventList = var1.split(' ');
		if ( typeVar2 === 'function' ) {
			// single/multiple event, single function
			for ( cEvent of eventList ) {
				direct(this, cEvent, var2);
			}
		} else if ( typeVar2 === 'object' ) {
			// single/multiple event, multiple subElements (compatibility with first function)

			// Ignorar si la tercera variable no es una función
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of eventList ) {
				for ( cSubEl of var2 ) {
					addEventListenerDelegate(this, cEvent, cSubEl, var3, var4);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// var2 is a type of element, third variable must be a function (or die)
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-string, v3-not valid');
				return this;
			}

			//
			for ( cEvent of eventList ) {
				addEventListenerDelegate(this, cEvent, var2, var3, var4);
			}
		} else {
			console.log('Element - se_on: Delegate . v1-string, v2-not valid', var2);
		}
	} else {
		console.error('Element - se_on: Method . v1-not valid', var1);
	}
	return this;
};

export function fixInputValues(domTarget) {
	let elementsToFix = domTarget.querySelectorAll('[se-fix]');
	
	//
	if ( !elementsToFix ) {
		console.error("no elements to fix in ", domTarget);
		return;
	}

	//
	for ( let cEl of elementsToFix ) {
		let fixVal = cEl.getAttribute('se-fix');


		// Only select, radio and checkbox should be affected
		switch ( cEl.tagName.toLowerCase() ) {
			//
			case 'select':
				cEl.value = fixVal;
				break;
			//
			case 'input':
				switch ( cEl.type) {
					//
					case 'checkbox':
						fixVal = parseInt(fixVal);
						cEl.checked = ( fixVal === 1 || fixVal === cEl.value );
						break;
					//
					case 'radio':
						let cForm = cEl.closest('form'),
							nEl = cForm.elements.namedItem(cEl.name);
						//
						if ( !nEl ) {
							console.trace("Form El Val, undefined element:", cEl.name);
							continue;
						}

						//
						if ( typeof fixVal !== 'undefined' ) {
							nEl.value = fixVal;
							//
							if ( nEl.length > 1 ) {
								for ( let i = 0; i < nEl.length; i++ ) {
									let curEl = nEl[i];
									if ( curEl.value === fixVal ) {
										curEl.checked = true;
										break;
									}
								}
							} else if ( nEl.type.toLowerCase() === 'checkbox' ) {
								nEl.checked = ( fixVal );
							}
						}
						break;
					default:
						console.log("AJAXSIMPLE - ElementFix - Input no definido");
						break;
				}
				break;
			default:
				console.log("AJAXSIMPLE - ElementFix - caso no definido", nEl.tagName);
				break;
		}
	}
}

//
export function htmlFromJson(cObj) {
	let cStruct = '';

	if ( typeof cObj !== 'object' ) {
		console.error("struct from json, error. type not valid", cObj);
	}

	//
	if ( cObj instanceof Array ) {
		for ( const cEl of cObj ) {
			cStruct+= htmlFromJson(cEl);
		}
	} else {
		let iniTag = cObj.tag;

		// Set attributes
		if ( cObj.hasOwnProperty('attr') ) {
			for ( const aName in cObj.attr ) {
				if ( !cObj.attr.hasOwnProperty(aName) ) { continue; }
				iniTag+= ' ' + aName + '="' + cObj.attr[aName] + '"';
			}
		}

		if ( cObj.hasOwnProperty('children') ) {
			let cBody = '';
			for ( const cEl of cObj.children) {
				cBody+= htmlFromJson(cEl);
			}
			//
			cStruct+= `<${ iniTag }>${ cBody }<${ cObj.tag }>`
		} else if ( cObj.hasOwnProperty('content') ) {
			//
			cStruct+= `<${ iniTag }>${ cObj['content'] }<${ cObj.tag }>`
		} else {
			cStruct+= `<${ iniTag } />`
		}
	}

	//
	return cStruct;
}