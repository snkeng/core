import * as test from "../library/dom-manipulation.mjs";

//
customElements.define('se-simple-tab', class extends HTMLElement {
	//<editor-fold desc="Elements">
	elTabList = this.querySelector('[role="tablist"]');
	elPanelGroup = this.querySelector('[se-elem="panelGroup"]');
	//</editor-fold>

	//
	properties = {
		hiddable: false
	};

	//
	constructor() {
		super();
	}

	connectedCallback() {
		// Set initial conditions (hidden and selected false)
		this.elTabList.se_updateChildren(':scope > [role="tab"]', ['attr', 'aria-selected', 'false']);
		this.elPanelGroup.se_updateChildren(':scope > [role="tabpanel"]', ['attr', 'hidden', true]);

		//
		if ( this.dataset['selectfirst'] !== 'false' ) {
			//
			this.setTab(0, null);
		}

		// Allow unselect?
		if ( this.dataset['hiddable'] === 'true' ) {
			this.properties.hiddable = true;
		}


		// Bind
		this.elTabList.addEventListener('click', this.tabClick.bind(this));
	}

	tabClick(e) {
		// Validate
		let cBtn = e.target.closest('[role="tab"]');
		if ( !cBtn ) { return; }

		// Hide selected (if posssible)
		if ( this.properties.hiddable && cBtn.getAttribute('aria-selected') === 'true' ) {
			this.elTabList.se_updateChildren(':scope > [role="tab"]', ['attr', 'aria-selected', 'false']);
			this.elPanelGroup.se_updateChildren(':scope > [role="tabpanel"]', ['attr', 'hidden', true]);
			return;
		}

		//
		e.preventDefault();
		e.stopPropagation();

		let cIndex = cBtn.getAttribute('aria-controls');

		if ( !cIndex ) {
			cIndex = Array.prototype.indexOf.call(cBtn.parentNode.children, cBtn);
		}

		//
		this.setTab(cIndex, cBtn);
	}

	setTab(elProperty, cBtn) {
		//
		this.elTabList.se_updateChildren(':scope > [role="tab"]', ['attr', 'aria-selected', 'false']);
		this.elPanelGroup.se_updateChildren(':scope > [role="tabpanel"]', ['attr', 'hidden', true]);
		//
		let cPanel;
		if ( typeof elProperty === 'number' ) {
			cPanel = this.elPanelGroup.querySelectorAll(':scope > [role="tabpanel"]')[elProperty];
		} else {
			cPanel = this.elPanelGroup.querySelector(':scope > #' + elProperty);
		}

		//
		cPanel.se_updateElement(['attr', 'hidden', null]);

		//
		if ( !cBtn ) {
			let cIndex = Array.prototype.indexOf.call(cPanel.parentNode.children, cPanel);
			cBtn = this.elTabList.querySelectorAll(':scope > [role="tab"]')[cIndex];
		}

		cBtn.se_updateElement(['attr', 'aria-selected', 'true']);
	}

});
