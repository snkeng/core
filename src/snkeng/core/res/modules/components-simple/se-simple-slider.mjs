import * as domManiupator from "../library/dom-manipulation.mjs";

//
customElements.define('se-simple-slider', class extends HTMLElement {
	//<editor-fold desc="Elements">
	//
	container = this.querySelector('.container');

	//</editor-fold>

	//
	timer = null;
	settings = {
		animation:'fade',
		duration:100,
		wait:4000
	};
	status = {
		slides:0,
		sCurrent:0,
		sNext:0,
		nextStop:false,
		pause:false
	};

	//
	constructor() {
		super();
	}

	connectedCallback() {
		// Get/Set Mode
		if ( !this.getAttribute('animation') ) {
			this.setAttribute('animation', 'fade');
		} else {
			this.settings.animation = this.getAttribute('animation');
		}

		// Time between switches
		this.settings.wait = this.getAttribute('wait') ?? 4000;
		
		//
		this.slideSet(0);

		if ( this.container.children.length > 1 ) {
			// Eventos
			this.timer = setInterval(this.slideNext.bind(this), this.settings.wait);
			let context = this;
			this.container.se_on({
				'mouseenter':() => {
					clearInterval(context.timer);
				},
				'mouseleave':() => {
					context.timer = setInterval(this.slideNext.bind(context), context.settings.wait);
				}
			});
		}
	}

	slideSet(index) {
		if ( index >= this.container.children.length ) { return; }
		//
		this.status.sCurrent = index;
		//
		switch ( this.settings.animation ) {
			case 'fade':
				this.container.children.se_updateElement(['attr', 'aria-hidden', 'true']);
				this.container.children[index].se_updateElement(['attr', 'aria-hidden', 'false']);
				break;
			case 'slide':
				break;
			default:
				console.error("simple slider, animation not defined", this.settings);
				break;
		}
	}

	//
	slideNext() {
		let nIndex = this.status.sCurrent + 1;
		if ( nIndex >= this.container.children.length ) { nIndex = 0; }

		//
		this.slideSet(nIndex);
	}
});
