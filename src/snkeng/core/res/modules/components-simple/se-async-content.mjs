import * as struct from "../library/struct.mjs";

// Define the new element
customElements.define('se-async-content', class extends HTMLElement {
	//
	constructor() {
		// Always call super first in constructor
		super();

		//
		this.settings = {
			url:null,
			isTable:false,
			scrolls:4
		};
		this.queryFilter = '';
		this.queryOrder = '';
		this.queryFilterObj = {};
		this.queryOrderObj = {};

		//
		this.navProps = {
			eIni: 0,
			eEnd: 0,
			eCur: 0,
			eLim: 20,
			eTot: 0,
			pCur: 0,
			pTot: 0,
		};

		//
		this.properties = {
			scrollCounter: 0,
			pageCurrent: 0,
			pageTotal: 0,
		};

		//
		this.queryData = {
			isNew: true,
			pCur: 0
		};

		//
		this.contentTemplate = this.querySelector('template');
		this.contentContainer = this.querySelector('[data-type="container"]');
	}


	//
	connectedCallback() {
		// Initial config
		this.settings.isTable = ( this.hasAttribute('istable') && this.getAttribute('istable') === "true");
		this.settings.url = ( this.hasAttribute('url') ) ? this.getAttribute('url') : null;
		this.settings.scrolls = ( this.hasAttribute('scrolls') ) ? parseInt(this.getAttribute('scrolls')) : 4;

		// Define custom event witch it listens to
		this.addEventListener("refresh", this.eventRefresh);
		this.addEventListener("externalOperation", this.externalOperation.bind(this));
		this.addEventListener("navPropCB", this.navigationPropCB.bind(this));
		this.addEventListener("filterMod", this.filterModCB.bind(this));
		this.addEventListener("filterClear", this.filterClearCB.bind(this));
		this.addEventListener("elementAdd", this.eventElementAdd.bind(this));
		this.addEventListener("elementUpdate", this.eventElementUpdate.bind(this));

		//
		// this.querySelector('.tbody').addEventListener('submit', this.submitElementForm.bind(this))

		//
		if ( !this.settings.isTable ) {
			this.loadingBtn = this.querySelector(':scope > button.loadMore');
		}

		// First load
		this.loadData(true, true);

		// If not table and has loading button, enable next page on scroll and activate button
		if ( !this.settings.isTable && this.loadingBtn ) {
			this.loadingBtn.addEventListener("click", this.loadBtnClick.bind(this), false);

			// Loading observer
			let observer = new IntersectionObserver(this.observeLoading.bind(this));
			observer.observe(this.loadingBtn);
		}
	}

	//
	submitElementForm(e) {
		e.preventDefault();

		console.log("Element is submited.");
	}

	//
	filterModCB(e) {
		let cName = e.detail.name,
			cVal = e.detail.value,
			cType = e.detail.type;

		//
		if ( !cName ) {
			console.error("invalid filter property", e.detail);
			return;
		}

		//
		switch ( cType ) {
			case 'filter':
				//
				if ( !cVal ) {
					delete this.queryFilterObj[cName];
				} else {
					this.queryFilterObj[cName] = cVal;
				}
				break;
			case 'order':
				//
				if ( !cVal ) {
					delete this.queryOrderObj[cName];
				} else {
					this.queryOrderObj[cName] = cVal;
				}
				break;
		}

		// Update
		this.loadData(true, true);
	}

	//
	filterClearCB() {
		this.queryFilterObj = {};
		this.queryOrderObj = {};
		//
		this.loadData(true, true);
	}

	//
	loadBtnClick() {
		this.properties.scrollCounter = 0;
		this.loadData();
	}

	//
	observeLoading() {
		//
		if ( this.dataset.condition === 'loading' ) {
			return;
		}

		//
		if ( this.properties.scrollCounter < this.settings.scrolls ) {
			this.loadData();
			this.properties.scrollCounter++;
		}
	}

	//
	externalOperation(e) {
		//
		let reload = false;

		//
		switch ( e.detail.operation ) {
			case 'reload':
				reload = true;
				break;

			case 'first':
				if ( this.properties.pageCurrent !== 0 ) {
					this.properties.pageCurrent = 0;
					reload = true;
				}
				break;

			case 'prev':
				if ( this.properties.pageCurrent > 0 ) {
					this.properties.pageCurrent--;
					reload = true;
				}
				break;

			case 'next':
				if ( this.properties.pageCurrent < this.properties.pageTotal - 1 ) {
					this.properties.pageCurrent++;
					reload = true;
				}
				break;

			case 'last':
				if ( this.properties.pageCurrent < this.properties.pageTotal - 1 ) {
					this.properties.pageCurrent = this.properties.pageTotal - 1;
					reload = true;
				}
				break;

			//
			case 'cpage':
				let posiblePage = intVal(e.detail.pageNumber) - 1;
				if ( posiblePage !== this.properties.pageCurrent && posiblePage >= 0 && posiblePage < this.properties.pageTotal ) {
					this.properties.pageCurrent = posiblePage;
					reload = true;
				}
				break;

			//
			default:
				console.log('FAIL - Act:', e.detail);
				return;
		}

		//
		if ( reload ) {
			this.loadData();
		}
	}

	//
	buildFilters() {
		this.queryFilter = '';
		this.queryOrder = '';

		//
		for ( let cName in this.queryFilterObj ) {
			this.queryFilter += 'where[' + cName + ']=' + encodeURIComponent(this.queryFilterObj[cName]) + '&';
		}
		//
		for ( let cName in this.queryOrderObj ) {
			this.queryOrder += 'order[' + cName + ']=' + encodeURIComponent(this.queryFilterObj[cName]) + '&';
		}
	}

	//
	async loadData(clean = false, isNew = false) {
		if ( this.dataset.condition === 'loading' ) {
			return;
		}

		//
		this.dataset.condition = 'loading';

		//
		if ( !this.settings.url ) {
			console.warn("Async Content will not load automatically. target url not defined.", this);
			return;
		}

		//
		if ( !this.settings.isTable ) {
			//
			this.queryData.isNew = false;
			this.properties.pageCurrent++;

			//
			if ( clean ) {
				this.contentContainer.textContent = '';
				this.properties.pageCurrent = 0;
				this.queryData.isNew = true;
			}
		} else {
			this.queryData.isNew = isNew;
			this.contentContainer.textContent = '';
		}

		//
		this.queryData.pCur = this.properties.pageCurrent;

		//
		const params = new URLSearchParams(this.queryData);

		//
		this.buildFilters();

		//
		let fResponse = await fetch(this.settings.url + '?' + this.queryFilter + this.queryOrder + params.toString());

		let msg = await fResponse.json();

		//
		if ( fResponse.status >= 500 ) {

			console.error(
`TITLE: ${msg.title}
DESCRIPTION: ${msg.description}
LOCATION: ${msg.location}
TRACE:
${msg.trace}`
			);

			alert("ERROR: COULD NOT LOAD CONTENT.\n" + msg.description);
			return;
		}

		if ( !msg.hasOwnProperty('d') ) {
			return;
		}

		//
		this.contentMultipleAdd('beforeend', msg.d)


		// Update information
		this.navProps.eIni = msg.p.eIni;
		this.navProps.eEnd = msg.p.eEnd;
		this.navProps.eCur = msg.p.eCur;
		this.navProps.pCur = msg.p.pCur;

		// Only update page current based on information
		this.properties.pageCurrent = msg.p.pCur;

		// Update only new properties
		if ( msg.p.isNew ) {
			//
			this.navProps.eLim = msg.p.eLim;
			this.navProps.eTot = msg.p.eTot;
			this.navProps.pTot = msg.p.pTot;

			// Update internal reference
			this.properties.pageTotal = msg.p.pTot;
		}

		// If callback
		if ( typeof this.navigationCB === 'function' ) {
			this.navigationCB(this.navProps);
		}

		// Set total pages
		if ( this.properties.pageTotal === 0 ) {
			this.properties.pageTotal = msg.p.pTot;
		}

		// Set current status
		this.dataset.condition = (this.properties.pageCurrent === this.properties.pageTotal) ? 'complete' : 'loaded';
	}

	//
	contentSingleAdd(cPosition, cContent) {
		const container = document.createElement("template");
		container.insertAdjacentHTML(cPosition, struct.stringPopulate(this.contentTemplate.innerHTML, cContent));
		struct.fixInputValues(container);
		this.contentContainer.insertAdjacentElement(cPosition, container.firstElementChild);
	}

	//
	contentMultipleAdd(cPosition, cContent) {
		const templateString = this.contentTemplate.innerHTML;
		const container = document.createElement("template");

		for ( let cRow of cContent ) {
			container.insertAdjacentHTML(cPosition, struct.stringPopulate(templateString, cRow));
			struct.fixInputValues(container);
			this.contentContainer.insertAdjacentElement(cPosition, container.firstElementChild);
		}
	}

	//
	navigationPropCB(e) {
		if ( !e.detail.hasOwnProperty('cbFunction') ) {
			console.error("Update content impossible, no data", e.detail);
			return;
		}

		//
		let cFunc = e.detail.cbFunction;

		//
		if ( typeof cFunc !== 'function' ) {
			console.error("No callback function");
			return;
		}

		//
		this.navigationCB = cFunc;
	}


	//
	eventRefresh(e) {
		console.log("refresh content", e.detail);
		this.loadData(true);
	}

	//
	eventElementAdd(e) {
		if ( !e.detail.hasOwnProperty('content') ) {
			console.error("add content impossible, no data", e.detail);
			return;
		}

		let cPosition = e.detail.position || 'beforeend',
			cContent = e.detail.content;

		if ( Array.isArray(cContent) ) {
			this.contentMultipleAdd(cPosition, cContent);
		} else {
			this.contentSingleAdd(cPosition, cContent);
		}
	}

	//
	eventElementUpdate(e) {
		if ( !e.detail.hasOwnProperty('content') ) {
			console.error("Update content impossible, no data", e.detail);
			return;
		}
		if ( !e.detail.hasOwnProperty('target') ) {
			console.error("Update content impossible, no target", e.detail);
			return;
		}

		//
		let cTarget = e.detail.target,
			cContent = e.detail.content;

		// Convert to node
		let template = document.createElement('template');
		template.innerHTML = struct.stringPopulate(this.contentTemplate.innerHTML, cContent);

		// Replace the node
		cTarget.replaceWith(template.content.firstChild)
	}
});
