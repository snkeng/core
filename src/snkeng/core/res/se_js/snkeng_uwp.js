﻿"use strict";
//
se.uwp = {};

//
se.uwp.httpPost = function ( url, post, config ) {
	let parameters = {
		headers: {}
	},
		defaults = {
			headers: {},
			response: null,
			timeOut: 0,
			onRequest: null,
			onComplete: null,
			onSuccess: null,
			onFail: null,
			onError: null
		},
		settings = se.object.merge( defaults, config ),
		i, cVal,
		//
		uri = new Windows.Foundation.Uri( url ),
		httpClient, requestContent,
		formData,
		httpPromise;

	// Preparación cliente (ignorar SSL)
	let filter = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();

	filter.ignorableServerCertificateErrors.push( Windows.Security.Cryptography.Certificates.ChainValidationResult.expired );
	filter.ignorableServerCertificateErrors.push( Windows.Security.Cryptography.Certificates.ChainValidationResult.untrusted );
	filter.ignorableServerCertificateErrors.push( Windows.Security.Cryptography.Certificates.ChainValidationResult.invalidName );

	//
	httpClient = new Windows.Web.Http.HttpClient( filter );
	requestContent = new Windows.Web.Http.HttpRequestMessage( Windows.Web.Http.HttpMethod.post, uri );

	//
	// Data
	if ( formData ) {
		/* other method */
		formData = new Windows.Web.Http.HttpMultipartFormDataContent();
		for ( i in post ) {
			if ( !post.hasOwnProperty( i ) ) { continue; }
			formData.add( new Windows.Web.Http.HttpStringContent( post[i] ), i );
		}
		//
		requestContent.content = formData;
	}


	//
	// Headers
	settings.headers['HTTP_USER_AGENT'] = settings.headers['HTTP_USER_AGENT'] || 'testprogram/1.0';

	//
	requestContent.headers.userAgent.tryParseAdd( settings.headers['HTTP_USER_AGENT'] );

	//
	if ( settings.headers.hasOwnProperty( 'Authorization' ) ) {
		// requestContent.headers.authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue( "Basic", "asdf:asdf" );
		requestContent.headers.authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue( "Basic", settings.headers['Authorization'] );
	}

	//
	if ( settings.response !== null ) {
		se.struct.notifAdvanced( settings.response, 'notification', 'Enviando información... ', '', 'fa-spinner', 'spin' );
	}

	// CB Request
	if ( typeof ( settings.onRequest ) === 'function' ) { settings.onRequest(); }

	// console.log("request content", requestContent);

	//
	httpPromise = httpClient.sendRequestAsync(requestContent)
		.then( ( response ) => {
			//
			if ( response.statusCode === 200 ) {
				return response.content.readAsStringAsync();
			}
			//
			if ( typeof ( settings.onFail ) === 'function' ) { settings.onFail(); }
		})
		.then( ( response ) => {
			console.log("RESPONSE BODY:\n\n" + response);

			//
			let msg, title, content;
			try {
				msg = JSON.parse(response);
			}
			catch ( err ) {
				console.warn("HTTP REQUEST\nStatus 200, BUT NOT JSON\nRESPONSE:\n---\n%s\n---", response);
				console.log('ERR:', err);
				if (settings.response !== null) {
					se.struct.notifAdvanced(settings.response, 'notification alert', 'ERROR', 'No pudo ser procesada la respuesta.', 'fa-remove');
				}
				if (typeof (settings.onError) === 'function') {
					settings.onError(err);
				}
				return;
			}

			//
			if ( msg.s.t === 1 ) {
				if (settings.response !== null) {
					title = ('d' in msg.s) ? msg.title : 'Actualizado';
					content = ('description' in msg) ? msg.description : ('date' in msg.s) ? msg.timestamp : '';
					se.struct.notifAdvanced(settings.response, 'notification success', title, content, 'fa-check');
				}
				if (typeof (settings.onSuccess) === 'function') {
					settings.onSuccess(msg);
				}
			} else {
				console.log("AJAX - 200, t=0:\n---\nE:\t%s \n---\nEX:\t%s", msg.s.e, msg.description);
				if (settings.response !== null) {
					title = ('e' in msg.s) ? 'ERROR: ' + msg.s.e : 'ERROR: Caso no definido';
					content = ('description' in msg) ? msg.description : ('date' in msg.s) ? msg.timestamp : '';
					se.struct.notifAdvanced(settings.response, 'notification alert', title, content, 'fa-remove');
				}
				if (typeof (settings.onFail) === 'function') {
					settings.onFail(msg);
				}
			}
		}
	);
	//
	httpPromise.done(
		() => {
			//
			if ( typeof ( settings.onComplete ) === 'function' ) { settings.onComplete(); }
		},
		( reason ) => {
			// Details in reason.message and ex.hresult.
			console.log( "HTTP REQUEST ERROR:\n\n", reason );
			if ( typeof ( settings.onFail ) === 'function' ) { settings.onFail( reason ); }
		}
	);

	//
	httpClient.close();
	//
};

//
se.uwp.pageParse = function() {

};

//
se.uwp.pageLoad = function (cTarget, cUrl, remember) {

};

//
se.uwp.page = {
	process:function (params) {
		let cTarget = $('#app_body'),
			cUrl = 'pages/'+ params.page +'.html';
		cTarget.se_html('loading...');
		fetch(cUrl)
			.then(
				( response ) => {
					if ( response.status !== 200 ) {
						console.log('Looks like there was a problem. Status Code: ' + response.status);
						return;
					}
					//
					response.text().then(function (data) {
						cTarget.se_html(data);
						se.main.pluginLoad(cTarget);
					});

				}
			)
			.catch(function (err) {
				console.log('Fetch Error :-S', err);
			});
	},
	//
	urlParse:function (url, setAddress) {
		let params = se.url.str_parse(url);
		if ( !params.page ) {
			params.page = 'default';
		}
		//
		if ( setAddress ) {
			window.history.replaceState(params, 'MENU', url);
		}
		//
		se.uwp.page.process(params);
	},
	//
	popState:function (event) {
		if ( event.state !== null ) { se.uwp.page.process(event.state); }
	},
	//
	navigation:function(event, element) {
		event.preventDefault();
		se.uwp.page.urlParse(element.getAttribute('se-nav-uwp'), true);
	}
};

//
se.uwp.sectionSetValues = (section, data) => {
	// Textos
	section.querySelectorAll('[se-elem-replace]').se_each((index, cEl) => {
		let dName = cEl.se_attr('se-elem-replace'),
			dVal = data[dName];
		//
		if ( typeof dVal === 'undefined' ) { console.log("Actualizar valores de sección. Lugar no disponible", dName, dVal); return; }
		//
		switch ( cEl.tagName.toLowerCase() ) {
			case 'select':
				cEl.value = dVal;
				break;
			case 'input':
				let cForm = cEl.se_closest('form'),
					cElName = cEl.name;
				cForm.se_formElVal(cElName, dVal);
				break;
			default:
				cEl.se_text(dVal);
				break;
		}

	});
};

//
se.uwp.elementTemplateProc = ( element, params ) => {
	let cText = element.innerHTML ,
		cString = cText.replace(/\&lt\;/g, '<').replace(/\&gt\;/g, '>').replace(/\&quot\;/g, '"').replace(/\&amp\;/g, "&"),
		tpl = eval('`' + cString.replace(/`/g,'\\`') + '`');
	// Trasnformation
	// console.log("Process", tpl, params);

	element.innerHTML = tpl;
};
//