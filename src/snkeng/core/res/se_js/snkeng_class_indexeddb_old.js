"use strict";
//
se.addon.se_indexeddb = function (dbStructure, initCB) {
	let dbObject;

	//
	function init(idbStruct, cb) {
		//
		if ( idbStruct.reset ) {
			window.indexedDB.deleteDatabase(idbStruct.name).onsuccess = function() {
				console.log("IDB DELETED. STARTING FROM SCRATCH.");
			};
		}
		//
		let openRequest = window.indexedDB.open(idbStruct.name, idbStruct.version),
			dbCreate;
		//
		openRequest.onupgradeneeded = function(e) {
			let thisDB = e.target.result,
				transactionObj = e.currentTarget.transaction,
				cDBNames = thisDB.objectStoreNames,
				tDBNames = Object.getOwnPropertyNames(idbStruct.structure),
				curDBOName,
				tDBICur,
				index;
			console.log("IDB Update. %d -> %d", e.oldVersion, e.newVersion);

			// Delete operations
			for ( index = 0; index < cDBNames.length; index++ ) {
				const curDBOName = cDBNames[index];
				//
				if ( tDBNames.indexOf( curDBOName ) === -1 ) {
					thisDB.deleteObjectStore( curDBOName );
				}
			}

			// Read new list for add/update
			for ( curDBOName in idbStruct.structure ) {
				if ( !idbStruct.structure.hasOwnProperty(curDBOName) ) { console.warn("but how?"); continue; }
				let curDBData = idbStruct.structure[curDBOName];
				//
				if ( cDBNames.contains(curDBOName) ) {
					console.log("DB OBJECT CHECK UPDATE", curDBOName);
					// It exists, check updates
					let idbObject = transactionObj.objectStore(curDBOName),
						nIndexes = Object.getOwnPropertyNames(curDBData.indexes),
						oIndexes = idbObject.indexNames,
						idbOIndex;
					// Check for delete
					for ( index = 0; index < oIndexes.length; index++ ) {
						const idbOIndex = oIndexes[index];
						if ( nIndexes.indexOf(idbOIndex) === -1 ) {
							idbObject.deleteIndex(idbOIndex);
						}
					}
					// Check for add
					for ( index = 0; index < oIndexes.length; index++ ) {
						const idbOIndex = oIndexes[index];
						if ( !oIndexes.contains(idbOIndex) ) {
							let nIndexData = curDBData.indexes[idbOIndex];
							idbObject.createIndex(idbOIndex, nIndexData.keyPath, nIndexData.params);
						}
					}
					// check for update...?
				} else {
					// Not exist, add
					dbCreate = thisDB.createObjectStore(curDBOName, curDBData.build);
					for ( tDBICur in curDBData.indexes ) {
						if ( !curDBData.indexes.hasOwnProperty(tDBICur) ) { console.warn("but how?"); continue; }
						let nIndexData = curDBData.indexes[tDBICur];
						dbCreate.createIndex(tDBICur, nIndexData.keyPath, nIndexData.params);
					}
				}
			}
		};

		//
		openRequest.onsuccess = function(e) {
			console.log("IDB Ready, version: %i", idbStruct.version);
			dbObject = e.target.result;
			if ( cb && typeof cb === 'function' ) { cb(); }
		};

		//
		openRequest.onerror = function(e) {
			console.log("IDB ERROR", e);
		};
	}
	
	//
	function idb_close() {}

	//
	function idb_query(table, options) {
		let defaults = {
				index:null,
				keyRange:null,
				queryDirection:null,
				sort:null,
				get:null,
				onSuccess:null,
				onEmpty:null,
				onComplete:null,
				onError:null
			},
			settings = se.object.merge(defaults, options),
			idbTxn = dbObject.transaction(table),
			idbObject = idbTxn.objectStore(table),
			result = [],
			request;
	
		//
		if ( settings.get ) {
			// Caso particular
			request = idbObject.get(settings.get);
			//
			request.onsuccess = function(e) {
				let result = e.target.result;
				if ( result ) {
					if ( typeof settings.onSuccess === 'function' ) {
						settings.onSuccess(result);
					} else {
						console.log("Result success: ", result);
					}
				} else {
					if ( typeof settings.onEmpty === 'function' ) {
						settings.onEmpty();
					} else {
						console.log("Result is empty.", settings);
					}
				}
			};
		} else {
			// Con filtros o sin ellos
			//
			if ( settings.index ) {
				idbObject = idbObject.index(settings.index);
			}
			//
			if ( settings.keyRange ) {
				if ( settings.queryDirection ) {
					request = idbObject.openCursor(settings.keyRange, settings.queryDirection);
				} else {
					request = idbObject.openCursor(settings.keyRange);
				}
			} else {
				request = idbObject.openCursor();
			}
			//
			request.onsuccess = function(e) {
				let cursor = e.target.result;
				if ( cursor ) {
					// Juntar todos los resultados
					result.push(cursor.value);
					cursor.continue();
				} else {
					// console.log("end", result);
					// Determinar si hay datos
					if ( result.length ) {
						// Contiene resultados
						if ( typeof settings.onSuccess === 'function' ) {
							// Juntar todos los resultados
							if ( settings.sort ) {
								result = se.object.sort(result, settings.sort.name, settings.sort.direction);
							}
							// regresar resultado
							settings.onSuccess(result);
						} else {
							console.log("Result success: ", result);
						}
					} else {
						// Esta vacío
						if ( typeof settings.onEmpty === 'function' ) {
							settings.onEmpty();
						} else {
							console.log("Result is empty.", settings);
						}
					}
				}
			};
		}
	
		//
		request.onerror = (e) => {
			console.log("error", e);
		};
		//
		if ( typeof settings.onComplete === 'function' ) {
			idbTxn.oncomplete = settings.onComplete;
		}
		//
		idbTxn.onerror = (e) => {
			console.log("Error en la consulta", e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError();
			}
		};
	}
	//
	function idb_add(table, data, options) {
		if ( typeof options === 'undefined' ) {
			options = {};
		}
		let idbTransaction = dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);
		//
		if ( typeof options.onSuccess === 'function' ) {
			idbTransaction.oncomplete = options.onSuccess;
		}
	
	
	
		if ( typeof data === 'undefined' ) {
			console.error('Data type undefined');
		} else if ( Array.isArray(data) ) {
			for ( let index in data ) {
				if ( !data.hasOwnProperty(index) ) { continue; }
				objStore.add(data[index]);
			}
		} else {
			objStore.add(data);
		}
	}
	//
	function idb_put(table, data, options) {
		if ( typeof options === 'undefined' ) {
			options = {};
		}
		let idbTransaction = dbObject.transaction(table, "readwrite"),
			objStore = idbTransaction.objectStore(table);
		//
		if ( typeof options.onSuccess === 'function' ) {
			idbTransaction.oncomplete = options.onSuccess;
		}
		if ( typeof data === 'undefined' ) {
			console.error('Data type undefined');
		} else if ( Array.isArray(data) ) {
			for ( let index in data ) {
				if ( !data.hasOwnProperty(index) ) { continue; }
				objStore.put(data[index]);
			}
		} else {
			objStore.put(data);
		}
	}
	//
	function idb_update(table, id, data, options) {
		let defaults = {
				onComplete:null,
				onError:null
			},
			settings = se.object.merge(defaults, options),
			idbTxn = dbObject.transaction(table, "readwrite"),
			idbObject = idbTxn.objectStore(table);
	
		//
		if ( typeof settings.onSuccess === 'function' ) {
			idbTxn.oncomplete = settings.onSuccess;
		}
		//
		idbTxn.onerror = (e) => {
			console.log("Error en la consulta", e);
			if ( typeof settings.onError === 'function' ) {
				settings.onError();
			}
		};
	
		//
		if ( typeof data === 'undefined' ) {
			console.error('IDB update error. Incomming data type undefined', data);
		} else {
			let request = idbObject.openCursor(IDBKeyRange.only(id));
			request.onsuccess  = function(e) {
				let cursor = e.target.result,
					newData;
				if ( cursor ) {
					newData = se.object.merge(cursor.value, data);
					idbObject.put(newData);
				}
			};
		}
	}
	//
	function idb_rebuild( table, cData, options ) {
		//
		if ( typeof cData === 'undefined' ) {
			console.error( 'Data type undefined' );
			return;
		}
		//
		let idbTransaction = dbObject.transaction( table, "readwrite" ),
			objStore = idbTransaction.objectStore( table );
	
		// Informa sobre el status de la transacción
		idbTransaction.oncomplete = function ( event ) {
			if ( typeof options.onComplete === 'function' ) {
				options.onComplete(event);
			}
			console.log( "Transacción se llevó a cabo con éxito.", event );
		};
	
		idbTransaction.onerror = function ( event ) {
			if ( typeof options.onError === 'function' ) {
				options.onError();
			}
			let error = event.target.error;
			console.log( 'IndexedDb.rebuild -> request -> onerror', error, error.name, event );
		};
	
		// Clear
		objStore.clear();
		// Agregar elementos
		for ( let j in cData ) {
			if ( !cData.hasOwnProperty( j ) ) { continue; }
			//
			objStore.add( cData[j] );
			//
			objStore.onsuccess = function ( event ) {
				console.log( "adding element: ", event );
			}
		}
	}

	//
	init(dbStructure, initCB);
	return {
		close:idb_close,
		query:idb_query,
		add:idb_add,
		put:idb_put,
		update:idb_update,
		rebuild:idb_rebuild
	};
};
