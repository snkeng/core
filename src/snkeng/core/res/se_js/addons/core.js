"use strict";

/**
 *
 * @param options
 * @returns {boolean}
 */
se.addon.floatWin = function(options) {
	// Float
	let defaults = {
			//
			width:null,
			height:null,
			// Window Props
			id:'se_dialog',
			fullScreen:false,
			hasHeader:true,
			hasClose:true,
			isDraggable:false,
			isResizeable:false,
			backBlock:true,
			backClose:true,
			customClass:'',
			//
			iniUrl:'',
			post:{},
			defHead:'Head',
			defBody:'Body',
			defJS:'',
			defMT:'',
			onLoad:null,
			onClose:null
		},
		settings = se.object.merge(defaults, options),
		// Ventana actual
		cDialog,
		cWindow, cHead, cBody;

	//
	function init() {
		// Ya hay ventana, cerrar
		if ( $('#' + settings.id) ) { return false; }
		//
		cDialog = document.body.se_prepend('<dialog id="' + settings.id + '" class="window ' + settings.customClass + '"><div class="win_main"><div class="win_header"><div class="title"></div></div><div class="win_content"></div><script></script></div></dialog>');

		//
		cWindow = cDialog.querySelector('.win_main');
		cHead = cWindow.querySelector('.win_header');
		cBody = cWindow.querySelector('.win_content');
		// Propiedades
		if ( settings.fullScreen ) {
			cWindow.se_classAdd('fullScreen');
		}
		else if ( settings.width || settings.height ) {
			if ( settings.width !== null ) {
				cWindow.se_css('width', settings.width + 'px');
			}
			if ( settings.height !== null ) {
				cWindow.se_css('height', settings.height + 'px');
			}
		}

		//
		if ( settings.hasClose ) {
			cHead.insertAdjacentHTML('beforeend', '<div class="close"><svg class="icon"><use xlink:href="#fa-remove"></use></div>');
			cHead.querySelector('.close').addEventListener('click', closeDialog);
		}

		//
		if ( settings.isDraggable ) {
			let calcLeft = ( window.outerWidth > settings.width) ? ( window.outerWidth - settings.width ) / 2 : 0,
				calcTop = ( settings.height !== 0 && window.outerHeight > settings.height) ? ( window.outerHeight - settings.height ) / 2 : 0;
			// cWindow.se_css({'top':calcTop+'px','left':calcLeft+'px'});
			// cWindow.se_classAdd('draggable float');
		}

		//
		if ( settings.isResizeable ) {
		}

		//
		if ( settings.backBlock ) {
			// cDialog.insertAdjacentHTML('afterbegin','<div class="bkg"></div>');
			if ( settings.backClose ) {
				cDialog.addEventListener('click', function (event) {
					event.preventDefault(); // disable selection
					closeDialog();
				});
				cWindow.addEventListener('click', function (event) {
					event.stopPropagation();
				});
			}
		}
		// Content
		if ( settings.iniUrl !== '' ) {
			let asyncLoadFull = function() {
				// Plugins
				se.main.pluginLoad(cBody);
				//
				if ( typeof settings.onLoad === 'function' ) {
					settings.onLoad.call(this);
				}
			};
			se.ajax.json(settings.iniUrl, settings.post, {
				onCall:() => {
					cHead.querySelector('.title').se_text('Cargando...');
					cBody.se_html('<span>Cargando...  <svg class="icon inline spin"><use xlink:href="#fa-spinner"></use></svg></span>');
				},
				onSuccess:(msg) => {
					let pData = msg.d;
					cHead.querySelector('.title').se_text(pData.title);
					cBody.se_html(pData.body);
					// Agregar JS
					if ( 'defJS' in settings && settings.defJS !== '' ) {
						pData.js += settings.defJS;
					}

					// Cargado asyncrónico de archivos
					se.ajax.fileLoadAsync(pData, '#se_dialog script', asyncLoadFull);
				},
				onFail:(msg) => {
					cHead.querySelector('.title').se_text('Error');
					cBody.se_html('No fue posible cargar el contenido.');
					console.log(msg);
				},
				onError:(title, content) => {
				}
			});
		}
		else {
			cHead.querySelector('.title').se_text(settings.defHead);
			cBody.se_html(settings.defBody);
			let jsText = '',
				jsCont = cWindow.querySelector('script');
			jsCont.se_text('');
			if ( 'defJS' in settings ) {
				jsText += settings.defJS;
			}
			if ( 'defMT' in settings ) {
				jsText += "(function()\n{\n" + settings.defMT + "\n}());";
			}
			if ( jsText !== '' ) {
				jsCont.se_text(jsText);
				window.eval(jsCont.se_text());
			}
			//
			if ( typeof settings.onLoad === 'function' ) {
				settings.onLoad.call(this);
			}
		}

		//
		se.dialog.openModal(cDialog);

		cDialog.addEventListener('close', closeDialog);
	}

	function closeDialog(ev) {
		if ( ev ) { ev.preventDefault(); }
		//
		if ( typeof settings.onClose === 'function' ) { settings.onClose.call(this); }
		//
		cDialog.se_remove();
	}

	//
	init();
	return cDialog;
};

/**
 *
 * @param options
 * @returns {boolean}
 */
se.addon.windowAction = function(options) {
	// Float
	let defaults = {
			width:null,
			height:null,
			title:'',
			description:'',
			defaults:{},
			buttons:[]
		},
		settings = se.object.merge(defaults, options)
		//
		let cDialog, cMenu, cOutput,
			btnsTxt = '', cResponse;
	// Build buttons
	for ( let cBtn of settings.buttons ) {
		btnsTxt+= `<button class="btn blue" se-act="action" data-link="${cBtn.link}"><svg class="icon inline mr"><use xlink:href="#${cBtn.icon}" /></svg>${cBtn.name}</button>`;
	}

	//
	function btnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'action':
				se.ajax.json(cBtn.se_data('link'), se.object.merge(settings.defaults, {}), {
					response:cResponse,
					onCall:() => {
						cBtn.querySelector('svg').se_classAdd('spin');
					},
					onSuccess:(msg) => {
						cOutput.se_text(msg.d);
					},
					onComplete:() => {
						cBtn.querySelector('svg').se_classDel('spin');
					}
				});
				break;
			//
			case 'close':
				break;
			//
			default:
				console.error("engine windows action method not recognized", cBtn);
				break;
		}
	}

	// Crear ventana
	se.addon.floatWin({
		width:settings.width,
		height:settings.height,
		isDraggable: true,
		defHead: settings.title,
		defBody: `<div>${settings.description}</div><div se-elem="menu">${btnsTxt}</div><output se-elem="response"></output><output se-elem="result"></output>`,
		onLoad:() => {
			cDialog = $('#se_dialog');
			cMenu = cDialog.querySelector('div[se-elem="menu"]');
			cOutput = cDialog.querySelector('output[se-elem="result"]');
			cResponse = cDialog.querySelector('output[se-elem="response"]');

			//
			cMenu.se_on('click', 'button', btnAction);
		}
	});
};

/**
 *
 * @param options
 * @returns {boolean}
 */
se.addon.windowForm = function(options) {
	// Float
	let defaults = {
			width:null,
			height:null,
			closeSuccess:true,
			title:'',
			description:'',
			// Form Only
			load_url:null,
			load_data:null,
			save_url:null,
			save_data:null,
			actName:null,
			elems:null,
			//
			onLoad:null,
			onRequest:null,
			onComplete:null,
			onSuccess:null,
			onFail:null
		},
		settings = se.object.merge(defaults, options);

	// Crear ventana
	se.addon.floatWin({
		width:settings.width,
		height:settings.height,
		isDraggable: true,
		defHead: settings.title,
		defBody: se.form.createFromObj( {save_url: settings.save_url, elems:settings.elems, actName:settings.actName} ),
		onLoad:() => {
			// Variables
			let form = $('#se_dialog form');
			if ( form ) {
				form.se_plugin('simpleForm', {
					load_url:settings.load_url,
					load_data:settings.load_data,
					save_url:settings.save_url,
					save_data:settings.save_data,
					//
					onLoad:settings.onLoad,
					onRequest:settings.onRequest,
					onComplete:settings.onComplete,
					onSuccess:settings.onSuccess,
					onFail:settings.onFail
				});
				form.elements[0].focus();
				// Plugin load
				se.main.pluginLoad(form);
			} else { console.log('No fue posible cargar el sitio.'); }
		}
	});
};

/**
 *
 * @param options
 * @returns {boolean}
 */
se.addon.notification = function(content, options) {
	options = (typeof options === 'undefined') ? {} : options;
	if ( typeof content === "object" ) {
		let cMsg = '<div class="notif">';
		if ( content.hasOwnProperty('image') && content.image ) {
			cMsg+= '<img src="' + content.image + '""/>'
		}
		cMsg+= '<div class="text"><div class="title">' + content.title + '</div><div class="desc">' + content.body + '</div>';
		if ( content.hasOwnProperty('time') && content.time ) {
			let cTime = new Date().toLocaleTimeString();
			cMsg+= '<div class="time">' + cTime + '</div>'
		}
		cMsg+= '</div></div>';
		//
		options.message = cMsg;
	} else if ( typeof content === "string" ) {
		options.message = content;
	} else {
		console.log("error");
	}

	//
	se.addon.sticky(options);
};
