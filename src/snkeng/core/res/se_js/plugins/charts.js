"use strict";

//
se.plugin.se_charts = function (plugElem, plugOptions) {
	const ns = 'http://www.w3.org/2000/svg';
	let svg,
		//
		pDefaults = {
			width:'100%',
			height:'100%',
			defaultColors:[
				'#1BBC9B',
				'#2DCC70',
				'#3598DB',
				'#9B58B5',
				'#34495E',
				'#F1C40F',
				'#E77E23',
				'#E84C3D',
				'#ECF0F1',
				'#95A5A5',
				'#16A086',
				'#27AE61',
				'#2A80B9',
				'#8F44AD',
				'#2D3E50',
				'#F39C11',
				'#D25400',
				'#BEC3C7',
				'#7E8C8D'
			],
		},
		pSettings = se.object.merge(pDefaults, plugOptions),

		//
		cStatus = {
			graph:'none',
			sizeX:0,
			sizeY:0
		},
		cElems = {
			chartTitle:null,
			chartArea:null,
			chartDescription:null,
			axisX:null,
			axisY:null
		},
		cDimensions = {
			margin_active:50,
			margin_inactive:10,
			borderX0:0,
			borderX1:0,
			borderY0:0,
			borderY1:0,
			sizeX:0,
			sizeY:0,
			sizeXUnits:0,
			sizeYUnits:0,
			yCuadrant:0,
			xCuadrant:0
		};

	//
	function init() {
		svg = document.createElementNS(ns, 'svg');
		svg.se_attr('width', pSettings.width);
		svg.se_attr('height', pSettings.height);
		svg.se_attr('view-box', '0,0,1000,1000');
		plugElem.appendChild(svg);
	}

	//<editor-fold desc="SVG Options">
	//
	function createElement(elName, elProps, elContent = null, parentElement = null) {
		let newElem = document.createElementNS(ns, elName);
		//
		if ( elProps ) {
			elementSetProperties(newElem, elProps);
		}
		//
		if ( elContent ) {
			newElem.se_html(elContent);
		}
		//
		if ( parentElement ) {
			parentElement.appendChild(newElem);
		} else {
			svg.appendChild(newElem);
		}
		return newElem;
	}

	//
	function elementSetProperties(element, properties) {
		//
		for ( const [cProp, cValues] of Object.entries(properties) ) {
			element.se_attr(cProp, cValues.toString());
		}
	}
	//</editor-fold>

	//<editor-fold desc="Auxiliar functions">

	//
	function clearAll() {
		svg.se_html('');
	}

	//
	function getSize() {
		let bBox = svg.getBBox();
		cStatus.sizeX = svg.width.baseVal.value;
		cStatus.sizeY = svg.height.baseVal.value;
	}

	//
	function chart(type, properties) {
		clearAll();
		getSize();

		//
		// console.log("Graficación: ", type, properties);


		// Título
		cElems.chartTitle = createElement('text',
			{
				id:'graphs_title',
				'text-anchor': 'middle',
				'font-size':"2em",
				x:'50%',
				dy:'1em'
			},
			properties.headers[0]
		);

		//
		switch ( type ) {
			//
			case 'bars':
				chart_plot_bars(properties);
				break;
			//
			case 'points':
				chart_plot_points(properties);
				break;
			case 'linear':
				chart_plot_linear(properties);
				break;
			case 'scatter-points':
				chart_plot_scatter_points(properties);
				break;
			case 'scatter-linear':
				chart_plot_scatter_linear(properties);
				break;
			case 'pie':
				chart_plot_pie(properties);
				break;
			default:
				console.error("Tipo de grápica no definida", type);
				break;
		}
	}

	//
	function chart_buildSimpleStructure(axisData) {
		//
		let xText;
		//
		cDimensions.borderX0 = cDimensions.margin_active;
		cDimensions.borderX1 = cStatus.sizeX - cDimensions.margin_inactive;
		cDimensions.borderY0 = cDimensions.margin_inactive;
		cDimensions.borderY1 = cStatus.sizeY - cDimensions.margin_active;
		cDimensions.sizeX = cDimensions.borderX1 - cDimensions.borderX0;
		cDimensions.sizeY = cDimensions.borderY1 - cDimensions.borderY0;

		// Crear estructura básica
		cElems.chartArea = createElement('svg', {
			id: 'graphs_container',
			class: 'bars',
			x: cDimensions.borderX0,
			y: cDimensions.borderY0,
			width: cDimensions.sizeX,
			height: cDimensions.sizeY,
		});

		// X

		//
		cElems.axisX = createElement('g', {
			id: 'x-axis'
		});
		createElement('line', {
			x1: cDimensions.borderX0,
			x2: cDimensions.borderX1,
			y1: cDimensions.borderY1,
			y2: cDimensions.borderY1,
			stroke: '#333',
			'stroke-width': '1'
		}, null, cElems.axisX);
		xText = createElement('text', {
			x:cDimensions.borderX0 + (0.5 * cDimensions.sizeX),
			y:cDimensions.borderY1 + 40,
			fill:'#333',
			'text-anchor':"middle"
		}, null, cElems.axisX);
		xText.se_text(axisData.x);



		//
		cElems.axisY = createElement('g', {
			id: 'y-axis'
		});
		// Barra Y
		createElement('line', {
			x1: cDimensions.borderX0,
			x2: cDimensions.borderX0,
			y1: cDimensions.borderY0,
			y2: cDimensions.borderY1,
			stroke: '#333',
			'stroke-width': '1'
		}, null, cElems.axisY);
		// Axis Y
		xText = createElement('text', {
			x:cDimensions.borderX0 - 40,
			y:cDimensions.borderY1 - (0.5 * cDimensions.sizeY),
			fill:'#333',
			'text-anchor':"middle",
			'writing-mode':'tb'
		}, null, cElems.axisY);
		xText.se_text(axisData.y);
	}

	//
	function chart_axisConverter(minX, maxX, minY, maxY) {
		// Cuadrante X
		if ( minX >= 0 && maxX >= 0 ) {
			cDimensions.sizeXUnits = maxX * 1.1;
			cDimensions.xCuadrant = 1;
		} else if ( minX >= 0 && maxX >= 0 ) {
			cDimensions.sizeXUnits = maxX * 1.1;
			cDimensions.xCuadrant = -1;
		} else {
			cDimensions.sizeXUnits = maxX * 1.2;
			cDimensions.xCuadrant = 0;
		}

		// Cuadrante Y
		if ( minY >= 0 && maxY >= 0 ) {
			cDimensions.sizeYUnits = maxY * 1.1;
			cDimensions.yCuadrant = 1;
		} else if ( minY >= 0 && maxY >= 0 ) {
			cDimensions.sizeYUnits = maxY * 1.1;
			cDimensions.yCuadrant = -1;
		} else {
			cDimensions.sizeYUnits = maxY * 1.2;
			cDimensions.yCuadrant = 0;
		}
	}
	//</editor-fold>

	//<editor-fold desc="Simple charts">

	//
	function chart_simple_minmax(data) {
		let minVal = data[0][1], maxVal = data[0][1],
			pointsLen = data[0].length,
			i, j;

		//
		for ( i = 0; i < data.length; i++ ) {
			for ( j = 1; j < pointsLen; j++ ) {
				if ( data[i][j] < minVal ) {
					minVal = data[i][j];
				} else if ( data[i][j] > maxVal ) {
					maxVal = data[i][j];
				}
			}
		}

		// Definir interalo directamente
		chart_axisConverter(0, 0, minVal, maxVal);

		return {
			minX:0,
			maxX:0,
			minY:minVal,
			maxY:maxVal
		};
	}

	//
	function chart_plot_bars(properties) {
		let i, j,
			teoLen = properties.headers.length,
			teoPoints = properties.data.length,
			//
			pointsLimit = chart_simple_minmax(properties.data),
			//
			barGroupWidth,
			barIndividualWidth;

		// Crear estructura básica
		chart_buildSimpleStructure(properties.axis);
		barGroupWidth = parseInt(cDimensions.sizeX / teoPoints);
		barIndividualWidth = parseInt(barGroupWidth / ( teoLen + 2 ));

		// Crear ajuste de ejes (externa?)

		// Content
		for ( i = 0; i < teoPoints; i++ ) {
			let cText = createElement('text', {
				x:cDimensions.borderX0 + ( i * barGroupWidth) + (0.5 * barGroupWidth),
				y:cDimensions.borderY1 + 20,
				fill:'#333',
				'text-anchor':"middle"
			}, null, cElems.axisX);
			cText.se_text(properties.data[i][0]);
			// Small bar (skip first)
			if ( i !== 0 ) {
				createElement('line', {
					x1: cDimensions.borderX0 + (i * barGroupWidth),
					x2: cDimensions.borderX0 + (i * barGroupWidth),
					y1: cDimensions.borderY1 - 5,
					y2: cDimensions.borderY1 + 5,
					stroke: '#333',
					'stroke-width': '1'
				}, null, cElems.axisX);
			}
		}

		// Dibujar barras
		for ( i = 0; i < teoPoints; i++ ) {
			for ( j = 0; j < teoLen; j++ ) {
				// Ajustes
				let barHeight = parseInt(cDimensions.sizeY * properties.data[i][j + 1] / cDimensions.sizeYUnits);

				// Imprimir
				createElement('rect',
					{
						x:( i * barGroupWidth) + ( ( j + 1 ) * barIndividualWidth ),
						width:barIndividualWidth,
						y:cDimensions.sizeY - barHeight,
						height:barHeight,
						fill:pSettings.defaultColors[j % pSettings.defaultColors.length]
					},
					null, cElems.chartArea
				);
			}
		}

		//
	}

	//
	function chart_plot_points(properties) {
		let i, j,
			teoLen = properties.headers.length,
			teoPoints = properties.data.length,
			//
			pointsLimit = chart_simple_minmax(properties.data),
			//
			barGroupWidth;

		// Crear estructura básica
		chart_buildSimpleStructure(properties.axis);
		barGroupWidth = parseInt(cDimensions.sizeX / teoPoints);
		//
		// Crear ajuste de ejes (externa?)

		// Content
		for ( i = 0; i < teoPoints; i++ ) {
			let xMid = cDimensions.borderX0 + ( i * barGroupWidth ) + ( 0.5 * barGroupWidth );

			let cText = createElement('text', {
				x:xMid,
				y:cDimensions.borderY1 + 20,
				fill:'#333',
				'text-anchor':"middle"
			}, null, cElems.axisX);
			cText.se_text(properties.data[i][0]);

			// Small bar (skip first)
			createElement('line', {
				x1: xMid,
				x2: xMid,
				y1: cDimensions.borderY1 - 5,
				y2: cDimensions.borderY1 + 5,
				stroke: '#333',
				'stroke-width': '1'
			}, null, cElems.axisX);
		}

		// Dibujar puntos
		for ( i = 0; i < teoPoints; i++ ) {
			for ( j = 0; j < teoLen; j++ ) {
				// Imprimir
				createElement('circle',
					{
						cx:( i * barGroupWidth) + ( 0.5 * barGroupWidth),
						cy:parseInt(cDimensions.sizeY - (cDimensions.sizeY * properties.data[i][j + 1] / cDimensions.sizeYUnits)),
						r:5,
						fill:pSettings.defaultColors[j % pSettings.defaultColors.length]
					}, null, cElems.chartArea
				);
			}
		}
		//
	}

	//
	function chart_plot_linear(properties) {
		let i, j,
			teoLen = properties.headers.length,
			teoPoints = properties.data.length,
			//
			pointsLimit = chart_simple_minmax(properties.data),
			//
			barGroupWidth;

		// Crear estructura básica
		chart_buildSimpleStructure(properties.axis);
		barGroupWidth = parseInt(cDimensions.sizeX / teoPoints);

		// Crear ajuste de ejes (externa?)

		// Content
		for ( i = 0; i < teoPoints; i++ ) {
			let cText = createElement('text', {
				x:cDimensions.borderX0 + ( i * barGroupWidth) + (0.5 * barGroupWidth),
				y:cDimensions.borderY1 + 20,
				fill:'#333',
				'text-anchor':"middle"
			}, null, cElems.axisX);
			cText.se_text(properties.data[i][0]);

			// Small bar (skip first)
			createElement('line', {
				x1: cDimensions.borderX0 + (i * barGroupWidth) + ( 0.5 * barGroupWidth),
				x2: cDimensions.borderX0 + (i * barGroupWidth) + ( 0.5 * barGroupWidth),
				y1: cDimensions.borderY1 - 5,
				y2: cDimensions.borderY1 + 5,
				stroke: '#333',
				'stroke-width': '1'
			}, null, cElems.axisX);
		}

		// Dibujar líneas
		for ( j = 0; j < teoLen; j++ ) {
			let cPoints = '';
			for ( i = 0; i < teoPoints; i++ ) {
				let xPos = parseInt(( i * barGroupWidth) + ( 0.5 * barGroupWidth)),
					yPos = parseInt(cDimensions.sizeY - (cDimensions.sizeY * properties.data[i][j + 1] / cDimensions.sizeYUnits));
				if ( i !== 0 ) { cPoints+= ' '; }
				cPoints+= xPos + ',' + yPos;
			}
			// Imprimir
			createElement('polyline',
				{
					points:cPoints,
					fill:'none',
					stroke:pSettings.defaultColors[j % pSettings.defaultColors.length],
					'stroke-width':'4'
				}, null, cElems.chartArea
			);
		}

		//
	}
	//</editor-fold>

	//<editor-fold desc="Scatter Charts">
	//
	function chart_scatter_minmax(data) {
		let minValX = data[0].points[0][0], maxValX = data[0].points[0][0],
			minValY = data[0].points[0][1], maxValY = data[0].points[0][1],
			i, j;
		//
		for ( i = 0; i < data.length; i++ ) {
			let cData = data[i];
			for ( j = 0; j < cData.points.length; j++ ) {
				// Determinar X
				if ( cData.points[j][0] < minValX ) {
					minValX = cData.points[j][0];
				} else if ( cData.points[j][0] > maxValX ) {
					maxValX = cData.points[j][0];
				}
				// Determinar Y
				if ( cData.points[j][1] < minValY ) {
					minValY = cData.points[j][1];
				} else if ( cData.points[j][1] > maxValY ) {
					maxValY = cData.points[j][1];
				}
			}
		}

		// Definir interalo directamente
		chart_axisConverter(minValX, maxValX, minValY, maxValY);

		return {
			minX:minValX,
			maxX:maxValX,
			minY:minValY,
			maxY:maxValY
		};
	}

	//
	function chart_plot_scatter_points(properties) {
		let i, j,
			pointsLimit = chart_scatter_minmax(properties.data);

		// Crear estructura básica
		chart_buildSimpleStructure(properties.axis);

		// Crear ajuste de ejes (externa?)

		// Imprimir puntos
		for ( i = 0; i < properties.data.length; i++ ) {
			let cData = properties.data[i];
			for ( j = 0; j < cData.points.length; j++ ) {
				let xPos = parseInt(cDimensions.sizeX * cData.points[j][0] / cDimensions.sizeXUnits),
					yPos = parseInt(cDimensions.sizeY - (cDimensions.sizeY * cData.points[j][1] / cDimensions.sizeYUnits));
				// Imprimir
				createElement('circle',
					{
						cx:xPos,
						cy:yPos,
						r:5,
						fill:pSettings.defaultColors[i % pSettings.defaultColors.length]
					}, null, cElems.chartArea
				);
			}
		}
		//
	}

	//
	function chart_plot_scatter_linear(properties) {
		let i, j,
			pointsLimit = chart_scatter_minmax(properties.data);

		// Crear estructura básica
		chart_buildSimpleStructure(properties.axis);

		// Crear ajuste de ejes (externa?)

		// Imprimir línea
		for ( i = 0; i < properties.data.length; i++ ) {
			let cData = properties.data[i],
				cPoints = '';
			// Juntar puntos
			for ( j = 0; j < cData.points.length; j++ ) {
				let xPos = parseInt(cDimensions.sizeX * cData.points[j][0] / cDimensions.sizeXUnits),
					yPos = parseInt(cDimensions.sizeY - (cDimensions.sizeY * cData.points[j][1] / cDimensions.sizeYUnits));
				if ( j !== 0 ) { cPoints+= ' '; }
				cPoints+= xPos + ',' + yPos;
			}
			// Imprimir
			createElement('polyline',
				{
					points:cPoints,
					fill:'none',
					stroke:pSettings.defaultColors[i % pSettings.defaultColors.length],
					'stroke-width':'4'
				}, null, cElems.chartArea
			);
		}
		//
	}
	//</editor-fold>

	//
	function chart_plot_pie(properties) {
		let //
			totalValue = 0,
			currentAngle = -Math.PI / 2,
			nextAngle,
			nextPoint,
			curValue, curPercentage,
			largeArc,
			i;

		// Crear estructura básica
		cElems.chartArea = createElement('svg', {
			id:'graphs_container',
			class:'pie',
			x:0,
			y:'10%',
			width:'70%',
			height:'90%',
			'view-box':'0,0,1000,1000'
		});
		//
		cElems.chartDescription = createElement('svg', {
			id: 'graphs_description',
			x:'72%',
			y:'20%',
			width:'28%',
			height:'80%',
			'view-box':'0,0,1000,1000'
		});

		// re adjust?
		let drawSizeX = cElems.chartArea.width.baseVal.value,
			drawSizeY = cElems.chartArea.height.baseVal.value,
			tempCenterX = drawSizeX / 2,
			tempCenterY = drawSizeY / 2,
			tempRadius = ( drawSizeX > drawSizeY ) ? drawSizeY * 0.4 : drawSizeX * 0.4,
			currentPoint = {
				x:tempRadius * Math.cos(currentAngle),
				y:tempRadius * Math.sin(currentAngle)
			}
		;

		// console.log("draw size", cStatus.sizeX, cStatus.sizeY, drawSizeX, drawSizeY);


		// Get total value
		for ( i = 0; i < properties.data.length; i++ ) {
			totalValue+= parseFloat(properties.data[i][1]);
		}

		// Create each slice
		// console.log("Creando círculo");
		for ( i = 0; i < properties.data.length; i++ ) {
			curValue = parseFloat(properties.data[i][1]);
			nextAngle = currentAngle + ((curValue / totalValue) * 2 * Math.PI);
			nextPoint = {
				x: tempRadius * Math.cos(nextAngle),
				y: tempRadius * Math.sin(nextAngle)
			};
			largeArc = ( ( nextAngle - currentAngle ) < Math.PI ) ? '0': '1';
			// console.log("SECCIÓN: ", properties.data[i]);
			// console.log("cV: %s,\ncAngle: %s, nAngle: %s, isShort: %s, \ncpX: %s, cpY: %s,\nnpX: %s, npY: %s", curValue, currentAngle, nextAngle , ( ( nextAngle - currentAngle ) < Math.PI ), currentPoint.x, currentPoint.y, nextPoint.x, nextPoint.y);

			// Angulos sin conversión
			let path = `M${tempCenterX} ${tempCenterY} L${ currentPoint.x + tempCenterX } ${ currentPoint.y + tempCenterY } A${ tempRadius } ${ tempRadius } 0 ${ largeArc } 1 ${ nextPoint.x + tempCenterX } ${ nextPoint.y + tempCenterY } Z`;



			let cEl = createElement('path',
				{
					d:path,
					fill:pSettings.defaultColors[i % pSettings.defaultColors.length],
					stroke:'#CCC',
					'stroke-width':'1',
				},
				null,
				cElems.chartArea
			);

			//
			curPercentage = se.number.precisionRound( curValue / totalValue * 100, 2);

			//
			createElement('title',
				{},
				properties.data[i][0] + ' [' + curValue + ', ' + curPercentage + '%]',
				cEl
			);

			// Update currentAngle
			currentPoint = nextPoint;
			currentAngle = nextAngle;

			// Crear texto
			if ( true ) {
				let cYPosIni = 16;
				// Rectángulo
				cElems.chartTitle = createElement('rect',
					{
						x:0,
						y:20 * i,
						width:20,
						height:20,
						fill:pSettings.defaultColors[i % pSettings.defaultColors.length],
						stroke:'#CCC',
					},
					null,
					cElems.chartDescription
				);
				// Título
				cElems.chartTitle = createElement('text',
					{
						'text-anchor':'start',
						x:30,
						y:cYPosIni + 20 * i,
						width:35,
						height:20
					},
					properties.data[i][0],
					cElems.chartDescription
				);
				// Valor
				cElems.chartTitle = createElement('text',
					{
						'text-anchor':'start',
						x:100,
						y:cYPosIni + 20 * i,
						width:40,
						height:20
					},
					curValue.toString() + '[' + curPercentage + '%]',
					cElems.chartDescription
				);
			}
		}


	}

	//
	init();

	//
	return {
		chart:chart
	};
};