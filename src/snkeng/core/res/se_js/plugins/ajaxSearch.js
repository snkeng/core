"use strict";
//
se.plugin.ajaxSearch = function(pElement, options) {
	// Float
	let defaults = {
			jsonUrl:null,
			act_defaults:null,
			lim:null,
			cSort:null,
			//
			eIni: 0,
			eEnd: 0,
			eCur: 0,
			eLim: 20,
			eTot: 0,
			pCur: 0,
			pTot: 0,
			//
			scrollCount: 3,
			reload: false,
			isNew: false,
			scroll_load:true,
			loadFirst:true,
			//
			as_table: false,
			forms_grid:true,
			urlFilter:true,
			//
			f_id:'',
			f_type:'static',
			//
			container:null,
			actions:null,
			preProcessFunc:null
		},
		pSettings = se.object.merge(defaults, options),
		//
		pStatus = { 'loading': false, 'scrollCount': 0, 'empty': false, 'isNew':false, 'asTable':false, 'fragmet':'div'},
		advFilterBtnClose, advFilterBtnOpen, advFilterModal,
		actBar,
		filter,
		sortContent,
		cSel,
		loadMoreBtn;

	// Structure
	const content = pElement.querySelector('[se-ajaxelem="content"]'),
		response = pElement.querySelector('[se-ajaxelem="response"]'),
		templateBody = pElement.querySelector('[se-ajaxelem="template"]').se_html(),
		navBar = pElement.querySelector('[se-elem="navBar"]'),
		advFilter = pElement.querySelector('[se-elem="adv_filter"]'),

		// formBar,
		pFilter = pElement.querySelector('[se-elem="ajax_filter"]')
		;


	//<editor-fold desc="Initialization">
	function init() {
		// INITIALIZE
		pElement.se_classAdd("se_ajaxSimple");

		// Formularios y botones
		if ( pSettings.actions !== null ) {
			let btns = '',
				forms = '',
				action, cAct;

			//
			for ( const [action, cAct] of Object.entries(pSettings.actions) ) {
				// Botones
				if ( 'menu' in cAct ) {
					btns += createBtn(cAct.menu.title, action, cAct.menu.icon);
				}

				// Formularios
				if ( cAct.action === 'form' && 'print' in cAct && cAct.print ) {
					// Agregar acción
					switch ( cAct['ajax-action'] ) {
						case 'add':
							cAct.form.data = [{'name':'se-ajaxact', 'value':'add'}];
							break;
						case 'addMany':
							cAct.form.data = [{'name':'se-ajaxact', 'value':'addMany'}];
							break;
						case 'upd':
							cAct.form.data = [{'name':'se-ajaxact', 'value':'upd'}];
							break;
						default:
							console.log("ajax-action:" + cAct['ajax-action']);
							break;
					}
					forms += '<div>';
					if ( 'title' in cAct.form ) {
						forms += '<h3>' + cAct.form.title + '</h3>';
					}
					forms += se.form.createFromObj(cAct.form);
					forms += '</div>';
				}
			}

			//
			if ( btns !== '' ) {
				pElement.se_prepend('<div se-ajaxelem="actBar" class="menu">' + btns + '</div>');
				// Definition
				actBar = pElement.querySelector('[se-ajaxelem="actBar"]');
				actBar.se_on('click', '[se-ajaxbtn]', (e, elem) => {
					btnSolver(e, elem, 'menu');
				});
			}

			//
			if ( forms !== '' ) {
				let formClass = ( pSettings.forms_grid ) ? 'grid' : 'gODB gOMB';
				pElement.se_prepend('<div se-ajaxelem="forms" class="' + formClass + '">' + forms + '</div>');
				// formBar = pElement.querySelector('[se-ajaxelem="forms"]');
				// formBar.se_on('submit', 'form', formSubmit);
				// Plugin load
				se.main.pluginLoad(pElement);
			}
		}

		// Barra de navegación
		if ( navBar ) {
			//
			pStatus.asTable = true;
			// Existe la barra
			navBar.se_on('click', '[se-nav-act]', navBarOps);
			navBar.se_on('change', 'input', navBarOps);
		} else {
			// No hay barra, usar loadMore
			console.log("ajaxSearch?", pElement);
			loadMoreBtn = pElement.se_site_coreend('<div se-ajaxelem="loadMore" class="btn_loadMore">Mostrar más</div>');
			console.log("ajaxSearch?", loadMoreBtn);
			loadMoreBtn.se_hide();
			// Funciones
			loadMoreBtn.se_on('click', (e) => {
				e.preventDefault();
				pStatus.scrollCount = 0;
				loadMore(false);
			});

			// Autoload
			if ( pSettings.eTot >= pSettings.eLim ) {
				loadMoreBtn.se_show();
				//
				if ( pSettings.scroll_load ) {
					se.windowEvent(pElement, 'scroll', scrollLoad);
				}
			}
		}

		// Filter
		filter = ( pSettings.f_id !== '' ) ? $('#' + pSettings.f_id) : ( pFilter ) ? pFilter : null;
		if ( filter ) {
			filter.se_on('click', 'input[type=checkbox]', cbFilter);
			filter.se_on('change', ['input:not([type=checkbox])', 'select'], filterInput);
			switch ( pSettings.f_type ) {
				case 'fluid':
					// Actuales
					filter.querySelector('.current').se_on('click', 'li', filterFluidDel);
					// Opciones
					filter.querySelector('.available').se_on('click', 'li', filterFluidAdd);
					break;
				case 'static':
					filter.se_on('click', 'button', filterStatic);
					break;
				case null:
				case undefined:
					console.log("ajaxSearch, NO FILTER OBJECT");
					break;
				default:
					console.log("ajaxSearch, ERROR IN FILTER DEFINITION", pSettings.filterMode);
					break;
			}
		}

		//
		if ( advFilter ) {
			advFilterBtnOpen = advFilter.querySelector('button[se-act="adv_filter_open"]');
			advFilterBtnClose = advFilter.querySelector('button[se-act="adv_filter_close"]');
			advFilterModal = advFilter.querySelector('dialog');
			//
			advFilterBtnOpen.se_on('click', advFilterModalOpen);
			advFilterBtnClose.se_on('click', advFilterModalClose);
			advFilterModal.se_on('change', ['input', 'textarea', 'select'], advFilterInputUpdate);
			advFilterModal.se_on('input', 'input[type="search"]', se.funcs.debounce(advFilterInputSearch, 500));
			advFilterModal.se_on('click', 'div[se-elem="optionList"] div', advFilterOptionSelect);
			advFilterModal.se_on('click', 'button[se-act="filter_remove"]', advFilterFilterRemove);
		}

		// Sort
		sortContent = pElement.querySelector('[se-elem="sort"]');
		if ( sortContent ) { sortContent.se_on('click', '.sortable[data-name]', sortManager); }

		// Fixer (pre loaded content)
		se.element.fixValues(content);

		// Determinar fragmento
		switch ( content.tagName.toLowerCase() ) {
			case 'table':
			case 'tbody':
				pStatus.fragmet = 'tbody';
				break;
			default:
				pStatus.fragmet = 'div';
				break;
		}

		// CONTENIDO

		// Botones
		content.se_on('click', '[se-ajaxbtn]', (e, elem) => {
			btnSolver(e, elem, 'obj');
		});

		// Inputs
		content.se_on('change', ['input', 'select'], rowChange);

		// Todos los formularios
		pElement.se_on('submit', 'form', formSubmit);

		//
		if ( pSettings.loadFirst ) {
			loadMore(true);
		}
	}
	//</editor-fold>

	//<editor-fold desc="Operaciones internas">

	//<editor-fold desc="Core">
	// Modo selección
	function navBarOps(event, pElement) {
		event.preventDefault();
		let reload = false;

		//
		switch ( pElement.se_attr('se-nav-act') ) {
			case 'reload':
				reload = true;
				break;

			case 'first':
				if ( pSettings.pCur !== 0 ) {
					pSettings.pCur = 0;
					reload = true;
				}
				break;

			case 'prev':
				if ( pSettings.pCur > 0 ) {
					pSettings.pCur--;
					reload = true;
				}
				break;

			case 'next':
				if ( pSettings.pCur < pSettings.pTot - 1 ) {
					pSettings.pCur++;
					reload = true;
				}
				break;

			case 'last':
				if ( pSettings.pCur < pSettings.pTot - 1 ) {
					pSettings.pCur = pSettings.pTot - 1;
					reload = true;
				}
				break;

			//
			case 'cpage':
				let posiblePage = intVal(pElement.value) - 1;
				if ( posiblePage !== pSettings.pCur && posiblePage >= 0 && posiblePage < pSettings.pTot ) {
					pSettings.pCur = posiblePage;
					reload = true;
				}
				break;

			//
			default:
				console.log('FAIL - Act:', pElement, pElement.se_attr('se-ajaxnavs'));
				break;
		}
		if ( reload ) {
			loadMore(false);
		}
	}
	//
	function rowChange(e, cObj) {
		let cElem = cObj.se_closest('[se-ajaxelem="obj"]');
		cElem.se_classAdd('mod');
	}
	//
	function formSubmit(event, cElem) {
		event.preventDefault();
		event.stopPropagation();
		console.log(event);
		//
		let action = cElem.se_attr('se-ajaxact'),
			shortElem = pSettings.actions[action],
			url = ( cElem.se_attr('action') !== '' ) ? cElem.se_attr('action') : null,
			post = new FormData(cElem),
			cResponse = cElem.querySelector('[se-elem="response"]'),
			defaults = null,
			onSuccess = null,
			onFail = null,
			solver = null;
		//
		if ( typeof shortElem === 'undefined' ) { return; }
		// Determine function location
		switch ( shortElem.action ) {
			case 'form':
				if ( 'form' in shortElem ) {
					solver = shortElem.form;
				} else { console.log('Formulario no definido.'); }
				break;
			case 'direct':
				solver = shortElem;
				break;
			default:
				console.log("JSON MAL DEFINIDO, no action");
				break;
		}
		url = ( url === null && 'save_url' in solver ) ? solver.save_url : url;
		defaults = ( defaults === null && 'save_data' in solver ) ? solver.save_data : null;
		// Agregar defaults
		if ( defaults !== null ) {
			for ( let i = 0, len = defaults.length; i < len; i++ ) {
				post.append(defaults[i].name, defaults[i].value);
			}
		}
		post.se_jsonAppend(pSettings.act_defaults);

		//
		switch ( shortElem['ajax-action'] ) {
			case 'add':
				onSuccess = (msg) => {
					addElement(msg.d);
				};
				break;
			case 'addMany':
				console.log("add many form");
				onSuccess = function(msg) {
					addMultiple(msg.d);
				};
				break;
			// Replaces the element
			case 'upd':
				post.append('id', cElem.se_data('objid'));
				onSuccess = function(msg) {
					updateElement(cElem, msg.d);
				};
				onFail = function(msg) {
					alert(msg.s.e);
				};
				break;
			// do nothing on success;
			case 'none':
				post.append('id', cElem.se_data('objid'));
				shortElem.reset = false;
				onSuccess = function(msg) {
					cElem.se_classDel('mod');
				};
				onFail = function(msg) {
					alert(msg.s.e);
				};
				break;
			case 'custom':
				if ( typeof solver.onSuccess === 'function' ) {
					onSuccess = function(msg) {
						solver.onSuccess(cElem, msg.d);
					}
				}
				if ( typeof solver.onFail === 'function' ) {
					onFail = function(msg) {
						solver.onFail(cElem, msg);
					}
				}
				break;
			//
			default:
				console.warn("ajax action not defined.");
				break;
		}
		// Reiniciar a menos que explicitamente soliciten que no
		if ( !('reset' in shortElem && !shortElem.reset) ) {
			cElem.reset();
			cElem.elements[0].focus();
		}
		//
		if ( url !== null && typeof url !== 'undefined' ) {
			// console.log(cElem, cResponse);
			se.ajax.json(url, post, {
				response:cResponse,
				onSuccess:onSuccess,
				onFail:onFail
			});
		} else { console.log('ajaxSearch - No query.', url, solver); }
	}
	//
	function createBtn(name, action, icon) {
		let btnTxt = '<button class="btn" title="' + name + '" se-ajaxbtn="' + action + '">';
		btnTxt += ( typeof icon !== 'undefined' && icon !== '' ) ? '<svg class="icon inline mr"><use xlink:href="#' + icon + '"></use></svg>' : '';
		btnTxt += name + '</button>';
		return btnTxt;
	}
	//
	function btnSolver(event, object, origin) {
		// event.preventDefault();
		// event.stopPropagation();
		let action = object.se_attr('se-ajaxbtn'),
			shortElem = pSettings.actions[action],
			cElem, objId;
		if ( typeof shortElem !== 'undefined' ) {
			if ( shortElem['ajax-elem'] !== 'obj' || (shortElem['ajax-elem'] === 'obj' && origin === 'obj' ) ) {
				switch ( shortElem.action ) {
					// Formulario
					case 'form':
						let cForm = shortElem.form,
							formLoadUrl = ( 'load_url' in cForm ) ? cForm.load_url : null,
							formLoadData = ( 'load_data' in cForm ) ? cForm.load_data : null,
							formSaveUrl = ( 'save_url' in cForm ) ? cForm.save_url : null,
							formSaveData = ( 'save_data' in cForm ) ? cForm.save_data : null,
							actName = ( 'actName' in cForm ) ? cForm.actName : null,
							onSuccess = null;

						// Determine element actions by type
						if ( shortElem['ajax-elem'] && origin === 'obj' && shortElem['ajax-action'] === 'edit' ) {
							cElem = object.se_closest('[se-ajaxelem="obj"]');
							objId = cElem.se_data('objid');
							let loadDataId = null,
								saveDataId = null;

							// update urls if needed or add to post information (old method)
							if ( formLoadUrl && formLoadUrl.includes('{id}') ) {
								formLoadUrl = formLoadUrl.replace('{id}', objId);
							} else {
								loadDataId = {'id': objId};
							}
							//
							if ( formSaveUrl && formSaveUrl.includes('{id}') ) {
								formSaveUrl = formSaveUrl.replace('{id}', objId);
							} else {
								saveDataId = {'id': objId};
							}

							//
							formLoadData = se.object.merge(pSettings.act_defaults, loadDataId, formLoadData);
							formSaveData = se.object.merge(pSettings.act_defaults, saveDataId, formSaveData);
							//
							let skipElemUpdate = ( !('ajax-update' in shortElem) || ( 'ajax-update' in shortElem && !shortElem['ajax-update'] ) );

							//
							if ( !skipElemUpdate ) {
								onSuccess = (msg) => {
									updateElement(cElem, msg.d);
								};
							}
						} else if ( shortElem['ajax-action'] === 'add' ) {
							formSaveData = se.object.merge(pSettings.act_defaults, formSaveData);
							console.log("ADD FORM", formSaveData);
							onSuccess = (msg) => {
								addElement(msg.d, true);
							};
						} else if ( shortElem['ajax-action'] === 'addMany' ) {
							console.log("button form add");
							formSaveData = se.object.merge(pSettings.act_defaults, formSaveData);
							onSuccess = (msg) => {
								addMultiple(msg.d, true);
							};
						} else if( shortElem['ajax-action'] === 'function' ) {
							//
							onSuccess = cForm.onSuccess;
						} else if( shortElem['ajax-action'] === 'editMany' ) {
							let cSelection = content.querySelectorAll('input:checked');
							if ( cSelection.length !== 0 ) {
								let targets = [];
								cSelection.se_each((id, cEl) => {
									targets.push(cEl.se_closest('[se-ajaxelem="obj"]').se_data('objid'));
								});
								//
								formSaveData = se.object.merge(pSettings.act_defaults, formSaveData, { 'idElems':targets });
								console.log("LKAJSDLFKJASDF", pSettings.act_defaults, formSaveData, {'idElems':targets}, formSaveData);
								onSuccess = (msg) => {
									reload();
								};
							} else {
								alert("No se detectan elementos seleccionados.");
								return;
							}
						}

						// Llamado
						se.addon.windowForm({
							title:cForm.title,
							load_url:formLoadUrl,
							load_data:formLoadData,
							save_url:formSaveUrl,
							save_data:formSaveData,
							elems:cForm.elems,
							actName:actName,
							onSuccess:onSuccess
						});
						break;

					//
					case 'actionWindow':
						cElem = object.se_closest('[se-ajaxelem="obj"]');
						objId = cElem.se_data('objid');
						//
						let cWindow = shortElem.actionWindow,
							actionWinParams = {
								title:( 'title' in cWindow ) ? cWindow.title : 'Encabezado',
								description:( 'description' in cWindow ) ? cWindow.description : '',
								defaults:se.object.merge(pSettings.act_defaults, { 'id':objId }),
								buttons:( 'buttons' in cWindow ) ? cWindow.buttons : []
							};

						// Llamado
						se.addon.windowAction(actionWinParams);
						break;

					// Función
					case 'function':
						let selected;
						if ( typeof shortElem.function ==='function' ) {
							switch ( shortElem['ajax-elem'] ) {
								//
								case 'obj':
									cElem = object.se_closest('[se-ajaxelem="obj"]');
									shortElem.function(cElem, object, pSettings.act_defaults, cElem.se_data('objid'), cElem.se_data('objtitle'));
									break;
								//
								case 'all':
									break;
								//
								case 'sel':
									selected = content.querySelectorAll('input:checked');
									if ( selected.length !== 0 ) {
										let targets = [];
										selected.se_each(function(id, cEl) {
											targets.push(cEl.se_closest('[se-ajaxelem="obj"]'));
										});
										shortElem.function(targets, pSettings.act_defaults);
									} else {
										console.log("No se detectan elementos seleccionados.");
									}
									break;
								//
								case 'mod':
									selected = content.querySelectorAll('input:checked');
									if ( selected.length !== 0 ) {
										shortElem.function(selected, pSettings.act_defaults);
									} else {
										console.log("No se detectan pElementos modificados.");
									}
									break;
								//
								case 'none':
									shortElem.function(content, pSettings.act_defaults);
									break;
								//
								default:
									console.log('shortElem.ajax-elem no definido', shortElem['ajax-elem']);
									break;
							}
						} else { console.log("Operación no válida."); }
						break;

					// Guardar todos
					case 'saveAll':
						// Seleccionar objetos
						let selects = content.querySelectorAll('form.mod');
						if ( selects.length !== 0 ) {
							// Convertir en arrays
							let cData = {},
								sendData = {};
							selects.se_each(function(i, cEl) {
								cData[cEl.se_data('objid')] = se.form.serializeToObj(cEl);
							});
							//
							sendData['data'] = JSON.stringify(cData);

							// If summary element
							let cSummary = pElement.querySelector('form[se-ajaxelem="summaryForm"]');
							if ( cSummary ) {
								sendData['summary'] = JSON.stringify(se.form.serializeToObj(cSummary));
							}

							// Enviar
							se.ajax.json(shortElem.save_url,
								se.object.merge(pSettings.act_defaults, sendData), {
									onSuccess:() => {
										// loadMore(true);
										selects.se_classDel('mod');
									},
									onFail:(msg) => {
										alert(msg.s.e);
									}
								}
							);
						} else {
							//
							console.log("Sin cambios");
						}
						break;

					// Borrar
					case 'del':
						cElem = object.se_closest('[se-ajaxelem="obj"]');
						objId = cElem.se_data('objid');
						if ( confirm('¿Desea borrar el pElemento "' + cElem.se_data('objtitle') + '" de la base de datos?') ) {
							se.ajax.json(
								shortElem.del_url,
								se.object.merge(pSettings.act_defaults, { 'id':objId }),
								{
									onSuccess:() => { cElem.se_remove(); }
								}
							);
						} else {
							console.log('fail');
						}
						break;

					//
					default:
						console.log('Función no definida', shortElem.action, object);
						break;
				}
			} else {
				content.addEventListener('click', modeUpdate);
			}
		} else {
			// Error, operación no definida
			console.log('Objeto no definido', object, action, pSettings.actions);
		}
	}
	//
	function modeUpdate(event, cObj) {
		event.preventDefault();
		event.stopPropagation();
		let cElem = cObj.se_closest('[se-ajaxelem="obj"]');
		if ( cElem ) {
			cSel = cElem;
			// Unbind
			content.removeEventListener('click', modeUpdate);
			// Seleccionar
			cElem.se_css('background-color', '#DEDEDE');
			//
			let cForm = pSettings.formData.edit;
			editForm(cElem, cForm)
		}

		return false;
	}
	//
	function modeDelete(event) {
		event.preventDefault();
		event.stopPropagation();
		let cObj = $(event.target),
			cElement = cObj.se_closest('[se-ajaxelem="obj"]');
		if ( cElement ) {
			cSel = cElement;
			// Unbind
			content.removeEventListener('click', modeDelete);
			//
			deleteObject(cElement);
		}
	}
	// Operaciones
	function editForm(cElement, cForm) {
		let objId = cElement.se_data('objid');
		se.addon.floatWin({
			'defHead': cForm.title,
			'defBody': se.form.createFromObj(cForm),
			'defJS': '',
			'defMT': '',
			'backClose': true,
			'onLoad': function() {
				let window = $('#se_dialog'),
					form = window.querySelector('form'),
					response = form.querySelector('[data-elemtype="response"]');
				// Cargar información
				if ( cForm.load !== '' ) {
					se.ajax.json(
						cForm.load,
						{ 'objId': objId },
						{
							'response':response,
							onCall:() => {
								se.struct.notifSimple(response, 'notification', '<span><svg class="icon inline spin"><use xlink:href="#fa-spinner"></use></svg></span>');
							},
							onSuccess:(msg) => {
								se.form.loadData(form, msg.d);
							}
						}
					);
				}
				// Crear simple form
				let params = { 'response': response };
				params.onSuccess = ( 'onSuccess' in cForm ) ? cForm.onSuccess : function(msgs) {
					cElement.se_replaceWith(createElement(msgs.d));
				};
				form.se_plugin('simpleForm', se.plugin.simpleForm, params);
				console.log(form.pElements[0]);
				form.pElements[0].focus();
				// Plugin load
				se.main.pluginLoad(window);
			}
		});
	}
	//</editor-fold>

	//<editor-fold desc="Filter básico">
	//
	function filterFluidAdd(e, cObj) {
		let container = cObj.parentElement,
			filterGroup = container.parentElement,
			cValId = cObj.se_attr('data-cfId'),
			cValIdNm = cObj.se_html(),
			cValGet = filterGroup.se_attr('data-cfGet'),
			cValTit = filterGroup.se_attr('data-cfTitle'),
			// Crear separación
			html = '<li data-cfId="' + cValId + '" data-cfIdNm="' + cValIdNm + '" data-cfGet="' + cValGet + '" data-cfTitle="' + cValTit + '">' + cValTit + ': ' + cValIdNm + '</li>';
		//
		filter.querySelector('.current').se_site_coreend(html);
		// Adicionar al filtro y cargar
		pSettings.lim[cValGet] = cValId;
		loadMore(true);
		// Esconder
		filterGroup.se_hide();
	}
	//
	function filterFluidDel(e, cEl) {
		let cValGet = cEl.se_attr('data-cfGet');
		// Mostrar
		filter.querySelector('.available [data-cfGet="' + cValGet + '"]').se_show();
		// Quitar filtro y cargar
		delete pSettings.lim[cValGet];
		loadMore(true);
		// Destruir el pElemento
		cEl.se_remove();
	}
	//
	function filterStatic(e, cObj) {
		e.preventDefault();
		let type = cObj.se_data('type'),
			name = cObj.se_data('name'),
			val = cObj.se_data('value'),
			cCont = cObj.se_closest('div');
		// Cambio de estilo
		cCont.querySelectorAll('.sel').se_classDel('sel');
		cObj.se_classAdd('sel');
		if ( val !== '' ) {
			pSettings.lim[name] = val;
		} else {
			delete pSettings.lim[name];
		}
		if ( type === 'filter' ) {
			loadMore(true);
		} else if ( type === 'sort' ) {
			loadMore(false);
		} else {
			console.log('Tipo de filtro no definido');
		}
	}
	//
	function cbFilter(e, el) {
		// Definir condición inicial
		if ( !el.se_data('checked') ) {
			if ( el.checked ) {
				el.se_data('checked', 0);
			} else {
				el.se_data('checked', 2);
			}
		}
		//
		let name = el.se_attr('name');
		switch ( el.se_data('checked') ) {
			// unchecked, going indeterminate
			case '0':
				el.se_data('checked',1);
				el.indeterminate = true;
				//
				pSettings.lim[name] = 0;
				break;

			// indeterminate, going checked
			case '1':
				el.se_data('checked',2);
				el.indeterminate = false;
				el.checked = true;
				//
				pSettings.lim[name] = 1;
				break;

			// checked, going unchecked
			default:
				el.se_data('checked',0);
				el.indeterminate = false;
				el.checked = false;
				//
				delete pSettings.lim[name];
				break;
		}
		loadMore(true);
	}
	//
	function filterInput(e, cObj) {
		console.log("ajaxSimple: ", cObj);
		let name = cObj.se_attr('name'),
			val = cObj.value;
		// Cambio de estilo
		if ( val !== '' ) {
			pSettings.lim[name] = val;
		} else {
			delete pSettings.lim[name];
		}
		loadMore(true);
	}
	//</editor-fold>

	//<editor-fold desc="Adv Filter">
	//
	function advFilterInputSearch(e, cObj) {
		if ( cObj.value.length < 3 ) {
			return;
		}
		console.log("SEARGHING FOR THE MEANING OF LIFE");

		let cLabel = cObj.se_closest('label'),
			cDataList = cLabel.querySelector('div[se-elem="optionList"]');
		cDataList.se_empty();

		//
		se.ajax.json(
			cObj.se_data('url'),
			se.object.merge(pSettings.act_defaults, {'title':cObj.value}),
			{
				onSuccess:(msg) => {
					cDataList.se_empty();
					msg.d.forEach((cEl) => {
						cDataList.se_site_coreend(`<div data-id="${ cEl.id }"> ${ cEl.title }</div>`);
					});
				},
				onRequest:() => {
					cDataList.se_text('Cargando...')
				}
			}
		);
	}
	//
	function advFilterInputUpdate(e, cObj) {
		console.log("INPUT UPDATED", e, cObj);
		let cLabel = cObj.se_closest('label'),
			cName = cObj.name,
			cVal = cObj.value.trim(), cMod, cFilterValue;
		//
		switch ( cLabel.se_data('type') ) {
			//
			case 'idGroup':
				cVal = cVal.replace(/[\s,]+/g, ',').replace(/[^\d,]/g, '').replace(/,+/g, ',');
				cMod = cVal.split(',');
				cMod = cMod.filter((item, index) => {
					return cMod.indexOf(item) === index;
				});
				cMod.sort((a, b) => {
					return a - b;
				});
				//
				cFilterValue = cMod.join(',');
				console.log("el valor filtrado es: ", cVal, cMod, cFilterValue);
				//
				cLabel.se_data('filtered', 1);
				cLabel.querySelector('div.cFilterValue span').se_text(cFilterValue);
				pSettings.lim[cName] = cFilterValue;
				break;
			//
			case 'dbList':
				//
				cLabel.se_data('filtered', 1);
				cLabel.querySelector('div.cFilterValue span').se_text(cObj.options[cObj.selectedIndex].text);
				pSettings.lim[cName] = cVal;
				break;
			//
			case 'dQuery':
				return;
				break;
			//
			default:
				console.error("AJAXNAVIGATION - ADVFILTER: CASO NO PROGRAMADO");
				break;
		}
	}
	//
	function advFilterOptionSelect(e, cBtn) {
		e.preventDefault();
		let cLabel = cBtn.se_closest('label'), cInput;
		//
		cLabel.se_data('filtered', 1);
		cLabel.querySelector('div.cFilterValue span').se_text(cBtn.se_text());
		cInput = cLabel.querySelector('input');
		pSettings.lim[cInput.name] = cBtn.se_data('id');
	}
	//
	function advFilterFilterRemove(e, cBtn) {
		let cLabel = cBtn.se_closest('label'),
			cName,
			spanText = cLabel.querySelector('div.cFilterValue span'),
			cInput;
		cLabel.se_data('filtered', 0);

		switch ( cLabel.se_data('type') ) {
			//
			case 'idGroup':
				cInput = cLabel.querySelector('input');
				cName = cInput.name;
				cInput.value = spanText.se_text();
				break;
			//
			case 'dbList':
				cInput = cLabel.querySelector('select');
				cName = cInput.name;
				cInput.selectedIndex = 0;
				break;
			//
			case 'dQuery':
				cInput = cLabel.querySelector('input');
				cName = cInput.name;
				break;
			//
			default:
				console.error("AJAXNAVIGATION - ADVFILTER: CASO NO PROGRAMADO");
				break;
		}
		// Desfiltrar
		delete pSettings.lim[cName];
		cLabel.querySelector('div.cFilterValue span').se_text('');
	}

	//
	function advFilterModalOpen(e) {
		se.dialog.open(advFilterModal);
	}

	//
	function advFilterModalClose(e) {
		se.dialog.close(advFilterModal);
		reload();
	}

	//</editor-fold>

	// Sorts
	function sortManager(e, cEl) {
		e.preventDefault();
		let name = cEl.se_data('name'),
			sort = cEl.se_data('sort'),
			nSort = '';
		//
		if ( sort === '' ) {
			nSort = 'asc';
		} else if ( sort === 'asc' ) {
			nSort = 'desc';
		} else {
			nSort = '';
		}

		// Clear all
		let curSel = sortContent.querySelectorAll('[data-sort]').se_data('sort', '');
		cEl.se_data('sort', nSort);

		// Updates
		if ( nSort === '' ) {
			pSettings.cSort = null;
		} else {
			pSettings.cSort = name+','+nSort;
		}
		//
		loadMore(false);
	}
	// scrollLoad
	function scrollLoad(ev) {
		let rect = loadMoreBtn.se_offset(),
			wintop = window.scrollY,
			winheight = window.outerHeight,
			loadMorePos = rect.y;
		if ( ((winheight + wintop) > loadMorePos) && !pStatus.empty && !pStatus.loading && pStatus.scrollCount < pSettings.scrollCount ) {
			pStatus.scrollCount++;
			loadMore(false);
		}
	}
	// Cargado
	function loadMore(vClear) {
		let fCount = 0;
		// Response
		if ( response ) {
			se.struct.notifSimple(response, 'notification', '<span>Cargando...  <svg class="icon inline spin"><use xlink:href="#fa-spinner"></use></svg></span>');
		}

		if ( pStatus.asTable ) {
			// Reinicia siempre
			content.se_html(pSettings.container.sprintf('<svg class="icon inline spin"><use xlink:href="#fa-spinner fa-spin" /></svg>'));
			if ( vClear ) {
				pSettings.pCur = 0;
				pSettings.isNew = true;
			}
		}
		else {
			// Técnica unlimited scroll
			if ( vClear ) {
				// Condición de reinicio
				content.se_html(pSettings.container.sprintf('<svg class="icon inline spin">< xlink:href="#fa-spinner fa-spin" /></svg>'));
				pSettings.pCur = 0;
				fCount = 1;
				pStatus.empty = false;
				pSettings.isNew = true;
				if ( pStatus.asTable ) {

				}
			} else {
				pSettings.pCur++;
			}
		}

		//
		pStatus.loading = true;
		let queryData = se.object.merge(
			pSettings.act_defaults,
			{
				order:pSettings.cSort,
				where:pSettings.lim,
				pCur:pSettings.pCur,
				fCount:fCount,
				isNew:pSettings.isNew
			}
		);


		//
		se.ajax.json(
			pSettings.jsonUrl + '?' + se.object.serialize(queryData),
			null,
			{
				onComplete:() => {
					response.se_html('');
					pStatus.loading = false;
					pSettings.isNew = false;
				},
				onSuccess:(msgs) => {
					// Borrar datos
					if ( pStatus.asTable || vClear ) {
						content.se_empty();
						pSettings.eCur = 0;
					}

					//
					if ( msgs.p.eCur !== 0 ) {
						// Agregar todos
						addMultiple(msgs.d);

						// Tabla con navegación
						if ( navBar ) {
							navBar.querySelector('span[data-count="eIni"]').se_text(msgs.p.eIni);
							navBar.querySelector('span[data-count="eEnd"]').se_text(msgs.p.eEnd);
							navBar.querySelector('input[data-count="pCur"]').value = pSettings.pCur + 1;
							//
							if ( msgs.p.isNew ) {
								navBar.querySelector('span[data-count="eTot"]').se_text(msgs.p.eTot);
								navBar.querySelector('span[data-count="pTot"]').se_text(msgs.p.pTot);
								pSettings.pTot = msgs.p.pTot;
							}
						}
						// Checar fin
						if ( loadMoreBtn ) {
							if ( msgs.p.eCur < msgs.p.eLim ) {
								loadMoreBtn.se_hide();
								pStatus.empty = true;
							} else {
								loadMoreBtn.se_show();
								pStatus.empty = false;
							}
						}
					} else {
						pStatus.empty = true;
						if ( pSettings.eCur === 0 ) {
							content.se_html(pSettings.container.sprintf('No hay resultados'));
						}
						if ( loadMoreBtn ) {
							loadMoreBtn.se_hide();
						}
					}
				}
			}
		);
	}
	//</editor-fold>

	//<editor-fold desc="Operaciones externas">

	//
	function createElement(data) {
		if ( typeof pSettings.preProcessFunc === 'function' ) {
			data = pSettings.preProcessFunc(data);
		}
		return se.struct.stringPopulate(templateBody, data);
	}

	//
	function addMultiple(msg, dirTop) {
		let tCont = document.createElement(pStatus.fragmet),
			direction = ( dirTop ) ? 'afterbegin' : 'beforeend';

		// Cargar datos
		for ( let i = 0, len = msg.length; i < len; i++) {
			pSettings.eCur++;
			tCont.insertAdjacentHTML(direction, createElement(msg[i]));
			// addElement(msgs.d[i]);
		}
		// Areglar pElementos (valores)
		se.element.fixValues(tCont);
		// Pasar
		while ( tCont.childNodes.length > 0 ) {
			if ( dirTop) {
				content.insertBefore(tCont.childNodes[0], content.children[0]);
			} else {
				content.appendChild(tCont.childNodes[0]);
			}
		}
	}

	//
	function addElement(msg, dirTop) {
		let direction = ( dirTop ) ? 'afterbegin' : 'beforeend',
			cEl;
		if ( pSettings.eCur === 0 ) { content.se_empty(); }
		content.insertAdjacentHTML(direction, createElement(msg));
		pSettings.eCur++;
		cEl = ( dirTop ) ? content.firstElementChild : content.lastElementChild;
		se.element.fixValues(cEl);
	}

	//
	function updateElement(cEl, msg) {
		let nEl = cEl.se_replaceWith(createElement(msg));
		se.element.fixValues(nEl);
		return nEl;
	}

	//
	function elementExists(id) {
		let oEl = content.querySelector('[data-objid="'+id+'"]');
		// console.log(element);
		return oEl;
	}

	//
	function updElement(msg) {
		let cData = [],
			cElemsL = pSettings.elems.length;
		//
		for ( let y = 0; y < cElemsL; y++ ) {
			cData.push(msg.d[pSettings.elems[y]]);
		}

		// Buscar pElemento original
		let oEl = content.querySelector('[data-objid="'+msg.d.id+'"]');
		if ( !oEl ) {
			console.warn("UPDELEMENT FAIL, not exists", msg.d.id);
			return;
		}

		//
		let nEl = cSel.se_replaceWith(se.struct.stringPopulate(pSettings.struct, cData));
		se.element.fixValues(nEL);
	}

	//
	function removeElement(msgs) {
		cSel.se_remove();
	}

	// Refresh
	function reload() {
		loadMore(true);
	}

	//</editor-fold>

	//
	init();

	//
	return {
		addMultiple:addMultiple,
		addElement:addElement,
		updElement:updElement,
		elementExists:elementExists,
		elementUpd:updateElement,
		removeElement:removeElement,
		reload:reload
	};
};
//
