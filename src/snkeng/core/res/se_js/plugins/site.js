"use strict";

//
class seLoginForm extends HTMLFormElement {
	//
	constructor() {
		// Always call super first in constructor
		super();

		//

		this.inputShowPass = this.querySelector('input[se-elem="showPass"]');

		//
		if ( this.inputShowPass ) {
			this.inputShowPass.se_on('change', this.passWordShowToggle);
		}

		//
		this.se_plugin('simpleForm', {
			save_url: '/api/user/log/in/email',
			onSuccess: (msg) => {
				se.user.logIn(msg, this);
			}
		});
	}

	//
	passWordShowToggle(e) {
		e.preventDefault();

		let inputPassword = this.querySelector('input[name="current-password"]');

		//
		if ( this.inputShowPass.checked ) {
			inputPassword.se_attr('type', 'text');
		} else {
			inputPassword.se_attr('type', 'password');
		}
	}
}

// Define the new element
customElements.define('se-login-form', seLoginForm, { extends: 'form' });



//
se.plugin.userSiteRegistration = function(element, options) {
	element.se_plugin('simpleForm', {
		save_url:'/api/user/register',
		onSuccess:(msg) => {
			se.user.logIn(msg, element);
		}
	});
};

//