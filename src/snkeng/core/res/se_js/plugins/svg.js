"use strict";
se.plugin.se_svg = function (plugElem, plugOptions) {
	const ns = 'http://www.w3.org/2000/svg';
	let svg,
		//
		pDefaults = {
			width:'100%',
			height:'100%'
		},
		pSettings = se.object.merge(pDefaults, plugOptions);

	//
	function init() {
		svg = document.createElementNS(ns, 'svg');
		svg.se_attr('width', pSettings.width);
		svg.se_attr('height', pSettings.height);
		plugElem.appendChild(svg);
	}

	//
	function createElement(elName, elProps, parentElement) {
		let newElem = document.createElementNS(ns, elName);
		elementSetProperties(newElem, elProps);
		if ( parentElement ) {
			parentElement.appendChild(newElem);
		} else {
			svg.appendChild(newElem);
		}
		return newElem;
	}

	//
	function elementSetProperties(element, properties) {
		//
		for ( const [cProp, cValues] of Object.entries(properties) ) {
			element.se_attr(cProp, cValues.toString());
		}
	}

	init();
	return {
		createElement:createElement,
		elementSetProperties:elementSetProperties
	};
};