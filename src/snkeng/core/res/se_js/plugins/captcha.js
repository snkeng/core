"use strict";

//
se.plugin.captcha = function(plugElem, plugOptions) {
	let defaults = {},
		settings = se.object.merge(defaults, plugOptions),
		cFormPos = null,
		image = null;

	//
	function init() {
		console.log("captcha initialized by init");
		generateElements();
	}

	//
	function load() {
		console.log("captcha initialized by load");
	}

	//
	function btnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'refresh':
				console.log("refreshing image");
				image.src = "/api/user/captcha/request?date=" + se.uniqueId()
				break;
			//
			case 'close':
				break;
			//
			default:
				console.error("engine windows action method not recognized", cBtn);
				break;
		}
	}

	//
	function generateElements() {
		cFormPos = $('#g_captcha');

		console.log("el elemento es:", cFormPos);

		if ( !cFormPos ) { console.error("form position not found."); return; }

		//
		cFormPos.se_on('click', 'button', btnAction);

		//
		cFormPos.se_site_coreend(`<label class="separator required">
<div class="cont"><span class="title">Captcha</span></div>
<div style="display:flex; justify-content:space-between; align-content:center; margin-bottom:10px;"><img src="/api/user/captcha/request" /><button class="btn blue" se-act="refresh"><svg class="icon inline"><use xlink:href="#fa-refresh" /></svg></button></div>
<input type="text" name="se-captcha-response" title="Captcha" required />
</label>`);

		image = cFormPos.querySelector('img');

		console.warn("cImage", image);
	}

	//
	init();
	return {
		load:load
	};
};
//
