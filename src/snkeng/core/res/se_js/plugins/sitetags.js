"use strict";

se.plugin.siteTags = function(plugEl, options) {
	var defaults = {
			art_id:null,
			tag_url:null,
			elem_url_add:null,
			elem_url_del:null,
			tag_list:null,
			tag_current:null
		},
		settings = se.object.merge(defaults, options),
		//
		tagForm = plugEl.querySelector('form'),
		tagName = plugEl.querySelector('input[name="tagName"]'),
		tagList = $('#tagList'),
		tagTemp = plugEl.querySelector('template'),
		tagCont = plugEl.querySelector('div[se-type="content"]'),
		//
		i, iLen, iCur, j, jLen, jCur, tCont;
	// Parse
	iLen = settings.tag_list.length;
	jLen = settings.tag_current.length;
	tCont = tagTemp.se_html();
	// All tags
	for ( i = 0; i < iLen; i++ ) {
		iCur = settings.tag_list[i];
		console.log("tagAdd", iCur);
		tagList.se_site_coreend(se.struct.stringPopulate("<option value='!title;' data-id='!id;'>", iCur));
		// Current
		for ( j = 0; j < jLen; j++ ) {
			jCur = settings.tag_current[j];
			if ( jCur.tId == iCur.id ) {
				tagCont.se_site_coreend(se.struct.stringPopulate(tCont, {rId:jCur.rId, tId:jCur.tId, title:iCur.title}));
			}
		}
	}
	// bindings
	tagCont.se_on('click', 'button', tagButton);
	tagForm.se_on('submit', tagFormSubmit);

	//
	function tagFormSubmit(ev) {
		ev.preventDefault();
		var name = tagName.se_val(),
			i, iCur, iLen = settings.tag_list.length;
		//
		for ( i = 0; i < iLen; i++ ) {
			iCur = settings.tag_list[i];
			if ( iCur.title == name ) {
				tagAdd(iCur.id, iCur.title);
				tagName.se_val('');
				break;
			}
		}
	}
	//
	function tagButton(ev) {
		ev.preventDefault();
		var btn = ev.target,
			cTag = btn.se_closest('.tag');
		console.log(cTag);
		switch ( btn.se_data('act') ) {
			case 'del':
				tagDel(cTag.se_data('rid'));
				break;
			default:
				console.log("ERROR");
				break;
		}
	}
	function tagAdd(tId, tTitle) {
		se.ajax.json(settings.elem_url_add, {aId:settings.art_id, tId:tId}, {
			onSuccess:function(msg) {
				tagCont.se_site_coreend(se.struct.stringPopulate(tCont, {rId:msg.d.rId, tId:tId, title:tTitle}));
			},
			onFail:function(msg) {
				console.log("tag add fail", msg);
			}
		});
	}
	function tagDel(rId) {
		se.ajax.json(settings.elem_url_del, {aId:settings.art_id, rId:rId}, {
			onSuccess:function(msg) {
				tagCont.querySelector('div[data-rid="'+rId+'"]').se_remove();
			},
			onFail:function(msg) {
				console.log("tag add fail", msg);
			}
		});
	}
	//
	return {};
};