"use strict";

/**
 * Enable tab action through aria roles and css selector (css selectors implemented in main code).
 * @param (plugElem) plugElem Element to implement into
 * @param (plugOptions) plugOptions custom parameters
 */
se.plugin.simpleTab = function(plugElem, plugOptions) {
	// Defaults
	let pDefaults = {
			callback:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions);

	// Elements
	const
		tabList = plugElem.querySelector('div[role="tablist"]'),
		panelGroup = plugElem.querySelector('div[se-elem="panelGroup"]');

	//
	function init() {
		// Modify children elements to convert into tabs.
		panelGroup.children.se_attr('role', 'tabpanel').se_attr('aria-hidden', 'true');
		tabList.children.se_attr('role', 'tab').se_attr('aria-selected', 'false');

		// Check if first element is selected, else ignore and pick first
		let cUrl = se.url.parse(),
			maxTabs = tabList.children.length,
			iniTab = 0;
		if ( 'tab' in cUrl.hash) {
			let testTab = intVal(cUrl.hash['tab']);
			if ( testTab < maxTabs ) {
				iniTab = testTab;
			}
		}

		//
		selector(iniTab);

		// Bindings
		tabList.se_on('click', 'div[role="tab"]', cSelect);
	}

	// Functions
	function cSelect(ev, elem) {
		ev.preventDefault();
		ev.stopPropagation();
		selector(elem.se_index());
	}

	//
	function selector(elNum) {
		tabList.querySelectorAll(':scope > div[role="tab"][aria-selected="true"]').se_attr('aria-selected', 'false');
		tabList.querySelectorAll(':scope > div[role="tab"]')[elNum].se_attr('aria-selected', 'true');
		panelGroup.querySelectorAll(':scope > div[role="tabpanel"][aria-hidden="false"]').se_attr('aria-hidden', 'true');
		panelGroup.querySelectorAll(':scope > div[role="tabpanel"]')[elNum].se_attr('aria-hidden', 'false');
		// CB
		if ( typeof pSettings.callback === 'function' ) {
			pSettings.callback.call(this, elNum);
		}
	}

	// Expose
	init();
	return {
		outSelect:selector
	};
};

/**
 * Enable pseudoDialog behaviour with callback to actually suported behaviour
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.pseudoDialog = function(plugElem, plugOptions) {
	let defaults = {
			backgroundHide:true
		},
		pSettings = se.object.merge(defaults, plugOptions),
		cWindow;

	//
	function init() {
		// Alter structure
		plugElem.se_classAdd('se_dialog');
		plugElem.se_css('display', 'none');

		// Displace
		cWindow = document.createElement('div');
		cWindow.se_classAdd('se_dialog_win');
		while ( plugElem.childNodes.length > 0 ) {
			cWindow.appendChild(plugElem.childNodes[0]);
		}
		plugElem.appendChild(cWindow);

		// Bindings
	}

	// Change status
	function show() {
		plugElem.se_css('display', 'flex');
		plugElem.addEventListener('click', hideEvent);
	}

	//
	function hide() {
		plugElem.se_css('display', 'none');
		plugElem.removeEventListener('click', hideEvent);
	}

	//
	function hideEvent(e) {
		let clickedElem = e.target;
		if ( !clickedElem.se_hasParent(cWindow) ) {
			hide();
		}
	}

	//
	init();
	return {
		show:show,
		hide:hide
	};
};

/**
 * Make a menu available by clicking an object
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.menuToggle = function(plugElem, plugOptions) {
	//
	let pDefaults = { callback: null },
		pSettings = se.object.merge(pDefaults, plugOptions);

	// elements
	const
		btn = plugElem.querySelector('.se_menu_button'),
		menu = plugElem.querySelector('.se_menu_hidden');

	//
	function init() {
		// Listeners
		btn.addEventListener('click', show);
	}

	// Funciones
	function show(e) {
		e.stopPropagation();
		// Mostrar
		menu.se_css('display', 'block');
		// Bind
		document.addEventListener('click', hide);
		// CB
		if ( typeof plugin.settings.callback === 'function' ) {
			plugin.settings.callback.call(this);
		}
	}
	//
	function hide(e) {
		e.preventDefault();
		let clickedElem = e.target;
		if ( !clickedElem.se_hasParent(plugElem) ) {
			document.removeEventListener('click', hide);
			menu.se_css('display', 'none');
		}
	}

	//
	init();

	//
	return {};
};

/**
 * Make a menu available by clicking an object
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.slideToggle = function(plugElem, plugOptions) {
	//
	let defaults = { callback: null },
		pSettings = se.object.merge(defaults, plugOptions);

	// elements
	const
		btn = plugElem.querySelector('[se-elem="btn"]'),
		menu = plugElem.querySelector('[se-elem="menu"]'),
		menuChild = menu.children[0];

	function init() {
		// Setup
		menu.style.transition = 'height 0.5s';
		menu.style.overflow = 'hidden';
		menu.style.height = 0;
		menuChild.style.transition = 'margin-top 0.5s';

		// Bindings
		btn.addEventListener('click', menuClick);
	}

	// Functions
	function menuClick(e, elem){
		e.preventDefault();
		if ( menu.clientHeight )
		{
			menu.style.height = 0;
			menuChild.style.marginTop = -menuChild.clientHeight + 'px';
		} else {
			menuChild.style.marginTop = 0;
			menu.style.height = menuChild.clientHeight + 'px';
		}
	}

	//
	init();

	//
	return {};
};

/**
 * Make a form automatically called by ajax, in options set default behaviour for reset, submit, errors, etc.
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.simpleForm = function(plugElem, plugOptions) {
	let defaults = {
			load_url:null,
			load_data:null,
			save_url:null,
			save_data:null,
			serialized:false,
			reset:false,
			successOnce:false,
			onLoad:null,
			onRequest:null,
			onComplete:null,
			onSuccess:null,
			onFail:null
		},
		pSettings = se.object.merge(defaults, plugOptions);

	// elements
	const
		//
		butSubmit = plugElem.querySelector('[type="submit"]'),
		actName = ( butSubmit && butSubmit.tagName.toLowerCase() === 'button' ) ? butSubmit.se_html() : '',
		response = plugElem.querySelector('[se-elem="response"]');

	// SETUP
	function init() {
		// Carga de consulta
		if ( pSettings.load_url !== null ) {
			se.ajax.json(pSettings.load_url, pSettings.load_data, {
				method:'GET',
				response:response,
				onSuccess:(msg) => {
					response.se_empty();

					//
					if ( typeof pSettings.onLoad === 'function' ) {
						[].push.call(arguments, plugElem);
						pSettings.onLoad.apply(this, arguments);
					}

					//
					se.form.loadData(plugElem, msg.d);
				}
			});
		} else {
			// Value fix
			se.element.fixValues(plugElem);
		}

		// Binding
		plugElem.se_on('change', ['input', 'textarea', 'select'], formUpdate);
		plugElem.se_on('submit', formSubmit);
	}

	// Funciones Privadas
	function updateButton(exData, iconMod) {
		if ( actName !== '' ) {
			iconMod = ( typeof iconMod === 'string' ) ? iconMod : '';
			butSubmit.se_html('<svg class="icon inline mr '+iconMod+'"><use xlink:href="#'+exData+ '" /></svg>' + actName);
		}
	}
	//
	function formUpdate(e) {
		//
		if ( response ) {
			response.se_empty();
		}
		//
		if ( plugElem.checkValidity() ) {
			updateButton('fa-circle-o', 'pulsate');
		} else {
			updateButton('fa-warning');
		}
	}
	//
	function formSuccess() {
		updateButton('fa-check');
		if ( pSettings.reset ) {
			plugElem.reset();
		}
		//
		if ( typeof pSettings.onSuccess === 'function' ) {
			[].push.call(arguments, plugElem);
			pSettings.onSuccess.apply(this, arguments);
		}
	}
	//
	function formFail() {
		updateButton('fa-remove');
		//
		if ( typeof pSettings.onFail === 'function' ) {
			[].push.call(arguments, plugElem);
			pSettings.onFail.apply(this, arguments);
		}
	}
	//
	function formComplete() {
		if ( butSubmit && !pSettings.successOnce ) {
			butSubmit.disabled = false;
		}
		//
		if ( typeof pSettings.onComplete === 'function' ) {
			[].push.call(arguments, plugElem);
			pSettings.onComplete.apply(this, arguments);
		}
	}
	//
	function formOnRequest() {
		//
		if ( butSubmit ) {
			butSubmit.disabled = true;
		}
		updateButton('fa-spinner', 'spin');
		//
		if ( typeof pSettings.onRequest === 'function' ) {
			[].push.call(arguments, plugElem);
			pSettings.onRequest.apply(this, arguments);
		}
	}
	//
	function formSubmit(e) {
		e.preventDefault();
		e.stopPropagation();
		let url = ( plugElem.action !== '' && pSettings.save_url === null ) ? plugElem.action : pSettings.save_url,
			post;

		// Send data
		if ( !url ) {
			console.error("SimpleForm ERROR: No save_url");
			return;
		}

		// Type of post data
		if ( pSettings.serialized ) {
			post = new FormData();
			post.append('data', JSON.stringify(se.form.serializeToObj(plugElem)));
		} else {
			post = new FormData(plugElem);
		}

		// AddDefaults
		if ( pSettings.save_data !== null ) {
			for ( const [name, value] of Object.entries(pSettings.save_data) ) {
				post.append(name, value.toString());
			}
		}

		// Enviar formulario
		se.ajax.json(url, post, {
			response:response,
			onRequest:formOnRequest,
			onComplete:formComplete,
			onSuccess:formSuccess,
			onFail:formFail
		});
	}
	//
	init();
	//
	return {};
};

/**
 * Front page type slide for images.
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.slider = function(plugElem, plugOptions) {
	//
	let defaults = {
			animation:'fade',
			duration:'100',
			wait:4000
		},
	//
		plugin = this,
		container = plugElem.querySelector('.container'),
		menu = plugElem.querySelector('.menu'),
		timer,
		transEvent = transitionEndEventName();

	//
	plugin.settings = {};
	plugin.status = { slides:0, sCurrent:0, sNext:0, nextStop:false, pause:false };

	//
	function init() {
		plugin.settings = se.object.merge(defaults, plugOptions);
		//
		container.children.se_each(function(index, cElem){
			if ( index !== 0 ) {
				cElem.style.opacity = 0;
				cElem.style.zIndex = 0;
			}
			plugin.status.slides++;
		});
		container.children[0].style.zIndex = 1;
		plugin.status.sCurrent = 0;
		plugin.status.sNext = 1;
		if ( plugin.status.slides > 1 ) {
			// Eventos
			timer = setInterval(nextSlide, plugin.settings.wait);
			container.se_on({
				'mouseenter':function() {
					clearInterval(timer);
				},
				'mouseleave':function() {
					timer = setInterval(nextSlide, plugin.settings.wait);
				}
			});
		}
	}
	function nextSlide(event) {
		// Animar
		let cElem = container.children[plugin.status.sCurrent],
			cNext = container.children[plugin.status.sNext];
		cElem.style.opacity = 0;
		cElem.style.zIndex = 0;
		cNext.style.opacity = 1;
		cNext.style.zIndex = 1;
		plugin.status.sCurrent = plugin.status.sNext;
		plugin.status.sNext++;
		if ( plugin.status.sNext >= plugin.status.slides) {
			plugin.status.sNext = 0;
		}
	}

	// Iniciar
	init();
	return null;
};

/**
 * Create a container that allows to add remove elements
 * @param (plugElem) DOM element to apply function to
 */
se.plugin.simpleContainer = function(plugElem) {
	let plugin = this,
		properties = {
			fragmet:'div'
		};

	const
		templateBody = plugElem.querySelector('template').se_html(),
		content = plugElem.querySelector('[se-elem="content"]');

	//
	function init() {
		// Determinar fragmento
		switch ( content.tagName.toLowerCase() ) {
			case 'table':
			case 'tbody':
				properties.fragmet = 'tbody';
				break;
			default:
				properties.fragmet = 'div';
				break;

		}
	}

	//
	function addMultiple(msg) {
		let tCont = document.createElement(properties.fragmet);

		// Cargar datos
		for ( let i = 0, len = msg.length; i < len; i++ ) {
			// plugin.settings.eCur++;
			tCont.se_site_coreend(se.struct.stringPopulate(templateBody, msg[i]));
			// plugin.addElement(msgs.d[i]);
		}
		// Areglar elementos (valores)
		se.element.fixValues(tCont);
		// Pasar
		while ( tCont.childNodes.length > 0 ) {
			content.appendChild(tCont.childNodes[0]);
		}
	}

	//
	function addElement(msg, pos = 'append') {
		let cEl;
		//
		switch ( pos ) {
			//
			case 'append':
				cEl = content.se_site_coreend(se.struct.stringPopulate(templateBody, msg));
				break;
			//
			case 'preppend':
				cEl = content.se_prepend(se.struct.stringPopulate(templateBody, msg));
				break;
			//
			default:
				console.error("position not valid", pos);
				break;
		}

		return cEl;
	}

	//
	function delElement(id) {
		let el = content.querySelector('[se-id=' + id + ']');
		if ( el ) {
			el.se_remove();
		} else {
			console.log("No existe el elemento");
		}
	}

	//
	function clear() {
		content.se_empty();
	}

	//
	init();
	return {
		addMultiple:addMultiple,
		addElement:addElement,
		delElement:delElement,
		clear:clear
	};
};

/**
 * Update contents of form
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.formOptUpdater = function(plugElem, plugOptions) {
	let defaults = {
			url:null,
			callback: null
		},
	//
		plugin = this;
	//
	plugin.settings = {};
	plugin.properties = {};
	//
	function init() {
		//
		plugin.settings = se.object.merge(defaults, plugOptions);
		plugin.properties.cForm = plugElem.se_closest('form');
		//
		plugElem.se_on('change', plugin.update);
	}
	//
	plugin.update = function(ev) {
		let cEl = ev.target,
			cVal = cEl.se_val();
		se.ajax.json(plugin.settings.url, {'cVal':cVal}, {
			onSuccess: (msg) => {
				se.object.each(msg.d,(cFormName, fElemData) => {
					let cElem = plugin.properties.cForm.se_formEl(cFormName);
					cElem.se_empty();
					se.object.each(fElemData, function(id, value){
						cElem.add(new Option(value.title, id));
					});
				});
			}
		});
	};
	//
	init();
};

/**
 * Make a menu available by clicking an object
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.se_gallery = function(plugElem, plugOptions) {
	let plugDefaults = {
			zoomType:'inner',
			cursor:'crosshair',
			position:'right',
			width:300,
			smallWidth:160
		},
		plugSettings = se.object.merge(plugDefaults, plugOptions),
		imageCurrent = plugElem.querySelector('[se-elem="cur"]'),
		imageMenu = plugElem.querySelector('[se-elem="menu"]');
	//
	plugSettings.width = plugElem.se_data('imagewidth');

	//
	function init() {
		//
		imageMenu = plugElem.se_site_coreend('<div se-elem="menu"></div>');

		//
		plugElem.querySelectorAll('img').se_each((index, cEl) => {
			imageMenu.se_site_coreend('<div></div>').se_site_coreend(cEl);
		});

		//
		let fileLoc = '/res/image/site/w_' + plugSettings.width + '/' + imageMenu.firstChild.firstChild.se_data('fname');
		imageCurrent = plugElem.se_prepend('<img se-elem="cur" src="' + fileLoc + '" />');

		//
		plugElem.se_data('ready', '1');

		// Binds
		imageMenu.se_on('click', 'img', selectImage);
	}

	//
	function selectImage(e, el) {
		e.preventDefault();
		imageCurrent.src = '/res/image/site/w_' + plugSettings.width + '/' + el.se_data('fname');
	}

	//
	init();
	return {};
};

/**
 * Make a menu available by clicking an object
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.autoSize = function(plugElem, plugOptions) {
	let defaults = {
			counter:true
		},
		settings = se.object.merge(defaults, plugOptions),
		properties = {
			minHeight:100,
			charMax:0
		},
		cEls = {
			container:null,
			counter:null
		};

	//
	function init() {
		let cs = plugElem.getBoundingClientRect();
		// styling defautls
		plugElem.style.minWidth = "300px";
		plugElem.style.maxHeight = "70vh";

		// create definitions
		properties.charMax = intVal(plugElem.se_attr('maxlength'));
		if ( settings.counter && properties.charMax ) {
			let curRem = plugElem.se_val().length;
			// Add HTML
			cEls.container = plugElem.se_after('<div class="textAreaCounter"><span> / '+properties.charMax+'</span></div>');
			cEls.counter = cEls.container.se_prepend('<span>'+curRem+'</span>');
			// Events
			plugElem.se_on('keyup', reCount);
		}

		//
		resize();

		// Events
		plugElem.se_on('input change', resize);
	}

	//
	function resize() {
		let cs = getComputedStyle(plugElem),
			offset, tSize;
		plugElem.style.height = "0";
		if ( cs.boxSizing === "border-box" ) {
			offset = plugElem.offsetHeight;
		}
		else if ( cs.boxSizing === "content-box" ) {
			offset = -plugElem.clientHeight;
		}
		tSize = plugElem.scrollHeight + offset;
		tSize =  ( tSize > properties.minHeight ) ? tSize : properties.minHeight;
		plugElem.style.height = tSize + "px";
	}

	//
	function reCount() {
		let cRem = plugElem.se_val().length;
		cEls.counter.se_text(cRem);
	}

	//
	init();
	return {};
};


/**
 * Make a menu available by clicking an object
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.charCounter = function(plugElem, plugOptions) {
	let maxSize = intVal(plugElem.se_attr('maxlength'));

	//
	function init() {
		// Events
		plugElem.se_on('input change', updateCounter);
	}

	//
	function updateCounter(ev) {
	}


	//
	if ( maxSize ) {
		init();
	}

	//
	return {};
};

/**
 * Simple sortable table
 * @param (plugElem) DOM element to apply function to
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.simpleTable = function(plugElem, plugOptions) {
	let tHead = plugElem.querySelector('thead'),
		tBody = plugElem.querySelector('tbody'),
		//
		pDefaults = {
			callback:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions);

	//
	function init() {
		tHead.se_on('click', '.sortable', sortColumn)
	}

	//
	function sortColumn(e, cEl) {
		e.preventDefault();
		let rows,
			i, x, y,
			xVal, yVal,
			shouldSwitch,
			switching = true,
			cIndex = cEl.se_index(),
			cOrder = cEl.se_data('sort'),
			nOrder = ( cOrder === 'desc' ) ? 'asc' : 'desc',
			oType = ( cEl.se_data('sorttype') === 'number' ) ? 'number' : 'string';

		// Fix style
		tHead.querySelectorAll('.sortable').se_data('sort', '');
		cEl.se_data('sort', nOrder);

		// console.log("Sorting:", nOrder, oType);

		/*Make a loop that will continue until
		no switching has been done:*/
		while ( switching ) {
			//start by saying: no switching is done:
			switching = false;
			rows = tBody.getElementsByTagName("TR");

			// Loop al the rows!
			for ( i = 0; i < ( rows.length - 1 ); i++ ) {
				//start by saying there should be no switching:
				shouldSwitch = false;
				/*Get the two elements you want to compare,
				one from current row and one from the next:*/
				x = rows[i].getElementsByTagName("TD")[cIndex];
				y = rows[i + 1].getElementsByTagName("TD")[cIndex];
				xVal = ( oType === 'string' ) ? x.innerHTML.toLowerCase() : parseInt(x.innerHTML);
				yVal = ( oType === 'string' ) ? y.innerHTML.toLowerCase() : parseInt(y.innerHTML);
				// console.log("R %s/%s - %s", i, rows.length, xVal);
				//check if the two rows should switch place:
				if ( ( nOrder === 'asc' && xVal > yVal ) || ( nOrder === 'desc' && xVal < yVal ) ) {
					//if so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				}
			}
			//
			if ( shouldSwitch ) {
				/*If a switch has been marked, make the switch
				and mark that a switch has been done:*/
				rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				switching = true;
			}
		}
	}

	//
	init();
	return {};
};

/**
 * Modifies element to support custom calendar input if not supported
 * @param (plugElem) DOM element to apply function to: must be input
 */
se.plugin.calendarInput = function(plugElem) {
	let container, i,
		hInput, btn,
		oType = plugElem.se_attr('type');

	//
	function init() {
		//
		if ( !['datetime-local', 'date', 'time'].includes(oType) ) {
			console.error("Calendar Input: type not supported.", oType);
			return;
		}


		let pSettings = {};
		// Process
		// check native support
		i = document.createElement("input");
		i.se_attr("type", oType);
		if ( i.type !== oType ) {
			// NOT Supported, use plugin
			switch ( oType ) {
				case 'datetime-local':
					pSettings.time = true;
					pSettings.date = true;
					break;
				case 'date':
					pSettings.time = false;
					pSettings.date = true;
					break;
				case 'time':
					pSettings.time = true;
					pSettings.date = false;
					break;
			}
			container = plugElem.se_before('<div class="modInput"></div>');
			container.se_site_coreend(plugElem);
			btn = container.se_site_coreend('<button type="button"><svg class="icon inline"><use xlink:href="#fa-calendar" /></svg></button>');
			btn.se_plugin('calendar', pSettings);
		}
		else {
			// Create container and new element with the name
			plugElem.se_attr('type', 'text');
			container = plugElem.se_before('<div></div>');
			container.se_site_coreend(plugElem);
			hInput = container.se_site_coreend('<input type="hidden">');
			hInput.name = plugElem.name;
			hInput.value = plugElem.value;
			hInput.se_attr('se-plugin-is', 'calendarInput');

			// Modify original input
			plugElem.se_attr('type', oType);
			plugElem.name = plugElem.name + '_input';

			// Binding
			plugElem.se_on('change', toMysql);
		}
	}

	//
	function setValue(cVal) {
		// console.error("SETTING VALUE", cVal, cEl, hInput);
		if ( !cVal ) {
			return;
		}

		//
		switch ( oType ) {
			//
			case 'datetime-local':
				let nDate = new Date(cVal);
				// Fix timezone issue
				nDate.setMinutes(nDate.getMinutes() - nDate.getTimezoneOffset());
				// Assign value
				plugElem.value = nDate.toISOString().slice(0,16);
				hInput.value = nDate.toJSON().slice(0, 19).replace('T', ' ')
				break;
			//
			case 'date':
				plugElem.value = cVal;
				hInput.value = cVal;
				break;
			//
			case 'time':
				plugElem.value = cVal;
				hInput.value = cVal;
				break;
			//
			default:
				console.error("butty why");
				break;
		}
	}

	//
	function doUpdate() {
		if ( hInput ) {
			toMysql();
		}
	}

	//
	function toMysql() {
		switch ( oType ) {
			//
			case 'datetime-local':
				let nDate = new Date(plugElem.value);
				// Fix timezone issue
				nDate.setMinutes(nDate.getMinutes() - nDate.getTimezoneOffset());
				// Assign value
				hInput.value = nDate.toJSON().slice(0, 19).replace('T', ' ');
				break;
			//
			case 'date':
				hInput.value = plugElem.value;
				break;
			//
			case 'time':
				hInput.value = plugElem.value;
				break;
			//
			default:
				console.error("to mysql error");
				break;
		}
	}

	//
	function toISO(cVal) {
		let b = cVal.split(/[- :]/);
		return b[0] + '-' + b[1] + '-' + b[2] + 'T' + b[3] + ':' + b[4] + ':' + b[5];
	}

	//
	function setUnix(value) {
		let a = new Date(value * 1000);
		//
		plugElem.value = a.toISOString().slice(0, 19).replace('T', ' ');
	}

	//
	function getUnix() {
		return parseInt((new Date(plugElem.value).getTime() / 1000).toFixed(0));
	}

	//
	function getDate() {
		let nDate = new Date(plugElem.value);
		nDate.setMinutes(nDate.getMinutes() - nDate.getTimezoneOffset());
		// Fixed timezone error
		return nDate;
	}

	// Empty return
	init();
	return {
		setUnix:setUnix,
		getUnix:getUnix,
		getDate:getDate,
		doUpdate:doUpdate,
		setValue:setValue
	};
};

//