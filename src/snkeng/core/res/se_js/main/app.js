"use strict";

//
window.addEventListener('DOMContentLoaded', function firstLoad() {
	// Single load remove
	document.removeEventListener("DOMContentLoaded", firstLoad, false);

	// CB
	function jsInitialize() {
		//
		se.main.init();
		se.user.logInitialCheck();
		postLoad();
	}

	// Indexed DB
	if ( window.indexedDB && 'idb' in site ) {
		console.log("IDB page initialize");
		if ( 'idb' in se ) {
			se.idb.init(jsInitialize);
		} else {
			console.error("SNK Engine IDB Component missing.");
		}
	} else {
		jsInitialize();
	}

	// Secondary function initialization
	if ( 'init' in site && typeof site.init === "function" ) {
		site.init();
	}

	// Service Worker
	if ( 'serviceWorker' in navigator ) {
		navigator.serviceWorker.register('/snkeng_main_sw.js').then(function(reg) {
			console.log("SW Registration:");
			if ( reg.installing ) {
				console.log('SW installing');
			} else if ( reg.waiting ) {
				console.log('SW installed');
			} else if ( reg.active ) {
				console.log('SW active');
			}
		}).catch(function(error) {
			console.log("SW REGISTER FAILURE", error);
		});
	} else {
		console.log('Service workers aren\'t supported in this browser.');
	}
});