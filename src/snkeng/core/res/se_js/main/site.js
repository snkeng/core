"use strict";

//
window.addEventListener('DOMContentLoaded', function firstLoad() {
	// Single load remove
	document.removeEventListener("DOMContentLoaded", firstLoad, false);

	// CB
	function jsInitialize() {
		//
		se.main.init();
		se.user.logInitialCheck();
		postLoad();
	}

	// Indexed DB
	if ( window.indexedDB && 'idb' in site ) {
		console.log("IDB page initialize");
		if ( 'idb' in se ) {
			se.idb.init(jsInitialize);
		} else {
			console.error("SNK Engine IDB Component missing.");
		}
	} else {
		jsInitialize();
	}

	// Secondary function initialization
	if ( 'init' in site && typeof site.init === "function" ) {
		site.init();
	}

	// Missing images callback
	document.addEventListener('error', (event) => {
		const targetImg = '/se_core/res/se_img/empty.svg';
		let cElem = event.target;
		if ( cElem.tagName.toLowerCase() !== 'img' ) { return; }
		if ( cElem.se_data('fix') === 'true' ) { return; }

		//
		let cElParent = cElem.parentElement;
		if ( cElParent.tagName.toLowerCase() === 'picture' ) {
			for ( let cChild of cElParent.children ) {
				cChild.se_data('fix', 'true');
				switch ( cChild.tagName.toLowerCase() ) {
					case 'source':
						cChild.type = 'image/svg';
						cChild.srcset = targetImg;
						break;
					case 'img':
						cChild.removeAttribute('srcset');
						cChild.removeAttribute('sizes');
						cElem.se_data('original', cElem.src);
						cChild.src = targetImg;
						cChild.alt = 'Image missing. =(';
						break;
				}
			}
		}
		else {
			cElem.se_data('original', cElem.src);
			cElem.se_data('fix', 'true');
			cElem.removeAttribute('srcset');
			cElem.removeAttribute('sizes');
			cElem.src = targetImg;
			cElem.alt = 'Image missing. =(';
			cElem.style.width = '100%';
			cElem.style.height = '100%';
			cElem.style.maxHeight = '80vh';
			cElem.style.maxWidth = '80vw';
		}
	}, true);

	// Service Worker
	if ( 'serviceWorker' in navigator ) {
		navigator.serviceWorker.register('/sw.js').then(function(reg) {
			console.log("SW Registration:");
			if ( reg.installing ) {
				console.log('SW installing');
			} else if ( reg.waiting ) {
				console.log('SW installed');
			} else if ( reg.active ) {
				console.log('SW active');
			}
		}).catch(function(error) {
			console.log("SW REGISTER FAILURE", error);
		});
	} else {
		console.log('Service workers aren\'t supported in this browser.');
	}
});