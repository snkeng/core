"use strict";
//
window.addEventListener('DOMContentLoaded', function firstLoad() {
	// Single load remove
	document.removeEventListener("DOMContentLoaded", firstLoad, false);

	//
	document.body.addEventListener('newPageLoad', (e) => {
		// Load plugins
		se.main.pluginLoad(e.detail.htmlTarget);
	});

	//
	console.warn("loading first set of plugins (does not support indexdb)");
	se.main.pluginLoad(document.body);
});