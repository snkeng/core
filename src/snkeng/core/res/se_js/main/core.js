"use strict";

// jQuery selector aprox
window.$ = function (selector) {
	let many = true,
		selection;
	// Determinar tipo
	if ( typeof selector === 'string' ) {
		if ( selector.indexOf('#') === 0 ) {
			selector = selector.substr(1, selector.length);
			let index = selector.indexOf(' ');
			if ( index === -1 ) {
				selection = document.getElementById(selector);
				many = false;
			} else {
				let id = selector.substr(0, index),
					query = selector.substr(index + 1, selector.length);
				selection = document.getElementById(id);
				if ( selection !== null) {
					selection = selection.querySelectorAll(query);
				}
			}
		} else {
			selection = document.querySelectorAll(selector);
		}
		//
		if ( many && ( !selection || selection.length === 0 )) {
			console.trace('$ - Empty: ', selector);
			selection = document.createDocumentFragment().querySelectorAll('div');
		} else if ( many && selection && selection.length === 1 ) {
			selection = selection[0];
		}
		//
		return selection;
	} else if ( typeof selector === 'undefined' ) {
		console.log('$ - No selector');
		return document.createDocumentFragment().querySelectorAll('div');
	} else {
		console.log('$ - Not recognized', selector);
		return document.createDocumentFragment().querySelectorAll('div');
	}
};

//<editor-fold desc="DOM PROTOTYPE">

//<editor-fold desc="ELEMENT">
FormData.prototype.se_jsonAppend = function() {
	let el;
	for ( let i = 0, cArg; cArg = arguments[i]; ++i ) {
		for ( el in cArg ) {
			this.append(el, cArg[el]);
		}
	}
};

/**
 * Get de element from a form
 * @param {string} elName Form element to search
  */
Node.prototype.se_formEl = function(elName) {
	return this.elements.namedItem(elName);
};

/**
 * Update the element in a form
 * @param {string} elName Form element to search
 * @param {string|number} cVal optional assign value
 */
Node.prototype.se_formElVal = function(elName, cVal) {
	let cEl = this.elements.namedItem(elName);
	//
	if ( !cEl ) {
		console.trace("Form El Val, undefined element:", elName);
		return;
	}

	//
	if ( typeof cVal !== 'undefined' ) {
		cEl.value = cVal;
		//
		if ( cEl.length > 1 ) {
			for ( let i = 0; i < cEl.length; i++ ) {
				let curEl = cEl[i];
				if ( curEl.value === cVal ) {
					curEl.checked = true;
					break;
				}
			}
		} else if ( cEl.type.toLowerCase() === 'checkbox' ) {
			cEl.checked = ( cVal );
		}
		return this;
	} else {
		if ( cEl.type && cEl.type.toLowerCase() === 'checkbox' ) {
			return cEl.checked;
		}
		else {
			return this.elements.namedItem(elName).value;
		}
	}
};
Node.prototype.se_remove = function() {
	this.parentElement.removeChild(this);
};
Node.prototype.se_index = function() {
	return Array.prototype.indexOf.call(this.parentNode.children, this);
};
Node.prototype.se_empty = function() {
	this.innerHTML = '';
	return this;
};
Node.prototype.se_html = function(html) {
	if ( typeof html !== 'undefined' ) {
		this.innerHTML = html;
		return this;
	} else {
		return this.innerHTML;
	}
};
Node.prototype.se_text = function(text) {
	if ( typeof text !== 'undefined' ) {
		this.innerHTML = se.element.htmlentities(text);
		return this;
	} else {
		return se.element.htmlentities(this.innerHTML);
	}
};
//
Node.prototype.se_data = function (var1, var2) {
	// Check type
	if ( typeof var1 === 'string') {
		var1 = 'data-'+var1;
		if ( typeof var2 !== 'undefined' ) {
			if ( var2 !== null ) {
				this.setAttribute(var1, var2);
			} else {
				this.removeAttribute(var1);
			}
			return this;
		} else {
			return this.getAttribute(var1);
		}
	} else if ( Array.isArray(var1) )  {
		for ( let cRow of var1 ) {
			cRow[0] = 'data-'+cRow[0];
			this.setAttribute(cRow[0], cRow[1]);
		}
	} else {
		console.error("se_data can't be applyed. Recieved: ", var1, var2);
	}
};
Node.prototype.se_replaceWith = function(nEl) {
	this.insertAdjacentHTML('afterEnd', nEl);
	let newElem = this.nextSibling,
		cParent = this.parentElement;
	cParent.replaceChild(newElem, this);
	return newElem;
};
Node.prototype.se_wrap = function(elem) {
	this.insertAdjacentHTML('afterEnd', elem);
	let newParent = this.nextSibling;
	newParent.appendChild(this);
};
Node.prototype.se_closest = function(selector, limit) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	//
	limit = ( typeof limit !== 'undefinded' ) ? limit : document.getElementsByTagName('body')[0];

	//
	do {
		if ( matches.call(el, selector) ) { return el; }
	} while ( (el = el.parentNode) && el.tagName !== 'HTML' && el !== limit );

	//
	return null;
};
Node.prototype.se_prev = function(sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	if ( typeof sel === 'string' ) {
		while ( (el = el.previousSibling) ) {
			if ( matches.call(el, sel) ) { return el; }
		}
		return null;
	} else {
		return this.previousSibling;
	}
};
Node.prototype.se_prevUntil = function(uEl, sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	while ( (el = el.previousSibling) && uEl !== el ) {
		if ( matches.call(el, sel) ) { return el; }
	}
	return null;
};
Node.prototype.se_next = function(sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	if ( typeof sel === 'string' ) {
		while ( (el = el.nextSibling) ) {
			if ( el.nodeType === 1 && matches.call(el, sel) ) { return el; }
		}
		return null;
	} else {
		return this.nextSibling;
	}
};
Node.prototype.se_nextUntil = function (uEl, sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	while ( (el = el.nextSibling) && uEl !== el ) {
		if ( el.nodeType === 1 && matches.call(el, sel) ) { return el; }
	}
	return null;
};
Node.prototype.se_hasParent = function (parent) {
	let el = this;
	do {
		if ( el === parent ) {
			return true;
		}
	} while ( (el = el.parentNode) );
	return false;
};
Node.prototype.se_searchParent = function(parent) {
	let el = this;
	do {
		if ( el.nodeType !== 1 ) {
			return false;
		}
		if ( el.matches(parent) ) {
			return el;
		}
	} while ( (el = el.parentNode) );
	return false;
};
Node.prototype.se_sibling = function (selector) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector,
		cEl = this.parentNode.firstElementChild;
	do {
		if ( el !== cEl && matches.call(cEl, selector) ) {
			return cEl;
		}
	} while ( cEl = cEl.nextSibling );
	return false;
};
Node.prototype.se_before = function(cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('beforebegin', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.parentNode.insertBefore(cEl, this);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.previousElementSibling;
};
Node.prototype.se_after = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('afterend', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.parentNode.insertBefore(cEl, this.nextSibling);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.nextElementSibling;
};

Node.prototype.se_insertManyFromString = function (mode, structure, content) {
	// Cargar datos
	for ( let i = 0, len = content.length; i < len; i++ ) {
		if ( mode === 'append' ) {
			this.se_site_coreend(se.struct.stringPopulate(structure, content[i]));
		} else {
			this.se_prepend(se.struct.stringPopulate(structure, content[i]));
		}
	}
	return this;
};
Node.prototype.se_site_coreend = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('beforeend', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.appendChild(cEl);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.lastElementChild;
};
Node.prototype.se_prepend = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('afterbegin', cEl);
	} else if ( typeof cEl === 'object' ) {
		if ( this.children.length !== 0 ) {
			this.insertBefore(cEl, this.children[0]);
		} else {
			this.append(cEl);
		}
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.firstElementChild;
};
Node.prototype.se_offset = function() {
	let offset = {
		x:window.scrollX,
		y:window.scrollY
	};
	// Al elemento
	let box = this.getBoundingClientRect();
	offset.x+= box.left;
	offset.y+= box.top;
	return offset;
};
Node.prototype.se_on = function(var1, var2, var3, var4 = null) {
	let direct = function (cElem, cEvent, cFunc) {
			cElem.addEventListener(cEvent, cFunc);
		},
		delegate = function (cElem, cEvent, cDelegate, cFunc, context = null) {
			cElem.addEventListener(cEvent,
				(e) => {
					let rTarget = e.target.se_closest(cDelegate, cElem);
					if ( rTarget !== null ) {
						cFunc.call(context, e, rTarget);
					}
				},
				false
			);
		},
		//
		typeVar1 = typeof var1,
		typeVar2 = typeof var2,
		typeVar3 = typeof var3,
		//
		eventList, cEvent,
		cSubEl;

	//
	if ( typeVar1 === 'object' ) {
		//
		if ( typeVar2 === 'object' ) {
			// Multiple events on multiple sub elements
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of var1 ) {
				for ( cSubEl of var2 ) {
					delegate(this, cEvent, cSubEl, var3);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// Multiple events and funcs related with single sub element, posible subDelegation
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				if ( typeof cFunc !== "function" ) { console.error("invalid function added"); }
				let subDelegation = ( var3 );
				delegate(this, cEvent, var2, cFunc, subDelegation);
			}
		} else if ( typeVar2 === 'undefined' ) {
			// Multiple events and funcs no sub element
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				direct(this, cEvent, cFunc);
			}
		} else {
			console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
		}
	}
	else if ( typeVar1 === 'string' ) {
		// Variable is a string, probably action

		eventList = var1.split(' ');
		if ( typeVar2 === 'function' ) {
			// single/multiple event, single function
			for ( cEvent of eventList ) {
				direct(this, cEvent, var2);
			}
		} else if ( typeVar2 === 'object' ) {
			// single/multiple event, multiple subElements (compatibility with first function)

			// Ignorar si la tercera variable no es una función
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of eventList ) {
				for ( cSubEl of var2 ) {
					delegate(this, cEvent, cSubEl, var3, var4);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// var2 is a type of element, third variable must be a function (or die)
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-string, v3-not valid');
				return this;
			}

			//
			for ( cEvent of eventList ) {
				delegate(this, cEvent, var2, var3, var4);
			}
		} else {
			console.log('Element - se_on: Delegate . v1-string, v2-not valid', var2);
		}
	} else {
		console.error('Element - se_on: Method . v1-not valid', var1);
	}
	return this;
};
Node.prototype.se_once = function (cMethod, action) {
	let tFunc = function(e) {
		e.target.removeEventListener(e.type, tFunc);
		return action(e);
	};
	//
	this.addEventListener(cMethod, tFunc);
	return this;
};
Node.prototype.se_classAdd = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.add(classes[i]);
	}
	return this;
};
Node.prototype.se_classDel = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.remove(classes[i]);
	}
	return this;
};
Node.prototype.se_classHas = function(nClass) {
	return this.classList.contains(nClass);
};
Node.prototype.se_classToggle = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.toggle(classes[i]);
	}
	return this;
};
Node.prototype.se_classSwitch = function(rClass, nClass) {
	this.se_classDel(rClass);
	this.se_classAdd(nClass);
	return this;
};
Node.prototype.se_css = function(var1, var2) {
	switch ( typeof(var1) )
	{
		case 'object':
			for ( let cStyle in var1 ) {
				if ( var1.hasOwnProperty(cStyle) ) {
					let value = ( typeof var1[cStyle] !== 'undefined' ) ? var1[cStyle] : null;
					this.style[cStyle] = value;
				}
			}
			return this;
			break;
		case 'string':
			if ( typeof(var2) === 'undefined' ) {
				return this.style[var1];
			} else {
				this.style[var1] = var2;
				return this;
			}
			break;
		default:
			break;
	}
};
Node.prototype.se_plugin = function(plugName, data) {
	// Avoid duplication
	if ( this[plugName] !== undefined ) { console.warn("Plugin already loaded in element.", plugName, this); return; }
	// Check if plugin is actually defined
	if ( typeof se.plugin[plugName] !== 'function' ) { console.error("Plugin no válido", plugName, se.plugin[plugName]); return; }
	// Load plugin
	this[plugName] = new se.plugin[plugName](this, data);
	// Setup self destruct function (if available)
	let destroyFunction = ( typeof this[plugName]._destroy === 'function' ) ? this[plugName]._destroy : null;
	// Define plugin list
	se.pluginManager.pluginList.push({'el':this, 'name':plugName, 'func':destroyFunction});
};
Node.prototype.se_hide = function() {
	let cStyle = document.defaultView.getComputedStyle(this, null);
	if ( cStyle.display !== 'none' ) {
		this.setAttribute('se-display', cStyle.display);
	}
	this.style.display = 'none';
};
Node.prototype.se_show = function() {
	let cStyle = document.defaultView.getComputedStyle(this, null);
	if ( cStyle.display !== 'none' ) { return; }
	this.style.display = this.getAttribute('se-display') || 'block';
	this.removeAttribute('se-display');
};
Node.prototype.se_attr = function(var1, var2) {
	if ( typeof var2 !== 'undefined' ) {
		if ( var2 === '' || var2 === false ) {
			this.removeAttribute(var1);
		} else {
			this.setAttribute(var1, var2);
		}
		return this;
	} else {
		return this.getAttribute(var1);
	}
};
Node.prototype.se_val = NodeList.prototype.se_val = function(var1) {
	if ( typeof var1 !== 'undefined' ) {
		this.value = var1;
		return this;
	} else {
		return this.value;
	}
};
//
// Múltiples
NodeList.prototype.se_testSel = HTMLCollection.prototype.se_testSel = function() {
	return se_listOperation(this, (cEl) => {
		console.log('testSel', i, cEl);
	});
};
NodeList.prototype.se_text = HTMLCollection.prototype.se_text = function(cText) {
	return se_listOperation(this, (cEl) => {
		cEl.se_text(cText);
	});
};
NodeList.prototype.se_html = HTMLCollection.prototype.se_html  = function(cText) {
	return se_listOperation(this, (cEl) => {
		cEl.se_html(cText);
	});
};
NodeList.prototype.se_val = HTMLCollection.prototype.se_val = function(cVal) {
	return se_listOperation(this, (cEl) => {
		cEl.se_val(cVal);
	});
};
NodeList.prototype.se_classAdd = HTMLCollection.prototype.se_classAdd = function(nClass) {
	return se_listOperation(this, (cEl) => {
		cEl.se_classAdd(nClass);
	});
};
NodeList.prototype.se_classDel = HTMLCollection.prototype.se_classDel = function(nClass) {
	return se_listOperation(this, (cEl) => {
		cEl.se_classDel(nClass);
	});
};
NodeList.prototype.se_classSwitch = HTMLCollection.prototype.se_classSwitch = function (rClass, aClass) {
	return se_listOperation(this, (cEl) => {
		cEl.se_classSwitch(rClass, aClass);
	});
};
NodeList.prototype.se_each = HTMLCollection.prototype.se_each = Array.prototype.se_each = function (callback, scope) {
	return se_listOperation(this, (cEl, cIndex) => {
		callback.call(scope, cIndex, cEl);
	});
};
NodeList.prototype.se_css = HTMLCollection.prototype.se_css = function (var1, var2) {
	return se_listOperation(this, (cEl) => {
		cEl.se_css(var1, var2);
	});
};
NodeList.prototype.se_hide = HTMLCollection.prototype.se_hide = function() {
	return se_listOperation(this, (cEl) => {
		cEl.se_hide();
	});
};
NodeList.prototype.se_show = HTMLCollection.prototype.se_show = function() {
	return se_listOperation(this, (cEl) => {
		cEl.se_show();
	});
};
NodeList.prototype.se_data = HTMLCollection.prototype.se_data = function (var1, var2) {
	return se_listOperation(this, (cEl) => {
		cEl.se_data(var1, var2);
	});
};
NodeList.prototype.se_attr = HTMLCollection.prototype.se_attr = function (var1, var2) {
	return se_listOperation(this, (cEl) => {
		cEl.se_attr(var1, var2);
	});
};
NodeList.prototype.se_remove = HTMLCollection.prototype.se_remove = function() {
	return se_listOperation(this, (cEl) => {
		if ( cEl && cEl.parentElement ) {
			cEl.parentElement.removeChild(cEl);
		}
	});
};

function se_listOperation(tEl, fDef) {
	if ( !tEl.length || typeof fDef !== 'function' ) { return tEl; }
	// Loop elements
	let i = 0;
	for ( let cEl of tEl ) {
		fDef(cEl, i);
		i++;
	}

	return tEl;
}

//</editor-fold>

//</editor-fold>
