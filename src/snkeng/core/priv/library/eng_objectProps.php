<?php
//
function se_objectPropertiesArrayRead($objId, $appName, $appVar, $elements) {
		//
	$sql_qry = <<<SQL
SELECT op_id AS id, op_value AS val
FROM sb_users_properties
WHERE a_id={$objId} AND app_name='{$appName}' AND app_type='{$appVar}'
LIMIT 1;
SQL;
	$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
		[
			'json' => ['val']
		],
		[
			'errorKey' => 'objPropArrayReadError',
			'errorDesc' => ''
		]
	);

	//
	$rData = [];
	foreach ( $elements as $cElem ) {
		$rData[$cElem] = ( isset($data['val'][$cElem]) ) ? $data['val'][$cElem] : '';
	}

	return $rData;
}
//
function se_objectPropertiesArrayUpdate($objId, $appName, $appVar, $rData, $sData) {
		//
	$sql_qry = "SELECT op_id AS id, op_value AS val FROM sb_users_properties
				WHERE a_id={$objId} AND app_name='{$appName}' AND app_type='{$appVar}' LIMIT 1;";
	$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
		[
			'json' => ['val']
		],
		[
			'errorKey' => 'objPropArrayReadError',
			'errorDesc' => ''
		]
	);

	// Update elements
	foreach ( $sData as $vName => $rValue ) {
		$data['val'][$vName] = $rData[$rValue];
	}

	// Guardar
	$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($data['val'], JSON_UNESCAPED_UNICODE));
	$upd_qry = "UPDATE sb_users_properties SET op_value='{$json_data}' WHERE op_id={$data['id']};";
	//
	\snkeng\core\engine\mysql::submitQuery($upd_qry,
		[
			'errorKey' => 'SEObjPropsArraySave',
			'errorDesc' => 'Properties could not be updated.'
		]
	);

	//
	return $data;
}
