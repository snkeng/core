<?php
// Enviar un mail
function se_ampPrint($parameters)
{
	global $siteVars;

	$defaults = [
		'siteName' => $siteVars['site']['name'],
		'logo' => $siteVars['site']['img'],
		'title' => '',
		'metaWord' => '',
		'metaDesc' => '',
		'content' => '',
		'script_head' => '',
		'script_body' => '',
		'type' => 'Article',
		'twType' => 'Article',
		'urlCanonical' => '',
		'metadata' => '',
		'image' => ''
	];
	$contents = array_merge($defaults, $parameters);



	// Process
	$contents['title'] = $contents['title'] . ' | ' . $siteVars['site']['name'];
	$contents['siteName'] = $siteVars['site']['name'];
	$contents['logo'] = $siteVars['site']['img'];
	$contents['script'] = '';


	if ( isset($_GET['debug']) && $_GET['debug'] ) {
		header('Content-Type: text/plain; charset=utf-8');
		echo <<<TEXT
CONTENIDO ORIGINAL:

-----------------------------------
{$contents['content']}
-----------------------------------\n\n\n
TEXT;
		//
	}

	//<editor-fold desc="Youtube">

	// Second pass
	$orig = [
		'/<iframe.*(youtube\.com\/embed|youtu\.be)\/([\w\-]*).*<\/iframe>/',
	];
	$replace = [
		"<amp-youtube data-videoid=\"$2\" layout=\"responsive\" width=\"480\" height=\"270\"></amp-youtube>"
	];
	//
	$contents['content'] = preg_replace($orig, $replace, $contents['content']);

	//
	if ( preg_match('/<amp-youtube/', $contents['content']) ) {
		$contents['script_head'] .= '<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>' . "\n";
	}

	//</editor-fold>

	//<editor-fold desc="Google Analytics">

	//
	if ( !empty($_ENV['GOOGLE_ANALYTICS_V3']) ) {
		$contents['script_head'] .= '<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>' . "\n";
		$contents['script_body'].= <<<HTML
<amp-analytics type="gtag" data-credentials="include">
<script type="application/json">
{
	"vars" : {
		"gtag_id": "{$_ENV['GOOGLE_ANALYTICS_V3']}",
		"config" : {
			"{$_ENV['GOOGLE_ANALYTICS_V3']}": { "groups": "default" }
		}
	}
}
</script>
</amp-analytics>\n
HTML;
		//
	}
	//</editor-fold>

	//<editor-fold desc="Images">

	// Image fix
	$contents['content'] = preg_replace_callback(
		'/<img([^>]*)>/',
		function ($matches) {
			// Get image parameters
			preg_match_all('/(\w+)=[\'"]([^\'"]*)/', $matches[0],$attributeMatches,PREG_SET_ORDER);

			$result = [
				'src' => '',
				'alt' => ''
			];
			foreach ( $attributeMatches as $match ) {
				$attrName = $match[1];

				//parse the string value into an integer if it's numeric,
				// leave it as a string if it's not numeric,
				$attrValue = is_numeric($match[2])? (int)$match[2]: trim($match[2]);

				$result[$attrName] = $attrValue; //add match to results
			}

			// Return image with available parameters
			return <<<HTML
<amp-img src="{$result['src']}" alt="{$result['alt']}" width="475" height="268" layout="responsive" class="m1">
<noscript><img src="{$result['src']}" width="475" height="268" alt="{$result['alt']}"></noscript>
</amp-img>
HTML;
			//
		},
		$contents['content']
	);

	//
	if ( isset($_GET['debug']) && $_GET['debug'] ) {
		echo <<<TEXT
CONTENIDO MODIFICADO:

-----------------------------------
{$contents['content']}
-----------------------------------\n\n\n
TEXT;
		//
		exit;
	}
	//</editor-fold>

	//
	header('Content-Type: text/html; charset=utf-8');
	echo stringPopulate(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/snkeng/site_core/templates/amp.html"), $contents);
	exit();
}
//

//
function se_ampContentClear($content) {
	$orig = [
		'/<\/?span[^>]*>/', '/<\/?div[^>]*>/', '/style=\"[^\"]*\"/',
		'/<p[^>]*>/', '/<a[^>]*href="([^"]*)"[^>]*>/'
	];
	$replace = [
		'', '', '',
		'<p>', "<a href=\"$1\">"
	];
	//
	$content = preg_replace($orig, $replace, $content);

	return $content;
}
//
