<?php
// Función para obtener random chars (max 32?)
function createRandString($size, $caps = false)
{
	$str = substr(str_shuffle(preg_replace('/[^A-Za-z0-9]/', '', base64_encode(uniqid(rand(), true)))), 0, $size);
	if ( $caps ) { $str = strtoupper($str); }
	return $str;
}
