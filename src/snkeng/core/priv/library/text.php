<?php
//
// INIT: printHTML
//	Convierte texto de la base de datos a texto imprimible. El texto debe ser el de bbtoHTML
function convertToHTML( $modString )
{
	$modString = str_replace("\\\"", "\"", $modString);		// Quitar protectores de comillas.
	$modString = htmlentities($modString);					// Proteger elementos HTML (cambio &) y cambiar caracteres no válidos
	$modString = htmlspecialchars_decode($modString);		// Desproteger elementos HTML.
	return $modString;
}
// END: printHTML
//
$htmlChars = array("<", ">", "&", "\"");
$xmlChars = array("_lt;", "_gt;", "_amp;", "_quote;");
function convertXMLtoHTML( $modString )
{
	global $htmlChars, $xmlChars;
	// Cambiar string por algo que sea html
	$modString = str_replace($xmlChars, $htmlChars, $modString);
	return $modString;
}

function convertHTMLtoXML( $modString )
{
	global $htmlChars, $xmlChars;
	// Cambiar string por algo que sea legible por xml
	$modString = str_replace($htmlChars, $xmlChars, $modString);
	return $modString;
}

//
// INIT: convertBBtoHTML
// Convierte estilo BB a HTML
function convertBBtoHTML ( $modString )
{
	//
	// INIT: Variables

	// Parametros para URL y mail
	$URLSearchString = " a-zA-Z0-9\:\/\-\?\&\.\=\_\~\#\'\%";
	$MAILSearchString = $URLSearchString . " a-zA-Z0-9\.@";

	// Convertidores BBCode y HTML
	$bbCode = array(
			'/\[b\](.+?)\[\/b\]/is',
			'/\[i\](.+?)\[\/i\]/is',
			'/\[u\](.+?)\[\/u\]/is',
			'/\[img\](.+?)\[\/img\]/is',
			'/\[img\=([0-9]*)x([0-9]*)\](.+?)\[\/img\]/',
			'/\[mail\](.+?)\[\/mail\]/is',
			'/\[mail\=(.+?)\](.+?)\[\/mail\]/is',
			'/\[url\](.+?)\[\/url\]/is',
			'/\[url\=(.+?)\](.+?)\[\/url\]/is',
			'/\[list\](.+?)\[\/list\]/is',
			'/\[quote\](.+?)\[\/quote\]/is'
			);

	$HTMLCode = array(
			'<b>$1</b>',
			'<i>$1</i>',
			'<u>$1</u>',
			'<img src="$1" />',
			'<img src="$3" height="$2" width="$1" />',
			'<a href="mailto:$1">$1</a>',
			'<a href="mailto:$1">$2</a>',
			'<a href="$1" target="_blank">$1</a>',
			'<a href="$1" target="_blank">$2</a>',
			'<ul class="listbullet">$1</ul>',
			'<p class="quote">$1</p>'
			);

	// END: variables
	//

	//
	//	OPERACIONES
	//

	// Eliminar elementos HTML indeseados
	$modString = htmlspecialchars($modString);
	// Convertir elementos bbCode en HTML
	$modString = preg_replace($bbCode, $HTMLCode, $modString);
	// Crear estructura de parrafos
	$modString = str_replace("\n", "</p><p>", $modString);
	// Aceptar código HTML (Inseguro)
	$modString = preg_replace_callback("/\[html\](.+?)\[\/html\]/",
								create_function('$matches', 
								'$string = $matches[1];
								$string = str_replace("</p><p>", "", $string);
								$string = htmlspecialchars_decode($string);
								$string = "<ihtml>".$string."</ihtml>";
								return $string;'),
								$modString);

	// Crear estructura de parrafos
	$modString = str_replace("</p><p>", "</p>\n<p>", $modString);
	// Adicionar primer y último elemento de parrafo
	$modString = "<p>".$modString."</p>";
	// Protección de comillas
	$modString = str_replace("\"", "\\\"", $modString);
	// Regresar String
	return $modString;
}
// END: convertBBtoHTML
//

//
// INIT: HTMLNoStylesSecure
// Quita tags de estilo (todas las tags) contenidas en los elementos p y span
function HTMLNoStylesSecure($html)
{
	$tagSearch = array(
			'/<(p|table|ul|li|td|th|quote|s|b|u|i)[^>]*>/',
			'/<span[^>]*>/',
			'/<\/span>/'
			);

	$tagReplace = array(
			'<$1>',
			'',
			''
			);
	$html = preg_replace($tagSearch, $tagReplace, $html);
	return $html;
}
// END: HTMLNoStylesSecure
//


//
// INIT: convertHTMLtoBB
// Convierte estilo HTML a BB
function convertHTMLtoBB ( $modString )
{
	//
	// INIT: Variables
	
	// Array para cambiar el comportamiento de algunas funciones
	$HTMLCode = array(
			'/<b>(.+?)<\/b>/',
			'/<i>(.+?)<\/i>/',
			'/<u>(.+?)<\/u>/',
			'/<img src=\"(.+?)\" \/>/',
			'/<img src=\"(.+?)\" height=\"(.+?)\" width=\"(.+?)\" \/>/',
			'/<ul class=\"listbullet\">(.+?)<\/ul>/',
			'/<p class=\"quote\">(.+?)<\/p>/'
			);

	$bbCode = array(
			'[b]$1[/b]',
			'[i]$1[/i]',
			'[u]$1[/u]',
			'[img]$1[/img]',
			'[img=$3x$2]$1[/img]',
			'[list]$1[/list]',
			'[quote]$1[/quote]'
			);

	// Parametros URL y mail
	$URLSearchString = " a-zA-Z0-9\:\/\-\?\&\.\=\_\~\#\'";
	$MAILSearchString = $URLSearchString . " a-zA-Z0-9\.@";

	// Elementos a eliminar
	$xtras = array("</p>", "<p>", "<p class=\"first\">");

	// END: Variables
	//

	//
	// Operaciones
	//

	// Eliminar la protección de las comillas
	$modString = str_replace("\\\"", "\"", $modString);

	// Convertir código html (inseguro)
	$modString = preg_replace_callback('/<ihtml>(.+?)<\/ihtml>/',
							create_function('$matches', 
											'$string = $matches[1];
											$string = htmlspecialchars(html_entity_decode($string));
											$string = "[html]".$string."[/html]";
											return $string;'),
											$modString);

	// Convertir todo menos URLs y correos
	$modString = preg_replace($HTMLCode, $bbCode, $modString);
	
	// Convertir Correos
	$modString = preg_replace_callback("/<a href=\"mailto:([$MAILSearchString]*)\">(.+?)<\/a>/",
							create_function('$matches',
									'if($matches[1]==$matches[2])
									{
									return "[mail]$matches[1][/mail]";
									} else {
									return "[mail=$matches[1]]$matches[2][/mail]";
									}'),
									$modString);

	// Convertir Direcciones URL
	$modString = preg_replace_callback("/<a href=\"([$URLSearchString]*)\" target=\"_blank\">(.+?)<\/a>/",
							create_function('$matches',
									'if($matches[1]==$matches[2]){
									return "[url]$matches[1][/url]";
									} else {
									return "[url=$matches[1]]$matches[2][/url]";
									}'),
									$modString);
	
	// Remover elementos extras
	$modString = str_replace($xtras, "", $modString);

	// Recuperar capacidad del elemento HTML para ser procesado
	$modString = preg_replace_callback("/\[html\](.+?)\[\/html\]/",
							create_function('$matches', 
									'$string = $matches[1];
									$string = html_entity_decode($string);
									$string = "[html]".$string."[/html]";
									return $string;'),
									$modString);

	// Regresar string
	return $modString;
}
// END: convertHTMLtoBB
//


//
// INIT: cleanHTML
//	Limpia el código de HTML para que ocupe menos espacio y se muestre adecuadamente.
function cleanHTML($html)
{
	/// <summary>
	/// Removes all FONT and SPAN tags, and all Class and Style attributes.
	/// Designed to get rid of non-standard Microsoft Word HTML tags.
	/// </summary>
	// start by completely removing all unwanted tags
	$removeNormal = array("&nbsp;");
	$removePCRE = array('/<(meta|link)([^>]*)>/',
						'/<style>(\w|\W)+?<\/style>/',
						'/<\/?(COL|XML|ST1|SHAPE|V:|O:|F:|F |PATH|LOCK|IMAGEDATA|STROKE|FORMULAS|ISTYLE|SPANSTYLE)[^>]*>/',
						'/\bCLASS="?(MSO\w*|Apple-style-span)"?/',
						'/<!--.*?-->/'
						);
	// Quitar espacios y caracteres inválidos
	$html = str_replace($removeNormal, "", $html);
	$html = preg_replace("/\s+/", " ", $html);
	// Quitar elementos provenientes de otros escritores de téxto
	$html = preg_replace($removePCRE, "", $html);
	// Quitar etiquetas que no se usan dentro del sitio
	$html = preg_replace_callback('/<(\w+) ([^>]+)>/', "cleanTags", $html);
	// Quitar span tags vacias
	$html = preg_replace('/<span>(.*?)<\/span>/', "$1", $html);
	$html = preg_replace('/<span[^>]*>\s*<\/span>/', "", $html);

	// Agregar espacios
	$html = preg_replace('/<(table|tbody|tr|thead|ul|ol)([^>]*)>/', "<$1$2>\n", $html);
	$html = preg_replace('/<\/(p|h1|h2|h3|h4|h5|h6|ol|ul|li|table|tbody|tr|td|th|thead)>?/', "</$1>\n", $html);
	$html = preg_replace('/<(li|th|tr|tbody|thead|ul)/', "\t<$1", $html);
	$html = preg_replace('/<\/[\s]*(li|th|tr|tbody|thead|ul) /', "\t</ $1", $html);
	return $html;
}
// END: cleanHTML
//

// Sub función de cleanHTML
function cleanTags ($matches)
{
	// Tag en minúsculars
	$tag = strtolower($matches[1]);
	$text = $matches[2];
	$tagProps = "";
	
	
	// obtener elementos
	preg_match_all('/(\w+)="([^"]*)"/', $text, $props);

	$count = count($props[0]);
	for ( $i = 0; $i < $count; $i++ )
	{
		$add = true;
		$att = strtolower($props[1][$i]);
		$val = trim(strval($props[2][$i]));
		
		if ( !empty($val) && $att != "lang" && ( $att != "style" || $tag == "span" ) && ( $att != "class" || !preg_match('/Mso/', $val) ) )
		{
			$tagProps.= " $att=\"$val\"";
		}
	}

	$tagProps = trim($tagProps);
	if ( !empty($tagProps) )
	{
		$tagProps = " ".$tagProps;
	}
	return "<$tag$tagProps>";
}

/**
// @author   "Sebastián Grignoli" <grignoli@framework2.com.ar>
// @package  forceUTF8
// @version  1.1
// @link     http://www.framework2.com.ar/dzone/forceUTF8-es/
// @example  http://www.framework2.com.ar/dzone/forceUTF8-es/
  */

function forceUTF8($text)
{
	// Function forceUTF8
	//
	// This function leaves UTF8 characters alone, while converting almost all non-UTF8 to UTF8.
	//
	// It may fail to convert characters to unicode if they fall into one of these scenarios:
	//
	// 1) when any of these characters:   Ã€ÃÃ‚ÃƒÃ„Ã…Ã†Ã‡ÃˆÃ‰ÃŠÃ‹ÃŒÃÃŽÃÃÃ‘Ã’ÓÃ”Ã•Ã–Ã—Ã˜Ã™ÃšÃ›ÃœÃÃžÃŸ
	//    are followed by any of these:  ("group B")
	//                                    Â¡Â¢Â£Â¤Â¥Â¦Â§Â¨Â©ÂªÂ«Â¬Â­Â®Â¯Â°Â±Â²Â³Â´ÂµÂ¶â€¢Â¸Â¹ÂºÂ»Â¼Â½Â¾Â¿
	// For example:   %ABREPRESENT%C9%BB. Â«REPRESENTÃ‰Â»
	// The "Â«" (%AB) character will be converted, but the "Ã‰" followed by "Â»" (%C9%BB) 
	// is also a valid unicode character, and will be left unchanged.
	//
	// 2) when any of these: Ã ÃºÃ¢Ã£Ã¤Ã¥Ã¦Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯  are followed by TWO chars from group B,
	// 3) when any of these: Ã°ñÃ²ó  are followed by THREE chars from group B.
	//
	// @name forceUTF8
	// @param string $text  Any string.
	// @return string  The same string, UTF8 encoded

  if(is_array($text))
    {
      foreach($text as $k => $v)
    {
      $text[$k] = forceUTF8($v);
    }
      return $text;
    }

    $max = strlen($text);
    $buf = "";
    for($i = 0; $i < $max; $i++)
	{
        $c1 = $text{$i};
        if($c1>="\xc0"){ //Should be converted to UTF8, if it's not UTF8 already
          $c2 = $i+1 >= $max? "\x00" : $text{$i+1};
          $c3 = $i+2 >= $max? "\x00" : $text{$i+2};
          $c4 = $i+3 >= $max? "\x00" : $text{$i+3};
            if($c1 >= "\xc0" & $c1 <= "\xdf"){ //looks like 2 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2;
                    $i++;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xe0" & $c1 <= "\xef"){ //looks like 3 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2 . $c3;
                    $i = $i + 2;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xf0" & $c1 <= "\xf7"){ //looks like 4 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf" && $c4 >= "\x80" && $c4 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2 . $c3;
                    $i = $i + 2;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } else { //doesn't look like UTF8, but should be converted
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = (($c1 & "\x3f") | "\x80");
                    $buf .= $cc1 . $cc2;				
            }
        } elseif(($c1 & "\xc0") == "\x80"){ // needs conversion
                $cc1 = (chr(ord($c1) / 64) | "\xc0");
                $cc2 = (($c1 & "\x3f") | "\x80");
                $buf .= $cc1 . $cc2;				
        } else { // it doesn't need convesion
            $buf .= $c1;
        }
    }
	$buf = preg_replace('/[\x00-\x09\x0B-\x1F\x80-\xFF]/', '', $buf);
    return $buf;
}

function forceLatin1($text) {
  if(is_array($text)) {
    foreach($text as $k => $v) {
      $text[$k] = forceLatin1($v);
    }
    return $text;
  }
  return forceUTF8($text);
}

function fixUTF8($text){
  if(is_array($text)) {
    foreach($text as $k => $v) {
      $text[$k] = fixUTF8($v);
    }
    return $text;
  }
  
  $last = "";
  while($last <> $text){
    $last = $text;
    $text = forceUTF8(forceUTF8($text));
  }
  return $text;    
}

?>