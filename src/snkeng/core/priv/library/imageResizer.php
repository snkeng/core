<?php
//
use snkeng\core\engine\imgResizer;

//
function imageResizer($id, $sizes, $killIfError = true) {
	$results = [];
	if ( gettype($sizes) === 'integer' ) {
		$sizes = [$sizes];
	}
	if ( intval($id) === 0 ) { se_killWithError("ID Imagen no válido.({$id})"); }
	$sql_qry = <<<SQL
SELECT
img.img_floc AS floc, img.img_fname AS fname
FROM st_site_images AS img
WHERE img.img_id={$id};
SQL;
	$imgData = $GLOBALS['mysql']->singleRowAssoc($sql_qry);
	$file = $_SERVER['DOCUMENT_ROOT'].$imgData['floc'];
	if ( file_exists($file) ) {
		$imgSize = getimagesize($file);
		$imageRez = new imgResizer();
		$baseName = substr($imgData['floc'], 0, strrpos($imgData['floc'], '.'));

		foreach ( $sizes as $size ) {
			$newImage = [];
			if ( $imgSize[0] > $size ) {
				// New Name
				$newName = $baseName . "_w{$size}.jpg";
				$newFullName = $_SERVER['DOCUMENT_ROOT'] . $newName;
				if ( !file_exists($newFullName) ) {
					// Process per image
					$imageRez->load($file);
					$imageRez->maxResize($size);
					$imageRez->save($newFullName, IMAGETYPE_JPEG, 60);

					//
					$newImage = [
						'floc' => $newName,
						'x' => $imageRez->imageSize[0],
						'y' => $imageRez->imageSize[1],
					];
				} else {
					$imgSize = getimagesize($newFullName);
					$newImage = [
						'floc' => $newName,
						'x' => $imgSize[0],
						'y' => $imgSize[1],
					];
				}
			} else {
				$newImage = [
					'floc' => $imgData['floc'],
					'x' => $imgSize[0],
					'y' => $imgSize[1],
				];
			}
			// Save
			$results[$size] = $newImage;
		}
	} else {
		if ( $killIfError ) { se_killWithError("Imágen no existe"); }
	}

	return $results;
}