<?php
// Imprimir una galería e incluir objeto para cargar el js
function zoomGallery($objects, $galClass, $galId)
{
	$response = array('html'=>'', 'mt'=>'', 'js'=>'');
	$menu = '';
	$current = '';
	$first = true;
	foreach ( $objects as $obj )
	{
		if ( $first )
		{
$current.= <<<EOD
<img id="{$galId}_img" src="{$obj['imgM']}" data-zoom-image="{$obj['imgB']}"/>
EOD;
		}
$menu.= <<<EOD
<a href="#" data-image="{$obj['imgM']}" data-zoom-image="{$obj['imgB']}"><img src="{$obj['imgS']}" /></a>\n
EOD;
		$first = false;
	}
	//
$response['html'].=<<<EOD
<div class="{$galClass}">
<div class="main">{$current}</div>
<div class="menu" id="{$galId}_menu">\n{$menu}</div>
</div>\n
EOD;
	//
$response['mt'].=<<<EOD
// $("#{$galId}_img").elevateZoom({gallery:'{$galId}_menu', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: '/se_site_core/res/img/ajax_black.gif',zoomWindowFadeIn: 500, zoomWindowFadeOut: 500, lensFadeIn: 500, lensFadeOut: 500, scrollZoom : true, zoomWindowPosition: "demo-container"});\n
EOD;
	//
	return $response;
}
?>