<?php
//
$fb = new twitter_se();
$user = $fb->getUser(true);

// Attemp login
if ( !user_loginMain::loginWithSocialNetwork('tw', $user['id']) ) {
	//
	$userData = $fb->getUserCoreData();

	// Register Data
	$response = [];
	\snkeng\core\user\management::userCreate($response, $userData['first_name'], $userData['last_name'], 3, 'socnet', [
		'socnet' => 2,
		'snId' => $user['id']
	]);

	//
	user_loginMain::forceFirstLogin($response['d']['id']);
}

//
header('Location: /');
exit();