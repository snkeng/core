<?php
//
$response = [];

//
$key = \snkeng\core\engine\nav::next();
$linkData = \snkeng\core\user\extlink::linkValidate( $key );

//
if ( empty($linkData) ) {
	se_killWithError("Vínculo no válido.");
}

//
switch ( $linkData['app'] ) {
	//
	case 'core':
		//
		switch ( $linkData['act'] ) {
			//
			case 'usr_mail':
				header('Location: /user/activation?key='.$key);
				exit;
				break;
			//
			case 'usr_recov':
				header('Location: /guest/recovery?key='.$key);
				exit;
				break;
			//
			case 'adm_recov':
				header('Location: /admin/guest/recovery?key='.$key);
				exit;
				break;
			//
			default:
				se_killWithError('Wrong config...wtf..');
				break;
		}
		break;

	//
	default:
		$mainApp = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/routing/smartlink.php';
		if ( !file_exists($mainApp) ) { se_killWithError("No existe definición de smartlink en este sitio."); }

		// Require if exists
		require $mainApp;
		break;
}
//
