<?php
// Basic functions (siteVars, server validation, lang, mysql)
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/head.php';


// Server process in other part (no session)
if ( \snkeng\core\engine\nav::current() === 'server') {
	\snkeng\core\engine\nav::next(); // Remove server

	require __DIR__ . "/server.php";
	exit;
}

// Session start
session_start();

// Define params
$params = [];

//
\snkeng\core\engine\nav::asyncCheck();

// Initial navigation
switch ( \snkeng\core\engine\nav::current() ) {
	//
	case 'system':
		\snkeng\core\engine\nav::next();
		require __DIR__ . '/system.php';
		exit();
		break;

	// (JSON Expected response for inner app operations)
	case 'api':
		\snkeng\core\engine\nav::next();
		require __DIR__ . '/api.php';
		exit();
		break;

	//
	case 'admin':
		\snkeng\core\engine\nav::next();

		//
		require __DIR__ . '/admin.php';
		exit();
		break;

	//
	case 'se_slink':
		\snkeng\core\engine\nav::next();
		require __DIR__ . '/se_slink.php';
		exit();
		break;

	//
	default:
		//
		\snkeng\core\engine\nav::asyncSetTemplate($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/templates/' . $siteVars['page']['template'] . '.php');

		//
		// require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/class/se_page.php';
		$page = \snkeng\core\engine\nav::pageGetStruct();

		// Sitio
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/routing/main.php';

		//
		if ( \snkeng\core\engine\nav::$templatePrint ) {
			require \snkeng\core\engine\nav::$templateFile;
		}

		// Imprimir página
		\snkeng\core\engine\nav::pagePrint();
		break;
}

//
die('Incorrect result: should not display');