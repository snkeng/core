<?php
// Default no cache
\snkeng\core\engine\nav::cacheSetType('');
\snkeng\core\engine\nav::cacheSetPrivacy('private');
\snkeng\core\engine\nav::cacheSetTime(14400);

// Set admin default template
\snkeng\core\engine\nav::asyncSetTemplate($_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/templates/main.php');

// Cargar estructura de la página
$page = \snkeng\core\engine\nav::pageGetStruct();

// Validate if not logged or in root
$pathNext = \snkeng\core\engine\nav::next();
if ( $pathNext !== null || \snkeng\core\engine\login::check_loginStatus() ) {
	require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/admin.php';
}

// Admin cache is private for security reasons
\snkeng\core\engine\nav::cacheSetPrivacy('private');

// Update params
$params = [
	'nav' => [
		'normal' => '',
		'breadcrumbs' => '',
	],
	'page' => [
		'main' => '',
		'ajax' => '',
	],
	'vars' => []
];

//
switch ( \snkeng\core\engine\nav::current() )
{
	//
	case 'core':
		//
		\snkeng\core\engine\login::kill_loginLevel('admin');

		//
		$appData['name'] = \snkeng\core\engine\nav::next();
		$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/'.$appData['name'];

		//
		$appData['url'] = '/admin/core/'.$appData['name'];
		$appData['url_json'] = '/api/module_adm/core/'.$appData['name'];

		//
		\snkeng\core\engine\login::permitsCheckKill($appData['name']);

		//
		$params['page']['main'] = $appData['url'];
		$params['page']['ajax'] = $appData['url_json'];

		//
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/pages/c_app_loader.php';
		break;

	//
	case 'site':
		//
		\snkeng\core\engine\login::kill_loginLevel('admin');

		//
		$appData['name'] = \snkeng\core\engine\nav::next();
		$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/'.$appData['name'];
		$appData['url'] = '/admin/site/'.$appData['name'];
		$appData['url_json'] = '/api/module_adm/site/'.$appData['name'];

		//
		\snkeng\core\engine\login::permitsCheckKill($appData['name']);

		//
		$params['page']['main'] = $appData['url'];
		$params['page']['ajax'] = $appData['url_json'];

		//
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/pages/c_app_loader.php';
		break;

	//
	case 'user':
		//
		\snkeng\core\engine\login::kill_loginLevel('admin');

		//
		$appData['name'] = \snkeng\core\engine\nav::current();
		$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/user/';
		$appData['url'] = '/admin/user/';
		$appData['url_json'] = '/api/module_adm/user/';

		//
		$params['page']['main'] = $appData['url'];
		$params['page']['ajax'] = $appData['url_json'];

		//
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/pages/c_app_loader.php';
		break;

	//
	case 'guest':
		\snkeng\core\engine\nav::asyncSetTemplate($_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/templates/container.php');
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/guest/map_main.php';
		break;

	// Inicio
	case '':
	case null:
		// No cache (to avoid user is logged in bug)
		\snkeng\core\engine\nav::cacheSetType('none');

		//
		if ( !\snkeng\core\engine\login::check_loginStatus() ) {
			// Conditions default
			\snkeng\core\engine\nav::asyncSetTemplate($_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/templates/container.php');
			require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/pages/b_page_login.php';
		} else {
			//
			\snkeng\core\engine\login::kill_loginLevel('admin');

			// Conditions default
			require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_core/pages/d_index.php';
		}
		break;

	// ERROR404
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

//
if ( \snkeng\core\engine\nav::$templatePrint ) {
	require \snkeng\core\engine\nav::$templateFile;
}

// Imprimir página
\snkeng\core\engine\nav::pagePrint();
