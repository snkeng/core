<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	// Picture
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing($response,
			[
				'email' => ['name' => 'Email', 'type' => 'email', 'validate' => true],
			]
		);

		//
		$response['d'] = \snkeng\core\user\management::mailAdd($rData['email'], $userData['id']);
		break;

	//
	case 'delete':

		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'mailId' => ['name' => 'Mail ID', 'type' => 'int', 'validate' => true]
			]
		);

		\snkeng\core\user\management::mailRemove($rData['mailId'], $userData['id']);
		break;

	//
	case 'primary':
		$pData = [
			'mailId' => ['name' => 'Mail ID', 'type' => 'int', 'validate' => true]
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		\snkeng\core\user\management::mailSetPrimary($rData['mailId'], $userData['id']);
		break;

	//
	case 'validate':
		$pData = [
			'mailId' => ['name' => 'Mail ID', 'type' => 'int', 'validate' => true]
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		$mailInfo = \snkeng\core\user\management::mailStatus($rData['mailId']);

		if ( $mailInfo['userId'] !== $userData['id'] ) {
			se_killWithError('El correo no corresponde al usuario', '');
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
