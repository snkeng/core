<?php

//

$pData = [
	'given-name' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 3, 'lMax' => 75, 'filter' => 'names', 'process' => false, 'validate' => true],
	'family-name' => ['name' => 'Apellido', 'type' => 'str', 'lMin' => 3, 'lMax' => 75, 'filter' => 'names', 'process' => false, 'validate' => true],
	'email' => ['name' => 'Email', 'type' => 'email', 'validate' => true],
	'new-password' => ['name' => 'Contraseña', 'type' => 'str', 'lMin' => 8, 'lMax' => 32, 'filter' => 'password', 'process' => false, 'validate' => true],
	'accept' => ['name' => 'Terminos y condiciones', 'type' => 'bool', 'validate' => true],
];
$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

// Revisar anti bot
if ( !\snkeng\core\external\recaptcha::verify() ) {
	se_killWithError("Captcha no aprobado.");
}

// Crear usuario
\snkeng\core\user\management::userCreate($response, $rData['given-name'], $rData['family-name'], 3, 'mail', [
	'eMail' => $rData['email'],
	'pass' => $rData['new-password'],
	'mailStatus' => 0
]);

// Force login (is registered)
\snkeng\core\engine\login::forceFirstLogin($response['d']['id']);
$response['s']['l'] = 1;

// VALIDAR CORREO, NUEVA FUNCIÓN?

$linkData = \snkeng\core\user\extlink::linkCreate('core', 'usr_mail', $response['mail']['id'], 90, false);
$response['debug']['linkData'] = $linkData;


// debugVariable($response);

// Enviar correo
$to = "{$response['d']['name_full']} <{$rData['email']}>";
$title = 'Nueva cuenta en '.$siteVars['site']['name'];

//
$message = <<<HTML
<p>Se ha creado una cuenta en {$siteVars['site']['name']}.</p>
<p>Para terminar su registro entre al siguiente url:</p>
<p><a href="{$linkData['url']}">{$linkData['url']}</a></p>
HTML;


//
$mail = \snkeng\core\general\mail::sendHTML($to, $title, $message, '/snkeng/site_core/templates/mail.php');
$response['debug']['mail'] = $mail['debug']['mail'];

//