<?php
//

switch ( \snkeng\core\engine\nav::next() ) {
	// Revisión login
	case 'in':
		\snkeng\core\engine\login::loginExceded();
		/*
		if ( !isset($_SESSION['login_redirect']) && isset($_SERVER['HTTP_REFERER']) )
		{
			$_SESSION['login_redirect'] = $_SERVER['HTTP_REFERER'];
		}*/
		// Login
		switch ( \snkeng\core\engine\nav::next() )
		{
			//
			case 'email':
				$uName = $_POST['email'];
				$uPass = $_POST['current-password'];
				$uSave = ( isset($_POST['a_save']) && $_POST['a_save'] === 't' );
				\snkeng\core\engine\login::loginWithPost($response, $uName, $uPass, $uSave);
				break;
			//
			case 'socnet':
				switch ( \snkeng\core\engine\nav::next() )
				{
					//
					case 'fb':
						require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/user/login_facebook.php';
						break;
					//
					case 'gp':
						require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/user/login_google.php';
						break;
					//
					default:
						\snkeng\core\engine\nav::invalidPage();
						break;
				}
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	case 'check':
		$response['d'] = \snkeng\core\engine\login::returnUserStatus();
		break;

	// Operación logout
	case 'out':
		\snkeng\core\engine\login::killLogin();
		$siteVars['user'] = [
			'login' => [
				'status' => 0
			]
		];
		//
		$iniFile = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/user_logout.php';
		if ( file_exists($iniFile) ) {
			require $iniFile;
		}

		$response['s']['t'] = 1;
		$response['s']['l'] = ( $siteVars['user']['login']['status'] ) ? 1 : 0;
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
