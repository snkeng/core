<?php
\snkeng\core\engine\nav::$async = true;

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'sitemap':
		require __DIR__."/../func/sitemap_basic.php";
		break;

	//
	case 'api':
		$apiName = \snkeng\core\engine\nav::next();
		if ( empty($apiName) ) { \snkeng\core\engine\nav::invalidPage('NO API NAME'); }

		// Build target location
		$target_file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/' . $apiName . '/routing/server/#map.php';

		// Check if location exists
		if ( !file_exists($target_file) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Call location
		require $target_file;
		break;

	//
	case '':
	case null:
		\snkeng\core\engine\nav::invalidPage();
		break;

	// Not a system default option, read site
	default:
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/routing/server.php';
		break;
}
//
