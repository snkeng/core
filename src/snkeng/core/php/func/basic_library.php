<?php

//<editor-fold desc="Imprimir finales">
/**
 * Kill all the current operations and return an  error according to  current scenario
 *
 * @param string $errTitle       Short error message
 * @param string $errDescription Extended error message, is still public
 * @param int    $eNum           return error type, mainly for http error responses.
 * @param array  $debugVariable  print (if in testing environment) all the debug variable informatio
 */
function se_killWithError($errTitle, $errDescription = '', $eNum = 0, $debugVariable = [])
{
	global $siteVars;
	//
	header("Cache-Control: no-cache, must-revalidate");

	// Missin login operation
	if ( $eNum === 1 ) {
		$tempUri = $_SERVER['REQUEST_URI'];
		if ( substr($tempUri, 0, 8) === "/partial" ) {
			$tempUri = substr($tempUri, strpos($tempUri, '/', 9));
		}

		//
		$_SESSION['login_redirect'] = $tempUri;
	}

	//
	if ( class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\nav::$async ) {
		global $response;
		//
		if ( $debugVariable && ($_ENV['SE_DEBUG'] || (class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\login::check_loginLevel('sadmin'))) ) {
			$response['debug']['killData'] = $debugVariable;
		}


		$response['title'] = $errTitle;
		$response['description'] = $errTitle;
		$response['timestamp'] = date('Y-m-d H:i:s');

		//
		$eNum = ($eNum !== 0) ? $eNum : 500;
		$eNum = ($eNum === 1) ? 401 : $eNum;

		//
		printJSONData($response, $eNum);
	} else {
		// Mostrar error deacuerdo a la situación
		switch ( $eNum ) {
			//
			case 1:
				header("Location: {$siteVars['site']['login_url']}", true, 307);
				break;
			//
			case 200:
				header('Content-type: text/plain; charset=utf-8');
				header("HTTP/1.1 200 OK");
				echo <<<TEXT
200 - SERVER ERROR...?
---
{$errTitle}
----
{$errDescription}
TEXT;
				break;
			//
			case 301:
			case 307:
				$_SESSION['target_url'] = $_SERVER['REQUEST_URI'];
				header("Location: {$errTitle}", true, $eNum);
				exit;
				break;

			// No encontrado
			case 404:
				http_response_code(404);
				header('Content-type: text/plain; charset=utf-8');
				echo <<<TEXT
404 - NOT FOUND
{$errTitle}
----
{$errDescription}
TEXT;
				break;

			// No autorizado
			case 401:
				header('Content-type: text/plain; charset=utf-8');
				http_response_code(401);
				echo <<<TEXT
401 - UNAUTHORIZED
----
{$errTitle}
-
{$errDescription}
TEXT;
				break;

			// Gone
			case 410:
				header('Content-type: text/plain; charset=utf-8');
				http_response_code(410);
				echo <<<TEXT
410 - GONE
{$errTitle}
----
{$errDescription}
TEXT;
				break;
			//
			case 500:
			default:
				header('Content-type: text/plain; charset=utf-8');
				http_response_code(500);
				echo <<<TEXT
500 - SERVER MALFUNCTION
---

{$errTitle}
-
{$errDescription}
TEXT;
				break;
		}

		if ( isset($debugVariable) && ($_ENV['SE_DEBUG'] || (class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\login::check_loginLevel('sadmin'))) ) {
			$debugVariable = debugConvertVariable($debugVariable);
			echo <<<TEXT
\n\nDEBUG:
{$debugVariable['type']}
{$debugVariable['content']}
TEXT;

		}

		exit();
	}
	//
	exit();
}


// Debug de variables
function debugVariable($var, $name = 'Not Defined', $kill = true)
{
	if ( $kill ) {
		http_response_code(500);
		header('Content-type: text/plain; charset=utf-8');
	}

	$result = debugConvertVariable($var);

	//
	echo <<<TEXT
--- DEBUGVARIABLE ---
Name: {$name}.
Type: {$result['type']}.
Var:
--- INI ---
{$result['content']}
--- END ---
---\n\n
TEXT;

	// Terminar operación?
	if ( $kill ) {
		exit();
	}
}

//
function debugConvertVariable($var)
{
	$pVar = '';
	$pType = gettype($var);

	switch ( $pType ) {
		case 'boolean':
		case 'integer':
		case 'double':
		case 'string':
			$pVar = $var;
			break;
		case 'array':
		case 'object':
		case 'resource':
			$pVar = print_r($var, true);
			break;
		case 'null':
			break;
		default:
			$pType = 'Desconocido';
			$pVar = $var . "\n\n---\n\n" . print_r($var, true);
			break;
	}

	//
	return [
		'type' => $pType,
		'content' => $pVar
	];
}

//
function debugVarToSQL($title, $var, $kill = false)
{

	$result = debugConvertVariable($var);

	//
	$report = <<<TEXT
Type: {$result['type']}.
Var:
--- INI ---
{$result['content']}
--- END ---
TEXT;

	$safeReport = \snkeng\core\engine\mysql::real_escape_string($report);

	//
	$ins_qry = <<<SQL
INSERT INTO sb_errorlog
(elog_dtadd, elog_type, elog_key, elog_content)
VALUES
(now(), 2, '{$title}', '{$safeReport}');
SQL;
	\snkeng\core\engine\mysql::submitQuery($ins_qry);

	//
	if ( $kill ) {
		http_response_code(500);
		header('Content-type: text/plain; charset=utf-8');
		echo "ERROR: {$title}\n";
		echo $report;
		exit;
	}
}

// Imprimir JSON
function printJSONData($array, $httpCode = 200)
{
	$_SERVER["HTTP_USER_AGENT"] = (!empty($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : '';
	if ( preg_match("/msie\s(\d+).(\d+)/", strtolower($_SERVER["HTTP_USER_AGENT"]), $arr) ) {
		header('Content-type: text/html; charset=utf-8');
	} else {
		header('Content-type: application/json; charset=utf-8');
	}

	//
	http_response_code($httpCode);

	// Quitar debug si es necesario
	if ( !empty($array['debug']) ) {
		global $siteVars;
		if ( !$_ENV['SE_DEBUG'] && (!class_exists('\snkeng\core\engine\login') || !\snkeng\core\engine\login::check_loginLevel('sadmin')) ) {
			unset($array['debug']);
		} else {
			$debugType = gettype($array['debug']);
			$search = ['/[\s]+/'];
			$destroy = [' '];
			switch ( $debugType ) {

				case 'string':
					$array['debug'] = preg_replace($search, $destroy, $array['debug']);
					break;

				case 'array':
					foreach ( $array['debug'] as &$value ) {
						if ( is_string($value) ) {
							$value = preg_replace($search, $destroy, $value);
						}
					}
					break;
			}
		}
	}

	//
	echo(json_encode($array, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	exit();
}

//</editor-fold>

//<editor-fold desc="Transformación variables">

// advanced stringPopulate
function stringPopulate($string, $variables)
{
	$search = [];
	$replace = [];
	foreach ( $variables as $name => $value ) {
		$search[] = "!{$name};";
		$replace[] = $value;
	}
	$string = str_replace($search, $replace, $string);
	return $string;
}

//
function se_timeString($cTime = null)
{
	//
	$cTime = ($cTime) ? intval($cTime) : time();

	//
	return se_num_base10ToN($cTime, 62);
}

//
function se_num_base10ToN($num, $base = 62)
{
	$index = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$res = '';
	//
	do {
		$res = $index[$num % $base] . $res;
		$num = intval($num / $base);
	} while ( $num );
	//
	return $res;
}

//
function se_num_baseNto10($num, $base = 62)
{
	$index = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	//
	$limit = strlen($num);
	$res = strpos($index, $num[0]);
	//
	for ( $i = 1; $i < $limit; $i++ ) {
		$res = $base * $res + strpos($index, $num[$i]);
	}
	//
	return intval($res);
}


//
function se_randString($len, $upperOnly = false, $uncomplicated = false)
{
	$str = '';
	//
	if ( $uncomplicated ) {
		$characters = ($upperOnly) ? '23456789ABCDEFGHIJKLMNPQRSTUVWXYZ' : '23456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	} else {
		$characters = ($upperOnly) ? '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ' : '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	}
	$charLen = strlen($characters) - 1;
	//
	for ( $p = 0; $p < $len; $p++ ) {
		$str .= $characters[mt_rand(0, $charLen)];
	}
	//
	return $str;
}

function base64url_encode($data)
{
	return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data)
{
	return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

//
function se_randNumberString($len)
{
	$str = '';
	$characters = '0123456789';
	$charLen = strlen($characters) - 1;
	//
	for ( $p = 0; $p < $len; $p++ ) {
		$str .= $characters[mt_rand(0, $charLen)];
	}
	//
	return $str;
}

//</editor-fold>

//<editor-fold desc="File loading">

// Funciones engine
function seLoad_engFunc($func)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/library/' . $func . '.php');
}

//
function seLoad_appParams($appName, $asMain = false)
{
	// Add to "conditions"
	global $conditions;
	$appDir = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/' . $appName;
	$conditions['app'][$appName] = (require($appDir . '/params.php'));
	// Language files
	//
	if ( $asMain ) {
		$conditions['load']['app'] = $appName;
	}
	//
	return $appDir;
}

//
function seLoad_appFunc($app, $func)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/' . $app . '/resources/func_' . $func . '.php');
}

//</editor-fold>


//<editor-fold desc="Manejo frontend">
// Reescribir título
function siteTitle($siteName, $cTitle = '')
{
	if ( !empty($cTitle) ) {
		$len = strlen($cTitle);
		if ( $len > 60 ) {
			preg_match('/(.{60}.*?)\b/', $cTitle, $matches);
			$title = rtrim($matches[1]);
		} else {
			$title = $cTitle;
		}
		$title .= ' | ' . $siteName;
	} else {
		$title = $siteName;
	}
	return $title;
}

//</editor-fold>
