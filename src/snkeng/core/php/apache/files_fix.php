<?php
// Move document root
$_SERVER['DOCUMENT_ROOT'] = substr($_SERVER['DOCUMENT_ROOT'], 0, -12);

// Autoload
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/autoload.php';

// Load environment varibles
\snkeng\core\engine\environment::loadEnv('.');

// Only available in development?
if ( !$_ENV['SE_DEBUG'] && file_exists($_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles/files.json') ) {
	\snkeng\core\engine\nav::killWithError('Op no válida.', 'Se solicitó una operación no válida en live.', 500);
}

header('Content-Type: text/plain; charset=utf-8');
//
echo("STARTING FULL REFRESH...(".time().")\n");

//
echo("BUNDLES:\n");
\snkeng\admin_modules\admin_uber\minify_bundles::exec($response, true);
echo("MODULES:\n");
\snkeng\admin_modules\admin_uber\minify_modules::exec($response, true);
echo("MOVING SVG-ICON LIBRARIES:\n");
\snkeng\admin_modules\admin_uber\svg_icons\library::copy($response, true);

//
// require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/superadmin/func/func_fontAwesomeSVG.php';
echo("FILE SYSTEM UPDATED.\n");
exit();