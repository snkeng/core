<?php

namespace se_core\php\apache;
class responses
{
	public static function op_killWithError($error, $errorAdvanced = '', $eNum = 0)
	{
		//
		header("Cache-Control: no-cache, must-revalidate");
		switch ($eNum) {
			// No autorizado
			case 404:
				header('Content-type: text/plain; charset=utf-8');
				header('HTTP/1.1 401 Internal Server Error');
				echo <<<TEXT
SNKENG: SERVER ERROR (401).
{$error}
----
{$errorAdvanced}
TEXT;
				break;

			// No autorizado
			case 401:
				header('Content-type: text/plain; charset=utf-8');
				header('HTTP/1.1 401 Internal Server Error');
				echo <<<TEXT
SNKENG: SERVER ERROR (401).
{$error}
----
{$errorAdvanced}
TEXT;
				break;

			// Gone
			case 410:
				header('Content-type: text/plain; charset=utf-8');
				header('HTTP/1.1 410 Internal Server Error');
				echo <<<TEXT
{$error}
----
{$errorAdvanced}
TEXT;
				break;
			default:
				header('Content-type: text/plain; charset=utf-8');
				header('HTTP/1.1 500 Internal Server Error');
				echo <<<TEXT
SNKENG: SERVER ERROR (500).
{$error}
----
{$errorAdvanced}
TEXT;
				break;
		}
		exit();
	}

	//
	public static function timeToGMT($timeToGMT)
	{
		return gmdate("D, d M Y H:i:s", $timeToGMT) . " GMT";
	}

	// send file
	public static function fileSend($fileName, $fType, $compressed = false, $debug = false)
	{
		$cacheTime = 604800;
		$file_length = filesize($fileName);
		$last_modified_time = filemtime($fileName);


		$ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);

		// Check if cache request exists, else print default cache
		if (@strtotime($ifModifiedSince) === $last_modified_time) {
			http_response_code(304);
			exit;
		}

		// Cache
		header('Cache-Control: public, max-age=' . $cacheTime . ', min-fresh=600, must-revalidate');
		header('ETag: "' . md5($last_modified_time) . '"');
		header('Last-Modified: ' . self::timeToGMT($last_modified_time));
		header('Expires: ' . self::timeToGMT(time() + $cacheTime));
		header('Pragma: cache');

		// File information
		switch ($fType) {
			//
			case 'jpeg':
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			//
			case 'webp':
				header('Content-Type: image/webp');
				break;
			//
			case 'avif':
				header('Content-Type: image/avif');
				break;
			//
			case 'css':
				header('Content-Type: text/css');
				break;
			//
			case 'js':
			case 'mjs':
				header('Content-Type: text/javascript');
				break;
		}

		//
		if ($compressed) {
			header('Content-Encoding: gzip');
		}

		//
		header('Content-Length: ' . $file_length);

		// Send file
		readfile($fileName);
		exit;
	}

}