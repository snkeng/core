<?php
// Admin head

if ( \snkeng\core\engine\login::check_loginStatus() ) {
	// Load permission file
	if ( !\snkeng\core\engine\login::check_loginLevel('sadmin') ) {
		\snkeng\core\engine\login::permitsSetRules($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_permits.php');
	}

	// Update database privileges
	if ( \snkeng\core\engine\login::check_loginLevel('uadmin') ) {
		\snkeng\core\engine\mysql::changeConnection($_ENV['MYSQL_ADM_USR'], $_ENV['MYSQL_ADM_PWD']);
	}
}
