<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'notifications':
		require __DIR__ . '/notifications.php';
		break;
	//
	case 'settings':
		require __DIR__ . '/settings.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
