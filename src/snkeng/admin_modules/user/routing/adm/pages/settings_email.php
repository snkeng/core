<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'user', '/pages/user-adm-pages-settings-email.mjs');

// USER - SETTINGS - EMAIL

//
$tableRow = <<<HTML
<tr data-id="!id;" data-validated="!isValidated;" data-primary="!isPrimary;">
	<td>!email;</td>
	<td><span class="fakeCheck" data-type="primary" data-value="!isPrimary;"></span></td>
	<td><span class="fakeCheck" data-type="validated" data-value="!isValidated;"></span></td>
	<td class="verticalListObjects">
		<button class="btn small wide" data-action="mailPrimary">Hacer primario</button>
		<button class="btn small wide" data-action="mailValidate">Validar</button>
		<button class="btn small wide red" data-action="mailDelete"><svg class="icon inline mr"><use xlink:href="#fa-trash-o" /></svg>Eliminar</button>
	</td>
</tr>
HTML;

// Información de los correos
$sql_qry = <<<SQL
SELECT
    am_id AS id, am_mail AS email,
    IF (am_type=1, 1, 0) AS isPrimary,
    IF (am_status=2, 1, 0) AS isValidated
FROM sb_users_mail
WHERE a_id='{$siteVars['user']['data']['id']}' AND am_status!=0
LIMIT 5;
SQL;

//
$mailPrint = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $tableRow);

//
$page['head']['title'] = 'Correos vínculados';

//
$page['body'] = <<<HTML
<div class="pageSubTitle">Correos vínculados</div>

<user-adm-pages-settings-email id="user_settings_emails" class="grid">
	<div class="gr_sz04 gr_ps01">
		<h3>Agregar Correo</h3>
		<form class="se_form" method="post" action="/api/user/settings/email/add/" is="se-async-form">
			<div class="separator required"><label>
				<div class="cont"><span class="title">Nuevo correo</span><span class="desc"></span></div>
				<input type="email" name="email" required>
			</label></div>
			<button type="submit">Agregar</button>
			<output></output>
		</form>
		</div>
	<div class="gr_sz06">
		<h3>Correos actuales</h3>
		<div>
			<table class="se_table wide border">
				<template>{$tableRow}</template>
				<thead>
					<tr><th>Correo</th><th>Primario</th><th>Validado</th><th>Acciones</th></tr>
				</thead>
				<tbody>
				{$mailPrint}
				</tbody>
			</table>
		</div>
	</div>
</user-adm-pages-settings-email>
HTML;
//
