<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'user', '/pages/user-adm-pages-settings-preferences.mjs');

//
$page['head']['title'] = 'Preferencias';

//
$page['body'] = <<<HTML
<div class="pageSubTitle">Preferencias</div>

<user-adm-pages-settings-preferences class="grid">
	<div class="gr_sz06 gr_ps03">Not defined</div>
</user-adm-pages-settings-preferences>
HTML;
//
