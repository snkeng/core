<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'user', '/pages/user-adm-pages-settings-profile.mjs');

// Image Exists
$sql_qry = <<<SQL
SELECT
    a_fname AS fName, a_lname AS lName, a_obj_name as uName,
    a_img_full AS imgFull, a_img_crop AS imgCrop,
    a_banner_full AS bannerFull, a_banner_crop AS bannerCrop
FROM sb_objects_obj
WHERE a_id='{$siteVars['user']['data']['id']}';
SQL;

$userData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
	[],
	[
		'errorKey' => '',
		'errorDesc' => '',
	]
);

$userPicture = '';
if ( $userData['imgFull'] !== 'default' ) {
	$fullFile = '/se_files/user_upload/img/objects/'.$userData['imgFull'];
	$autoLoadPicture = ( file_exists($_SERVER['DOCUMENT_ROOT'].$fullFile) ) ? $fullFile : '';

	$userPicture = '/res/image/objects/w_160/'.$userData['imgCrop'].'.jpg';
}

$userBanner = '';
if ( $userData['bannerFull'] !== 'default' ) {
	$fullFile = '/se_files/user_upload/img/objects/'.$userData['bannerFull'];
	$autoLoadBanner = ( file_exists($_SERVER['DOCUMENT_ROOT'].$fullFile) ) ? $fullFile : '';

	$userBanner = '/res/image/objects/w_450/'.$userData['bannerCrop'].'.jpg';
}

//
$default = [
	'sn_fb_id' => '',
	'sn_fb_nm' => '',
	'sn_tw_id' => '',
	'sn_tw_nm' => '',
	'sn_gp_id' => '',
	'desc' => ''
];
//
$data = \snkeng\core\socnet\object_properties::readArray($siteVars['user']['data']['id'], 'snkeng', 'profile', $default);

//
$page['head']['title'] = 'Perfil Online';

//
$page['body'] = <<<HTML
<style>
user-profile-photo { display:flex; gap:20px; }
user-profile-photo[data-type="banner"] { flex-direction:column; }
user-profile-photo .userBanner { display:block; width: 100%; border-radius:3px; border:10px solid #EEE; }
user-profile-photo .userPicture { display:block; width: 180px; height: 180px; border-radius:3px; border:10px solid #EEE; }
user-profile-photo .options { display:flex; flex-direction:column; flex:1; gap:1em; }
user-profile-photo dialog { position:absolute; width:90vw; height:90vh; top:5vh; left:5vw; z-index:10; }
user-profile-photo canvas { width:100%; height:100%; }
user-profile-photo .photoCrop .options { display:flex; flex-direction:row; gap:20px; }
user-profile-photo .photoCrop .options * { flex:1; }
</style>
<div class="pageSubTitle">Perfil online</div>

<user-adm-pages-settings-profile class="grid">

	<div class="gr_sz04 gr_ps02">

		<div class="gOMB">
			<h2>Foto Banner</h2>
			<user-profile-photo data-type="banner" data-url="{$params['page']['ajax']}" data-image="{$userBanner}" data-id="{$siteVars['user']['data']['id']}" data-ekey="">
				<img class="userBanner" data-elem="mainPicture" src="{$userBanner}" width="450" height="150" />
				<div class="options">
					<form data-elem="imageUpload" action="/api/user/settings/image/upload/" method="post" is="se-async-form">
						<label class="btn wide blue">
							<input type="file" name="image" style="display:none;" accept=".jpeg, .jpg, .png" onchange="this.form.submit();" />
							<svg class="icon inline mr"><use xlink:href="#fa-upload" /></svg>Subir nueva foto
						</label>
					</form>
					<button class="btn wide blue" se-act="photoCrop"><svg class="icon inline mr"><use xlink:href="#fa-crop" /></svg>Ajustar foto</button>
				</div>
				<dialog>
					<div class="photoCrop">
						<h2>Editar</h2>
						<div class="options">
						<form data-elem="imageCrop" action="/api/user/settings/image/crop/" method="post" is="se-async-form">
							<input type="hidden" name="img_x" value="0" />
							<input type="hidden" name="img_y" value="0" />
							<input type="hidden" name="img_w" value="160" />
							<input type="hidden" name="img_h" value="160" />
							<button class="btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Actualizar</button>
							<output se-type="response"></output>
						</form>
						<button class="btn wide blue" se-act="cropClose"><svg class="icon inline mr"><use xlink:href="#fa-arrow-left" /></svg>Regresar</button>
						</div>
					</div>
					
					<div class="cropImage">
						<canvas data-autoload="{$autoLoadBanner}"></canvas>
					</div>
				</dialog>
			</user-profile-photo>
		</div>

		<div class="gOMB">
			<h2>Foto Perfil</h2>
			<user-profile-photo id="objectPhoto" data-type="profile" data-url="{$params['page']['ajax']}" data-image="{$userPicture}" data-id="{$siteVars['user']['data']['id']}" data-ekey="">
				<img class="userPicture" data-elem="mainPicture" src="{$userPicture}" />
				<div class="options">
					<form data-elem="imageUpload" action="/api/user/settings/image/upload/" method="post" is="se-async-form">
						<label class="btn wide blue">
							<input type="file" name="image" style="display:none;" accept=".jpeg, .jpg, .png" onchange="this.form.dispatchEvent(new Event('submit', {'bubbles': true, 'cancelable' : true }) );"  />
							<svg class="icon inline mr"><use xlink:href="#fa-upload" /></svg>Subir nueva foto
						</label>
					</form>
					<button class="btn wide blue" se-act="photoCrop"><svg class="icon inline mr"><use xlink:href="#fa-crop" /></svg>Ajustar foto</button>
				</div>
				<dialog>
					<div class="photoCrop">
						<h2>Editar</h2>
						<div class="options">
						<form data-elem="imageCrop" action="/api/user/settings/image/crop/" method="post" is="se-async-form">
							<input type="hidden" name="img_x" value="0" />
							<input type="hidden" name="img_y" value="0" />
							<input type="hidden" name="img_w" value="160" />
							<input type="hidden" name="img_h" value="160" />
							<button class="btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Actualizar</button>
							<output se-type="response"></output>
						</form>
						<button class="btn wide blue" se-act="cropClose"><svg class="icon inline mr"><use xlink:href="#fa-arrow-left" /></svg>Regresar</button>
						</div>
					</div>
					
					<div class="cropImage">
						<canvas data-autoload="{$autoLoadPicture}"></canvas>
					</div>
				</dialog>
			</user-profile-photo>
		</div>

	</div>

	<div class="gr_sz04">

		<div>
			<h2>Datos personales</h2>
			<form class="se_form" method="post" action="/api/user/settings/names" is="se-async-form">
				<input type="hidden" name="id" value="{$data['id']}">
				<label class="separator">
					<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
					<input type="text" name="fName" value="{$userData['fName']}" minlength="3" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Apellido</span><span class="desc"></span></div>
					<input type="text" name="lName" value="{$userData['lName']}" minlength="3" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Nombre de usuario</span><span class="desc"></span></div>
					<input type="text" name="uName" value="{$userData['uName']}" minlength="6" maxlength="30">
				</label>
				<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save"/></svg>Guardar</button>
				<ouput></ouput>
			</form>
		</div>

		<div>
			<h2>Perfil online</h2>
			<form class="se_form" method="post" action="/api/user/settings/profile" is="se-async-form">
				<input type="hidden" name="id" value="{$data['id']}">
				<label class="separator">
					<div class="cont"><span class="title">Facebook ID</span><span class="desc"></span></div>
					<input type="text" name="sn_fb_id" value="{$data['data']['sn_fb_id']}" minlength="6" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Facebook Name</span><span class="desc"></span></div>
					<input type="text" name="sn_fb_nm" value="{$data['data']['sn_fb_nm']}" minlength="3" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Twitter ID</span><span class="desc"></span></div>
					<input type="text" name="sn_tw_id" value="{$data['data']['sn_tw_id']}" minlength="3" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Twitter Name</span><span class="desc"></span></div>
					<input type="text" name="sn_tw_nm" value="{$data['data']['sn_tw_nm']}" minlength="3" maxlength="30">
				</label>
				<label class="separator">
					<div class="cont"><span class="title">Descripción</span><span class="desc"></span></div>
					<textarea se-plugin='autoSize' name='desc' maxlength='300'>{$data['data']['desc']}</textarea>
				</label>
				<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save"/></svg>Guardar</button>
				<ouput></ouput>
			</form>
		</div>
	</div>

</user-adm-pages-settings-profile>
HTML;
//
