// Plugin menu
se.plugin.se_usr_settings_password = function (plugElem, plugSettings) {
	// Elements
	const
		inputNewPass01 = plugElem.querySelector('input[name="pass_new1"]'),
		inputNewPass02 = plugElem.querySelector('input[name="pass_new2"]');

	//
	function init() {
		console.log("PASSWORD CHECKER ONLINE");

		//
		plugElem.se_on('change', 'input[name^=pass_new]', passCheck);

		//
		plugElem.se_plugin('simpleForm');
	}

	//
	function passCheck() {
		console.log("checking password", inputNewPass01.value, inputNewPass02.value, inputNewPass01.checkValidity(), inputNewPass02.checkValidity());
		inputNewPass02.setCustomValidity('');
		if ( inputNewPass01.checkValidity() && inputNewPass02.value !== '' && inputNewPass01.value !== inputNewPass02.value ) {
			console.log("test faailed, report");
			inputNewPass02.setCustomValidity("Las nuevas contraseñas no coinciden.");
		}
		//
	}

	//
	init();
	//
	return {};
};
//
