import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('user-adm-pages-settings-photo', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.structureForm.addEventListener('submit', this.submitForm.bind(this));
	}

	submitForm(e) {
		e.preventDefault();

		//
		formActions.formSubmit(
			this.structureForm,
			{
				onComplete:(msg) => {
					this.querySelector('output[se-elem="response"]').innerText = msg.d;
				}
			}
		);
	}
});

// Plugin menu
se.plugin.se_usr_settings_photo = function (plugElem, plugSettings) {
	const //
		mainUrl = '/api/user/settings/' + '/image',
		//
		actionMain = plugElem.querySelector('div.action[data-name="main"]'),
		objPhoto = actionMain.querySelector('img[se-type="userPhoto"]'),
		//
		actionUpload = plugElem.querySelector('div.action[data-name="photoUpload"]'),
		photoUploadForm = actionUpload.querySelector('form'),
		//
		actionAdjust = plugElem.querySelector('div.action[data-name="photoAdjust"]'),
		photoAdjustForm = actionAdjust.querySelector('form'),
		photoCanvas = actionAdjust.querySelector('canvas');

	//
	function init() {
		// Plugin
		photoCanvas.se_plugin('cropper', {
			onNewSel:(data) => {
				photoAdjustForm.se_formElVal('img_x', data.x);
				photoAdjustForm.se_formElVal('img_y', data.y);
				photoAdjustForm.se_formElVal('img_w', data.w);
				photoAdjustForm.se_formElVal('img_h', data.h);
			},
			ratio:'1:1',
			min:'160x160'
		});
		//
		photoUploadForm.se_plugin('simpleForm', {
			save_url: mainUrl + '/upload',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});
		photoAdjustForm.se_plugin('simpleForm', {
			save_url: mainUrl + '/crop',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});

		//
		plugElem.se_on('click', 'button[se-act]', bntAction);
	}

	//
	function bntAction(e, cBtn) {
		e.preventDefault();
		//
		switch ( cBtn.getAttribute('se-act') ) {
			//
			case 'actionSwitch':
				actionSwitch(cBtn.dataset['action']);
				break;
			//
			default:
				console.error("op not valid.");
				break;
		}
	}


	//
	function updateImages(imgName) {
		let nUrl = '/res/image/objects',
			imageEnd = imgName + '?' + uniqId();

		//
		objPhoto.src = nUrl + '/w_160/' + imageEnd;
		$('#se_user img').src = nUrl + '/w_35/' + imageEnd;
	}


	//
	function actionSwitch(actName) {
		//
		plugElem.querySelectorAll('div.action').dataset['active'] = 0;
		plugElem.querySelectorAll('div.action[data-name="' + actName + '"]').dataset['active'] = 1;

		if ( actName === 'photoAdjust' ) {
			photoCanvas.cropper.canvasReDraw();
		}
	}

	//
	init();
	//
	return {};
};
//
