<?php
//
namespace snkeng\admin_modules\site;

//
use snkeng\admin_modules\site\imgResizer;

class image {
	//
	public static function process($originalFile, $newFileName, $adjustable) {
		$newFileName = $_SERVER['DOCUMENT_ROOT'] . $newFileName;

		// Copy file
		if ( !@copy($originalFile, $newFileName) ) {
			\snkeng\core\engine\nav::killWithError('It was not posible to copy image. Origin: ', $originalFile. '. Target: '. $newFileName, 500);
		}

		// Default data
		$data = [
			'fSize' => round(filesize($originalFile) / 1024, 2),
			'w' => 100,
			'h' => 100
		];

		if ( $adjustable ) {
			$cDim = getimagesize($originalFile);
			$data['w'] = $cDim[0];
			$data['h'] = $cDim[1];
		}

		return $data;
	}

	//
	public static function remove(&$response, $subFolder, $fileName) {
		$fileFolder = "/se_files/user_upload/img/{$subFolder}/";
		$fileFolderProc = "/se_files/user_processed/img/{$subFolder}/";

		// Remove filetype if exists
		if ( strpos($fileName, '.') !== false ) {
			$fileName = substr($fileName, 0 , (strrpos($fileName, ".")));
		}

		// Fail safe to avoid all file deletion
		if ( empty($fileName) ) {
			return;
		}

		//
		$response['debug']['files_del'] = [];
		// Delete previous files
		foreach ( glob($_SERVER['DOCUMENT_ROOT'].$fileFolder.$fileName."*") as $file ) {
			$response['debug']['files_del'][] = $file;
			unlink($file);
		}
		//
		foreach ( glob($_SERVER['DOCUMENT_ROOT'] .$fileFolderProc.$fileName."*") as $file ) {
			$response['debug']['files_del'][] = $file;
			unlink($file);
		}
	}

	//
	private static function getFileName ($id, $title, $fileType, $folder) {
		//
		$img_id_pad = str_pad($id, 6, '0', STR_PAD_LEFT);
		$imgUrlName = \snkeng\core\general\saveData::buildFriendlyTitle($title);

		//
		$fileName = "{$img_id_pad}_{$imgUrlName}";
		$folder = "/se_files/user_upload/img/{$folder}/";

		//
		return [
			'name' => $fileName,
			'urlName' => $imgUrlName,
			'loc' => $folder.$fileName.'.'.$fileType,
			'struct' => $fileName
		];
	}

	//
	public static function add(&$response, $rData, $folder, $modeType, $modeId = null)
	{
		global $mysql, $siteVars;

		// Id siguiente archivo
		$img_id = \snkeng\core\engine\mysql::getNextId("sc_site_images");

		//
		$fileLocs = self::getFileName($img_id, $rData['imgName'], $rData['imgFile']['fType'], $folder);

		//
		$resData = self::process($rData['imgFile']['tmp_name'], $fileLocs['loc'], $rData['imgPropEditable']);
		// debugVariable($resData);

		$newCode = se_randString(8);
		$fStuct = base64url_encode($img_id . ":" .$newCode);

		//
		$modeId = intval($modeId);
		$modeId = ( empty($modeId) ) ? 'NULL' : "'{$modeId}'";

		//
		$ins_qry = <<<SQL
INSERT INTO sc_site_images (
	img_id, sit_id, obj_id,
	img_mode_type, img_mode_id,
	img_name, img_name_url, img_desc, img_code,
	img_fname, img_ftype,
	img_floc, img_fstruct,
	img_fsize, img_width, img_height,
	img_prop_adjustable
) VALUES (
	{$img_id}, {$rData['imgType']}, {$siteVars['user']['data']['id']},
	'{$modeType}', {$modeId},
	'{$rData['imgName']}', '{$fileLocs['urlName']}', '{$rData['imgDesc']}', '{$newCode}',
	'{$fileLocs['name']}', '{$rData['imgFile']['fType']}',
	'{$fileLocs['loc']}', '{$fStuct}',
	'{$resData['fSize']}', '{$rData['imgPropWidth']}', '{$rData['imgPropHeight']}',
	'{$rData['imgPropEditable']}'
);
SQL;

		// debugVariable($ins_qry);
		//
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'siteImageAdd',
			'errorDesc' => 'No pudo ser guardada la imagen. (SQL)',
			'onError' => function () use ($fileLocs) {
				// Borrar los archivos creados
				foreach ( glob($_SERVER['DOCUMENT_ROOT'].$fileLocs['loc']."*") as $file ) {
					unlink($file);
				}
			}
		]);

		// Reporte
		$response['d'] = [
			'id' => $img_id,
			'fstruct' => $fileLocs['loc'],
			'name' => $rData['imgName'],
			'desc' => $rData['imgDesc'],
			'width' => $resData['w'],
			'height' => $resData['h']
		];
	}

	//
	public static function dataUpd(&$response, $imgId, $rData) {
				//
		$upd_qry = <<<SQL
UPDATE sc_site_images SET img_name='{$rData['imgName']}', img_desc='{$rData['imgDesc']}' WHERE img_id={$imgId};
SQL;

		// debugVariable($ins_qry);
		//
		\snkeng\core\engine\mysql::submitQuery(
			$upd_qry,
			[
				'errorKey' => 'siteImageDataUpd',
				'errorDesc' => 'No pudo ser guardada la imagen. (SQL)'
			]
		);
	}

	//
	public static function imageReplace(&$response, $imgId, $rData, $folder)
	{
		// Eliminar archivos anteriores
		$sql_qry = <<<SQL
SELECT
    img_name AS dName, img_desc AS dDesc,
    img_fname AS fName, img_ftype AS fType, img_prop_adjustable AS isAdjustable
FROM sc_site_images
WHERE img_id={$imgId}
LIMIT 1;
SQL;
		$origFileData = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			[
				'int' => ['isAdjustable'],
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);

		// Avoid invalid type
		if (
			( $origFileData['isAdjustable'] && !in_array($rData['imgFile']['fType'], ['webp', 'png', 'jpg', 'jpeg', 'avif']) ) ||
			( !$origFileData['isAdjustable'] && $origFileData['fType'] !== $rData['imgFile']['fType'] )
		) {
			\snkeng\core\engine\nav::killWithError('Archivo no corresponde al que está remplazando.');
		}

		// Operación
		self::remove($response, $folder, $origFileData['fName']);

		// Final Loc may change if file type changes
		$newFileLoc = "/se_files/user_upload/img/{$folder}/{$origFileData['fName']}.{$rData['imgFile']['fType']}";

		// Editar el archvio
		$resData = self::process($rData['imgFile']['tmp_name'], $newFileLoc, $origFileData['isAdjustable']);

		$response['debug']['imgData'] = $resData;

		// Guardar en base de datos
		$upd_qry = <<<SQL
UPDATE sc_site_images SET
	img_ftype='{$rData['imgFile']['fType']}', img_floc='{$newFileLoc}',
	img_fsize={$resData['fSize']}, img_width={$rData['imgPropWidth']}, img_height={$rData['imgPropHeight']}
WHERE img_id={$imgId};
SQL;
		//
		\snkeng\core\engine\mysql::submitQuery(
			$upd_qry,
			[
				'errorKey' => 'siteImageUpd',
				'errorDesc' => 'No pudo ser actualizada la imagen.',
				'onError' => function () use ($newFileLoc) {
					// Borrar los archivos creados
					unlink($newFileLoc);
				}
			]
		);

		// Report
		$response['d'] = [
			'id' => $imgId,
			'fstruct' => $newFileLoc,
			'name' => $origFileData['dName'],
			'desc' => $origFileData['dDesc'],
			'width' => $rData['imgPropWidth'],
			'height' => $rData['imgPropHeight']
		];
	}


	//
	public static function imageUpdate(&$response, $imgId, $rData, $folder)
	{
		global $mysql, $siteVars;

		//
		$fileLocs = self::getFileName($imgId, $rData['imgName'], $rData['imgFile']['fType'], $folder);


		// Eliminar archivos anteriores
		$sql_qry = <<<SQL
SELECT img_fname
FROM sc_site_images
WHERE img_id={$imgId}
LIMIT 1;
SQL;
		$oldFileName = \snkeng\core\engine\mysql::singleValue($sql_qry, [
			'errorKey' => '',
			'errorDesc' => '',
		]);

		// Operación
		self::remove($response, $folder, $oldFileName);

		// Editar el archvio
		$resData = self::process($rData['imgFile']['tmp_name'], $fileLocs['loc']);
		$response['debug']['imgData'] = $resData;

		// Guardar en base de datos
		$upd_qry = <<<SQL
UPDATE sc_site_images SET
	obj_id={$siteVars['user']['data']['id']}, img_dt_mod=NOW(),
	img_name='{$rData['imgName']}', img_desc='{$rData['imgDesc']}',
	img_fname='{$fileLocs['name']}', img_ftype='{$rData['imgFile']['fType']}',
    img_floc='{$fileLocs['loc']}', img_fstruct='{$fileLocs['struct']}',
	img_fsize={$resData['fSize']}, img_width={$resData['w']}, img_height={$resData['h']}
WHERE img_id={$imgId};
SQL;
		//
		\snkeng\core\engine\mysql::submitQuery($upd_qry, [
			'errorKey' => 'siteImageUpd',
			'errorDesc' => 'No pudo ser actualizada la imagen.',
			'onError' => function () use ($fileLocs) {
				// Borrar los archivos creados
				foreach ( glob($_SERVER['DOCUMENT_ROOT'].$fileLocs['loc']."*") as $file ) {
					unlink($file);
				}
			}
		]);

		// Report
		$response['d'] = [
			'id' => $imgId,
			'fstruct' => $fileLocs['loc'],
			'name' => $rData['imgName'],
			'desc' => $rData['imgDesc'],
			'width' => $resData['w'],
			'height' => $resData['h']
		];
	}

	//
	public static function copy(&$response, $imgId)
	{
		global $mysql, $siteVars;

		// Extraer datos
		$sql_qry = <<<SQL
SELECT
    sit_id, obj_id,
	img_mode_type, img_mode_id,
    img_name, img_name_url, img_desc, img_code,
	img_ftype, img_floc, img_fsize, img_width, img_height, img_fstruct,
	img_prop_adjustable
FROM sc_site_images
WHERE img_id={$imgId};
SQL;
		$imgData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorKey' => 'siteImageCopyRead',
			'errorDesc' => 'No pudo ser leida de la imagen original.',
		]);

		// Id siguiente archivo
		$img_id = \snkeng\core\engine\mysql::getNextId("sc_site_images");

		//
		$fileLocs = self::getFileName($img_id, $imgData['img_name'], $imgData['img_ftype'], 'site');

		// Copy all related files
		$cFiles = glob($_SERVER['DOCUMENT_ROOT'].$imgData['img_fstruct']."*");
		$cLength = strlen($_SERVER['DOCUMENT_ROOT'].$imgData['img_fstruct']);
		$baseFile_2 = $_SERVER['DOCUMENT_ROOT'].$fileLocs['struct'];
		foreach ( $cFiles as $key => $file ) {
			$suffix = substr($file, $cLength);
			copy($file, $fileLocs['loc'].$suffix);
		}

		//
		$newCode = se_randString(8);
		$fStuct = base64url_encode($img_id . ":" .$newCode);


		// Crear - SQL
		$ins_qry = <<<SQL
INSERT INTO sc_site_images (
	img_id, sit_id, obj_id,
	img_mode_type, img_mode_id,
	img_name, img_name_url, img_desc, img_code,
	img_fname, img_ftype,
	img_floc, img_fstruct,
	img_fsize, img_width, img_height,
	img_prop_adjustable
) VALUES (
	{$img_id}, {$imgData['sit_id']}, {$siteVars['user']['data']['id']},
	'{$imgData['img_mode_type']}', '{$imgData['img_mode_id']}',
    '{$imgData['img_name']}', '{$imgData['img_name_url']}', '{$imgData['img_desc']}', '{$newCode}',
	'{$fileLocs['name']}', '{$fileLocs['img_ftype']}',
	'{$fileLocs['loc']}', '{$fStuct}',
	{$imgData['img_fsize']}, {$imgData['img_width']}, {$imgData['img_height']},
    '{$fileLocs['img_prop_adjustable']}',
);
SQL;
		//
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'siteImageAdd',
			'errorDesc' => 'No pudo ser copiada la imagen.',
			'onError' => function () use ($fileLocs) {
				// Borrar los archivos creados
				foreach ( glob($_SERVER['DOCUMENT_ROOT'].$fileLocs['loc']."*") as $file ) {
					unlink($file);
				}
			}
		]);

		// Reporte
		$response['d'] = [
			'id' => $img_id,
			'fstruct' => $fileLocs['loc'],
			'name' => $imgData['img_name'],
			'desc' => $imgData['img_desc'],
			'width' => $imgData['img_width'],
			'height' => $imgData['img_height']
		];
	}

	//
	public static function resize(&$response, $imgId, $imgType, $imgSizeX, $imgSizeY, $imgBkgCol = '')
	{
				// Datos de la imagen
		$sql_qry = <<<SQL
SELECT
	img_id, sit_id, obj_id, img_dt_add, img_name, img_desc,
	img_fname, img_ftype, img_floc, img_fsize, img_width, img_height
FROM sc_site_images
WHERE img_id={$imgId}
LIMIT 1;
SQL;
		//
		$imgData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorKey' => 'siteImageResizeRead',
			'errorDesc' => 'No pudo ser leida de la imagen original.',
		]);

		$fileLocFull = $_SERVER['DOCUMENT_ROOT'].$imgData['img_floc'];

		// Procesar imagen
		// Imagen
		$image = new imgResizer();
		$image->load($fileLocFull);
		switch ( $imgType ) {
			// Tamaño original
			case 0:
				break;
			// Forzar tamaño
			case 1:
				$image->resize($imgSizeX, $imgSizeY);
				break;
			// Ajuste inteligente a medidas máximas
			case 2:
				$image->maxResize($imgSizeX, $imgSizeY);
				break;
			// Ajuste con recuadro (imagen completa, borde de color)
			case 3:
				$image->smartThumb($imgSizeX, $imgSizeY, $imgBkgCol);
				break;
			// Ajuste con recuadro (imagen recortada)
			case 4:
				$image->resizeCrop($imgSizeX, $imgSizeY, $imgBkgCol);
				break;
		}
		// Guardar
		$image->save($fileLocFull);

		// SQL
		$ins_qry = <<<SQL
UPDATE sc_site_images SET
img_fsize='{$image->imageFileSize}', img_width='{$image->imageSize[0]}', img_height='{$image->imageSize[1]}'
WHERE img_id={$imgId}
LIMIT 1;
SQL;

		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'siteImageResize',
			'errorDesc' => 'No pudo ser actualizada la información de la imagen.'
		]);

		$response['d'] = [
			'width' => $image->imageSize[0],
			'height' => $image->imageSize[1]
		];
	}

	//
	public static function cut(&$response, $imgId, $imgOriginX, $imgSizeX, $imgOriginY, $imgSizeY, $imgEndSizeX, $imgEndSizeY)
	{
				// Datos de la imagen
		$sql_qry = "SELECT
						img_id, sit_id, obj_id, img_dt_add, img_name, img_desc,
						img_fname, img_ftype, img_floc, img_fsize, img_width, img_height
					FROM sc_site_images
					WHERE img_id={$imgId}
					LIMIT 1;";
		//
		$imgData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorKey' => 'siteImageCutRead',
			'errorDesc' => 'No pudo ser leida de la imagen original.',
		]);

		//
		$fileLocFull = $_SERVER['DOCUMENT_ROOT'].$imgData['img_floc'];


		// Imagen
		$image = new imgResizer();
		$image->load($fileLocFull);
		$image->crop($imgSizeX, $imgSizeY, $imgOriginX, $imgOriginY, $imgEndSizeX, $imgEndSizeY);
		$image->save($fileLocFull);

		// Guardar nuevo tamaño
		// SQL
		$ins_qry = "UPDATE sc_site_images SET
					img_fsize='{$image->imageFileSize}', img_width='{$image->imageSize[0]}', img_height='{$image->imageSize[1]}'
					WHERE img_id={$imgId}
					LIMIT 1;";
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'siteImageCut',
			'errorDesc' => 'No pudo ser actualizada la información de la imagen.'
		]);

		// Reporte
		$response['d'] = ['width' => $image->imageSize[0], 'height' => $image->imageSize[1]];
	}

	//
	public static function delete(&$response, $imgId)
	{
		
		// File locations
		$subFolder = 'site'; // For future upgrade, allow renaming

		// Datos de la imagen
		$sql_qry = <<<SQL
SELECT img_fname
FROM sc_site_images
WHERE img_id={$imgId}
LIMIT 1;
SQL;
		$oldFileName = \snkeng\core\engine\mysql::singleValue($sql_qry, [
			'errorKey' => '',
			'errorDesc' => 'No fue posible leer las propiedades de la imagen.'
		]);

		// Operación
		self::remove($response, $subFolder, $oldFileName);

		// Procesar db
		$del_qry = "DELETE FROM sc_site_images WHERE img_id='{$imgId}' LIMIT 1;";
		\snkeng\core\engine\mysql::submitQuery($del_qry, [
			'errorKey' => 'siteImageDelete',
			'errorDesc' => 'No pudo ser eliminada la imagen.'
		]);
	}

}
//
