<?php
//
namespace snkeng\admin_modules\site;

class markdown_page {
	//
	public static function md_text_get($appName, $appMode, $appId, $lang) : array {
		$qry_sel = <<<SQL
SELECT
	txt.txt_id AS txtId, txt.txt_id_parent AS parentId,
	txt.txt_dt_add AS dtAdd, txt.txt_dt_mod AS dtMod,
	txt.txt_app_id AS appId,
	txt.txt_lang AS txtLang, txt.txt_type AS txtType, txt.txt_content AS content
FROM sc_site_texts AS txt
WHERE txt.txt_app_name='{$appName}' AND txt.txt_app_mode='{$appMode}' AND txt.txt_app_id='{$appId}' AND txt.txt_lang='{$lang}' AND txt.txt_type='md'
LIMIT 1;
SQL;
		$textData = \snkeng\core\engine\mysql::singleRowAssoc(
			$qry_sel,
			[
				'int' => ['txtId', 'parentId', 'appId'],
				'stripCSlashes' => ['content']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);

		//
		if ( empty($textData) ) {

			//
			$qry_ins = <<<SQL
INSERT INTO sc_site_texts
(txt_app_name, txt_app_mode, txt_app_id, txt_lang, txt_type)
VALUES
('{$appName}', '{$appMode}', '{$appId}', '{$lang}', 'md');
SQL;
			\snkeng\core\engine\mysql::submitQuery(
				$qry_ins,
				[
					'errorKey' => '',
					'errorDesc' => '',
				]
			);

			//
			$textData = [
				'id' => \snkeng\core\engine\mysql::getLastId(),
				'appId' => $appId,
				'txtLang' => $lang,
				'content' => '',
			];
		}

		//
		return $textData;
	}

}
//
