<?php
//
namespace snkeng\admin_modules\site;

//
class image_window {
	//
	public static function printNormal(string $ajaxUrl, array $defaults = []) {
		global $page;
		$data = self::createWindow($ajaxUrl, $defaults);
		$page['title'] = $data['title'];
		$page['body'] = $data['body'];
	}

	//
	public static function printFloatWindow(string $ajaxUrl, array $defaults = []) {
		global $page;

		// ADD PAGE
		// require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/class/se_page.php';
		$page = \snkeng\core\engine\nav::pageGetStruct();

		//
		$data = self::createWindow($ajaxUrl, $defaults);

		$page['title'] = $data['title'];
		$page['body'] = $data['body'];

		// File conversion
		\snkeng\core\engine\nav::$async = true;
		\snkeng\core\engine\nav::pagePrint();
	}

	public static function createWindow(string $ajaxUrl, array $defaults = []) {
		//
		\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
		\snkeng\core\engine\nav::cacheFinalCheck();

		//
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-image-picker.mjs');
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-image-picker.css');

		// Cargar archivos
		// \snkeng\core\engine\nav::pageFileGroupAdd(['site_adm_image']);

		// Operation defaults:
		$opDefaults = ( !empty($defaults) ) ? \json_encode($defaults) : '{}';

		// Leer categorías actuales
		$curImgTypeOpt = '<option value="0">No modificada</option>';

		// CallBack
		$cText['title'] = 'Imagenes';

		//
		$cText['body'] = <<<HTML
<div class="pageSubTitle">Imágenes</div>

<site-adm-pages-image-picker data-url="{$ajaxUrl}">

<script se-elem="defaults" type="application/json">
{
	'postDefaults':{$opDefaults}
}
</script>

<!-- INI:photoUploader -->
<div class="window photoNavFullWindow" data-element="uploader" aria-hidden="true">
	<div class="imgCont"><img se-elem="container" /></div>
	<div class="imgEdit">
		<div class="bar">
			<div class="title">Agregar imagen</div>
			<div class="close"><button data-image-window-onclick="photoUploader_window_close"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg></button></div>
		</div>
		<div>
			<div class="section">
				<div class="subElTitle">Propiedades</div>
				<form se-elem="imageForm" method="post" class="se_form">
					<input type="hidden" name="id" value="0" />
					<label class="separator required">
						<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
						<input type="text" name="title" pattern="[A-z\u00C0-\u00ff0-9\- ]+" minlength="4"  maxlength="75" required />
					</label>
					<label class="separator required">
						<div class="cont"><span class="title">Contenido</span><span class="desc"></span></div>
						<textarea name="content"></textarea>
					</label>
					<button type="submit" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-save" /></svg> Guardar</button>
					<output se-elem="response"></output>
				</form>
				<table class="st_dualtab">
					<tr><td>W (px)</td><td se-elem="width"></td></tr>
					<tr><td>H (px)</td><td se-elem="height"></td></tr>
					<tr><td>S (Kb)</td><td se-elem="fileSize"></td></tr>
					<tr><td>T</td><td se-elem="fileType"></td></tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- END:photoUploader -->

<!-- INI:photoEditor -->
<div class="window photoNavFullWindow photoEditor" data-element="editor" aria-hidden="true" data-adjustable="0" data-imgId="0" data-status-editing="0">
	<div class="imgCont">
		<img se-elem="container" />
		<canvas se-elem="canvas" aria-hidden="true"></canvas>
	</div>
	<div class="imgEdit">
		<div class="bar">
			<div class="title">Editar Imagen</div>
			<div class="close"><button data-image-window-onclick="photoEditor_window_close"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg></button></div>
		</div>
		<div class="contents" se-elem="options">
			<div se-elem="options_main" aria-hidden="false">
				<div class="section">
					<div class="subElTitle">Propiedades</div>
					<form se-elem="imageDataForm" class="se_form" method="post">
						<input type="hidden" name="imgId" value="0" />
						<label class="separator required">
							<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
							<input type="text" name="imgName" pattern="[A-z\u00C0-\u00ff0-9\-\. ]+" minlength="4" maxlength="75" required />
						</label>
						<label class="separator">
							<div class="cont"><span class="title">Contenido</span><span class="desc"></span></div>
							<textarea name="imgDesc"></textarea>
						</label>
						<button type="submit" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-save" /></svg> Guardar</button>
						<output se-elem="response"></output>
					</form>
				</div>
				<div class="section" se-elem="imageReplace" aria-hidden="false">
					<div class="subElTitle">Reemplazar imagen</div>
					<button class="btn blue wide" data-image-window-onclick="photoEditor_image_replace_on"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg> Reemplazar</button>
				</div>
				<div class="section" se-elem="imageEditableCall" aria-hidden="false">
					<div class="subElTitle">Editar imagen</div>
					<button class="btn blue wide" data-image-window-onclick="photoEditor_image_edit_on"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg> Editar</button>
				</div>
			</div>
			<!-- DIV -->
			<div se-elem="options_replace" aria-hidden="true">
				<div class="section" se-elem="imageReplace">
					<div class="subElTitle">Reemplazar imagen</div>
					<div class="se_separated_elements_row">
						<input se-elem="image_input_replace" type="file" accept="image/*" />
						<div class="photo_cont">
							<svg class="icon inline"><use xlink:href="#fa-paste" /></svg>
							<div se-elem="image_paste_replace" class="photo_paste" contenteditable="true"></div>
						</div>
						<button class="btn blue wide" data-image-window-onclick="photoEditor_image_replace_exec"><svg class="icon inline"><use xlink:href="#fa-upload" /></svg> Subir</button>
						<button class="btn red wide" data-image-window-onclick="photoEditor_image_replace_off"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg> Cancelar</button>
						<output data-elem="photoEditor_image_replace"></output>
					</div>
				</div>
			</div>
			<!-- DIV -->
			<div se-elem="options_edit" aria-hidden="true">
				<h3>Recortar</h3>
				<form se-elem="photoEditor_imageCropForm">
					<fieldset>
					<legend>Por proporción</legend>
					<table class="photo_table">
						<tr><td>X:</td><td><input type="text" name="x" /></td><td>Y:</td><td><input type="text" name="y" /></td></tr>
						<tr><td>W:</td><td><input type="text" name="w" /></td><td>H:</td><td><input type="text" name="h" /></td></tr>
					</table>
					<h4>Límites</h4>
					<div class="photoCropLims">
						<button class="btn" aria-selected="false" data-image-window-onclick="photoEditor_image_cropLimits" data-min="40x40" data-ratio="">Libre</button>
						<button class="btn" aria-selected="true" data-image-window-onclick="photoEditor_image_cropLimits" data-min="40x40" data-ratio="1:1">1:1</button>
						<button class="btn" aria-selected="false" data-image-window-onclick="photoEditor_image_cropLimits" data-min="40x30" data-ratio="4:3">4:3</button>
						<button class="btn" aria-selected="false" data-image-window-onclick="photoEditor_image_cropLimits" data-min="80x45" data-ratio="16:9">16:9</button>
						<button class="btn" aria-selected="false" data-image-window-onclick="photoEditor_image_cropLimits" data-min="105x45" data-ratio="21:9">21:9</button>
					</div>
					<button type="button" data-image-window-onclick="photoEditor_image_crop" id="photo_crop" class="btn blue wide">Cortar</button>
					</fieldset>
				</form>
				<form se-elem="photoEditor_imageCropTargetForm">
					<fieldset>
						<legend>Definir tamaño</legend>
						<table class="photo_table">
							<tr><td>X:</td><td><input type="text" name="w" /></td><td>Y:</td><td><input type="text" name="h" /></td>
							<td><button class="btn blue" data-image-window-onclick="photoEditor_image_target_crop">Definir</button></td>
							<td><button class="btn blue" data-image-window-onclick="photoEditor_image_target_crop_cut">Cortar</button></td>
							</tr>
						</table>
					</fieldset>
				</form>
				<hr>
				<h3>Tamaño</h3>
				<form se-elem="photoEditor_imageResizeForm">
					<table id="photoResizeData" class="photo_table">
					<tr><td>Actual</td><td>W:</td><td><input type="text" name="wo" readonly/></td><td>H:</td><td><input type="text" name="ho" readonly /></td></tr>
					<tr><td colspan="5"><label><input type="checkbox" name="ar" checked/> <svg class="icon inline"><use xlink:href="#fa-lock" /></svg></label></td></tr>
					<tr><td>Nueva</td><td>W:</td><td><input type="text" name="wn" onchange="photoEditor.editor.resizeMod(this);" /></td><td>H:</td><td><input type="text" name="hn" onchange="photoEditor.editor.resizeMod(this);" /></td></tr>
					<tr><td>Tipo:</td><td colspan="4"><select name="rszType">
						<option value="1">Forzado</option>
						<option value="2">Máximo permitido</option>
						<option value="3">Recuadro fijo (margen blanco)</option>
						<option value="4">Recuadro fijo (recortada)</option>
						</select></td></tr>
					</table>
					<button type="button" data-image-window-onclick="photoEditor_image_resize" id="photo_resize" class="btn blue wide">Ajustar</button>
				</form>
				<hr>
				<h3>Rotar</h3>
				<div>
					<button class="btn" data-image-window-onclick="photoEditor_image_rotate" data-direction="-90"><svg class="icon inline"><use xlink:href="#fa-rotate-left" /></svg></button>
					<button class="btn" data-image-window-onclick="photoEditor_image_rotate" data-direction="90"><svg class="icon inline"><use xlink:href="#fa-rotate-right" /></svg></button>
					<button class="btn" data-image-window-onclick="photoEditor_image_flip" data-direction="vertical"><svg class="icon inline"><use xlink:href="#fa-arrows-v" /></svg></button>
					<button class="btn" data-image-window-onclick="photoEditor_image_flip" data-direction="horizontal"><svg class="icon inline"><use xlink:href="#fa-arrows-h" /></svg></button>
				</div>
				<form se-elem="imageReplaceForm" class="se_form">
					<input type="hidden" name="id" value="0" />
					<label class="separator required">
						<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
						<input type="text" name="title" pattern="[A-z\u00C0-\u00ff0-9\-\. ]+" minlength="4" maxlength="75" required />
					</label>
					<label class="separator required">
						<div class="cont"><span class="title">Contenido</span><span class="desc"></span></div>
						<textarea name="content"></textarea>
					</label>
					<label><input type="checkbox" name="autoadjust" value="1" checked />Autoajustar (limitar a 1920x1920)</label>
					<button type="submit" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-save" /></svg> Guardar</button>
					<output se-elem="response"></output>
				</form>
				<button class="btn blue wide" data-image-window-onclick="photoEditor_image_edit_exec"><svg class="icon inline"><use xlink:href="#fa-save" /></svg> Guardar</button>
				<button class="btn red red" data-image-window-onclick="photoEditor_image_edit_off"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg> Cancelar</button>
			</div>
		</div>
	</div>
</div>
<!-- END:photoEditor -->

<!-- INI:photoPicker -->
<div class="window photoPicker" data-element="picker" aria-hidden="false">
	<div class="photo_new">
		<div>Agregar</div>
		<div>
			Por archivo:
			<input type="file" accept="image/*" se-elem="image_input_add" />
		</div>
		<div>
			Pegar imagen:
			<div class="photo_cont">
				<svg class="icon inline"><use xlink:href="#fa-paste" /></svg>
				<div class="photo_paste" se-elem="image_paste_add" contenteditable="true"></div>
			</div>
		</div>
	</div>
	<div class="photo_current">
		<div class="photo_current_nav">
			<div class="photo_gal">
				<div data-type="galleryFilter" class="grid">
					<div class="modInput"><svg class="icon inline"><use xlink:href="#fa-search" /></svg><input type="search" name="text" /></div>
					<select name="type"><option selected value="0">Todas</option>{$curImgTypeOpt}</select>
					<button class="btn small" data-image-window-onclick="photoPicker_image_reload"><svg class="icon inline"><use xlink:href="#fa-refresh" /></svg></button>
				</div>
				
				<se-async-content class="photoElements" url="{$ajaxUrl}/readAll" istable="true">
					<template>
						<div class="galObj" data-imgId="!id;" data-imgWidth="!width;" data-fname="!fname;" data-imgHeight="!height;" data-imgFloc="!floc;" data-isadjustable="!propAdjustable;">
							<img src="!imgFileLoc;" title="!name;" />
							<span class="title" title="!content;">!name;</span>
							<span class="props">!width; x !height; px</span>
						</div>
					</template>
					<div data-type="container"></div>
				</se-async-content>
			</div>
			<div class="photo_nav">
				<div class="photo_sel" se-elem="photoPicker_imageSelected">
					<h3>Seleccionada</h3>
					<div id="win_imgSearch_photoSelOp"></div>
					<div class="content">
						<img src="" />
						<div class="title"></div>
						<div class="props"></div>
						<div class="desc"></div>
						<div class="id">ID: <span></span></div>
						<button class="btn wide gOMT" data-image-window-onclick="photoPicker_image_finalSelect" data-name="select" style="display:none;"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg> Seleccionar</button>
						<button class="btn wide gOMT" data-image-window-onclick="photoPicker_image_edit" ><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg> Editar</button>
						<button class="btn wide gOMT" data-image-window-onclick="photoPicker_image_copy" ><svg class="icon inline"><use xlink:href="#fa-copy" /></svg> Copiar</button>
						<button class="btn wide gOMT" data-image-window-onclick="photoPicker_image_delete"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg> Borrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END:photoPicker -->

</site-adm-pages-image-picker>
HTML;
		//

		//
		return $cText;
	}
}
//
