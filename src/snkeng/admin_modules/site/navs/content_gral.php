<?php
//
$nav = [
	'post' => [
		'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtadd, art.art_dt_mod AS dtmod,
				(CASE
				WHEN now() > art.art_dt_pub && art.art_status_published=1 THEN 0
				WHEN now() < art.art_dt_pub && art.art_status_published=1 THEN 1
				ELSE 2
				END) AS published,
				art.art_count_views AS views,
				art.art_url AS urlFull,
				cat.cat_id AS catId, cat.cat_title AS catTitle,
				user.a_id AS aId, user.a_uname AS author",
		'from' => 'sc_site_articles AS art
					LEFT OUTER JOIN sc_site_category AS cat ON art.cat_id=cat.cat_id
					LEFT OUTER JOIN sb_objects_obj AS user ON art.a_id=user.a_id',
		'lim' => 20,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
			'user' => ['name' => 'Usuario', 'db' => 'user.a_uname', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'art.art_id', 'set' => 'DESC']
		]
	]
];
return $nav;
