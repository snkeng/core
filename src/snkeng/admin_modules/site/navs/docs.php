<?php
//
$nav = [
	'documentation' => [
		'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				DATE(art.art_dtpub) AS dtPub, art.art_dt_add AS dtadd, art.art_dt_mod AS dtmod,
				art.art_count_views AS views,
				art.art_url AS urlFull,
				cat.cat_id AS catId, cat.cat_title AS catTitle,
				obj.a_id AS userId, obj.a_obj_name AS userName",
		'from' => 'sc_site_articles AS art
					LEFT OUTER JOIN sc_site_category AS cat ON art.cat_id=cat.cat_id
					LEFT OUTER JOIN sb_objects_obj AS obj ON art.art_id=obj.obj_id',
		'lim' => 20,
		'where' => [
			'artType' => ['name' => 'Type', 'db' => 'art.art_type_primary', 'vtype' => 'str', 'stype' => 'eq', 'set'=>'docs'],
			'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
			'user' => ['name' => 'Usuario', 'db' => 'user.a_uname', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			's_id' => ['Name' => 'ID', 'db' => 'art.art_id', 'set' => 'DESC']
		]
	],
	'relationships' => [
		'sel' => "dacc.dacc_id AS id,
				dacc.dacc_dt_add AS dtAdd, dacc.dacc_dt_mod AS dtMod,
				dacc.dacc_status AS dStatus,
				docs.docs_title_normal AS docTitle,
				obj.a_id AS userId, obj.a_obj_name AS userName",
		'from' => 'sc_site_documents_access AS dacc
					INNER JOIN sc_site_documents AS docs ON dacc.doc_id=docs.docs_id
					INNER JOIN sb_objects_obj AS obj ON obj.a_id=dacc.user_id',
		'lim' => 20,
		'where' => [
			'daccId' => ['name' => 'DACC ID', 'db' => 'dacc.dacc_id', 'vtype' => 'int', 'stype' => 'eq'],
			'userId' => ['name' => 'User ID', 'db' => 'obj.a_id', 'vtype' => 'int', 'stype' => 'eq'],
			'docId' => ['name' => 'Docs ID', 'db' => 'dacc.doc_id', 'vtype' => 'int', 'stype' => 'eq'],
			'docTitle' => ['name' => 'Título', 'db' => 'docs.docts_title_normal', 'vtype' => 'str', 'stype' => 'like'],
			'userName' => ['name' => 'Usuario', 'db' => 'obj.a_obj_name', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			'daccIdOrd' => ['Name' => 'ID', 'db' => 'dacc.dacc_id', 'set' => 'DESC']
		]
	],
	'docsMaster' => [
		'sel' => "docs.docs_id AS id, docs.img_id AS imgId,
					docs.docs_dt_add AS dtAdd, docs.docs_dt_mod AS dtMod,
					docs.docs_title_normal AS title, docs.docs_title_url AS urlTitle, docs.docs_description,
					docs.docs_img_struct, docs.docs_access_level, docs.docs_access_link_req,
					cat.cat_id AS catId, cat.cat_title AS catTitle",
		'from' =>
			'sc_site_documents AS docs
			LEFT OUTER JOIN sc_site_category AS cat ON docs.cat_id=cat.cat_id',
		'lim' => 20,
		'where' => [
			'title' => ['name' => 'Título', 'db' => 'docs.docs_title_normal', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			's_title' => ['Name' => 'Título', 'db' => 'docs.docs_title_normal', 'set' => 'DESC']
		]
	]
];

//
return $nav;
