<?php
//
$nav = [
	'category' => [
		'sel' => "cat.cat_id AS id, cat.cat_title AS title, cat.cat_urltitle AS urltitle, cat.cat_mode_type AS cType",
		'from' => 'sc_site_category AS cat',
		'lim' => 20,
		'where' => [
			'catType' => ['name' => 'ID', 'db' => 'cat.cat_mode_type', 'vtype' => 'str', 'stype' => 'eq', 'set'=>'blog'],
			'id' => ['name' => 'ID', 'db' => 'cat.cat_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'cat.cat_title', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'Título', 'db' => 'cat.cat_title', 'set' => 'DESC']
		]
	],
];
return $nav;
