<?php
//
$nav = [
	'images' => [
		'sel' => "img.img_id AS id,
				img.img_mode_type AS modeType, img.img_mode_id AS modeId,
				img.img_dt_add AS dtAdd, img.img_dt_mod AS dtMod, 
				img.img_name AS name, img.img_desc AS content,
				img.img_floc AS floc, img.img_fname AS fname,
				img.img_width AS width, img.img_height as height,
				IF(img.img_prop_adjustable, CONCAT('/res/image/site/w_160/', img.img_fname, '.webp'), img.img_floc) AS imgFileLoc,
				img.img_prop_adjustable AS propAdjustable",
		'from' => 'sc_site_images AS img',
		'lim' => 25,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'img.img_id', 'vtype' => 'int', 'stype' => 'eq'],
			'text' => ['name' => 'Nombre', 'db' => 'img.img_name, img.img_desc', 'vtype' => 'str', 'stype' => 'match'],
			'type' => ['name' => 'Tipo', 'db' => 'img.img_ftype', 'vtype' => 'int', 'stype' => 'eq'],
			'modeType' => ['name' => 'Tipo', 'db' => 'img.img_mode_type', 'vtype' => 'str', 'stype' => 'eq'],
			'modeId' => ['name' => 'Tipo', 'db' => 'img.img_mode_id', 'vtype' => 'int', 'stype' => 'eq']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'img.img_id', 'set' => 'DESC']
		]
	],
];

//
return $nav;
