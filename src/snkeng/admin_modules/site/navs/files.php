<?php
//
$nav = [
	'files' => [
		'sel' => "file.file_id AS id, file.file_title AS title,
					file.file_mode_type AS modeType, file.file_mode_id AS modeId,
					file.file_dt_add AS dtAdd, file.file_dt_mod AS dtMod,
					file.file_fname AS fileName, file.file_ftype AS fileType, file.file_floc AS fileLoc,
					FORMAT(file.file_fsize/1024, 2) AS fileSize",
		'from' => 'sc_site_files AS file',
		'lim' => 20,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'file.file_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'file.file_title', 'vtype' => 'str', 'stype' => 'like'],
			'fileName' => ['name' => 'Archivo', 'db' => 'file.file_fname', 'vtype' => 'str', 'stype' => 'like'],
			'modeType' => ['name' => 'Modo', 'db' => 'file.file_mode_type', 'vtype' => 'str', 'stype' => 'eq'],
			'modeId' => ['name' => 'Modo', 'db' => 'file.file_mode_id', 'vtype' => 'int', 'stype' => 'eq']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'file.file_id', 'set' => 'DESC']
		]
	]
];

//
return $nav;
