<?php
//
$nav = [
	'gallery' => [
		'sel' => "gal.gal_id AS id, gal.gal_dt_add AS dtAdd, gal.gal_dt_mod AS dtMod, gal.gal_title AS title",
		'from' => 'sc_site_gallery AS gal',
		'lim' => 20,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'gal.gal_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'gal.gal_title', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'gal.gal_id', 'set' => 'DESC']
		]
	]
];
//
return $nav;
