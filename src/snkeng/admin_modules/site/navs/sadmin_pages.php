<?php
//
$nav = [
	'pages' => [
		'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
				DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
				art.art_status_published AS isPub,
				art.art_status_indexable AS isIndexed,
				art.art_count_views AS views,
				art.art_url AS fullUrl",
		'from' => 'sc_site_articles AS art',
		'lim' => 20,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
			'artType' => ['name' => 'Tipo', 'db' => 'art.art_type_primary', 'vtype' => 'str', 'stype' => 'eq'],
		],
		'order' => [
			['Name' => 'ID', 'db' => 'art.art_id', 'set' => 'ASC']
		]
	],
];
//
return $nav;
