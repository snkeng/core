<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!id;</span>
	<span><input type="text" name="title" value="!title;" /></span>
	<span><input type="text" name="fullUrl" value="!fullUrl;" placeholder="/url-de-la-pagina/" pattern="[A-z0-9\-_\.]" /></span>
	<span><input type="checkbox" name="isPub" value="1" se-fix="!isPub;"/></span>
	<span><input type="checkbox" name="isIndexed" value="1" se-fix="!isIndexed;"/></span>
	<span><input type="text" name="artType" value="!artType;" /></span>
	<span><input type="text" name="artTypeSub" value="!artTypeSub;" /></span>
	<span>!dtAdd;</span>
	<span>!dtMod;</span>
	<span>
		<a class="btn small" target="_blank" href="!fullUrl;" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
		<button class="btn small" type="submit" title="Guardar"><svg class="icon inline"><use xlink:href="#fa-save" /></svg></button>
		<button class="btn small" type="button" data-dyntab-onclick="element_op" data-action="metaEdit" title="Meta"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" type="button" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</span>
</form>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Título', 'filter' => 1, 'fName'=>'title'],
		['name' => 'URL'],
		['name' => 'Publicado'],
		['name' => 'Indexadable'],
		['name' => 'Tipo', 'filter' => 1, 'fName'=>'artType'],
		['name' => 'Tipo Sub'],
		['name' => 'Agregado'],
		['name' => 'Modificado'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"action":"form",
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"form":{
			"title":"Nueva página",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":[
				{"ftype":"text", "vname":"title", "fname":"Nombre", "fdesc":"", "dtype":"str", "regexp":"simpleTitle", "lMin":3, "lMax":50, "requiered":true},
				{"ftype":"text", "vname":"urlPath", "fname":"URL", "fdesc":"", "dtype":"str", "regexp":"urlPath", "requiered":true},
			]
		}
	},
	"upd":
	{
		"action":"direct",
		"ajax-action":"none",
		"save_url":"{$params["page"]["ajax"]}/upd"
	},
	"metaEdit":
	{
		"ajax-action":"edit",
		"ajax-elem":"none",
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/metaTextRead",
			"save_url":"{$params["page"]["ajax"]}/metaTextUpd",
			"actName":"Editar",
			"elems":[
				{"ftype":"hidden", "vname":"id", "fname":"ID", "dtype":"int", "requiered":true},
				{"ftype":"textarea", "vname":"metaDesc", "fname":"Meta Desc", "fdesc":"", "dtype":"str", "regexp":"simpleText", "lMin":0, "lMax":200, "requiered":true},
				{"ftype":"textarea", "vname":"metaWord", "fname":"Meta Word", "fdesc":"", "dtype":"str", "regexp":"simpleText", "lMin":0, "lMax":200, "requiered":true},
			]
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Todas las páginas</div>

{$tableStructure}
HTML;
//
