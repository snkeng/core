<?php
//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Cargar archivos
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-gallerySelect.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-gallerySelect.mjs');

//
$params['vars']['galId'] = intval(\snkeng\core\engine\nav::next());
//
if ( $params['vars']['galId'] === 0 ) {
	require __DIR__ . '/gallery_all.php';
} else {
	require __DIR__ . '/gallery_single.php';
}
