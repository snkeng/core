<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!dtAdd;</td>
	<td>
		<a class="btn small" se-nav="app_content" href="{$appData['url']}/gallery/!id;/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></a>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Título', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Fecha Agregado'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva Galería",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 100,
						"data-regexp": "simpleTitle"
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Galería</div>

{$tableStructure}
HTML;
//
