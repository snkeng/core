<?php
//
$params['vars']['cms_data']['name'] = 'site';
$params['vars']['cms_data']['type'] = 'docs';
$params['vars']['cms_data']['parentId'] = 0;
$params['vars']['cms_data']['lang'] = 'es';


//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'] . '/docs', 'Documentaciones');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/', 'Inicio'],
	[$params['page']['main'].'/docs', 'Documentaciones'],
	[$params['page']['main'].'/categories', 'Categorías'],
	[$params['page']['main'].'/tags', 'Tags'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'docs':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/docs_#index.php';
		}
		else {
			//
			$params['vars']['cms_data']['parentId'] = intval(\snkeng\core\engine\nav::next());
			$params['vars']['dId'] = $params['vars']['cms_data']['parentId'];

			// Add number
			$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
			$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();

			// Current Nav
			\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Documento actual');
			\snkeng\core\admin\navBuild::navLevelSet([
				[$params['page']['main'].'/', 'Inicio'],
				[$params['page']['main'].'/structure', 'Estructura'],
				[$params['page']['main'].'/relationships', 'Relaciones'],
			]);

			//
			switch ( \snkeng\core\engine\nav::next() )
			{
				//
				case 'relationships':
					//
					\snkeng\core\admin\navBuild::navLevelClear();
					//
					$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
					$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();
					require __DIR__ . '/docs_[id]_relationships.php';
					break;

				//
				case 'structure':
					//
					$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
					$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();

					//
					\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Estructura');
					\snkeng\core\admin\navBuild::navLevelSet([
						[$params['page']['main'].'/', 'Estructura'],
						[$params['page']['main'].'/images', 'Imágenes'],
						[$params['page']['main'].'/files', 'Archivos'],
					]);

					//
					$params['vars']['windows']['pages'] = $params['page']['main'];

					//
					if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
						//
						switch ( \snkeng\core\engine\nav::next() ) {
							//
							case 'images':
								//
								$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
								$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();
								//
								\snkeng\admin_modules\site\image_window::printNormal(
									$params['page']['ajax'],
									[
										'dId' => $params['vars']['dId']
									]
								);
								break;
							//
							case 'files':
								//
								$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
								$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();
								\snkeng\admin_modules\site\file_window::printNormal(
									$params['page']['ajax'],
									[
										'dId' => $params['vars']['dId']
									]
								);
								break;
							//
							case 'images_window':
								\snkeng\admin_modules\site\image_window::printFloatWindow(
									$params['page']['ajax'].= '/images',
									[
										'dId' => $params['vars']['dId']
									]
								);
								break;
							//
							case 'files_window':
								\snkeng\admin_modules\site\file_window::printFloatWindow(
									$params['page']['ajax'].= '/images',
									[
										'dId' => $params['vars']['dId']
									]
								);
								break;
							//
							case '':
							case null:
								require __DIR__ . '/docs_[id]_structure.php';
								break;
							//
							default:
								\snkeng\core\engine\nav::invalidPage();
								break;
						}
					}
					else {
						//
						$params['vars']['pId'] = intval(\snkeng\core\engine\nav::next());

						//
						$params['page']['ajax'] .= '/' . \snkeng\core\engine\nav::current();
						$params['page']['main'] .= '/' . \snkeng\core\engine\nav::current();

						// Current Nav
						\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Página actual');
						\snkeng\core\admin\navBuild::navLevelSet([
							[$params['page']['main'].'/', 'Inicio'],
							[$params['page']['main'].'/text', 'Texto'],
							[$params['page']['main'].'/props', 'Propiedades'],
							[$params['page']['main'].'/textHTML', 'Texto HTML'],
						]);

						//
						switch ( \snkeng\core\engine\nav::next() ) {
							//
							case 'text':
								//
								require __DIR__ . '/docs_[id]_structure_page_textMD.php';
								break;
							//
							case 'textHTML':
								//
								require __DIR__ . '/docs_[id]_structure_page_textHTML.php';
								break;
							//
							case 'props':
								//
								require __DIR__ . '/docs_[id]_structure_page_props.php';
								break;
							//
							case '':
							case null:
								require __DIR__ . '/docs_[id]_structure_page_main.php';
								break;
							//
							default:
								\snkeng\core\engine\nav::invalidPage();
								break;
						}
					}
					break;
				//
				case '':
				case null:
					require __DIR__ . '/docs_[id]_#index.php';
					break;
				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		}
		break;

	//
	case 'categories':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		$params['vars']['cms_data']['type'] = 'docs';
		//
		require __DIR__ . '/../general/categories.php';
		break;

	//
	case 'tags':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
		//
		$params['vars']['cms_data']['type'] = 'docs';
		//
		require __DIR__ . '/../general/tags.php';
		break;

	//
	case '':
		require __DIR__ . '/#index.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

//
$nav = \snkeng\core\admin\navBuild::navCreate();

// Menu
$page['body'] = <<<HTML
<div class="pageTitle">Documentación</div>

{$nav}

{$page['body']}
HTML;
//
