<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Categorías
$catOptsJSON = '';
$sql_qry = "SELECT cat_id AS id, cat_title AS title FROM sc_site_category WHERE cat_mode_type='docs' LIMIT 30;";
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	$first = true;
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
		$catOptsJSON .= ($first) ? '' : ', ';
		$catOptsJSON .= "{$datos['id']}:'{$datos['title']}'";
		$first = false;
	}
}

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!urlTitle;</td>
	<td>!catTitle;</td>
	<td>!dtAdd;</td>
	<td>!dtMod;</td>
	<td>
		<a class="btn small" se-nav="app_content" href="./!id;/structure/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></a>
		<a class="btn small" se-nav="app_content" href="./!id;/relationships/" title="Usuarios"><svg class="icon inline"><use xlink:href="#fa-group" /></svg></a>
		|
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Propiedades"><svg class="icon inline"><use xlink:href="#fa-list" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="context" title="Contexto"><svg class="icon inline"><use xlink:href="#fa-user" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="permits" title="Permisos"><svg class="icon inline"><use xlink:href="#fa-lock" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="image" title="Imagen"><svg class="icon inline"><use xlink:href="#fa-photo" /></svg></button>
		<a class="btn small" target="_blank" href="/docs/!urlTitle;/" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_url' => $params['page']['ajax'] . '/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Título', 'filter' => 1, 'fName' => 'title'],
		['name' => 'URL', 'filter' => 1, 'fName' => 'author'],
		['name' => 'Categoría'],
		['name' => 'DT Add'],
		['name' => 'DT Mod'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva documentación",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"urlTitle": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre URL",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"catId": {
					"field_type": "list",
					"info_name": "Categoría",
					"attributes": {
						"options": {{$catOptsJSON}}
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Publicación",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/titles",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/titles",
			"actName":"Actualizar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"urlTitle": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre URL",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"catId": {
					"field_type": "list",
					"info_name": "Categoría",
					"attributes": {
						"options": {{$catOptsJSON}}
					}
				},
				"contentBundles": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Bundles (JS/CSS) adicionales.",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"contentBodyMod": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "HTML Component name (empty=div)",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"shortDescription": {
					"field_type": "textarea",
					"info_name": "Descripción corta"
				},
				"content": {
					"field_type": "textarea",
					"info_name": "Contenido"
				}
			}
		}
	},
	"context":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Contexto",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/context",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/context",
			"actName":"Actualizar",
			"elems":{
				"authorId": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "ID Autor",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				},
				"docType": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Tipo",
					"attributes": {
						"minlength": 4,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				}
			}
		}
	},
	"permits":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Permisos",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/permits",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/permits",
			"actName":"Actualizar",
			"elems":{
				"isPub": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Publicado",
					"attributes": {
						"value": 1
					}
				},
				"accessLevel": {
					"field_type": "list",
					"info_name": "Acceso",
					"attributes": {
						"options": {
							"0": "Uber administración",
							"1": "Super Administración",
							"2": "Administradores",
							"3": "Usuarios",
							"4": "Público"
						}
					}
				},
				"isLinkReq": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Requiere vínculo",
					"attributes": {
						"value": 1
					}
				}
			}
		}
	},
	"image":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Imagen",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/imageUpd",
			"actName":"Actualizar",
			"elems":{
				"imgId": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "ID Imagen",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/{id}/properties/del"}
}
JSON
	//
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Documentación</div>

{$tableStructure}
HTML;
//
