<?php

//
switch ( \snkeng\core\engine\nav::next() ) {

	//
	case 'blog':
		//
		\snkeng\core\engine\login::permitsCheckKill('site', 'blog');

		//
		require __DIR__ . '/blog/#map.php';
		break;

	//
	case 'docs':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		require __DIR__ . '/documentation/#map.php';
		break;

	//
	case 'pages':
		//
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		require __DIR__ . '/pages/#map.php';
		break;

	//
	case 'banners':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		require __DIR__ . '/banners/banners.php';
		break;

	//
	case 'contact':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		require __DIR__ . '/contact/#map.php';
		break;

	//
	case 'gallery':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		require __DIR__ . '/gallery/#map.php';
		break;

	//
	case 'content_gral':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');
		require __DIR__ . '/general/content_gral.php';
		break;

	//
	case 'sadmin':
		//
		\snkeng\core\engine\login::kill_loginLevel('sadmin');

		//
		require __DIR__ . '/sadmin/#map.php';
		break;

	//
	case 'uadmin':
		//
		\snkeng\core\engine\login::kill_loginLevel('uadmin');

		//
		require __DIR__ . '/uadmin/#map.php';
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
