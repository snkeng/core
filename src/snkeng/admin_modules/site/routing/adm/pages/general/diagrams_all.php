<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!type;</td>
	<td>
		<a class="btn small" se-nav="app_content" href="{$params['page']['main']}/!id;/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></a>
		<button class="btn small" type="submit" title="Edit"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Nombres', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Tipo'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"print":false,
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"action":"form",
		"form":{
			"title":"Nuevo Diagrama",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				}
			}
		}
	},
	"upd":
	{
		"action":"direct",
		"ajax-action":"upd",
		"save_url":"{$params["page"]["ajax"]}/upd",
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];
//

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// $post->debugQuery();

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Diagramas</div>

{$tableStructure}
HTML;
//
