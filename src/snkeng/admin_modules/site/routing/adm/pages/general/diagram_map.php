<?php

if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	require __DIR__ . '/diagrams_all.php';
} else {
	$params['vars']['dId'] = intval(\snkeng\core\engine\nav::next());

	if ( $params['vars']['dId'] === 0 ) { \snkeng\core\engine\nav::killWithError('ID no válido.'); }

	require __DIR__ . '/diagrams_single.php';
}