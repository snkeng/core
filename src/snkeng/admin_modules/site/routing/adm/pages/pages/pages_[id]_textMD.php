<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// debugVariable($page);

//
$page['head']['title'].= ' Texto';

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Texto (Mark Down)</div>\n\n
HTML;
//

// Add form
$page['body'].= \snkeng\core\site\markdown_form::formPrint(
	'site',
	'page',
	$params['vars']['pId'],
	'es',
	$params['page']['ajax'],
	$params['vars']['windows']['pages']
);
//


// Final cache is other order, since it may be modified by the creation of the content
\snkeng\core\engine\nav::cacheFinalCheck();