<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!id;</span>
	<span><input type="text" name="title" value="!title;" /></span>
	<span><input type="text" name="fullUrl" value="!fullUrl;" placeholder="/url-de-la-pagina/" pattern="[A-z0-9\-\.\/_]+" /></span>
	<span><input type="checkbox" name="isPub" value="1" se-fix="!isPub;"/></span>
	<span><input type="checkbox" name="isIndexed" value="1" se-fix="!isIndexed;"/></span>
	<span>!dtAdd;</span>
	<span>!dtMod;</span>
	<span>
		<button class="btn small" type="submit" title="Guardar"><svg class="icon inline"><use xlink:href="#fa-save" /></svg></button>
		<a class="btn small" se-nav="app_content" href="./!id;/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></a>
		<a class="btn small" target="_blank" href="!fullUrl;" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" type="button" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</span>
</form>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Título', 'filter' => 1, 'fName'=>'title'],
		['name' => 'URL'],
		['name' => 'Publicado'],
		['name' => 'Indexadable'],
		['name' => 'Agregado'],
		['name' => 'Modificado'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"action":"form",
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"form":{
			"title":"Nueva página",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"fullUrl": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "URL",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": "urlPath"
					}
				}
			}
		}
	},
	"upd":
	{
		"action":"direct",
		"ajax-action":"none",
		"save_url":"{$params["page"]["ajax"]}/upd"
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Páginas</div>

{$tableStructure}
HTML;
//
