<?php

// Add blog
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

//
$params['vars']['cms_data']['type'] = 'blog';

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Blog');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/', 'Inicio'],
	[$params['page']['main'].'/posts', 'Posts'],
	[$params['page']['main'].'/categories', 'Categorías'],
	[$params['page']['main'].'/tags', 'Tags'],
	[$params['page']['main'].'/images', 'Imágenes'],
	[$params['page']['main'].'/diagrams', 'Diagramas'],
	[$params['page']['main'].'/files', 'Archivos'],
]);

//
$params['vars']['windows']['pages'] = $params['page']['main'];

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'posts':
		//
		$params['page']['main'].= '/posts';
		$params['page']['ajax'].= '/posts';

		//
		\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Posts');

		//
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/posts_#index.php';
		} else {
			//
			$params['vars']['pId'] = intval(\snkeng\core\engine\nav::next());

			// Add number
			$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
			$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

			// Nav
			\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Post Actual');
			\snkeng\core\admin\navBuild::navLevelSet([
				[$params['page']['main'].'/', 'Inicio'],
				[$params['page']['main'].'/text', 'Texto MD'],
				[$params['page']['main'].'/props', 'Propiedades'],
				[$params['page']['main'].'/textHTML', 'Texto HTML'],
			]);

			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'text':
					require __DIR__ . '/posts_[id]_textMD.php';
					break;
				//
				case 'props':
					require __DIR__ . '/posts_[id]_properties.php';
					break;
				//
				case 'textHTML':
					require __DIR__ . '/posts_[id]_textHtml.php';
					break;
				//
				case '':
				case null:
					require __DIR__ . '/posts_[id]_#index.php';
					break;
				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}

		}
		break;

	//
	case 'categories':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		$params['page']['main'].= '/categories';
		$params['page']['ajax'].= '/categories';

		//
		require __DIR__ . '/../general/categories.php';
		break;

	//
	case 'tags':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		$params['page']['main'].= '/tags';
		$params['page']['ajax'].= '/tags';

		//
		require __DIR__ . '/../general/tags.php';
		break;

	//
	case 'diagrams':
		\snkeng\core\engine\login::permitsCheckKill('site', 'site_adm');

		//
		$params['page']['main'].= '/diagrams';
		$params['page']['ajax'].= '/diagrams';

		//
		require __DIR__ . '/../general/diagram_map.php';
		break;

	//
	case 'images':
		//
		$params['page']['main'].= '/images';
		$params['page']['ajax'].= '/images';
		//
		\snkeng\admin_modules\site\image_window::printNormal(
			$params['page']['ajax'],
			[]
		);
		break;
	//
	case 'files':
		//
		$params['page']['main'].= '/files';
		$params['page']['ajax'].= '/files';
		//
		\snkeng\admin_modules\site\file_window::printNormal(
			$params['page']['ajax'],
			[]
		);
		break;
	//
	case 'images_window':
		\snkeng\admin_modules\site\image_window::printFloatWindow(
			$params['page']['ajax'] . '/images',
			[]
		);
		break;
	//
	case 'files_window':
		\snkeng\admin_modules\site\file_window::printFloatWindow(
			$params['page']['ajax'] . '/files',
			[]
		);
		break;

	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Menu
$page['body'] = <<<HTML
<div class="pageTitle">Blog</div>

{$nav}

{$page['body']}
HTML;
//
