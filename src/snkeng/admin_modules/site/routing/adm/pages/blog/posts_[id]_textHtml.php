<?php
// Contenido
$sql_qry = <<<SQL
SELECT
	art_id AS id, art_content AS content, art_dt_mod AS dtMod
FROM sc_site_articles AS art
WHERE art_id={$params['vars']['pId']}
LIMIT 1;
SQL;
$postData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
	'int' => ['id']
]);

// Require files
\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-form.mjs');

// Cache
\snkeng\core\engine\nav::cacheCheckDate($postData['dtMod']);
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Contenido
$page['body'] .= <<<HTML
<div class="pageSubTitle">Texto HTML</div>

<div class="grid">

	<div class="gr_sz12">
		<form class="se_form" method="post" action="{$params['page']['ajax']}/textHTMLUpd" enctype="multipart/form-data" is="se-async-form">
			<input type="hidden" name="id" value="{$postData['id']}" />
			<label class="separator required adv_text">
				<div class="cont"><span class="title">Contenido</span><span class="desc"></span></div>
				<textarea se-plugin="wysiwyg" name="content">{$postData['content']}</textarea>
			</label>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Guardar</button>
			<output se-elem="response"></output>
		</form>
	</div>
	
</div>
HTML;
//
