<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
// Categorías
$catOptsJSON = '';
$sql_qry = "SELECT cat_id AS id, cat_title AS title FROM sc_site_category WHERE cat_mode_type='blog' LIMIT 30;";
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	$first = true;
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
		$catOptsJSON .= ($first) ? '' : ', ';
		$catOptsJSON .= "{$datos['id']}:'{$datos['title']}'";
		$first = false;
	}
}

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!author;</td>
	<td>!catTitle;</td>
	<td>!dtPub;</td>
	<td style="text-align:center"><span class="pStatus s!published;"></span></td>
	<td>!views;</td>
	<td>
		<a class="btn small" se-nav="app_content" href="./!id;/text/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></a>
		<a class="btn small" se-nav="app_content" href="./!id;/props/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></a>
		|
		<a class="btn small" href="!urlFull;" target="_blank" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Título', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Autor', 'filter' => 1, 'fName' => 'author'],
		['name' => 'Categoría'],
		['name' => 'Fecha Publicación'],
		['name' => 'Publicado'],
		['name' => 'Vistas'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo post",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":
			{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"catId": {
					"field_type": "list",
					"info_name": "Categoría",
					"attributes": {
						"options": {{$catOptsJSON}}
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Posts</div>

{$tableStructure}
HTML;
//
