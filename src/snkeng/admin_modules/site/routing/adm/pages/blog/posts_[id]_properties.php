<?php
// Post data
$sql_qry = <<<SQL
SELECT
	art.art_id AS id, art.cat_id AS catId,
    art.art_dt_mod AS dtMod,
	art.art_dt_pub AS dtPub, art.art_status_published AS isPub, art.art_status_indexable AS isIndexed,
	( NOW()>art.art_dt_pub AND art.art_status_published=1 ) AS isPublished,
	art.img_id AS imageId, art.art_img_struct AS imageStruct, art.art_img_alt_text AS imageAltText,
	art.art_title AS title, art.art_urltitle AS urltitle, art.art_url AS urlfull,
	art.art_meta_word AS metaWord, art.art_meta_desc AS metaDesc,
	art.art_type_secondary AS typeSub,
	art.art_content_extra AS contentExtra, art.art_content_extra_b AS contentExtraB,
	art.art_content_bundles AS contentBundles,
	art.art_content_header_type AS contentHeaderType, art.art_content_header_content AS contentHeaderContent,
	art.art_content_body_component AS contentBodyComponent,
	art.art_content_redirect AS contentRedirect
FROM sc_site_articles AS art
WHERE art_id={$params['vars']['pId']}
LIMIT 1;
SQL;
$postData = \snkeng\core\engine\mysql::singleRowAssoc(
	$sql_qry,
		[
		'int' => ['imgId'],
		'bool' => ['isPub', 'isIndexed', 'isPublished']
	]
);

// Cache
\snkeng\core\engine\nav::cacheCheckDate($postData['dtMod']);
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

function postEditable($pId)
{
	global $siteVars, $mysql;
	$editable = 0;
	$sql_qry = "SELECT a_id, (now()>art_dt_pub AND art_status_published=1) AS published FROM sc_site_articles WHERE art_id={$pId} LIMIT 1;";
	$postData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
	$aId = intval($postData['a_id']);
	$published = ($postData['published'] === '1') ? true : false;

	if ( ($siteVars['user']['data']['id'] === $aId && !$published)
		|| \snkeng\core\engine\login::check_loginLevel('sadmin')
	) {
		$editable = 1;
	}
	return $editable;
}



//
$isPub = ( $postData['isPub'] === '1' ) ? ' checked' : '';

// Editable
if ( postEditable($params['vars']['pId']) ) {
	$postData['isEditable'] = 1;
} else {
	$postData['isEditable'] = 0;
}


// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Propiedades</div>\n\n
HTML;
//

// Add form
$page['body'].= \snkeng\admin_modules\site\page::propertiesPrintForm(
	"blog",
	$params['page']['ajax'],
	$params['vars']['windows']['pages'],
	[
		'metadata' => 1,
		'title' => 1,
		'category' => 1,
		'image' => 1,
		'imageAltText' => 1,
		'publish' => 1,
		'behaviour' => 1,
		'tags' => 1,
		'rating' => 0,
	],
	$postData
);
//
