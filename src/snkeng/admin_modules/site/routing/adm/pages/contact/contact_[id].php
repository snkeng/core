<?php
//
// \snkeng\core\engine\nav::pageFileGroupAdd(['engine_core_html_editor']);

// \snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-articles.css');
// \snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-articles.mjs');

// Contenido
$sql_qry = <<<SQL
SELECT
	uc.uc_id AS id, uc.uc_dt_add AS dtAdd, uc.uc_dt_mod as dtMod,
	uc.uc_usr_uname AS usrName, uc.uc_usr_email AS usrMail,
	INET6_NTOA(uc.uc_usr_ip) AS usrIp, uc.uc_usr_useragent AS userAgent,
	uc.uc_usr_message AS usrMessage,
	uc.uc_status_revised AS statusRevised, uc.uc_status_spam AS statusSpam
FROM sc_site_contact AS uc
WHERE uc.uc_id={$params['vars']['cId']}
LIMIT 1;
SQL;
$contactData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
	'int' => ['id']
]);

// Contenido
$page['body'] .= <<<HTML
<div class="pageSubTitle">Contacto: {$contactData['id']}</div>

<div class="grid">

	<div class="gr_sz04">
		<table class="st_dualtab">
			<tr><td>Usuario</td><td>{$contactData['usrName']}</td></tr>
			<tr><td>Email</td><td>{$contactData['usrMail']}</td></tr>
		</table>
	</div>

	<div class="gr_sz04">
		<table class="st_dualtab">
			<tr><td>ID</td><td>{$contactData['id']}</td></tr>
			<tr><td>Dt Add</td><td>{$contactData['dtAdd']}</td></tr>
			<tr><td>Dt Mod</td><td>{$contactData['dtMod']}</td></tr>
			<tr><td>IP</td><td>{$contactData['usrIp']}</td></tr>
			<tr><td>User-Agent</td><td>{$contactData['userAgent']}</td></tr>
		</table>
	</div>

	<div class="gr_sz04">
		<table class="st_dualtab">
			<tr><td>Revisado</td><td><span class="fakeCheck" data-value="{$contactData['statusRevised']}"></span></td></tr>
			<tr><td>SPAM</td><td><span class="fakeCheck" data-value="{$contactData['statusSpam']}"></span></td></tr>
		</table>
	</div>
	
</div>

<div class="grid">

	<div class="gr_sz09">
		<h2>Mensaje</h2>
		<pre>{$contactData['usrMessage']}</pre>
	</div>

</div>
HTML;
//
