<?php
// Script execution
$_POST['script'] = ( isset($_POST['script']) ) ? $_POST['script'] : '';
$_POST['exec'] = ( isset($_POST['exec']) && $_POST['exec'] === '1' );
$_POST['params'] = ( isset($_POST['params']) ) ? $_POST['params'] : '';

// Direct file
$file = __DIR__ . '/../../func/ua_scripts/' . $_POST['script'] . '.php';
if ( !file_exists($file) ) {
	\snkeng\core\engine\nav::invalidPage('', 'file not found');
}

//
require $file;