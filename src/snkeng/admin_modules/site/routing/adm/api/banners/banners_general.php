<?php
// Banners
switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		//
		\snkeng\core\general\saveData::sql_complex_rowAdd(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 5, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'size' => ['name' => 'Tamaño', 'type' => 'int', 'validate' => true],
				'type' => ['name' => 'Tipo', 'type' => 'int', 'validate' => true],
			],
			[
				'elems' => [
					'ban_title' => 'title',
					'ban_size' => 'size',
					'ban_type' => 'type',
				],
				'tName' => 'sc_site_banners',
				'tId' => ['nm' => 'id', 'db' => 'ban_id'],
			]
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' =>
					"ban.ban_id AS id, ban.ban_title AS title, ban.ban_size AS bSize,
					ban.ban_type AS bTypeNum, ban.ban_type AS bTypeTxt,
					ban.ban_active AS bPublished,
					ban.ban_size AS bSizeTxt, ban.ban_size AS bSizeNum,
					IF(
						ban_active=1 &&
						( now()>ban.ban_dtini || ban.ban_dtini IS NULL )&&
						( now()<ban.ban_dtend || ban.ban_dtini IS NULL ) &&
						( ban.ban_maxclicks=0 || ban.ban_curclicks<ban.ban_maxclicks ) &&
						( ban.ban_maxprints=0 || ban.ban_curprints<ban.ban_maxprints )
					, 1, 0 ) AS bStatus",
				'from' => 'sc_site_banners AS ban',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'ban.ban_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'ban.ban_title', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'banId' => ['Name' => 'ID', 'db' => 'ban.ban_id', 'set' => 'DESC']
				],
				'default_order' => [
					['banId', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'bSizeTxt' => [
							'1' => 'Banner Principal',
							'2' => 'Secundario Lateral Vertical',
							'3' => 'Secundario Horizontal',
							'4' => 'Secundario',
							'5' => 'Cuadrado',
						],
						'bTypeTxt' => [
							'1' => 'Imagen',
							'2' => 'Código',
							'3' => 'Video'
						]
					]
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
