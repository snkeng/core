<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		\snkeng\core\general\saveData::sql_complex_rowAdd($response,
			[
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urltitle' => ['name' => 'Nombre URL',   'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'tag_title' => 'title',
					'tag_urltitle' => 'urltitle',
					'tag_mode_type' => 'cType',
				],
				'tName' => 'sc_site_tags',
				'tId' => ['nm' => 'id', 'db' => 'tag_id'],
			],
			[
				'setData' => [
					'cType' => $params['vars']['cms_data']['type']
				]
			]
		);
		break;

	// Actualizar
	case 'upd':
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'process' => false, 'validate' => true],
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 5, 'lMax' => 30, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urltitle' => ['name' => 'Nombre URL',   'type' => 'str', 'lMin' => 5, 'lMax' => 30, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'tag_title' => 'title',
					'tag_urltitle' => 'urltitle',
				],
				'tName' => 'sc_site_tags',
				'tId' => ['nm' => 'id', 'db' => 'tag_id'],
				'dtAdd' => '',
				'dtMod' => '',
			]
		);
		break;

	// Leer Uno
	case 'readSingle':
		\snkeng\core\general\saveData::sql_complex_rowRead($response,
			[
				'elems' => [
					'tag_title' => 'title',
					'tag_urltitle' => 'urltitle',
				],
				'tName' => 'sc_site_tags',
				'tId' => ['nm' => 'id', 'db' => 'tag_id'],
				'dtAdd' => '',
				'dtMod' => '',
			],
			'id'
		);
		break;

	// Borrar
	case 'del':
		\snkeng\core\general\saveData::sql_table_delRow($response,
			[
				'tName' => 'sc_site_tags',
				'tId' => ['db' => 'tag_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "tag.tag_id AS id, tag.tag_title AS title, tag.tag_urltitle AS urltitle",
				'from' => 'sc_site_tags AS tag',
				'lim' => 20,
				'where' => [
					'cType' => ['name' => 'Tipo', 'db' => 'tag.tag_mode_type', 'vtype' => 'str', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['type'] ?? NULL],
					'id' => ['name' => 'ID', 'db' => 'tag.tag_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'tag.tag_title', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'title' => ['Name' => 'Título', 'db' => 'tag.tag_title']
				],
				'default_order' => [
					['title', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
