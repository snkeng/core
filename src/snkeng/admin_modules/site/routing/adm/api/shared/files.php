<?php
// Archivos
switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 5, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'content' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 200, 'filter' => '', 'process' => false, 'validate' => true],
				'file' => ['name' => 'Archivo', 'type' => 'file', 'mSize'=>2048, 'validate' => true]
			]
		);

		//
		$nextId = \snkeng\core\engine\mysql::getNextId('sc_site_files');
		$name = \snkeng\core\general\saveData::buildFriendlyTitle($rData['title']);
		$filePad = str_pad($nextId, 6, '0', STR_PAD_LEFT);
		$fileName = $filePad.'_'.$name;

		// Save file in system
		$fileData = [];
		if ( !\snkeng\core\general\saveData::fileManager($rData['file'], $fileData, $fileName, '/se_files/user_upload/files/', 2097152, []) ) {
			\snkeng\core\engine\nav::killWithError('No fue posible guardar el archivo en el servidor.');
		}

		//
		$ins_qry = <<<SQL
INSERT INTO sc_site_files
(file_id, file_title, file_desc, file_ftype, file_fname, file_floc, file_fsize, file_mode_type, file_mode_id)
VALUES
({$nextId}, '{$name}', '{$content}', '{$fileData['ext']}', '{$fileData['name']}', '{$fileData['path']}', '{$fileData['size']}', '{$params['vars']['cms_data']['type']}', '{$params['vars']['cms_data']['parentId']}');
SQL;
		\snkeng\core\engine\mysql::submitQuery(
			$ins_qry,
			[
				'errorKey' => 'SiteAdmBannerVideoUp',
				'errorDesc' => 'No fue posible guardar el archivo.',
				'onError' => function () use ($fileData) {
					unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
				}
			]
		);

		//
		$response['d'] = ['id'=>$nextId, 'title' => $name, 'fileName' => $fileData['name'], 'fileType' => $fileData['type'], 'fileSize' => $fileData['size']];
		break;

	// Actualizar
	case 'upd':
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'content' => ['name' => 'Contenido', 'type' => 'str', 'lMin' => 0, 'lMax' => 5000, 'filter' => '', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'file_title' => 'title',
					'file_desc' => 'content',
				],
				'tName' => 'sc_site_files',
				'tId' => ['nm' => 'id', 'db' => 'file_id'],
			]
		);
		break;

	// Actualizar archivo
	case 'fileUpd':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'file' => ['name' => 'Archivo', 'type' => 'file', 'mSize'=>1024, 'validate' => true]
			]
		);

		// Revisar si hay archivo previo y remover
		$fileData = \snkeng\core\engine\mysql::singleRowAssoc("SELECT file_title AS name, file_floc AS path FROM sc_site_files WHERE file_id={$rData['id']};");
		if ( !empty($fileData['path']) && file_exists($_SERVER['DOCUMENT_ROOT'].$fileData['path']) ) {
			unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
		}

		// Re-crear nombre
		$name = \snkeng\core\general\saveData::buildFriendlyTitle($fileData['name']);
		$filePad = str_pad($rData['id'], 6, '0', STR_PAD_LEFT);
		$fileName = $filePad.'_'.$name;

		// Guardar archivo
		$fileData = [];
		if ( !\snkeng\core\general\saveData::fileManager($rData['file'], $fileData, $fileName, '/se_files/user_upload/files/', 1048576, null) ) {
			\snkeng\core\engine\nav::killWithError('No fue posible guardar el archivo en el servidor.');
		}

		//
		$ins_qry = <<<SQL
UPDATE sc_site_files SET
	file_ftype='{$fileData['ext']}', file_fname='{$fileData['name']}',
	file_floc='{$fileData['path']}', file_fsize='{$fileData['size']}'
WHERE file_id={$rData['id']};
SQL;
		\snkeng\core\engine\mysql::submitQuery(
			$ins_qry,
			[
				'errorKey' => 'SiteAdmFileUp',
				'errorDesc' => 'No pudo ser guardado en la base de datos.',
				'onError' => function () use ($fileData) {
					unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
				}
			]
		);

		//
		$response['d'] = ['id'=>$rData['id'], 'title' => $name, 'fileName' => $fileData['name'], 'fileExt' => $fileData['ext'], 'fileSize' => $fileData['size']];
		break;

	// Leer Uno
	case 'readSingle':
		$objId = intval($_POST['id']);
		if ( $objId === 0 ) {
			\snkeng\core\engine\nav::killWithError('ID information missing');
		}
		//
		elementReturnSingle($response, $objId);
		break;

	// Borrar
	case 'del':
		$fileId = intval($_POST['id']);
		if ( empty($fileId) ) { \snkeng\core\engine\nav::killWithError('Datos incompletos', 'No hay id'); }
		// datos
		$fileData = \snkeng\core\engine\mysql::singleRowAssoc("SELECT file_floc AS path FROM sc_site_files WHERE file_id={$fileId};");
		// Remover anterior
		unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
		\snkeng\core\engine\mysql::submitQuery("DELETE FROM sc_site_files WHERE file_id={$fileId};");
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "file.file_id AS id, file.file_title AS title,
					file.file_mode_type AS modeType, file.file_mode_id AS modeId,
					file.file_dt_add AS dtAdd, file.file_dt_mod AS dtMod,
					file.file_fname AS fileName, file.file_ftype AS fileType, file.file_floc AS fileLoc,
					FORMAT(file.file_fsize/1024, 2) AS fileSize",
				'from' => 'sc_site_files AS file',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'file.file_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'file.file_title', 'vtype' => 'str', 'stype' => 'like'],
					'fileName' => ['name' => 'Archivo', 'db' => 'file.file_fname', 'vtype' => 'str', 'stype' => 'like'],
					'modeType' => ['name' => 'Modo', 'db' => 'file.file_mode_type', 'vtype' => 'str', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['type'] ],
					'modeId' => ['name' => 'Modo', 'db' => 'file.file_mode_id', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['parentId'] ]
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'file.file_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//



//
// INIT: jsonSelection
//	Operaciones que regresan json
function elementReturnSingle(&$response, $objId)
{
	//
	\snkeng\core\general\saveData::sql_table_readRow($response,
		[
			'elems' => [
				'file_title' => 'title',
				'file_desc' => 'content',
			],
			'tName' => 'sc_site_files',
			'tId' => ['nm' => 'id', 'db' => 'file_id'],
			'dtAdd' => 'file_dtadd',
			'dtMod' => 'file_dtmod',
		],
		$objId
	);
}
// END: jsonSelection
//

