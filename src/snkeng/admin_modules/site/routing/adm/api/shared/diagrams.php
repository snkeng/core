<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		//
		\snkeng\core\general\saveData::sql_complex_rowAdd($response,
			[
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'd_title' => 'title',
					'a_id' => 'userId',
					'd_mode_type' => 'modeType',
				],
				'tName' => 'sc_site_diagrams',
				'tId' => ['nm' => 'id', 'db' => 'd_id']
			],
			[
				'setData' => [
					'modeType' => $params['vars']['cms_data']['type'],
					'userId' => $siteVars['user']['data']['id'],
				]
			]
		);
		break;

	// Actualizar
	case 'upd':
		// Validar
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'process' => false, 'validate' => true],
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 5, 'lMax' => 30, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urltitle' => ['name' => 'Nombre URL',   'type' => 'str', 'lMin' => 5, 'lMax' => 30, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'd_title' => 'title',
					'd_urltitle' => 'urltitle',
				],
				'tName' => 'sc_site_diagrams',
				'tId' => ['nm' => 'id', 'db' => 'd_id']
			]
		);
		break;

	// Leer Uno
	case 'readSingle':
		\snkeng\core\general\saveData::sql_complex_rowRead($response,
			[
				'elems' => [
					'd_title' => 'title',
					'd_urltitle' => 'urltitle',
				],
				'tName' => 'sc_site_diagrams',
				'tId' => ['nm' => 'id', 'db' => 'd_id']
			],
			'id'
		);
		break;

	// Borrar
	case 'del':
		\snkeng\core\general\saveData::sql_table_delRow($response,
			[
				'tName' => 'sc_site_diagrams',
				'tId' => ['db' => 'd_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "d.d_id AS id, d.d_title AS title, d.d_mode_type AS cType",
				'from' => 'sc_site_diagrams AS d',
				'lim' => 20,
				'where' => [
					'modeType' => ['name' => 'ID', 'db' => 'd.d_mode_type', 'vtype' => 'str', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['type']],
					'id' => ['name' => 'ID', 'db' => 'd.d_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'd.d_title', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'title' => ['Name' => 'Título', 'db' => 'd.d_title']
				],
				'default_order' => [
					['title', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
