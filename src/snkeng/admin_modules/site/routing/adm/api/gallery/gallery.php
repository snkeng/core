<?php
//
function site_gallery_rebuild($galId) {
		//
	$sql_qry = <<<SQL
SELECT
gel.gel_id AS id,
gel.gel_title AS title, gel.gel_description AS description,
img.img_fname AS fName
FROM sc_site_gallery_elements AS gel
INNER JOIN sc_site_images AS img ON gel.img_id=img.img_id
WHERE gel.gal_id='{$galId}'
ORDER BY gel.gel_order DESC
LIMIT 50;
SQL;
	$data = \snkeng\core\engine\mysql::returnArray($sql_qry, []);

	//
	$galCount = count($data);

	//
	$sql_qry = <<<SQL
SELECT
gal.gal_title AS title, gal.gal_description AS description,
gal.gal_ammount AS ammount
FROM sc_site_gallery AS gal
WHERE gal.gal_id='{$galId}'
LIMIT 1;
SQL;
	$galData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

	//
	$galData['d'] = $data;

	$galDataTxt = json_encode($galData);
	//
	$upd_qry = "UPDATE sc_site_gallery SET gal_structure='{$galDataTxt}', gal_ammount={$galCount} WHERE gal_id={$galId};";
	\snkeng\core\engine\mysql::submitQuery($upd_qry, [
		'errorKey' => '',
		'errorDesc' => '',
	]);
}

// Páginas
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'add':
		\snkeng\core\general\saveData::sql_complex_rowAdd(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'gal_title' => 'title',
				],
				'tName' => 'sc_site_gallery',
				'tId' => ['nm' => 'id', 'db' => 'gal_id']
			]
		);
		break;

	// Actualizar
	case 'upd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'fullUrl' => ['name' => 'Nombre URL', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlSimple', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'gal_title' => 'title',
					'gal_description' => 'description',
					'gal_structure' => 'structure',
				],
				'tName' => 'sc_site_gallery',
				'tId' => ['nm' => 'id', 'db' => 'gal_id']
			]
		);
		break;

	//
	case 'gallery':
		//
		$galId = intval($_POST['galId']);
		if ( !$galId ) {
			\snkeng\core\engine\nav::killWithError('ID de la galería no disponible.');
		}

		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			//
			case 'add':
				$rData = \snkeng\core\general\saveData::postParsing($response,
					[
						'imgId' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
						'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
						'description' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 255, 'process' => false, 'validate' => false],
					]
				);

				// Contar candidad de elementos
				$qry_sel = <<<SQL
SELECT COUNT(*)
FROM sc_site_gallery_elements
WHERE gal_id={$galId};
SQL;
				$currentElements = \snkeng\core\engine\mysql::singleNumber($qry_sel);

				if ( $currentElements >= 15 ) {
					\snkeng\core\engine\nav::killWithError('Ya se alcanzó el número límite de fotografías en esta galería');
				}

				//
				$rData['galId'] = $galId;

				//
				\snkeng\core\general\saveData::sql_table_addRow($response,
					[
						'elems' => [
							'gal_id' => 'galId',
							'img_id' => 'imgId',
							'gel_title' => 'title',
							'gel_description' => 'description',
						],
						'tName' => 'sc_site_gallery_elements',
						'tId' => ['nm' => 'id', 'db' => 'gel_id']
					],
					$rData
				);
				break;

			//
			case 'upd':
				//
				\snkeng\core\general\saveData::sql_complex_rowUpd($response,
					[
						'imgId' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
						'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
						'description' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 255, 'process' => false, 'validate' => false],
						// 'cOrder' => ['name' => 'Orden', 'type' => 'int', 'validate' => false],
					],
					[
						'elems' => [
							'gel_title' => 'title',
							'gel_description' => 'description',
							// 'gel_order' => 'corder',
						],
						'tName' => 'sc_site_gallery_elements',
						'tId' => ['nm' => 'imgId', 'db' => 'gel_id']
					]
				);
				break;

			//
			case 'del':
				//
				\snkeng\core\general\saveData::sql_complex_rowDel($response,
					[
						'tName' => 'sc_site_gallery_elements',
						'tId' => ['db' => 'gel_id', 'nm' => 'id']
					],
					'id'
				);
				break;

			//
			case 'upload':
				$rData = \snkeng\core\general\saveData::postParsing($response,
					[
						'files' => ['name' => 'Archivos', 'type' => 'files', 'fType' => ['jpg', 'jpeg'], 'mSize' => 512, 'process' => false, 'validate' => false],
					]
				);

				// Contar candidad de elementos
				$qry_sel = <<<SQL
SELECT COUNT(*)
FROM sc_site_gallery_elements
WHERE gal_id={$galId};
SQL;
				$currentElements = \snkeng\core\engine\mysql::singleNumber($qry_sel);

				if ( $currentElements >= 15 ) {
					\snkeng\core\engine\nav::killWithError('Ya se alcanzó el número límite de fotografías en esta galería');
				}

				//
				$sql_qry = "SELECT gal_title AS title, gal_ammount AS galCount FROM sc_site_gallery WHERE gal_id={$galId};";
				$galData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
					'int' => ['galCount']
				]);

				// Id siguiente archivo
				$img_id = \snkeng\core\engine\mysql::getNextId("sc_site_images");
				$gel_id = \snkeng\core\engine\mysql::getNextId('sc_site_gallery_elements');
				$titleBase = \snkeng\core\general\saveData::buildFriendlyTitle($galData['title']);
				$fileFolder = "/se_files/user_upload/img/site/";

				//
				$img_count = count($rData['files']);

				// First part
				$sql_ins = <<<SQL
INSERT INTO sc_site_images (
img_id, sit_id, obj_id, img_dt_add, img_name, img_desc,
img_fname, img_ftype, img_floc, img_fstruct,
img_fsize, img_width, img_height
) VALUES\n
SQL;
				$sql_ins_gal = <<<SQL
INSERT INTO sc_site_gallery_elements (gel_id, gal_id, img_id, gel_title) VALUES \n
SQL;


				//
				$tempFileList = [];


				//
				$first = true;
				for ( $i = 0; $i < $img_count; $i++ ) {
					$img_rel_count = $galData['galCount'] + $i + 1;
					//
					$img_id_pad = str_pad($img_id, 6, '0', STR_PAD_LEFT);
					$img_count_pad = str_pad($img_rel_count, 2, '0', STR_PAD_LEFT);
					$fileName = "{$img_id_pad}_{$titleBase}_{$img_count_pad}.jpg";
					$baseFile = $fileFolder.$fileName;

					//
					$cDim = getimagesize($rData['files'][$i]['tmp_name']);
					$fileSize = round(filesize($rData['files'][$i]['tmp_name']) / 1024, 2);

					$fileTitle = $galData['title'] . " " . $img_count_pad;

					//
					if ( !$first ) {
						$sql_ins.= ",\n";
						$sql_ins_gal.= ",\n";
					}
					//
					$sql_ins.= <<<SQL
(
{$img_id}, 1, {$siteVars['user']['data']['id']}, NOW(), '{$fileTitle}', '',
'{$fileName}', 'jpg', '{$baseFile}', '{$baseFile}',
'{$fileSize}', '{$cDim[0]}', '{$cDim[1]}'
)
SQL;
					//
					$sql_ins_gal.= <<<SQL
({$gel_id}, {$galId}, {$img_id}, '{$fileTitle}')
SQL;
					//

					// Copy files
					$fullNewLocation = $_SERVER['DOCUMENT_ROOT'] . $baseFile;
					if ( !copy($rData['files'][$i]['tmp_name'], $fullNewLocation) ) {
						\snkeng\core\engine\nav::killWithError('It was not posible to copy image', $rData['files'][$i]['tmp_name']. ' - '. $fullNewLocation);
					}
					//
					$tempFileList[] = $fullNewLocation;

					// Response
					$response['d'][] = [
						'id' => $gel_id,
						'fName' => $fileName,
						'title' => $fileTitle,
						'description' => '',
					];

					//
					$first = false;
					$gel_id++;
					$img_id++;
				}

				//
				$sql_ins.= ";\n";
				$sql_ins_gal.= ";";

				// Actualizar Galería
				$galTotal = $galData['galCount'] + $img_count;
				$sql_ins.= "UPDATE sc_site_gallery SET gal_ammount='{$galTotal}' WHERE gal_id={$galId};\n";
				$sql_ins.= $sql_ins_gal;

				//
				// debugVariable($sql_ins);
				\snkeng\core\engine\mysql::submitMultiQuery($sql_ins);

				//
				if ( \snkeng\core\engine\mysql::isError ) {
					// Borrar los archivos creados
					foreach ( $tempFileList as $cFile ) {
						unlink($cFile);
					}
					//
					\snkeng\core\engine\mysql::killIfError('IMGUPDATE INSERT', '');
				}
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		//
		site_gallery_rebuild($galId);
		break;

	// Borrar
	case 'del':
		//
		\snkeng\core\general\saveData::sql_complex_rowDel(
			$response,
			[
				'tName' => 'sc_site_gallery',
				'tId' => ['db' => 'gal_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "gal.gal_id AS id, gal.gal_dt_add AS dtAdd, gal.gal_dt_mod AS dtMod, gal.gal_title AS title",
				'from' => 'sc_site_gallery AS gal',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'gal.gal_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'gal.gal_title', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'galId' => ['Name' => 'ID', 'db' => 'gal.gal_id']
				],
				'default_order' => [
					['galId', 'DESC']
				]
			],
			'post'
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
