<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'add':
		//
		\snkeng\core\general\saveData::sql_complex_rowAdd($response,
			[
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'ste_title' => 'title',
				],
				'tName' => 'sc_site_textedit',
				'tId' => ['nm' => 'id', 'db' => 'ste_id']
			]
		);
		break;

	// Actualizar
	case 'upd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true]
			],
			[
				'elems' => [
					'ste_title' => 'title',
				],
				'tName' => 'sc_site_textedit',
				'tId' => ['nm' => 'id', 'db' => 'ste_id']
			]
		);
		break;

	//
	case 'updLive':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'validate' => true],
				'content' => ['name' => 'Contenido',   'type' => 'str', 'process' => true, 'validate' => true],
			],
			[
				'elems' => [
					'ste_content' => 'content',
				],
				'tName' => 'sc_site_textedit',
				'tId' => ['nm' => 'id', 'db' => 'ste_id']
			]
		);
		break;

	// Leer Uno
	case 'readSingle':
		//
		\snkeng\core\general\saveData::sql_complex_rowRead($response,
			[
				'elems' => [
					'ste_title' => 'title',
				],
				'tName' => 'sc_site_textedit',
				'tId' => ['nm' => 'id', 'db' => 'ste_id']
			],
			'id'
		);
		break;

	// Borrar
	case 'del':
		$tData = [
			'tName' => 'sc_site_textedit',
			'tId' => ['db' => 'ste_id']
		];
		//
		\snkeng\core\general\saveData::sql_complex_rowDel($response, $tData, 'id');
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson([
			'sel' => "ste.ste_id AS id, ste.ste_title AS title",
			'from' => 'sc_site_textedit AS ste',
			'lim' => 20,
			'where' => [
				'id' => ['name' => 'ID', 'db' => 'ste.ste_id', 'vtype' => 'int', 'stype' => 'eq'],
				'title' => ['name' => 'Título', 'db' => 'ste.ste_title', 'vtype' => 'str', 'stype' => 'like']
			],
			'order' => [
				'id' => ['Name' => 'ID', 'db' => 'ste.ste_id']
			],
			'default_order' => [
				['id', 'DESC']
			]
		], 'post');
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
