<?php
// Set parameters
$params['vars']['cms_data']['name'] = 'site';
$params['vars']['cms_data']['type'] = 'page';
$params['vars']['cms_data']['lang'] = 'es';
$params['vars']['cms_data']['parentId'] = 0;

// Páginas
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'content':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/page_general.php';
		}
		else {
			$params['vars']['pageId'] = intval(\snkeng\core\engine\nav::next());

			//
			require __DIR__ . '/page_properties.php';
		}
		break;

	//
	case 'categories':
		require __DIR__ . '/../shared/categories.php';
		break;

	//
	case 'tags':
		require __DIR__ . '/../shared/tags.php';
		break;

	//
	case 'files':
		require __DIR__ . '/../shared/files.php';
		break;

	//
	case 'images':
		require __DIR__ . '/../shared/images.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
