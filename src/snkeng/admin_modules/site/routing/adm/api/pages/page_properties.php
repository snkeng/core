<?php
// Set parameters

switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'properties':
		// Most operations are done here
		\snkeng\admin_modules\site\page::propertiesUpdate(
			$response,
			'pages',
			$params['vars']['pageId'],
			\snkeng\core\engine\nav::next(),
			[
				'metadata' => 1,
				'category' => 0,
				'image' => 1,
				'imageAltText' => 1,
				'publish' => 1,
				'behaviour' => 1,
				'tags' => 0,
				'rating' => 0,
			]
		);
		break;

	//
	case 'propertiesRead':
		//
		switch ( \snkeng\core\engine\nav::next() ) {
			//
			case 'metadata':
				//
				\snkeng\core\general\saveData::sql_complex_rowRead(
					$response,
					[
						'elems' => [
							'art_meta_word' => 'metaword',
							'art_meta_desc' => 'metadesc',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					'id'
				);
				break;
			//
			case 'publish':
				//
				\snkeng\core\general\saveData::sql_complex_rowRead(
					$response,
					[
						'elems' => [
							'art_dt_pub' => 'dtPub',
							'art_status_published' => 'isPub',
							'art_status_indexable' => 'isIndexed',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					'id'
				);
				break;
			//
			case 'behaviour':
				//
				\snkeng\core\general\saveData::sql_complex_rowRead(
					$response,
					[
						'elems' => [
							'art_type_secondary' => 'typeSub',
							'art_content_extra_b' => 'extraBasic',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					'id'
				);
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	case 'updLive':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'content' => ['name' => 'Contenido', 'type' => 'str', 'process' => true, 'validate' => true],
			],
			[
				'elems' => [
					'art_content' => 'content',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			]
		);
		break;

	// texto update
	case 'textHTMLUpd':
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
				'content' => ['name' => 'Contenido', 'type' => 'str', 'lMin' => 0, 'lMax' => 55000, 'filter' => '', 'process' => true, 'validate' => true],
			],
			[
				'elems' => [
					'art_content' => 'content',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			]
		);
		break;

	//
	case 'textMD':
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			//
			case 'GET':
				//
				$response = \snkeng\admin_modules\site\markdown_page::md_text_get(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['pageId'],
					$params['vars']['cms_data']['lang']
				);
				break;

			//
			case 'POST':
				//
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'txtId' => ['name' => 'ID Texto', 'type' => 'int', 'process' => false, 'validate' => true],
						'lang' => ['name' => 'Lang', 'type' => 'str', 'process' => false, 'validate' => true],
						'content' => ['name' => 'Content', 'type' => 'straightStr', 'process' => false, 'validate' => true],
					]
				);

				//
				$content = \snkeng\core\site\texts::upd(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['pageId'],
					$params['vars']['cms_data']['lang'], // Ignoring sent lang...?
					$rData['txtId'],
					$rData['content'],
					['amp'],
					['html']
				);

				// Secure data
				$content['html'] = \snkeng\core\engine\mysql::real_escape_string($content['html']);

				//
				\snkeng\core\general\saveData::sql_table_updRow(
					$response,
					[
						'elems' => [
							'art_content' => 'content',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'id' => $params['vars']['pageId'],
						'content' => $content['html']
					]
				);
				break;
		}

		break;

	// Leer Uno
	case 'readSingle':
		//
		\snkeng\core\general\saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'art_title' => 'title',
					'art_urltitle' => 'urltitle',
					'art_status_published' => 'isPub',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			'id'
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
