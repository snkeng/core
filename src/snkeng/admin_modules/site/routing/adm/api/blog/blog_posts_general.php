<?php

//

switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		\snkeng\core\engine\login::permitsCheckKill('site', 'blog_admin');

		//
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'catId' => ['name' => 'ID Categoría', 'type' => 'int', 'process' => false, 'validate' => true],
			]
		);

		// Get url form
		$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

		//
		\snkeng\admin_modules\site\page::addPage(
			$response,
			'blog',
			'normal',
			$rData['title'],
			$appData['settings']['blog']['postUrl'],
			'<p>Insertar contenido del post.</p>',
			[
				'catId' => $rData['catId']
			]
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtadd, art.art_dt_mod AS dtmod,
				(CASE
				WHEN now() > art.art_dt_pub && art.art_status_published=1 THEN 0
				WHEN now() < art.art_dt_pub && art.art_status_published=1 THEN 1
				ELSE 2
				END) AS published,
				art.art_count_views AS views,
				art.art_url AS urlFull,
				cat.cat_id AS catId, cat.cat_title AS catTitle,
				user.a_id AS aId, user.a_obj_name AS author",
				'from'=>'sc_site_articles AS art
					LEFT OUTER JOIN sc_site_category AS cat ON art.cat_id=cat.cat_id
					LEFT OUTER JOIN sb_objects_obj AS user ON art.a_id=user.a_id',
				'lim' => 20,
				'where' => [
					'artType' => ['name' => 'Tipo', 'db'=>'art.art_type_primary', 'vtype'=>'str', 'stype'=>'eq', 'set'=> 'blog'],
					'title' => ['name' => 'Título', 'db'=>'art.art_title', 'vtype'=>'str', 'stype'=>'like'],
					'id' => ['name' => 'ID', 'db'=>'art.art_id', 'vtype'=>'int', 'stype'=>'eq'],
				],
				'order' => [
					'artId' => ['Name'=>'ID', 'db'=>'art.art_id']
				],
				'default_order' => [
					['artId', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
