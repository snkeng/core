<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'properties':
		// Most operations are done here
		\snkeng\admin_modules\site\page::propertiesUpdate(
			$response,
			'blog',
			$params['vars']['postId'],
			\snkeng\core\engine\nav::next(),
			[
				'metadata' => 1,
				'category' => 1,
				'image' => 1,
				'imageAltText' => 1,
				'publish' => 1,
				'behaviour' => 1,
				'tags' => 1,
				'rating' => 0,
			]
		);
		break;

	// texto update
	case 'textHTMLUpd':
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
				'content' => ['name' => 'Contenido', 'type' => 'str', 'lMin' => 0, 'lMax' => 55000, 'filter' => '', 'process' => true, 'validate' => true],
			],
			[
				'elems' => [
					'art_content' => 'content',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			[
				'setData' => ['id' => $params['vars']['postId']]
			]
		);
		break;

	//
	case 'textMD':
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			//
			case 'GET':
				//
				$response = \snkeng\admin_modules\site\markdown_page::md_text_get(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['postId'],
					$params['vars']['cms_data']['lang']
				);
				break;

			//
			case 'POST':
				//
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'txtId' => ['name' => 'ID Texto', 'type' => 'int', 'process' => false, 'validate' => true],
						'lang' => ['name' => 'Lang', 'type' => 'str', 'process' => false, 'validate' => true],
						'content' => ['name' => 'Content', 'type' => 'straightStr', 'process' => false, 'validate' => true],
					]
				);

				//
				$content = \snkeng\core\site\texts::upd(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['postId'],
					$params['vars']['cms_data']['lang'], // Ignoring sent lang...?
					$rData['txtId'],
					$rData['content'],
					['amp'],
					['html']
				);

				// Secure data
				$content['html'] = \snkeng\core\engine\mysql::real_escape_string($content['html']);

				//
				\snkeng\core\general\saveData::sql_complex_rowUpd(
					$response,
					[
						'appId' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
					],
					[
						'elems' => [
							'art_content' => 'content',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'appId', 'db' => 'art_id'],
					],
					[
						'setData' => [
							'id' => $params['vars']['postId'],
							'content' => $content['html']
						]
					]
				);
				break;
		}
		break;

	// Actualizar
	case 'titleUpd':
		//
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urltitle' => ['name' => 'Nombre URL', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
			]
		);

		// Information
		$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

		//
		\snkeng\admin_modules\site\page::updateTitle($response, 'blog', $params['vars']['postId'], $rData['title'], $rData['urltitle'], $appData['settings']['blog']['postUrl']);
		break;

	// Borrar
	case 'del':
		//
		\snkeng\core\general\saveData::sql_table_delRow(
			$response,
			[
				'tName' => 'sc_site_articles',
				'tId' => ['db' => 'art_id']
			],
			'id'
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
