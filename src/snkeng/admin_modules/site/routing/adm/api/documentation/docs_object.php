<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'add':
		//
		\snkeng\core\general\saveData::sql_complex_rowAdd(
			$response,
			[
				'catId' => ['name' => 'ID Categoría', 'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urlTitle' => ['name' => 'Nombre URL', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'cat_id' => 'catId',
					'docs_title_normal' => 'title',
					'docs_title_url' => 'urlTitle',
				],
				'tName' => 'sc_site_documents',
				'tId' => ['nm' => 'id', 'db' => 'docs_id']
			]
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "docs.docs_id AS id, docs.img_id AS imgId,
					docs.docs_dt_add AS dtAdd, docs.docs_dt_mod AS dtMod,
					docs.docs_title_normal AS title, docs.docs_title_url AS urlTitle, docs.docs_description,
					docs.docs_img_struct, docs.docs_access_level, docs.docs_access_link_req,
					cat.cat_id AS catId, cat.cat_title AS catTitle",
				'from' =>
					'sc_site_documents AS docs
			LEFT OUTER JOIN sc_site_category AS cat ON docs.cat_id=cat.cat_id',
				'lim' => 20,
				'where' => [
					'title' => ['name' => 'Título', 'db' => 'docs.docs_title_normal', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'title' => ['Name' => 'Título', 'db' => 'docs.docs_title_normal']
				],
				'default_order' => [
					['title', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
