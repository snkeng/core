<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'add':
		//
		\snkeng\core\general\saveData::sql_complex_rowAdd(
			$response,
			[
				'docId' => ['name' => 'ID Documento', 'type' => 'int', 'validate' => true],
				'userId' => ['name' => 'ID Usuario', 'type' => 'int', 'validate' => true],
			],
			[
				'elems' => [
					'doc_id' => 'docId',
					'user_id' => 'userId',
				],
				'tName' => 'sc_site_documents_access',
				'tId' => ['nm' => 'id', 'db' => 'dacc_id']
			]
		);
		break;

	//
	case 'statusData':

		break;

	//
	case 'statusRead':
		//
		$objId = intval($_POST['id']);
		if ( empty($objId) ) {
			\snkeng\core\engine\nav::killWithError('ID No válido');
		}

		//
		\snkeng\core\general\saveData::sql_table_readRow(
			$response,
			[
				'elems' => [
					'dacc_status' => 'dStatus',
				],
				'tName' => 'sc_site_documents_access',
				'tId' => ['nm' => 'id', 'db' => 'dacc_id']
			],
			$objId
		);
		break;

	//
	case 'statusUpd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'dStatus' => ['name' => 'Status', 'type' => 'bool'],
			],
			[
				'elems' => [
					'dacc_status' => 'dStatus',
				],
				'tName' => 'sc_site_documents_access',
				'tId' => ['nm' => 'id', 'db' => 'dacc_id']
			]
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "dacc.dacc_id AS id,
				dacc.dacc_dt_add AS dtAdd, dacc.dacc_dt_mod AS dtMod,
				dacc.dacc_status AS dStatus,
				docs.docs_title_normal AS docTitle,
				obj.a_id AS userId, obj.a_obj_name AS userName",
				'from' => 'sc_site_documents_access AS dacc
					INNER JOIN sc_site_documents AS docs ON dacc.doc_id=docs.docs_id
					INNER JOIN sb_objects_obj AS obj ON obj.a_id=dacc.user_id',
				'lim' => 20,
				'where' => [
					'daccId' => ['name' => 'DACC ID', 'db' => 'dacc.dacc_id', 'vtype' => 'int', 'stype' => 'eq'],
					'userId' => ['name' => 'User ID', 'db' => 'obj.a_id', 'vtype' => 'int', 'stype' => 'eq'],
					'docId' => ['name' => 'Docs ID', 'db' => 'dacc.doc_id', 'vtype' => 'int', 'stype' => 'eq'],
					'docTitle' => ['name' => 'Título', 'db' => 'docs.docts_title_normal', 'vtype' => 'str', 'stype' => 'like'],
					'userName' => ['name' => 'Usuario', 'db' => 'obj.a_obj_name', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'daccIdOrd' => ['Name' => 'ID', 'db' => 'dacc.dacc_id']
				],
				'default_order' => [
					['daccIdOrd', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}