<?php

//
$sql_qry = <<<SQL
SELECT
	 docs_id AS id, docs_title_url AS urlTitle
FROM sc_site_documents
WHERE docs_id={$params['vars']['cms_data']['parentId']};
SQL;
$docsData = \snkeng\core\engine\mysql::singleRowAssoc(
	$sql_qry,
	[
		'int' => ['id']
	],
	[
		'errorKey' => '',
		'errorDesc' => '',
		'errorEmpty' => 'No se encontró el documento.',
	]
);

$docsData['baseUrl'] = '/docs/' . $docsData['urlTitle'];

//
switch ( \snkeng\core\engine\nav::next() ) {
	// Agregar
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'orderLvl' => ['name' => 'Orden Nivel', 'type' => 'int'],
				'orderCur' => ['name' => 'Orden Actual', 'type' => 'str', 'lMin' => 0, 'lMax' => 4],
				'orderHierarchy' => ['name' => 'Orden jerarquía', 'type' => 'str', 'lMin' => 0, 'lMax' => 24],
				'baseUrl' => ['name' => 'baseUrl', 'type' => 'str', 'lMin' => 0, 'lMax' => 200],
			]
		);

		// Convert current title tu urlTitle
		$urlTitle = \snkeng\core\general\saveData::buildFriendlyTitle($rData['title']);

		//
		\snkeng\admin_modules\site\page::addPage(
			$response,
			'docs',
			'',
			$rData['title'],
			$docsData['baseUrl'] . $rData['baseUrl'] . "/{$urlTitle}/",
			'<p>Insertar contenido del documento.</p>',
			[
				'catId' => $docsData['id'],
				'orderLvl' => $rData['orderLvl'],
				'orderCur' => $rData['orderCur'],
				'orderHierarchy' => $rData['orderHierarchy'],
			]
		);

		// Return missing parameters
		$response['d']['orderCur'] = base_convert($response['d']['orderCur'], 36, 10);
		$response['d']['urlTitle'] = $urlTitle;
		break;

	// Update order
	case 'upd_order_many':
		// Is JSON body
		$data = json_decode(file_get_contents('php://input'), true);


		$docs = $data['elements'];
		if ( empty($docs) ) {
			\snkeng\core\engine\nav::killWithError('No docs available');
		}

		//
		$upd_qry = '';
		foreach ( $docs as $doc ) {
			$doc['id'] = intval($doc['id']);
			$doc['pId'] = intval($doc['pId']);
			$doc['level'] = intval($doc['level']);
			$doc['published'] = intval($doc['published']);
			$doc['indexable'] = intval($doc['indexable']);

			//
			$doc['urlTitle'] = \snkeng\core\general\saveData::buildFriendlyTitle($doc['title']);
			$doc['fullUrl'] = $docsData['baseUrl'] . $doc['fullUrl'] . '/';

			//
			$doc['title'] = \snkeng\core\engine\mysql::real_escape_string($doc['title']);
			$doc['urlTitle'] = \snkeng\core\engine\mysql::real_escape_string($doc['urlTitle']);
			$doc['fullUrl'] = \snkeng\core\engine\mysql::real_escape_string($doc['fullUrl']);

			//
			$upd_qry .= <<<SQL
UPDATE sc_site_articles SET
	art_order_parent={$doc['pId']}, art_order_level={$doc['level']}, art_order_current='{$doc['order']}', art_order_hierarchy='{$doc['fullOrder']}',
	art_status_published='{$doc['published']}', art_status_indexable='{$doc['indexable']}',
	art_title='{$doc['title']}', art_urltitle='{$doc['urlTitle']}', art_url='{$doc['fullUrl']}'
WHERE art_id={$doc['id']};\n
SQL;
			//
		}

		//
		\snkeng\core\engine\mysql::submitMultiQuery(
			$upd_qry,
			[
				'errorKey' => 'updMany',
				'errorDesc' => 'Unable to save order'
			]
		);

		//
		$response['debug']['qry'] = $upd_qry;
		break;

	//
	case 'files':
		require __DIR__ . '/../shared/files.php';
		break;

	//
	case 'images':
		require __DIR__ . '/../shared/images.php';
		break;

	//
	case 'pages':
		require __DIR__ . '/docs_single_structure_pages.php';
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
				DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
				art.art_status_published AS isPub,
				art.art_status_indexable AS isIndexed,
				art.art_count_views AS views,
				art.art_url AS fullUrl",
				'from' => 'sc_site_articles AS art',
				'lim' => 30,
				'where' => [
					'artType' => ['name' => 'Tipo', 'db'=>'art.art_type_primary', 'vtype'=>'str', 'stype'=>'eq', 'set'=> 'page'],
					'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'urlTitle' => ['Name' => 'ID', 'db' => 'art.art_url']
				],
				'default_order' => [
					['urlTitle', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
