<?php
// Banners
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' =>
					"uc.uc_id AS id, uc.uc_dt_add AS dtAdd, uc.uc_dt_mod as dtMod,
					uc.uc_usr_uname AS usrName, uc.uc_usr_email AS usrMail, INET6_NTOA(uc.uc_usr_ip) AS usrIp,
					uc.uc_status_revised AS statusRevised, uc.uc_status_spam AS statusSpam",
				'from' => 'sc_site_contact AS uc',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'uc.uc_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Nombre', 'db' => 'uc.uc_usr_uname', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'uc.uc_id']
				],
				'default_order' => [
					['id', 'DESC']
				],
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
