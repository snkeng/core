/*
 * Change [data-animate] property when element enters screen, animation handled trough CSS by changing element from [data-animate="0"] to [data-animate="1"].
 */
import mermaid from 'https://unpkg.com/mermaid@9/dist/mermaid.esm.min.mjs';

customElements.define('se-site-diagram-build', class extends HTMLElement {
	//
	constructor() {
		super();

		this.autoUpdate = true;
		this.textArea = this.querySelector('textarea');
		this.svgArea = this.querySelector('#graphElement');
	}

	//
	connectedCallback() {
		this.textArea.addEventListener("change", this.updateImage.bind(this));
		this.textArea.addEventListener('keydown', this.keyboardTabSupport);
		console.log("textarea connected", this.textArea);

		mermaid.initialize({ startOnLoad:false });

		this.updateImage();
	}

	//
	async updateImage() {
		//
		if ( !this.autoUpdate ) {
			return;
		}

		//
		// Example of using the API var
		const graphCode = this.textArea.value;
		const graph = await mermaid.mermaidAPI.render('graphDiv', graphCode, (svgCode) => {
			console.log("inserting svg?", svgCode);
			this.svgArea.innerHTML = svgCode;
		});
	}

	//
	keyboardTabSupport(e) {
		if ( e.key !== 'Tab' ) {
			return;
		}
		e.preventDefault();
		var start = this.selectionStart;
		var end = this.selectionEnd;

		// set textarea value to: text before caret + tab + text after caret
		this.value = this.value.substring(0, start) + "\t" + this.value.substring(end);

		// put caret at right position again
		this.selectionStart = this.selectionEnd = start + 1;
	}
});