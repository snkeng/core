import * as formOps from '/snkeng/core/res/modules/library/forms.mjs';
import * as formCreator from '/snkeng/core/res/modules/library/form-creator.mjs'
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';

import * as simpleTab from '/snkeng/core/res/modules/components-simple/se-simple-tab.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('site-adm-pages-markdownform', class extends HTMLElement {
	// Variables
	apiurl = '';
	imgurl = '';

	//
	el_options = this.querySelector('div[se-elem="options"]');
	//
	el_insertTableElement = this.querySelector('div[se-elem="buildedTable"]');
	//
	el_textForm = this.querySelector('form[se-elem="text"]');
	el_textArea = this.el_textForm.querySelector('textarea');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Values are set once, use once, ignoring fancy solution
		this.apiurl = this.dataset['apiurl'];
		this.imgurl = this.dataset['imgurl'];

		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		this.querySelector('form[data-addoperation="table"]').se_on('change', ['input'], this.updateTableForm.bind(this));
		this.el_textArea.se_on('keydown', this.keyHandler.bind(this));
		this.el_options.se_on('submit', 'form[data-addoperation]', this.formSubmit.bind(this));

		// Load initial text
		this.getInitialText();
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-markdownform-onclick]');
		if ( !cBtn ) { return; }

		//
		let operation = 'onClick_' + cBtn.dataset['markdownformOnclick'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	async getInitialText() {
		try {
			const respuesta = await fetch(this.dataset.apiurl + '/textMD');

			const results = await respuesta.json();

			formOps.formSetData(this.el_textForm, results);
		} catch (error) {
			console.error(error);
		}
	}

	//
	keyHandler(e) {
		let TABKEY = 9;
		if ( e.keyCode === TABKEY ) {
			this.insertToText("\t");
			//
			if ( e.preventDefault ) { e.preventDefault(); }
			return false;
		}
	}

	//
	formSubmit(e, formEl) {
		e.preventDefault();
		e.stopPropagation();
		//
		const cData = formOps.serializeFormToJSON(formEl);
		let instTxt = "";
		console.log(cData);

		// Operations
		switch ( formEl.dataset['addoperation'] ) {
			//
			case 'notification':
				// Agregar líneas a elementos
				cData.content = this.fixContentInsideBlock(cData.content);

				//
				if ( cData.style !== "" ) {
					cData.style = ", " + cData.style;
				}

				//
				instTxt = `
>[!NOTIFICATION] (#${ cData.icon }, ${ cData.title }${ cData.style })
> ${ cData.content }
`;
				break;

			//
			case 'note':
				// Agregar líneas a elementos
				cData.content = this.fixContentInsideBlock(cData.content);

				//
				if ( cData.style !== "" ) {
					cData.style = ", " + cData.style;
				}

				//
				instTxt = `
>[!NOTE] (${ cData.title }${ cData.style })
> ${ cData.content }
`;
				break;

			//
			case 'quote':
				// Agregar líneas a elementos
				cData.content = this.fixContentInsideBlock(cData.content);

				//
				instTxt = `
>[!QUOTE] (${ cData.source }, ${ cData.author }, ${ cData.url })
> ${ cData.content }
`;
				break;

			//
			case 'video':
				//
				instTxt = `
>[!VIDEO] (${ cData.url })
`;
				break;

			//
			case 'image':
				// Agregar líneas a elementos

				cData.description = ( cData.description.length ) ?  ', ' + cData.description : '';
				cData.adjustable = parseInt(cData.adjustable);
				console.warn("LOS DATOS DEL FORMULARIO SON ", cData);
				//
				let imageProps = {
					'name':( !cData.adjustable ) ? 'D' : '',
					'fileName':( !cData.adjustable ) ? cData.floc : `${ cData.folder }, ${ cData.fname }`,
				};


				//
				instTxt = `
>[!IMAGE${ imageProps.name }] (${ imageProps.fileName }, ${ cData.width }, ${ cData.height }${ cData.description })
> ${ cData.footer }
> C: ${ cData.credit }
`;
				break;

			//
			case 'table':
				instTxt = this.tableFormInsert();
				break;

			//
			default:
				console.error("formulario no reconocido", formEl.se_data('addoperation'));
				break;
		}
		//
		this.insertToText("\n" + instTxt + "\n");
	}

	//
	fixContentInsideBlock(cString) {
		console.log("original string", cString);
		cString = cString.replaceAll(/\n/gi, "\n> ");
		console.log("original string", cString);
		return cString;
	}

	//
	async onClick_imageSelect(e, cBtn) {
		e.preventDefault();

		//
		let imageDialog = await formCreator.Modal.page(this.imgurl + '/images_window', true, () => {
			// Wait for actuall definition of the object.
			window.customElements.whenDefined('site-adm-pages-image-picker').then(() => {
				let cElement = document.body.querySelector("site-adm-pages-image-picker");

				// Only do something if the element exists on the page.
				if ( !cElement ) {
					console.error("bad appending");
					return;
				}

				//
				cElement.dispatchEvent(
					new CustomEvent("imageSelectSet", {
						detail: {
							callback:(imgData) => {
								// Fill form data
								formOps.formSetData(this.el_options.querySelector('form[data-addoperation="image"]'), {
									'folder': 'site',
									'fname': imgData['fname'],
									'floc': imgData['floc'],
									'description': imgData['desc'],
									'width': imgData['width'],
									'height': imgData['height'],
									'adjustable': imgData['adjustable'],
								});

								// Close the current window (it destroys it)
								imageDialog.forceClose();
							}
						},
					}),
				);
			});
		});
	}



	//
	inserTableFormData() {
		let tableParams = se.form.serializeToObj(this.insertTableForm);

		//
		tableParams.rows_head = parseInt(tableParams.rows_head);
		tableParams.rows_body = parseInt(tableParams.rows_body);
		tableParams.rows_foot = parseInt(tableParams.rows_foot);

		//
		return tableParams;
	}

	//
	updateTableForm(e) {
		let tableParams = this.inserTableFormData(),
			//
			cols_show = ( tableParams.cols > 5) ? 5 : tableParams.cols,
			rows_head_show = ( tableParams.rows_head > 2 ) ? 2 : tableParams.rows_head,
			rows_body_show = ( tableParams.rows_body > 3 ) ? 3 : tableParams.rows_body,
			rows_foot_show = ( tableParams.rows_foot > 2 ) ? 2 : tableParams.rows_foot,
			//
			table_class = '',
			table_txt = '',
			rows_head_txt = '<tr>',
			rows_body_txt = '<tr>'
		;

		//
		for ( let i = 0; i < cols_show; i++ ) {
			rows_head_txt+= "<th> - </th>";
			rows_body_txt+= "<td> - </td>";
		}
		rows_head_txt+= '</tr>';
		rows_body_txt+= '</tr>';

		//
		if ( tableParams.border ) { table_class+= ' border'; }
		if ( tableParams.wide ) { table_class+= ' wide'; }
		if ( tableParams.fixed ) { table_class+= ' fixed'; }
		if ( tableParams.alternate ) { table_class+= ' alternate'; }

		//
		table_txt = '<table class="se_table';
		if ( table_class ) { table_txt+= table_class; }
		table_txt+= '">';

		//
		if ( rows_head_show > 0 ) {
			table_txt+= '<thead>';

			for ( let i = 0; i < rows_head_show; i++ ) {
				table_txt+= rows_head_txt;
			}

			table_txt+= '</thead>';
		}

		//
		if ( rows_body_show > 0 ) {
			table_txt+= '<tbody>';

			for ( let i = 0; i < rows_body_show; i++ ) {
				table_txt+= rows_body_txt;
			}

			table_txt+= '</tbody>';
		}

		//
		if ( rows_foot_show > 0 ) {
			table_txt+= '<tfoot>';

			for ( let i = 0; i < rows_foot_show; i++ ) {
				table_txt+= rows_body_txt;
			}

			table_txt+= '</tfoot>';
		}

		//
		this.el_insertTableElement.innerHTML = table_txt;
	}

	//
	tableFormInsert() {
		let tableParams = this.inserTableFormData(),
			//
			tableString = '',
			table_class = '',
			rows_body_txt = '>'
		;

		//
		for ( let i = 0; i < tableParams.cols; i++ ) {
			rows_body_txt+= "| - ";
		}
		rows_body_txt+= "|\n";

		//
		if ( tableParams.border ) { table_class+= ' border'; }
		if ( tableParams.wide ) { table_class+= ' wide'; }
		if ( tableParams.fixed ) { table_class+= ' fixed'; }
		if ( tableParams.alternate ) { table_class+= ' alternate'; }


		//
		if ( tableParams.rows_head > 0 ) {

			for ( let i = 0; i < tableParams.rows_head; i++ ) {
				tableString+= rows_body_txt;
			}

			tableString+= ">| ---- |\n";
		}

		//
		if ( tableParams.rows_body > 0 ) {
			for ( let i = 0; i < tableParams.rows_body; i++ ) {
				tableString+= rows_body_txt;
			}
		}

		//
		if ( tableParams.rows_foot > 0 ) {
			tableString+= ">| ---- |\n";

			for ( let i = 0; i < tableParams.rows_foot; i++ ) {
				tableString+= rows_body_txt;
			}
		}

		//
		console.log("table params", tableParams, tableString);

		return `
>[!TABLE] (${ table_class }, ${ tableParams.title }, ${ tableParams.description })
${ tableString }
`;
	}

	//
	insertToText(value) {
		this.insertAtCursor(this.el_textArea, value);
	}

	//
	insertAtCursor(myField, myValue) {
		// IE support
		if ( document.selection ) {
			myField.focus();
			let sel = document.selection.createRange();
			sel.text = myValue;
		}
		// MOZILLA and others
		else if ( myField.selectionStart || myField.selectionStart === '0' ) {
			let startPos = myField.selectionStart;
			let endPos = myField.selectionEnd;
			//
			myField.value = myField.value.substring(0, startPos)
				+ myValue
				+ myField.value.substring(endPos, myField.value.length);
			myField.selectionStart = startPos + myValue.length;
			myField.selectionEnd = startPos + myValue.length;
		}
		// What others?
		else {
			myField.value += myValue;
		}
	}

});
//