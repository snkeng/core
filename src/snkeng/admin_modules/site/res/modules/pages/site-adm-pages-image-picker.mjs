import * as formOps from '/snkeng/core/res/modules/library/forms.mjs';
import * as shortRequest from '/snkeng/core/res/modules/library/short-request.mjs';
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';

import * as simpleTab from '/snkeng/core/res/modules/components-simple/se-simple-tab.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';
import * as asyncContent from '/snkeng/core/res/modules/components-simple/se-async-content.mjs';

//
customElements.define('site-adm-pages-image-picker', class extends HTMLElement {
	//
	el_photoPicker = this.querySelector('div.photoPicker');
	el_photoPicker_imageSelected = this.el_photoPicker.querySelector('div[se-elem="photoPicker_imageSelected"]');
	el_photoPicker_elements = this.el_photoPicker.querySelector('se-async-content.photoElements');
	// Filter
	el_photoPicker_filter = this.el_photoPicker.querySelector('div[data-type="galleryFilter"]');
	// Add images
	el_imageInputAdd = this.el_photoPicker.querySelector('input[se-elem="image_input_add"]');
	el_imagePasteAdd = this.el_photoPicker.querySelector('div[se-elem="image_paste_add"]');
	// Uploader
	el_photoUploader = this.querySelector(':scope > .window[data-element="uploader"]');
	el_photoUploaderContainer = this.el_photoUploader.querySelector('img[se-elem="container"]');
	el_photoUploader_imageForm = this.el_photoUploader.querySelector('form[se-elem="imageForm"]');
	// Editor
	el_photoEditor = this.querySelector(':scope > .window[data-element="editor"]');
	el_photoEditor_image_container = this.el_photoEditor.querySelector('img[se-elem="container"]');
	el_photoEditor_image_canvas = this.el_photoEditor.querySelector('canvas[se-elem="canvas"]');
	el_photoEditor_menu_contents = this.el_photoEditor.querySelector('div[se-elem="options"]');
	// Replace images
	el_photoEditor_imageInputAdd = this.el_photoEditor.querySelector('input[se-elem="image_input_replace"]');
	el_photoEditor_imagePasteAdd = this.el_photoEditor.querySelector('div[se-elem="image_paste_replace"]');
	// Edit images
	el_photoEditor_imageDataForm = this.el_photoEditor.querySelector('form[se-elem="imageDataForm"]');
	el_photoEditor_imageReplaceForm = this.el_photoEditor.querySelector('form[se-elem="imageReplaceForm"]');
	el_photoEditor_imageCropForm = this.el_photoEditor.querySelector('form[se-elem="photoEditor_imageCropForm"]');
	el_photoEditor_imageCropTargetForm = this.el_photoEditor.querySelector('form[se-elem="photoEditor_imageCropTargetForm"]');
	el_photoEditor_imageResizeForm = this.el_photoEditor.querySelector('form[se-elem="photoEditor_imageResizeForm"]');
	//
	el_imageCropLims = this.querySelector('div.photoCropLims');
	//
	imageSelectCallBack = null;
	//
	apiUrl = ''
	maxFileSize = 2097152;

	//
	imgResult = {
		id: 0,
		name: '',
		desc: '',
		width: '',
		height: '',
		fname: '',
		floc: '',
		rfloc: '',
		props: '',
		adjustable: ''
	};
	imgProperties = {
		id: 0,
		size: 0,
		width: 0,
		height: 0,
		editable: false,
		fileType: null,
 		file: null
	};
	nav_photoParams = {
		onSelect: null,
		tW: 800,
		tH: 600
	};

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Form modifications (defaults)
		/*
		for ( let cElem in pSettings.postDefaults ) {
			if ( !pSettings.postDefaults.hasOwnProperty(cElem) ) { continue; }
			//
			let cValue = pSettings.postDefaults[cElem];
			//
			this.el_photoEditor_imageResizeForm.se_prepend(`<input type="hidden" name="${ cElem }" value="${ cValue }" />`);
		}

		 */
		this.apiUrl = this.dataset['url'];

		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		this.addEventListener('imageSelectSet', this.setSelectCallBack.bind(this));

		// En este caso se hace un llamado al elemento que contiene los elementos (otro plugin)
		this.el_photoPicker_elements.se_on('click', '.galObj', this.photoPicker_image_select.bind(this));
		//

		// Filtros
		this.el_photoPicker_filter.se_on('change', 'input', this.photoPicker_filter.bind(this));

		// Agregar
		this.el_imageInputAdd.se_on('change', this.photoPicker_image_newImage_input.bind(this));
		this.el_imagePasteAdd.se_on('paste', this.photoPicker_image_newImage_paste.bind(this));

		// Upload image
		this.el_photoUploader_imageForm.action = this.apiUrl + "/imgAdd";
		this.el_photoUploader_imageForm.se_on('submit', this.photoUploader_image_save.bind(this));

		// Replace
		this.el_photoEditor_imageInputAdd.se_on('change', this.photoEditor_image_replace_input.bind(this));
		this.el_photoEditor_imagePasteAdd.se_on('paste', this.photoEditor_image_replace_paste.bind(this));

		// Update image information

		this.el_photoEditor_imageDataForm.addEventListener('submit', this.photoEditor_imageDataForm_submit.bind(this));


		// Plugin cropper
		/*
		this.el_photoEditor_image_canvas.se_plugin('cropper', {
			'onNewSel':(data) => {
				this.el_photoEditor_imageCropForm.se_formElVal('x', data.x);
				this.el_photoEditor_imageCropForm.se_formElVal('y', data.y);
				this.el_photoEditor_imageCropForm.se_formElVal('w', data.w);
				this.el_photoEditor_imageCropForm.se_formElVal('h', data.h);
			},
			'onNewImg':(data) => {
				this.el_photoEditor_imageResizeForm.se_formElVal('wo', data.iw);
				this.el_photoEditor_imageResizeForm.se_formElVal('ho', data.ih);
				this.el_photoEditor_imageResizeForm.se_formElVal('wn', data.iw);
				this.el_photoEditor_imageResizeForm.se_formElVal('hn', data.ih);
			}
		});

		//
		this.el_photoEditor_imageCropForm.se_on('change', 'input', (e, elem) => {
			e.preventDefault();
			let data = this.el_photoEditor_image_canvas.cropper.setCrop(
				parseInt(this.el_photoEditor_imageCropForm.se_formElVal('x')),
				parseInt(this.el_photoEditor_imageCropForm.se_formElVal('y')),
				parseInt(this.el_photoEditor_imageCropForm.se_formElVal('w')),
				parseInt(this.el_photoEditor_imageCropForm.se_formElVal('h')),
				elem.getAttribute('name')
			);

			//
			this.el_photoEditor_imageCropForm.se_formElVal('x', data.x);
			this.el_photoEditor_imageCropForm.se_formElVal('y', data.y);
			this.el_photoEditor_imageCropForm.se_formElVal('w', data.w);
			this.el_photoEditor_imageCropForm.se_formElVal('h', data.h);
		});
		*/
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-image-window-onclick]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['imageWindowOnclick'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida. =(", operation, cBtn, cBtn.dataset);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	photoEditor_imageDataForm_submit(e) {
		e.preventDefault();

		//
		this.el_photoEditor_imageDataForm.action = this.apiUrl + '/' + this.imgProperties.id + '/dataUpd';
		formOps.formSubmit(this.el_photoEditor_imageDataForm);
	}

	setSelectCallBack(e) {
		if ( typeof e.detail.callback !== 'function' ) {
			console.error("Invalid callback", e.detail);
			return;
		}

		// Save
		this.imageSelectCallBack = e.detail.callback;

		// Enable select button
		this.el_photoPicker_imageSelected.querySelector('button[data-name="select"]').style.display = 'block';
	}

	//
	onClick_photoEditor_image_replace_on() {
		//
		this.photoEditor_option_display('options_replace');
		//
		this.imagePropertiesReset();

		// Get original image
		console.warn("SELECTED IMAGE INFO", this.imgResult);

		//
		this.imgProperties.id = this.imgResult.id;
		this.el_photoEditor_image_container.src = this.imgResult.rfloc;

		console.warn("image properties", this.imgProperties);

		// Disable button
		this.el_photoEditor.querySelector('button[data-image-window-onclick="photoEditor_image_replace_exec"]').setAttribute('aria-diabled', 'true');
	}

	//
	photoEditor_image_replace_update(imageData) {
		this.el_photoEditor_image_container.src = imageData;
		this.imgProperties.id = this.imgResult.id;
		this.el_photoEditor.querySelector('button[data-image-window-onclick="photoEditor_image_replace_exec"]').setAttribute('aria-diabled', 'false');
	}

	//
	onClick_photoEditor_image_replace_exec() {
		if ( !this.imgProperties.file ) {
			alert("No hay archivo");
			return;
		}

		if ( this.imgProperties.id === 0 ) {
			console.trace("id evitar");
			alert("No hay id de la imagen.");
			return;
		}

		console.trace("replacing image information", this.imgProperties);

		//
		let sendFormData = new FormData();
		sendFormData.append('imgId', this.imgProperties.id);
		sendFormData.append('imgPropWidth', this.imgProperties.width);
		sendFormData.append('imgPropHeight', this.imgProperties.height);
		sendFormData.append('imgFile', this.imgProperties.file, 'image.' + this.imgProperties.fileType);

		const cFormButton = this.querySelector('button[data-image-window-onclick="photoEditor_image_replace_exec"]');

		//
		shortRequest.postRequest(
			this.apiUrl + '/' + this.imgProperties.id + '/imgReplace',
			sendFormData,
			{
				response: this.querySelector('output[data-elem="photoEditor_image_replace"]'),
				onRequest: () => {
					cFormButton.disabled = true;
				},
				onComplete: () => {
					cFormButton.disabled = false;
				},
				onSuccess: (msg) => {
				},
				onFail: (msg) => {
					console.log("Imagen no guardada.", msg);
				}
			}
		);
	}

	//
	onClick_photoEditor_image_replace_off() {
		this.photoEditor_option_display('options_main');
	}

	//
	onClick_photoEditor_image_edit_on() {
		this.photoEditor_option_display('options_edit');
	}

	//
	photoEditor_image_edit_exec(e) {
		console.log("subir cambios en la imagen");

		e.preventDefault();

		let options = {
			'limit': true,
			'format': 'webp',
			'quality': 0.95
		};

		//
		this.el_photoEditor_image_canvas.cropper.getImage((cImage) => {
			let id = parseInt(this.el_photoEditor_imageDataForm.se_formElVal('id')),
				url = (id === 0) ? this.apiUrl + '/imgAdd' : this.apiUrl + '/' + id + '/imgUpd',
				post = new FormData(),
				response = this.el_photoEditor_imageDataForm.querySelector('output[se-elem="response"]'),
				imageType = (cImage.type === 'image/webp') ? 'webp' : 'jpg';

			//
			post.append('imgId', id);
			post.append('imgName', this.el_photoEditor_imageDataForm.se_formElVal('title'));
			post.append('imgDesc', this.el_photoEditor_imageDataForm.se_formElVal('content'));
			post.append('imgType', 1);
			post.append('imgFile', cImage, 'image.' + imageType);

			// Add defaultsk
			for ( let cElem in pSettings.postDefaults ) {
				if ( !pSettings.postDefaults.hasOwnProperty(cElem) ) {
					continue;
				}
				//
				let cValue = pSettings.postDefaults[cElem];
				//
				post.append(cElem, cValue);
			}

			//
			shortRequest.postRequest(url, post, {
				response: response,
				onSuccess: (msg) => {
					if ( id === 0 ) {
						this.el_photoEditor_imageDataForm.querySelector('input[name="id"]').se_val(msg.d.id);
						this.onClick_photoPicker_image_reload();
					} else {
						let img = this.el_photoPicker_imageSelected.querySelector('img');
						img.src = img.src + '?' + se.uniqueId();
					}
				},
				onFail: (msg) => {
					console.log("Imagen no guardada.", msg);
				}
			});
		}, options);
	}

	//
	photoEditor_image_edit_off() {
		this.photoEditor_option_display('options_main');
	}

	//
	photoEditor_image_crop() {
		this.el_photoEditor_image_canvas.cropper.imageCrop();
	}

	//
	photoEditor_image_target_crop() {
		this.el_photoEditor_image_canvas.cropper.setFinalSize(
			this.el_photoEditor_imageCropTargetForm.se_formElVal('w'),
			this.el_photoEditor_imageCropTargetForm.se_formElVal("h")
		);
	}

	//
	photoEditor_image_target_crop_cut() {
		this.el_photoEditor_image_canvas.cropper.setFinalSize(
			this.el_photoEditor_imageCropTargetForm.se_formElVal("w"),
			this.el_photoEditor_imageCropTargetForm.se_formElVal("h")
		);
	}

	//
	photoEditor_image_resize() {
		//
		this.el_photoEditor_image_canvas.cropper.resize(
			this.el_photoEditor_imageResizeForm.se_formElVal("wn"),
			this.el_photoEditor_imageResizeForm.se_formElVal("hn"),
			this.el_photoEditor_imageResizeForm.se_formElVal("rszType")
		);
	}

	//
	photoUploader_image_save(e) {
		e.preventDefault();

		//
		let sendFormData = new FormData();
		sendFormData.append('imgType', 1);
		sendFormData.append('imgName', this.el_photoUploader_imageForm.se_formElVal('title'));
		sendFormData.append('imgDesc', this.el_photoUploader_imageForm.se_formElVal('content'));
		sendFormData.append('imgPropWidth', this.imgProperties.width);
		sendFormData.append('imgPropHeight', this.imgProperties.height);
		sendFormData.append('imgPropEditable', this.imgProperties.editable);
		sendFormData.append('imgFile', this.imgProperties.file, 'image.' + this.imgProperties.fileType);

		//
		shortRequest.postRequest(
			this.apiUrl + '/imgAdd',
			sendFormData,
			{
				response: this.el_photoUploader_imageForm.querySelector('output'),
				onSuccess: (msg) => {
					this.el_photoUploader_imageForm.reset();
					this.onClick_photoUploader_window_close();
					this.onClick_photoPicker_image_reload();
				},
				onFail: (msg) => {
					console.log("Imagen no guardada.", msg);
				}
			}
		);
	}

	//
	photoUploader_window_open(imgData, svg) {
		//
		// document.body.se_prepend(this.el_photoUploader);

		console.log("window open is now", imgData);

		this.el_photoUploaderContainer.src = imgData;

		//
		console.error("INFORMACIÓN FINAL DEL ARCHIVO: ", this.imgProperties);

		//
		domManipulator.updateChildren(this.el_photoUploader, {
			'[se-elem="width"]': [
				['text', this.imgProperties.width]
			],
			'[se-elem="height"]': [
				['text', this.imgProperties.height]
			],
			'[se-elem="fileSize"]': [
				['text', Math.round(parseInt(this.imgProperties.size) / 1024, 2)]
			],
			'[se-elem="fileType"]': [
				['text', this.imgProperties.fileType]
			],
		});

		// Display
		this.el_photoUploader.setAttribute('aria-hidden', 'false');
		this.el_photoPicker.setAttribute('aria-hidden', 'true');
		this.el_photoEditor.setAttribute('aria-hidden', 'true');
	}

	//
	onClick_photoUploader_window_close() {
		this.el_photoUploader.setAttribute('aria-hidden', 'true');
		this.el_photoPicker.setAttribute('aria-hidden', 'false');
		this.el_photoEditor.setAttribute('aria-hidden', 'true');
		// this.se_prepend(this.el_photoUploader);
	}

	//
	photoEditor_window_open() {
		// document.body.se_prepend(this.el_photoEditor);
		this.el_photoPicker.setAttribute('aria-hidden', 'true');
		this.el_photoEditor.setAttribute('aria-hidden', 'false');
	}

	//
	onClick_photoEditor_window_close() {
		this.el_photoPicker.setAttribute('aria-hidden', 'false');
		this.el_photoEditor.setAttribute('aria-hidden', 'true');
		// this.se_prepend(this.el_photoEditor);
	}

	//
	photoEditor_editor_resizeMod() {
		let cont = this.el_photoEditor_imageResizeForm,
			cv = this.cObj.value,
			ow = cont.querySelector('[name="ho"]').se_val(),
			oh = cont.querySelector('[name="wo"]').se_val(),
			nv;
		if ( cont.querySelector('[name="ar"]').checked ) {
			if ( this.cObj.name === 'wn' ) {
				nv = parseInt(cv * ow / oh);
				cont.querySelector('[name="hn"]').se_val(nv);
			} else {
				nv = parseInt(cv * oh / ow);
				cont.querySelector('[name="wn"]').se_val(nv);
			}
		}
	}

	//</editor-fold>

	//<editor-fold desc="Photo Picker">
	//
	photoPicker_setup(data) {
		this.nav_photoParams = se.object.merge(this.nav_photoParams, data);
		if ( typeof nav_photoParams.onSelect === 'function' ) {
			this.el_photoPicker_imageSelected.querySelector('button[data-name="select"]').se_show();
		}
	}

	//
	onClick_photoPicker_image_reload() {
		console.warn("should reload");
		this.el_photoPicker_elements.dispatchEvent(
			new CustomEvent("externalOperation", {
				detail: { operation:"reload" },
			}),
		);
	}

	photoPicker_filter(e, cEl) {
		//
		this.el_photoPicker_elements.dispatchEvent(
			new CustomEvent("filterMod", {
				detail: {
					type:'filter',
					name: cEl.name,
					value: cEl.value
				}
			}),
		);
	}

	//
	photoPicker_image_select(e, elem) {
		this.imgResult.id = parseInt(elem.dataset['imgid']);
		this.imgResult.name = elem.querySelector('.title').innerText;
		this.imgResult.desc = elem.querySelector('.title').getAttribute('title');
		this.imgResult.width = parseInt(elem.dataset['imgwidth']);
		this.imgResult.height = parseInt(elem.dataset['imgheight']);
		this.imgResult.floc = elem.querySelector('img').getAttribute('src');
		this.imgResult.fname = elem.dataset['fname'];
		this.imgResult.rfloc = elem.dataset['imgfloc'];
		this.imgResult.adjustable = parseInt(elem.dataset['isadjustable']);
		this.imgResult.props = elem.querySelector('.props').innerText;

		//
		this.imgResult.floc = this.imgResult.floc.replace('/w_160/', '/w_250/');
		//
		this.photoPicker_image_loadSelected();
	}

	//
	photoPicker_image_unSelect() {
		this.imgResult.id = 0;
		this.imgResult.name = '';
		this.imgResult.desc = '';
		this.imgResult.width = '';
		this.imgResult.height = '';
		this.imgResult.floc = '';
		this.imgResult.fname = '';
		this.imgResult.rfloc = '';
		this.imgResult.props = '';
		this.imgResult.adjustable = false;
	}

	//
	photoPicker_image_loadSelected() {
		//
		this.el_photoPicker_imageSelected.querySelector('img').setAttribute('src', this.imgResult.floc);
		this.el_photoPicker_imageSelected.querySelector('.title').innerText = this.imgResult.name;
		this.el_photoPicker_imageSelected.querySelector('.props').innerText = this.imgResult.props;
		this.el_photoPicker_imageSelected.querySelector('.desc').innerText = this.imgResult.desc;
		this.el_photoPicker_imageSelected.querySelector('.id span').innerText = this.imgResult.id;
		//
		this.el_photoPicker_imageSelected.querySelector('.content').style.display = 'block';
	}

	//
	photoPicker_image_unLoadSelected() {
		//
		this.el_photoPicker_imageSelected.querySelector('img').setAttribute('src', '');
		this.el_photoPicker_imageSelected.querySelector('.title').innerText = '';
		this.el_photoPicker_imageSelected.querySelector('.props').innerText = '';
		this.el_photoPicker_imageSelected.querySelector('.desc').innerText = '';
		//
		this.el_photoPicker_imageSelected.querySelector('.content').style.display = 'none';
	}

	//
	onClick_photoPicker_image_finalSelect() {
		if ( this.imgResult.id === 0 ) {
			alert('No hay foto seleccionada.');
			return;
		}
		if ( typeof this.imageSelectCallBack !== 'function' ) {
			alert('Callback no definido.');
			return;
		}
		//
		this.imageSelectCallBack(this.imgResult);
	}

	//
	onClick_photoPicker_image_edit() {
		console.log('IMGResult: ', this.imgResult);
		//
		this.photoEditor_window_open();

		//
		this.el_photoEditor.dataset['adjustable'] = this.imgResult.adjustable;
		this.el_photoEditor.dataset['imgId'] = this.imgResult.id;

		//
		if ( this.imgResult.adjustable ) {
			this.el_photoEditor.querySelector('div[se-elem="imageEditableCall"]').setAttribute('aria-hidden', 'false');
		} else {
			this.el_photoEditor.querySelector('div[se-elem="imageEditableCall"]').setAttribute('aria-hidden', 'true');
		}

		// Display image
		this.el_photoEditor_image_container.setAttribute('aria-hidden', 'false');
		this.el_photoEditor_image_canvas.setAttribute('aria-hidden', 'true');

		//
		// this.photoEditor_option_display('options_main');

		//
		this.el_photoEditor_image_container.src = this.imgResult.rfloc;
		this.imgProperties.id = this.imgResult.id;

		// Set default values for form
		formOps.formSetData(this.el_photoEditor_imageDataForm, {
			'imgId': this.imgResult.id,
			'imgName': this.imgResult.name,
			'imgDesc': this.imgResult.desc
		});
	}

	//
	photoEditor_option_display(winName) {
		Array.from(this.el_photoEditor_menu_contents.children).forEach((cEl) => {
			cEl.setAttribute('aria-hidden', 'true');
		});
		this.el_photoEditor_menu_contents.querySelector(':scope > div[se-elem="' + winName + '"]').setAttribute('aria-hidden', 'false');
	}

	//
	photoPicker_image_copy() {
		if ( confirm('Se creara un duplicado en el sistema de la imagen actual.\\n¿Desea continuar?') ) {
			shortRequest.postRequest(
				this.apiUrl + '/' + this.imgProperties.id + '/imgCopy',
				null,
				{
					onSuccess: (msg) => {
						this.onClick_photoPicker_image_reload();
					}
				}
			);
		}
	}

	//
	photoPicker_image_delete() {
		if ( confirm('Se borrará la imagen actual del sistema.\\n¿Desea continuar?') ) {
			shortRequest.postRequest(
				this.apiUrl + '/' + this.imgProperties.id + '/imgDelete',
				{'imgId': this.imgResult.id},
				{
					onSuccess: (msg) => {
						//
						this.el_photoPicker_elements.querySelector('div.galObj[data-imgId="' + imgResult.id + '"]').se_remove();
						//
						this.photoPicker_image_unSelect();
						this.photoPicker_image_unLoadSelected();
					}
				}
			);
		}
	}

	//
	photoPicker_image_newImage() {
		this.el_photoEditor_imageDataForm.se_formElVal('imgId', 0);
	}

	//
	photoPicker_image_newImage_input(e) {
		this.photoPicker_image_newImage();
		this.photoEditor_image_loadFromInput(e, true);
	}

	//
	photoPicker_image_newImage_paste(e) {
		this.photoPicker_image_newImage();
		this.photoEditor_image_loadFromPaste(e, true);
	}

	//
	photoEditor_image_replace_input(e) {
		this.photoEditor_image_loadFromInput(e, false);
	}

	//
	photoEditor_image_replace_paste(e) {
		this.photoEditor_image_loadFromPaste(e, false);
	}

	//
	imagePropertiesReset(resetId = true) {
		this.imgProperties = {
			id: (resetId) ? 0 : this.imgProperties.id,
			size: 0,
			width: 0,
			height: 0,
			editable: false,
			fileType: null,
			file: null
		}
	}

	//
	async photoEditor_image_processing(imageFile) {}

	//
	async photoEditor_image_loadFromInput(event, isNewFile) {
		event.preventDefault();

		this.processFile(event.target.files[0], isNewFile);
	}

	async processFile(targetFile, isNewFile) {
		//
		this.imagePropertiesReset(false);

		// File
		this.imgProperties.file = targetFile;

		//
		if ( !this.imgProperties.file ) {
			alert("Archivo no encontrado.");
			return;
		}

		//
		this.imgProperties.size = this.imgProperties.file.size;
		this.imgProperties.fileType = this.imgProperties.file.name.split('.').pop();

		//
		if ( !this.imgProperties.file.type ) {
			alert("Archivo parece no tener formato.");
			return;
		}


		// Check for file compatibility
		if (
			!isNewFile &&
			(
				(this.imgResult.adjustable && !['png', 'jpg', 'webp', 'avif'].includes(this.imgProperties.fileType)) ||
				(!this.imgResult.adjustable && this.imgProperties.fileType !== this.imgResult.rfloc.split('.').pop())
			)
		) {
			alert("El archivo debe ser del mismo tipo que el original para ser reemplazado");
			return;
		}

		// Operations per type
		switch ( this.imgProperties.file.type ) {
			//
			case 'image/svg+xml':
				// This cant be resized, notify limit
				if ( this.imgProperties.size > this.maxFileSize ) {
					alert("El archivo es demasiado grande. >2MB");
					return;
				}

				// Get text from file
				const svgData = await this.imgProperties.file.text()

				// Create HTML element, read SVG properties
				const parser = new DOMParser(),
					svgCont = parser.parseFromString(svgData, "image/svg+xml"),
					svgEl = svgCont.firstElementChild;

				// Check if valid svg?
				if ( svgEl.tagName !== 'svg' ) {
					alert("Not valid svg?");
					return;
				}

				// Get properties
				let box = svgEl.viewBox.baseVal;
				this.imgProperties.width = box.width;
				this.imgProperties.height = box.height;

				this.imgProperties.editable = false;

				// SVG string to image conversion
				const imgData = "data:image/svg+xml;base64," + btoa(svgData);

				//
				if ( isNewFile ) {
					this.photoUploader_window_open(imgData);
				} else {
					this.photoEditor_image_replace_update(imgData);
				}
				break;
			//
			case 'image/avif':
			case 'image/webp':
			case 'image/jpg':
			case 'image/jpeg':
			case 'image/png':
				// Initial conditions
				const fileURLData = await this.fileAsDataURL(this.imgProperties.file);
				this.imgProperties.editable = true;

				// Check for animation (does not resize and check for size)
				if ( this.imgProperties.file.type === 'image/webp' ) {
					// Read file as text
					const fileTextData = await this.fileAsDataText(this.imgProperties.file);

					// If anim in text, is animation
					if ( fileTextData.includes('ANIM') ) {
						// This are not editable
						this.imgProperties.editable = false;

						// This cant be resized, notify limit
						if ( this.imgProperties.size > this.maxFileSize ) {
							alert("El archivo es demasiado grande. >2MB");
							return;
						}

						// Send images as is
						if ( isNewFile ) {
							this.photoUploader_window_open(fileURLData);
						} else {
							this.photoEditor_image_replace_update(fileURLData);
						}
						return;
					}
				}

				// Is not animation, can resize and do other stuff

				// Read initial file conditions
				const tempImg = new Image();
				tempImg.src = fileURLData;
				await tempImg.decode();

				let imgNewX = tempImg.naturalWidth,
					imgNewY = tempImg.naturalHeight;

				// Resize
				if ( tempImg.width > 4000 ) {
					imgNewX = 4000;
					imgNewY = Math.round(4000 * tempImg.naturalHeight / tempImg.naturalWidth);
				}

				// Canvas
				const canvas = document.createElement('canvas');
				canvas.width = imgNewX;
				canvas.height = imgNewY;
				canvas.getContext('2d').drawImage(tempImg, 0, 0, imgNewX, imgNewY);

				// Conversion?
				const blob = await new Promise((resolve) => canvas.toBlob(resolve, 'image/webp'));
				this.imgProperties.file = new File([blob], 'new-file.webp', {type: blob.type});

				// New blob data
				const fileURLData2 = await this.fileAsDataURL(this.imgProperties.file);

				// New data
				this.imgProperties.width = imgNewX;
				this.imgProperties.height = imgNewY;
				//
				this.imgProperties.size = this.imgProperties.file.size;
				this.imgProperties.fileType = this.imgProperties.file.name.split('.').pop();

				//
				if ( isNewFile ) {
					this.photoUploader_window_open(fileURLData2);
				} else {
					this.photoEditor_image_replace_update(fileURLData2);
				}

				break;
			//
			default:
				alert("archivo no soportado.");
				break;
		}
	}

	async fileAsDataURL (inputFile) {
		const temporaryFileReader = new FileReader();

		return new Promise((resolve, reject) => {
			temporaryFileReader.onerror = () => {
				temporaryFileReader.abort();
				reject(new DOMException("Problem parsing input file."));
			};

			temporaryFileReader.onload = () => {
				resolve(temporaryFileReader.result);
			};
			temporaryFileReader.readAsDataURL(inputFile);
		});
	};

	fileAsDataText (inputFile) {
		const temporaryFileReader = new FileReader();

		return new Promise((resolve, reject) => {
			temporaryFileReader.onerror = () => {
				temporaryFileReader.abort();
				reject(new DOMException("Problem parsing input file."));
			};

			temporaryFileReader.onload = () => {
				resolve(temporaryFileReader.result);
			};
			temporaryFileReader.readAsText(inputFile);
		});
	};

	//
	photoEditor_image_loadFromPaste(e, isNewFile) {
		let target = e.target;

		//
		this.imagePropertiesReset();

		// Unknown error making supossedly chrome only to not work. Should fix?
		if ( e.clipboardData.items && e.clipboardData.getData && false ) {
			let items = e.clipboardData.items;
			console.warn("ITEMS", items);
			// Chrome solution
			// Evitar que la pegue
			e.preventDefault();

			// Chrome, puede leer clipboard
			for ( let i = 0; i < items.length; i++ ) {
				// get the clipboard item
				let clipboardItem = e.clipboardData.items[i],
					type = clipboardItem.type;
				// if it's an image add it to the image field
				if ( type.indexOf("image") !== -1 ) {
					// get the image content and create an img dom element
					let blob = clipboardItem.getAsFile(),
						URLObj = window.URL || window.webkitURL,
						file = URLObj.createObjectURL(blob);
					//
					console.warn("current file", file);

					// this.el_photoEditor_image_canvas.cropper.setImageByFile(file);
				} else {
					console.log("Not supported: ", type);
				}
			}
		} else {
			console.warn("fall back method or text?");
			// Ver resultado (no clipboard items, Firefox)
			target.se_empty();
			setTimeout(async () => {
				console.log("Fallback content loaded", target.innerHTML);
				// Leer contenido
				const image = target.querySelector('img');
				if ( image ) {
					console.log("Image tag found.");

					//
					let cFile = null;

					// Is it a link to a file or content in src.
					if ( image.src.startsWith('http') ) {
						cFile = await this.urlToImageFile(image.src, 'file');
					} else {
						cFile = this.dataURLtoImageFile(image.src, 'file');
					}

					//
					this.processFile(cFile, isNewFile);
				} else {
					let cText = target.se_html(),
						fileUrl = '/snkeng/image-downloader/?url=';

					console.log("Not any images... checking for url or encoded");
					//
					cText = cText.replace(/<[^>]*>/g, "");
					if ( /data:image/.test(cText) ) {
						console.log("Encoded image?");
						// console.log("BASE Load: ", cText);
						//
						let image = new Image;
						image.crossOrigin = "Anonymous";
						image.src = fileUrl;
						image.onload = () => {
							console.log("is image", image);

							let cFile = this.dataURLtoImageFile(image.src, 'file');
							this.processFile(cFile, isNewFile);
						};
					} else if ( /(https?|ftp)/.test(cText) ) {
						console.log("URL Method: ", fileUrl + cText);
						fileUrl += encodeURIComponent(cText);

						//
						let image = new Image;
						image.crossOrigin = "Anonymous";
						image.src = fileUrl;
						image.onload = async () => {
							console.warn("IMAGE LOADED", image);

							let cFile = await this.urlToImageFile(fileUrl, 'file');
							this.processFile(cFile, isNewFile);
						};

					} else {
						console.error("unknown image", cText);
						return;
					}
				}

				// this.el_photoEditor_window_open();
				// Reiniciar elemento
				target.se_empty();
			}, 1);
		}
	}

	//
	dataURLtoImageFile(dataurl, filename) {
		let arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
		//
		while ( n-- ) {
			u8arr[n] = bstr.charCodeAt(n);
		}

		//
		return this.getFile(u8arr, mime);
	}

	//
	async urlToImageFile(imageUrl) {
		const response = await fetch(imageUrl);
		// here image is url/location of image
		const blob = await response.blob();

		//
		return this.getFile(blob, blob.type);
	}

	//
	getFile(arrayBuff, mime) {
		let fileType;

		//
		switch ( mime ) {
			//
			case 'image/svg+xml':
				fileType = 'svg';
				break;
			//
			case 'image/webp':
				fileType = 'webp';
				break;
			//
			case 'image/jpg':
			case 'image/jpeg':
				fileType = 'jpg';
				break;
			//
			case 'image/png':
				fileType = 'png';
				break;
			//
			default:
				return;
				break;
		}

		//
		return new File([arrayBuff], 'image.' + fileType, {type: mime});
	}

});
