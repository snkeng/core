import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';
import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('site-adm-pages-docs-structure', class extends HTMLElement {
	//
	sort_content = this.querySelector('div[se-elem="content"]');
	//
	addForm = this.querySelector('form[se-elem="add"]');
	
	//
	settings = {
		'saveUrl': null,
		'docId': 0
	};

	//
	properties = {
		'selId':null
	};

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Add defaults
		this.settings.dId = parseInt(this.dataset['docid']);
		this.settings.saveUrl = parseInt(this.dataset['saveurl']);

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.addForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: this.addFormSuccess.bind(this)
					},
				}),
			);
		});

		// Enable buttons
		this.addEventListener('click', this.onClickCallBacks.bind(this));

		// Actions
		this.sort_content.se_on('change', '.elem input', this.inputCheck.bind(this));
		this.sort_content.se_on('click', '.elem', this.selectEl.bind(this));

		//
		this.detectNextPosition();
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	detectNextPosition() {
		let elements,
			last,
			relOrder,
			// Default initial parameters
			sLevel = 1,
			sSubLevel = '0001',
			sOrderHierarchy = '0001',
			sBaseUrl = '/'
		;

		// Element selected
		if ( this.properties.selId ) {
			let cEl = this.sort_content.querySelector('.sel'),
				tEl,
				cLevel = parseInt(cEl.dataset['level']),
				relOrder = 1;

			//
			sLevel = cLevel + 1

			// Select last child element within same level
			while ( ( tEl = cEl.nextSibling ) && parseInt(tEl.dataset['level']) > cLevel ) {
				let curLevel = parseInt(tEl.dataset['level']);
				if ( curLevel === sLevel ) {
					relOrder = parseInt(tEl.dataset['order']) + 1;
				}
				cEl = tEl;
			}

			sSubLevel = this.parseOrder(relOrder);
			sOrderHierarchy = sSubLevel;

			// Back track to root with all elements
			let tempLvl = sLevel;

			//
			tEl = cEl;
			do {
				let curLevel = parseInt(tEl.dataset['level']);
				if ( curLevel < tempLvl ) {
					tempLvl--;
					sOrderHierarchy = this.parseOrder(parseInt(tEl.dataset['order'])) + sOrderHierarchy;
					sBaseUrl = '/' + tEl.dataset['url'] + sBaseUrl;
				}
				cEl = tEl;
			} while ( ( tEl = cEl.previousSibling ) && tempLvl > 1 );
		} else {
			// No element selected, get last element
			elements = this.sort_content.querySelectorAll('div.elem[data-level="1"]');

			// Not first place, get info from last
			if ( elements.length ) {
				last = elements[elements.length - 1];
				relOrder = parseInt(last.dataset['order']) + 1;

				//
				sSubLevel = this.parseOrder(relOrder);
				sOrderHierarchy = sSubLevel;
			}
		}

		//
		console.log("NEXT will be at: LEVEL:%d. SubLevel: %s. Full Hierarchy: %s. Base URL: %s.", sLevel, sSubLevel, sOrderHierarchy, sBaseUrl);

		// Set parameters
		this.addForm.se_formElVal('orderLvl', sLevel);
		this.addForm.se_formElVal('orderCur', sSubLevel);
		this.addForm.se_formElVal('orderHierarchy', sOrderHierarchy);
		this.addForm.se_formElVal('baseUrl', sBaseUrl);
	}

	//
	addFormSuccess(msg) {
		let newPage = domManipulator.stringPopulate(this.querySelector('template').innerHTML, msg.d);

		//
		if ( this.properties.selId ) {
			let cEl = this.sort_content.querySelector('.sel'),
				cLevel = parseInt(cEl.dataset['level']),
				cOrder = parseInt(cEl.dataset['order']) + 1,
				tLevel = cLevel + 1,
				//
				nEl = cEl,
				//
				cNextSame;

			//
			while ( (nEl = nEl.nextSibling) ) {
				let nLevel = parseInt(nEl.dataset['level']);
				if ( nEl.nodeType !== 1 || !nLevel ) { console.log("Elemento no válido para análisis", nEl, nLevel, nEl.nodeType); continue; }
				console.log("n attempt ", nLevel, cLevel, (nLevel < cLevel), nEl);
				if ( nLevel < tLevel  ) {
					cNextSame = nEl;
					break;
				}
			}

			//
			console.log("next...", cNextSame, cLevel, cOrder);

			if ( cNextSame ) {
				cNextSame.insertAdjacentHTML('beforebegin', newPage);
			} else {
				this.sort_content.insertAdjacentHTML('beforeend', newPage);
			}
		} else {
			this.sort_content.insertAdjacentHTML('beforeend', newPage);
		}

		//
		this.detectNextPosition();
	}

	//
	inputCheck(e) {
		e.stopPropagation();
		let cInput = e.target,
			cEl = cInput.closest('div.elem');

		//
		switch ( cInput.name ) {
			//
			case 'published':
			case 'indexable':
				break;
			//
			case 'title':
				let urlTitle = cInput.value.toString()                                 // Convert to string
					.normalize('NFD')                                // Change diacritics
					.replace(/[\u0300-\u036f]/g,'') // Remove illegal characters
					.replace(/\s+/g,'-')            // Change whitespace to dashes
					.toLowerCase()                                         // Change to lowercase
					.replace(/[^a-z0-9\-]/g,'')     // Remove anything that is not a letter, number or dash
					.replace(/-+/g,'-')             // Remove duplicate dashes
					.replace(/^-*/,'')              // Remove starting dashes
					.replace(/-*$/,'');             // Remove trailing dashes;
				//
				cEl.dataset['url'] = urlTitle;
				cEl.dataset['title'] = cInput.value;
				break;
			//
			default:
				console.log("input no programado");
				break;
		}
	}

	//
	selectEl(ev, cEl) {
		// Remove selected
		domManipulator.updateChildren(
			this.sort_content,
			{
				'.elem.sel':['class', 'remove', 'sel']
			}
		);

		// Add selected
		cEl.classList.add('sel');
		this.properties.selId = cEl.dataset['id'];

		//
		this.detectNextPosition();
	}

	//
	onClick_navigation(ev, cBut) {
		ev.preventDefault();
		if ( this.properties.selId ) {
			let cEl = this.querySelector('.elem[data-id="'+this.properties.selId+'"]'),
				cElD = this.elGetObj(cEl),
				pOEl,
				pNEl,
				tLvl,
				el, pLvl, index;

			//
			switch ( cBut.dataset['target'] ) {
				//
				case 'up':
					if ( cElD.ord !== 1 ) {
						tLvl = cElD.lvl - 1;
						pNEl = cEl.se_prev('[data-level="'+tLvl+'"]'); // parent
						pOEl = cEl.se_prev('[data-level="'+cElD.lvl+'"]'); // switch with
						//
						el = cEl;
						pLvl = cElD.lvl;
						index = cEl.se_index();

						do {
							pOEl.se_before(el);
							index++;
						} while ( (el = this.sort_content.children[index]) && parseInt(el.dataset['level']) > pLvl );
						this.sortParent(pNEl);
					} else {
						console.log("Max rel level");
					}
					break;
				//
				case 'down':
					let docEl = document.documentElement,
						matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
					pOEl = null;
					pLvl = cElD.lvl - 1;
					tLvl = cElD.lvl;
					el = cEl;
					// Search
					while ( (el = el.nextSibling) && parseInt(el.dataset['level']) > pLvl && !pOEl ) {
						if ( matches.call(el, '[data-level="'+tLvl+'"]') ) {
							pOEl = el;
						}
					}

					// Excecute
					if ( pOEl ) {
						console.log(pOEl);
						pLvl = cElD.lvl;
						el = pOEl;
						index = pOEl.se_index();
						do {
							cEl.se_before(el);
							index++;
						} while ( (el = this.sort_content.children[index]) && parseInt(el.dataset['level']) > pLvl );
						this.sortParent(pNEl);
					} else {
						console.log("No next available");
					}
					break;
				//
				case 'left':
					if ( cElD.lvl > 1 ) {
						tLvl = cElD.lvl - 2;
						pNEl = cEl.se_prev('[data-level="'+tLvl+'"]'); // New
						tLvl = cElD.lvl - 1;
						pOEl = cEl.se_prev('[data-level="'+tLvl+'"]'); // Old
						this.elUpdateLevel(cEl, -1);
						console.log("sort:", pOEl, pOEl);
						this.sortParent(pNEl);
						this.sortParent(pOEl);
					} else {
						console.log("Min lvl");
					}
					break;
				//
				case 'right':
					if ( cElD.ord !== 1 ) {
						pNEl = cEl.se_prev('[data-level="'+cElD.lvl+'"]'); // New
						tLvl = cElD.lvl + 1;
						pOEl = cEl.se_prev('[data-level="'+tLvl+'"]'); // Old
						if ( pNEl ) {
							this.elUpdateLevel(cEl, +1);
							this.sortParent(pNEl);
							this.sortParent(pOEl);
						} else {
							console.log("plugElem has no parent... imposibru");
						}
					} else {
						console.log("Max rel level");
					}
					break;
				//
				default:
					console.log("ERROR: Not coded.");
					break;
			}
		} else {
			console.log("No plugElem selected.");
		}
	}

	//
	elGetObj(el) {
		return {
			lvl:parseInt(el.dataset['level']),
			ord:parseInt(el.dataset['order'])
		};
	}

	//
	elUpdateLevel(el, lvl) {
		let docEl = document.documentElement,
			matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector,
			elData = this.elGetObj(el),
			pLvl = elData.lvl, nLvl;

		// Update plugElem and children
		do {
			// console.log("avance:", lvl, el);
			nLvl = parseInt(el.dataset['level']) + lvl;
			el.dataset['level'] = nLvl;
		} while ( (el = el.nextSibling) && parseInt(el.dataset['level']) > pLvl );
		// console.log("END ADVANCE", parseInt(el.dataset['level']), pLvl , ( parseInt(el.dataset['level']) < pLvl ), el);
	}

	//
	sortParent(pEl) {
		let docEl = document.documentElement,
			matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector,
			el, cLvl, pLvl, count = 1;
		if ( pEl ) {
			el = pEl;
			pLvl = parseInt(el.dataset['level']);
			cLvl = pLvl + 1;
		} else {
			el = this.sort_content.firstChild;
			pLvl = 0;
			cLvl = 1;
		}
		// console.log("INI Sort: ", el, pLvl, cLvl);
		do {
			if ( matches.call(el, '[data-level="'+cLvl+'"]') ) {
				// console.log("SORT SUCCESS", cLvl, el);
				el.dataset['order'] = count;
				count++;
			}
		} while ( (el = el.nextSibling) && parseInt(el.dataset['level']) > pLvl );
		// console.log("END Sort", parseInt(el.dataset['level']), pLvl , ( parseInt(el.dataset['level']) > pLvl ), el);
	}

	//
	onClick_save() {
		let el = this.sort_content.firstChild,
			data = [],
			rData = {},
			sLvl = 0,
			//
			title, urlTitle,
			cLvl, fullUrl, cOrd, cId, i, j,
			orderStr;

		//
		do {
			orderStr = this.parseOrder(parseInt(el.dataset['order']));
			cId = parseInt(el.dataset['id']);
			cLvl = parseInt(el.dataset['level']);

			//
			title = el.dataset['title'];
			urlTitle = el.dataset['url'];

			fullUrl = '/'+urlTitle;
			//
			rData[cLvl] = {
				id:cId,
				url:fullUrl,
				ord:orderStr
			};

			// In need for optimization
			fullUrl = '';
			cOrd = '';
			for ( i = 0; i < cLvl; i++ ) {
				j = i + 1;
				fullUrl+= rData[j].url;
				cOrd+= rData[j].ord;
			}

			//
			data.push({
				id:cId,
				pId:( cLvl > 1 ) ? rData[cLvl - 1].id : 0,
				level:cLvl,
				order:orderStr,
				title:title,
				urlTitle:urlTitle,
				fullUrl:fullUrl,
				fullOrder:cOrd,
				published:( el.querySelector('input[name="published"]').checked ) ? 1 : 0,
				indexable:( el.querySelector('input[name="indexable"]').checked ) ? 1 : 0
			});
		} while ( (el = el.nextSibling) );
		//
		formActions.postRequest(
			this.dataset['saveurl'],
			{
				'dId':parseInt(this.dataset['docid']),
				'elements':data
			},
			{
				onSuccess:(msg) => {
					console.log("success");
				}
			}
		);
	}

	//
	parseOrder(ord) {
		let pad = "0000",
			txt = "" + ord.toString(36);
		return pad.substring(0, pad.length - txt.length) + txt;
	}
});
