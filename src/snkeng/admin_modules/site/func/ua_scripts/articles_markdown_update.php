<?php

//
$upd_qry = '';
$first = true;

//
$markDown = new \snkeng\core\site\markdown();

//
$sql_qry = <<<SQL
SELECT
	sct.txt_id AS id, sct.txt_app_id AS appId, sct.txt_content AS content
FROM sc_site_texts AS sct
WHERE sct.txt_type='md';
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		//
		$content = \snkeng\core\engine\mysql::real_escape_string($markDown->text($datos['content']));

		//
		$upd_qry.= <<<SQL
UPDATE sc_site_articles SET art_content='{$content}' WHERE art_id='{$datos['id']}';\n
SQL;
	}
}

//
$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;
