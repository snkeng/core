<?php

use snkeng\core\engine\imgResizer;

$imageRz = new imgResizer();
$upd_qry = '';
die("don't use");
//
$sql_qry = <<<SQL
SELECT
img.img_id AS id,
img.img_floc AS floc,
img.img_fsize AS fsize, img.img_width AS fwidth, img.img_height AS fheight,
img.img_fstruct AS fstruct
FROM sc_site_images AS img;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		$fileLoc = $_SERVER['DOCUMENT_ROOT'].$datos['floc'];
		$baseFile = strstr($datos['floc'], '.', true);
		// Borrar
		$cFiles = glob($_SERVER['DOCUMENT_ROOT'].$baseFile."*");
		foreach ( $cFiles as $key => $file ) {
			if ( $fileLoc !== $file ) {
				unlink($file);
			}
		}

		// Crear
		$resData = $imageRz->batchJPG($fileLoc, $baseFile, [
			[
				'force' => true,
				'suffix' => '_w160',
				'op' => 'maxResize',
				'width' => 160,
				'heigth' => 0
			],
			[
				'force' => false,
				'suffix' => '_w320',
				'op' => 'maxResize',
				'width' => 320,
				'heigth' => 0
			],
			[
				'force' => false,
				'suffix' => '_w832',
				'op' => 'maxResize',
				'width' => 832,
				'heigth' => 0
			]
		]);
	}
}
//
$response['d'] = 'Exitoso';