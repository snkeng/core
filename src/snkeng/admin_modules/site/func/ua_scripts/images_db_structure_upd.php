<?php
//
$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
	img.img_id AS id,
	img.img_floc AS floc, img_code AS imgCode,
	img.img_name AS tName
FROM sc_site_images AS img;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		// Obtain file parts
		$fileParts = pathinfo($datos['floc']);

		// New code if needed
		$newCode = ( empty($datos['imgCode']) ) ? se_randString(8) : $datos['imgCode'];

		// Rehash and rename
		$fStuct = \base64url_encode($datos['id'] . ":" . $newCode);
		$nameUrl = \snkeng\core\general\saveData::buildFriendlyTitle($datos['tName']);

		//
		$upd_qry.= <<<SQL
UPDATE sc_site_images
SET img_fname='{$fileParts['filename']}', img_name_url='{$nameUrl}', img_code='{$newCode}', img_fstruct='{$fStuct}'
WHERE img_id='{$datos['id']}';\n
SQL;
		//
	}
}

//
$response['d'] = "UPDATE DB STRUCTURE (code, structe, fname)\n\nEXCECUTED: ";

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;
