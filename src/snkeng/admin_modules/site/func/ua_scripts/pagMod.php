<?php
$upd_qry = <<<SQL
INSERT INTO sc_site_articles (
a_id, cat_id, img_id, art_id_parent, com_id,
art_dt_add, art_dt_mod, art_dt_pub, art_status_published,
art_order, art_type, art_type_secondary, art_lang, art_img_struct,
art_title, art_urltitle, art_url,
art_meta_word, art_meta_desc, art_content, art_content_extra, art_content_extra_b,
art_count_views, art_count_likes, art_count_dislikes
) VALUES\n
SQL;
$first = true;
//
$sql_qry = <<<SQL
SELECT
pgm.pgm_id,
pgm.pgm_dtadd, pgm.pgm_dtmod,
pgm.pgm_title, pgm.pgm_urltitle,
pgm.pgm_metawords,
pgm.pgm_metadesc,
pgm.pgm_content
FROM sc_site_pagmod AS pgm;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		$upd_qry.= ( $first ) ? '' : ",\n";
		$first = false;
		$datos['pgm_metawords'] = \snkeng\core\engine\mysql::real_escape_string($datos['pgm_metawords']);
		$datos['pgm_metadesc'] = \snkeng\core\engine\mysql::real_escape_string($datos['pgm_metadesc']);
		$datos['pgm_content'] = \snkeng\core\engine\mysql::real_escape_string($datos['pgm_content']);
		$upd_qry.= <<<SQL
(
'{$siteVars['user']['data']['id']}', 0, 0, 0, 0,
'{$datos['pgm_dtadd']}', '{$datos['pgm_dtadd']}', '{$datos['pgm_dtadd']}', '1',
'0', 'page', '', 'es_mx', '',
'{$datos['pgm_title']}', '', '{$datos['pgm_urltitle']}',
'{$datos['pgm_metawords']}', '{$datos['pgm_metadesc']}', '{$datos['pgm_content']}', '', '',
0, 0, 0
)
SQL;
	}
}
$upd_qry.= ";";

//
\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
	'errorKey' => 'admin',
	'errorDesc' => 'asdf'
]);

//
$response['d'] = $upd_qry;