<?php
//
$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
	ssd.docs_id AS id,
    ssi.img_fname AS imgFName, ssi.img_name_url AS imgUrl, ssi.img_name AS imgAlt, ssi.img_width AS imgWidth, ssi.img_height AS imgHeigth
FROM sc_site_documents AS ssd
INNER JOIN sc_site_images ssi ON ssd.img_id = ssi.img_id;
SQL;
//
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		//
		$upd_qry.= <<<SQL
UPDATE sc_site_documents SET docs_img_struct='{$datos['imgFName']}', docs_img_name_url='{$datos['imgUrl']}', docs_img_alt_text='{$datos['imgAlt']}', docs_img_width='{$datos['imgWidth']}', docs_img_height='{$datos['imgHeigth']}' WHERE docs_id='{$datos['id']}';\n
SQL;
		//
	}
}

$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;