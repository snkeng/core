<?php
//
$upd_qry = '';

//
$sql_qry = <<<SQL
SELECT
img.img_id AS id, img.img_floc AS floc
FROM sc_site_images AS img;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		$fileLoc = $_SERVER['DOCUMENT_ROOT'].$datos['floc'];
		$baseFile = strstr($datos['floc'], '.', true);
		$imageData = getimagesize($fileLoc);
		$imageSize = round(filesize($fileLoc) / 1024, 2);

		$upd_qry.= <<<SQL
UPDATE sc_site_images SET img_fstruct='{$baseFile}', img_fsize='{$imageSize}', img_width='{$imageData[0]}', img_height='{$imageData[1]}' WHERE img_id={$datos['id']};\n
SQL;
	}
}

//
\snkeng\core\engine\mysql::submitMultiQuery($upd_qry);

//
$response['d'] = $upd_qry;