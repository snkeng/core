<?php
//
$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
art.art_id AS id, art.art_urltitle AS urlTitle
FROM sc_site_articles AS art
WHERE art_type_primary='blog';
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		//
		$newTitle = stringPopulate($appData['settings']['blog']['postUrl'], [
			'id' => $datos['id'],
			'idPad' => str_pad($datos['id'], 4, "0", STR_PAD_LEFT),
			'urlTitle' => $datos['urlTitle']
		]);
		//
		$upd_qry.= <<<SQL
UPDATE sc_site_articles SET art_url='{$newTitle}' WHERE art_id='{$datos['id']}';\n
SQL;
	}
}

//
$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;
