<?php

$params = [];
$params['title'] = 'Sitio';
$params['menu'] = <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}">Inicio</a></div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">Blog</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/blog/posts/">Posts</a>
			<a se-nav="app_content" href="{$appData['url']}/blog/tags/">Tags</a>
			<a se-nav="app_content" href="{$appData['url']}/blog/categories/">Categorías</a>
			<a se-nav="app_content" href="{$appData['url']}/blog/images/">Imágenes</a>
			<a se-nav="app_content" href="{$appData['url']}/blog/diagrams/">Diagramas</a>
			<a se-nav="app_content" href="{$appData['url']}/blog/files/">Archivos</a>
		</div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">Documentación</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/docs/docs/">Documentación</a>
			<a se-nav="app_content" href="{$appData['url']}/docs/tags/">Tags</a>
			<a se-nav="app_content" href="{$appData['url']}/docs/categories/">Categorías</a>
		</div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">Páginas simples</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/pages/content/">Páginas</a>
			<a se-nav="app_content" href="{$appData['url']}/pages/tags/">Tags</a>
			<a se-nav="app_content" href="{$appData['url']}/pages/categories/">Categorías</a>
			<a se-nav="app_content" href="{$appData['url']}/pages/images/">Imágenes</a>
			<a se-nav="app_content" href="{$appData['url']}/pages/files/">Archivos</a>
		</div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/gallery/">Galería</a></div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/banners/">Banners</a></div>
	</div>\n
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/contact/">Contact</a></div>
	</div>\n
HTML;

if ( \snkeng\core\engine\login::check_loginLevel('sadmin') ) {
	$params['menu'].= <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">S. Admin.</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/sadmin/pages/">Páginas</a>
			<a se-nav="app_content" href="{$appData['url']}/sadmin/texts/">Textos editables</a>
			<a se-nav="app_content" href="{$appData['url']}/sadmin/files/">Archivos</a>
			<a se-nav="app_content" href="{$appData['url']}/sadmin/images/">Imágenes</a>
			<a se-nav="app_content" href="{$appData['url']}/sadmin/categories/">Categorías</a>
			<a se-nav="app_content" href="{$appData['url']}/sadmin/tags/">Tags</a>
		</div>
	</div>\n
HTML;
}

if ( \snkeng\core\engine\login::check_loginLevel('uadmin') ) {
	$params['menu'].= <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">U. Admin.</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/uadmin/">Empty</a>
			<a se-nav="app_content" href="{$appData['url']}/uadmin/scripts/">Scripts</a>
		</div>
	</div>\n
HTML;
}
return $params;
