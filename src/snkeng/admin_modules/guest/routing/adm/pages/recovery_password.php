<?php
// GUEST PASSWORD RECOVERY UPDATE




//debugVariable($params['vars']['linkData']['status']);

if ( $params['vars']['linkData']['status']['expired'] || !$params['vars']['linkData']['status']['valid'] ) {
	//
	$page['body'] = <<<HTML
<div class="gOMB">
	Vínculo expirado. Si lo desea puede iniciar el proceso de nuevo.
</div>

<a class="btn wide" se-nav="se_middle" href="/guest/recovery/">Recuperar contraseña</a>\n
HTML;
	//
} else {
	//
	$page['body'] = <<<HTML
<h2>Escriba una nueva contraseña</h2>
<form class="tabLessForm" id="acc_recovery_2" action="/api/user/recovery/validate">
	<input type="hidden" name="key" value="{$params['vars']['key']}" />
	<input type="password" name="uPass1" class="elem" minlength="8" maxlength="32" title="Contraseña" required placeholder="Nueva contraseña" />
	<input type="password" name="uPass2" class="elem" minlength="8" maxlength="32" title="Reescriba su contraseña." required placeholder="Confirmar contraseña" />
	<div class="elem"><button class="btn blue wide" type="submit">Crear una nueva contraseña</button></div>
	<output se-elem="response"></output>
</form>\n
HTML;
	//


	//
	$page['mt'] .= <<<JS
$('#acc_recovery_2').se_plugin('simpleForm', {
	onSuccess:() => {
		setTimeout(
			() => {
		    	se.ajax.pageLink('/admin', 'se_template_root');
		    },
		    3000
		);
		
	}
});
JS;
	//
}
//

//
$page['body'] = <<<HTML
<style>
.container { display:flex; align-items:center; justify-content:center; height:100vh; width:100vw; }
.microPage { background-color:#FFF; border:thin solid #999; border-radius:5px; width:500px; box-shadow:0 3px 5px #e2e2e2; }
.microPage > .fullTitle { padding:10px; margin:0; color:#FFFFFF; background-color:#999999; font-size:1.8rem; font-weight:bold; }
.microPage > .content { padding:10px; }
/* */
@media only screen and (max-width:767px) {
	.container { display:block; }
	#microPage { display:block; margin:10px auto; width:calc(100% - 20px); }
}
</style>
<div class="microPage">
	<div class="fullTitle">Recuperación de contraseña</div>
	<div class="content">
		{$page['body']}
	</div>
</div>
HTML;
//
