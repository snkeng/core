<?php
$header = [];
$header['title'] = 'Super administración';

//
$header['menu'] = <<<HTML
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}">Inicio</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/modules/">Módulos</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/tables/">Tablas</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">Entorno</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/env/envvar/">Variables</a>
		<a se-nav="app_content" href="{$appData['url']}/env/errors/">Errores</a>
		<a se-nav="app_content" href="{$appData['url']}/env/apache_errors/">Apache errores</a>
	</div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">Contenido</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/content/icons_add/">Iconos Agregar</a>
		<a se-nav="app_content" href="{$appData['url']}/content/icons_process/">Iconos Procesar</a>
		<a se-nav="app_content" href="{$appData['url']}/content/minify/">Minify</a>
	</div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">SVG Icons</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/svg-icons/groups/">Groups</a>
		<a se-nav="app_content" href="{$appData['url']}/svg-icons/libraries/">Libraries</a>
	</div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">SQL</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/sql/direct/">Querys directos</a>
		<a se-nav="app_content" href="{$appData['url']}/sql/structure/">Estructura</a>
		<a se-nav="app_content" href="{$appData['url']}/sql/ops/">Operaciones</a>
		<a se-nav="app_content" href="{$appData['url']}/sql/tables/">Tablas</a>
		<a se-nav="app_content" href="{$appData['url']}/sql/backup/">Backup</a>
	</div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">Sistema</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/system/scripts/">Scripts</a>
		<a se-nav="app_content" href="{$appData['url']}/system/dbstruct/">Estructura DB</a>
		<a se-nav="app_content" href="{$appData['url']}/system/coreupdate/">Actualizar core</a>
		<a se-nav="app_content" href="{$appData['url']}/system/console/">Consola</a>
		<a target="_blank" href="{$appData['url']}/system/phpinfo/">PHP INFO</a>
	</div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><span class="text">External</span></div>
	<div class="nav3_menu_hidden">
		<a se-nav="app_content" href="{$appData['url']}/external/sync/">Sync</a>
		<a se-nav="app_content" href="{$appData['url']}/external/sql/">SQL</a>
		<a se-nav="app_content" href="{$appData['url']}/external/install/">Instalación</a>
	</div>
</div>\n
HTML;
//


//
return $header;
