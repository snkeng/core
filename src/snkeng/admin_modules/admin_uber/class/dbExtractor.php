<?php
//
namespace snkeng\admin_modules\admin_uber;

//
class dbExtractor
{
	//
	// INI: Variables
	public $mysql;
	public $siteVars;
	
	public $tables = [];
	public $getContent = false;
	
	public $fileName = "";
	public $txtFile = "";

	// Reporte
	private $tableCount = 0;
	private $tableSavedNames = array();

	// END: Variables
	//

	//
	// INI: construct
	public function __construct()
	{
		global $siteVars;
		$this->siteVars =& $siteVars;
	}
	// END: construct
	//

	//
	public function fullBackup()
	{
		$sql_qry = "SHOW TABLES;";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$this->tables[] = $datos[0];
			}
		}
		$this->runBackup("Full");
	}

	//
	public function structureFull()
	{
		$dataBase = \snkeng\core\engine\mysql::getDatabase();
		// Filtro para sólo seleccionar las que inicien con sa (las únicas oficialmente editables)
		$sql_qry = <<<SQL
SHOW TABLES
FROM `{$dataBase}`
WHERE `Tables_in_{$dataBase}` LIKE 'sa_%'
    OR `Tables_in_{$dataBase}` LIKE 'sb_%'
    OR `Tables_in_{$dataBase}` LIKE 'sc_%';
SQL;
		//
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$this->tables[] = $datos[0];
			}
		}
		$this->runBackup("Structure");
	}

	//
	public function structure()
	{
		$dataBase = \snkeng\core\engine\mysql::getDatabase();
		// Filtro para sólo seleccionar las que inicien con sa (las únicas oficialmente editables)
		/*$sql_qry = <<<SQL
SHOW TABLES
FROM `{$dataBase}`
WHERE `Tables_in_{$dataBase}` LIKE 'sa_%'
    OR `Tables_in_{$dataBase}` LIKE 'sb_%'
    OR `Tables_in_{$dataBase}` LIKE 'sc_%';
SQL;
		*/
		//
		$sql_qry = <<<SQL
SHOW TABLES
FROM `{$dataBase}`
WHERE `Tables_in_{$dataBase}` LIKE 'sa_%';
SQL;
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$this->tables[] = $datos[0];
			}
		}
		$this->runBackup("Structure");
	}

	//
	public function runBackup($header)
	{	
		$this->createHeader($header);
		foreach ( $this->tables as $table )
		{
			$this->extractTable($table, true);
		}
		$this->createReport();
	}

	//
	public function saveData($compression) {
		$this->printData($compression);
	}

	//
	// INI: createHeader
	//	
	private function createHeader($opType)
	{
		// Información sobre el archivo
		$info['fecha'] = date("Y-m-d H:m:s");
		$info['mysqlver'] = \snkeng\core\engine\mysql::server_info();
		$info['phpver'] = phpversion();
		$info['nLong'] = $this->siteVars['site']['name'];
		$info['nShort'] = \snkeng\core\general\saveData::buildFriendlyTitle($this->siteVars['site']['name']);
		$info['engVer'] = \snkeng\core\engine\siteVars::read("engVer");
		$info['sqlRev'] = \snkeng\core\engine\siteVars::read("sqlRev");
		
		$serverMode = ( $this->siteVars['server']['test'] ) ? 'test' : 'live';
		$opType = \snkeng\core\general\saveData::buildFriendlyTitle($opType);
		$saveDate = date('Y-m-d_H-m-s');
		
		$this->fileName = "{$info['nShort']}_{$serverMode}_{$opType}_{$saveDate}";
$this->txtFile.= <<<TXT
# +--------------------------------------------------------------------------------
# | BACKUP DB - {$info['nLong']}
# | Server - {$serverMode}
# | Op Type - {$opType}
# | Generado - {$info['fecha']}
# | Servidor: {$_SERVER['HTTP_HOST']}
# | SNK Engine V.: {$info['engVer']}
# | SNK SQL R.: {$info['sqlRev']}
# | MySQL Version: {$info['mysqlver']}
# | PHP Version: {$info['phpver']}
# +--------------------------------------------------------------------------------\n

# FIX
SET FOREIGN_KEY_CHECKS=0; 
TXT;
		//
	}
	// END: createReport
	//

	//
	// INI: extractTable
	//	
	private function extractTable($tabName, $getContent)
	{
		$preg_original = [
			"/[\s]+/", // Spaces
			"/ DEFAULT CHARSET=[\w]*/",
			"/ COLLATE=[\w]*/",
			"/ COLLATE [\w]*/"
		];
		$preg_replace = [
			' ',
			'',
			'',
			''
		];
		$select = [",", "` ( ", ") ENGINE"];
		$change = [",\r\n", "`\r\n(\r\n ", "\r\n)\r\nENGINE"];

		//
		$insert_into_query = "";
		
		// Obtener información de las tablas
		$create_table_query = "";
		$sql_qry = "SHOW CREATE TABLE {$tabName};";
		// Tabla válida agregar al reporte
		$this->tableSavedNames[] = $tabName;
		$this->tableCount++;

		$tabData = \snkeng\core\engine\mysql::singleRow($sql_qry);

		$text = preg_replace($preg_original, $preg_replace, $tabData[1]);
		$text = str_replace($select, $change, $text);
		$text = trim($text);
		//
		$create_table_query = "$text;";
		
		// Guardar tabla
$this->txtFile.= <<<EOD


# +------------------------------------->
# | TABLA '{$tabName}':
# +------------------------------------->
# |
# | ESTRUCTURA '{$tabName}'
# |
DROP TABLE IF EXISTS {$tabName};
{$create_table_query}\n
EOD;
		// Si requiere incluir contenido
		if ( $getContent )
		{
			// Generar query de llenado de datos
			$sql_qry = "SELECT * FROM $tabName;";
			if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
			{
				$insert_into_query.= "INSERT INTO `$tabName` VALUES \n";
				$first = true;
				$counter1 = 0;
				$counter2 = 1;
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_row() )
				{
					if ( $counter1 > 249 )
					{
						$insert_into_query.= ";\n#\n# Break {$counter2}\n#\n";
						$insert_into_query.= "INSERT INTO `$tabName` VALUES \n";
						$counter1 = 0;
						$counter2++;
						$first = true;
					}
					$ammount = sizeof($datos);
					for ( $i=0; $i<$ammount; $i++ )
					{
						if ( gettype($datos[$i]) == "NULL" )
						{
							$values[] = "NULL";
						} else {
							$values[] = "'". \snkeng\core\engine\mysql::real_escape_string($datos[$i])."'";
						}
					}
					if ( $first )
					{
						$first = false;
					} else {
						$insert_into_query.= ",\n";
					}
					$insert_into_query.= "(".implode(", ", $values).")";
					$counter1++;
					unset($values);
				}
				$insert_into_query.= ";";
			}
// Guardar los objetos.
$this->txtFile.= <<<EOD
# |
# | DATOS '{$tabName}'
# |
{$insert_into_query}\n
EOD;
		}
	}
	// END: extractTable
	//

	//
	// INI: createReport
	//	
	private function createReport()
	{
		$tablesSuccess = "";
		$first = true;
		$count = 0;
		foreach ( $this->tableSavedNames as $table )
		{
			$tablesSuccess.= "\r\n# |\t".$table;
			$count++;
		}
$this->txtFile.= <<<EOD
# END OF FIX
SET FOREIGN_KEY_CHECKS=1;


# +--------------------------------------------------------------------------------
# | Reporte final
# | Tablas Aprovadas: {$this->tableCount}
# | Tablas:
# | ---{$tablesSuccess}
# | ---
# |
# +--------------------------------------------------------------------------------
EOD;
	}
	// END: createReport
	//

	//
	public function getData() {
		$select = ["\r", "\n"];
		$change = ["", "\r\n"];
		$this->txtFile = str_replace($select, $change, $this->txtFile);
		return $this->txtFile;
	}

	//
	// INI: printData
	//	
	private function printData( $compression = '', $printDirect = false )
	{
		$select = ["\r", "\n"];
		$change = ["", "\r\n"];
		$this->txtFile = str_replace($select, $change, $this->txtFile);
		// Salvado de la información.
		if ( !headers_sent() )
		{
			header("Pragma: no-cache");
			header("Expires: 0");
			header("Content-Transfer-Encoding: binary");
			switch ( $compression )
			{
				case 'gz':
					header("Content-Disposition: attachment; filename=".$this->fileName.".gz");
					header("Content-type: application/x-gzip");
					echo \gzencode($this->txtFile, 9);
					break;
				case 'bz2': 
					header("Content-Disposition: attachment; filename=".$this->fileName.".bz2");
					header("Content-type: application/x-bzip2");
					echo \bzcompress($this->txtFile, 9);
					break;
				default:
					if ( !$printDirect ) {
						header("Content-Disposition: attachment; filename=" . $this->fileName . ".txt");
						header("Content-type: application/force-download");
					} else {
						header('Content-Type: text/plain; charset=utf-8');
					}
					echo $this->txtFile;
			}
			exit();
		} else {
			echo "<b>ATENCION: Probablemente ha ocurrido un error</b><br />\n<pre>\n".$this->txtFile."\n</pre>";
		}
	}
	// END: printData
	//
}
//
