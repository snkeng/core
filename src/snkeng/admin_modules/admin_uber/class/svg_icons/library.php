<?php
namespace snkeng\admin_modules\admin_uber\svg_icons;

// Icon management
class library
{
	public static function copy($fileName, $list) {
		// Directories
		$dir_libraries  = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/libraries';
		$dir_final      = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final';

		echo "MOVE FILES {$_SERVER['DOCUMENT_ROOT']}\n";

		//
		foreach ( glob($dir_libraries."/*.svg") as $file ) {
			echo "FILE:$file.\n";

			$svgFileNameFinal = $dir_final . '/' . basename($file, ".svg");
			$svgData = file_get_contents($file);
			// Save normal
			file_put_contents($svgFileNameFinal . '.svg', $svgData);
			// gZip
			$fileZip = gzencode($svgData, 9);
			// Save Zip
			file_put_contents($svgFileNameFinal . '.svgz', $fileZip);
		}
	}
}
//
