<?php
//
namespace snkeng\admin_modules\admin_uber;

//
class module_manager
{
	public $mysql;

	// INI: construct
	public function __construct()
	{
	}
	// END: construct

	// INI: moduleExtract
	//
	public function moduleExtract($modId, $test = false) {
		// Module data
		$sql_qry = "SELECT mod_name, mod_ver, mod_desc FROM sa_modules WHERE mod_id={$modId};";
		$modData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		$qry = <<<SQL
# ---
# Module {$modData['mod_name']}
INSERT INTO sa_modules (mod_name, mod_ver, mod_desc) VALUES('{$modData['mod_name']}', '{$modData['mod_ver']}', '{$modData['mod_desc']}');
SET @modId=LAST_INSERT_ID();\n\n
SQL;
		// Tables
		$tabIds = [];
		$tabRow = <<<TEXT
(!t_id; now(), '!t_name;', '!t_desc;', '!t_dbname;', '!t_dbnick;', '!t_dbtype;')
TEXT;

		$sql_qry = "SELECT t_id, t_name, t_desc, t_dbname, t_dbnick, t_dbtype FROM sa_table_object WHERE mod_id={$modId} ORDER BY t_name ASC;";
		$tabData = \snkeng\core\engine\mysql::returnArray($sql_qry);
		//
		foreach ( $tabData AS $cTable ) {
			//
			$qry.= <<<SQL
# -----------------------
# Table {$cTable['t_name']}
INSERT INTO sa_table_object (mod_id, t_dt_add, t_name, t_desc, t_dbname, t_dbnick, t_dbtype)
VALUES (@modId, now(), '{$cTable['t_name']}', '{$cTable['t_desc']}', '{$cTable['t_dbname']}', '{$cTable['t_dbnick']}', '{$cTable['t_dbtype']}');
SET @tabId=LAST_INSERT_ID();
# Elements
INSERT INTO sa_table_column (
t_id, te_dt_add, te_name, te_desc, te_pos,
te_db_name, te_db_type, te_db_typedesc, te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default, te_db_fulltext,
te_d_nick, te_d_optional, te_d_special, te_d_parenttable, te_f_minl, te_f_maxl, te_f_maxli
) VALUES
SQL;

			// Elements
			$sql_qry = <<<SQL
SELECT
t_id, te_dt_add, te_name, te_desc, te_pos,
te_db_name, te_db_type, te_db_typedesc, te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default, te_db_fulltext,
te_d_nick, te_d_optional, te_d_special, te_d_parenttable,
te_f_minl, te_f_maxl, te_f_maxli
FROM sa_table_column
WHERE t_id={$cTable['t_id']}
ORDER BY te_pos ASC;
SQL;
			$first = true;
			if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
			{
				while ( $data = \snkeng\core\engine\mysql::$result->fetch_assoc() )
				{
					$qry.= ($first) ? "" : ",";
					$first = false;
					$qry.= <<<SQL
\n(@tabId, now(), '{$data['te_name']}', '{$data['te_desc']}', '{$data['te_pos']}',
'{$data['te_db_name']}', '{$data['te_db_type']}', '{$data['te_db_typedesc']}', '{$data['te_db_notnull']}', '{$data['te_db_unsigned']}', '{$data['te_db_autoinc']}', '{$data['te_db_prikey']}', '{$data['te_db_default']}', '{$data['te_db_fulltext']}',
'{$data['te_d_nick']}', '{$data['te_d_optional']}', '{$data['te_d_special']}', '{$data['te_d_parenttable']}', '{$data['te_f_minl']}', '{$data['te_f_maxl']}', '{$data['te_f_maxli']}')
SQL;
				}
				$qry.= ";\n\n";
			} else {
				\snkeng\core\engine\mysql::killIfError("No se pudieron leer las tablas.", "full");
			}
		}

		//
		if ( $test ) {
			header('Content-Type: text/plain; charset=utf-8');
		} else {
			header("Pragma: no-cache");
			header("Expires: 0");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=" . $modData['mod_name'] . ' - ' . $modData['mod_ver'] . ".txt");
			header("Content-type: application/force-download");
		}
		echo $qry;
		exit();
	}
	// END: moduleExtract
}
