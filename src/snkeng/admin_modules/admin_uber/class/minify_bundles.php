<?php
//
namespace snkeng\admin_modules\admin_uber;

//
use MatthiasMullie\Minify;

//
class minify_bundles
{
	private static $files_end = [];
	private static $rPathLen = [];
	private static $minFolder = [];
	private static $sPath = [];

	private static $debug = false;

	//
	private static function getFileProps(&$fileExt, $cFile)
	{
		if ( !file_exists($cFile) ) {
			echo "MINIFY FILE NOT FOUND (SKIPPING): $cFile.\n";
			return;
		}

		$fileExt['lastMod'] = max($fileExt['lastMod'], filemtime($cFile));
		$fileExt['files'][] = $cFile;
		$fileExt['fileCount']++;
	}

	// Extract viable files
	private static function getFilesFromFile($fileName)
	{
		$fileList = file($fileName);
		foreach ( $fileList as &$cFile ) {
			$cFile = $_SERVER['DOCUMENT_ROOT'] . trim($cFile);
			if ( !file_exists($cFile) ) {
				\snkeng\core\engine\nav::killWithError('archivo no encontrado', "REF: {$fileName}. Searched: $cFile");
			}
		}

		return $fileList;
	}

	// Direct folder adding
	private static function processGroupFolder(
		bool $isFolder,
		string $groupFolder, string $groupName,
		array $cssFiles, array $jsFiles, string $svgFile, string $svgzFile,
		array $modules = []
	)
	{
		//
		self::$files_end[$groupName]['data'] = [
			'isFolder' => $isFolder,
			'origin' => $groupFolder
		];

		//
		$cFileData = [
			'css' => [
				'lastMod' => 0,
				'fileCount' => 0,
				'files' => []
			],
			'js' => [
				'lastMod' => 0,
				'fileCount' => 0,
				'files' => []
			],
			'svg' => [],
			'svgz' => [],
			'modules' => []
		];

		//
		foreach ( $jsFiles as $cFile ) {
			self::getFileProps($cFileData['js'], $cFile);
		}
		//
		foreach ( $cssFiles as $cFile ) {
			self::getFileProps($cFileData['css'], $cFile);
		}

		// Now actually check for the created groups
		foreach ( $cFileData as $cFileType => $cFileDatum ) {
			//
			switch ( $cFileType ) {
				//
				case 'modules':
					foreach ( $modules as $fileList ) {
						$fileType = pathinfo($fileList[2], PATHINFO_EXTENSION);
						//
						switch ( $fileList[0] ) {
							//
							case 'site_modules':
								self::$files_end[$groupName]['files']['modules'][] = [
									$fileType,
									'/snkeng/site_modules/' . $fileList[1] . '/res/modules' . $fileList[2],
									'/res/modules/site_modules/' . $fileList[1] . $fileList[2]
								];
								break;
							//
							case 'admin_modules':
								self::$files_end[$groupName]['files']['modules'][] = [
									$fileType,
									'/snkeng/admin_modules/' . $fileList[1] . '/res/modules' . $fileList[2],
									'/res/modules/admin_modules/' . $fileList[1] . $fileList[2]
								];
								break;
							//
							case 'site_core':
								self::$files_end[$groupName]['files']['modules'][] = [
									$fileType,
									'/snkeng/site_core/res/modules' . $fileList[2],
									'/res/modules/site_core' . $fileList[2]
								];
								break;
							//
							case 'admin_core':
								self::$files_end[$groupName]['files']['modules'][] = [
									$fileType,
									'/snkeng/admin_core/res/modules' . $fileList[2],
									'/res/modules/admin_core' . $fileList[2]
								];
								break;
							//
							case 'core':
								self::$files_end[$groupName]['files']['modules'][] = [
									$fileType,
									'/snkeng/core/res/modules' . $fileList[2],
									'/res/modules/core' . $fileList[2]
								];
								break;
						}
					}
					break;

				//
				case 'css':
				case 'js':
					// Check if exist/modified continue if same
					if (
						isset(self::$files_end[$groupName][$cFileType]) &&
						self::$files_end[$groupName][$cFileType]['utime'] === $cFileDatum['lastMod'] &&
						self::$files_end[$groupName][$cFileType]['count'] === $cFileDatum['fileCount']
					) {
						break;
					}

					// Skip for empty css and js in min_json format
					if ( !$isFolder && $cFileDatum['fileCount'] === 0 ) {
						break;
					}

					// Clean len
					$cleanLen = strlen($_SERVER['DOCUMENT_ROOT'] );

					// Modify last file name (htaccess -> mod_alias)
					$realFiles = [];
					$fileAlias = [];
					foreach ( $cFileDatum['files'] as $cFile ) {
						$realFiles[] = $cFile;

						// Remove initial part, and res and add mod_alias information
						$nFile = substr($cFile, $cleanLen);

						// Add
						$fileAlias[] = $nFile;
					}

					// New current definition
					self::$files_end[$groupName]['files'][$cFileType] = [
						"mod" => date("c", $cFileDatum['lastMod']),
						"utime" => $cFileDatum['lastMod'],
						"count" => $cFileDatum['fileCount'],
						"fName" => "/res/bundles/{$groupName}." . date("ymd-His", $cFileDatum['lastMod']) . ".{$cFileType}.gz",
						"rfName" => self::$minFolder . "{$groupName}.{$cFileType}.gz",
						"files" => $fileAlias
					];

					// Now group
					if ( $cFileType === 'css' ) {
						$minifier = new Minify\CSS();
					} else {
						$minifier = new Minify\JS();
					}

					// Add real file to group
					foreach ( $cFileDatum['files'] as $cFile ) {
						$minifier->add($realFiles);
					}

					// Directly write data
					file_put_contents(self::$sPath . $groupName . "." . $cFileType . '.gz', $minifier->gzip());

					unset($minifier);
					break;

				//
				case 'svg':
					if ( empty($svgFile) || !file_exists($_SERVER['DOCUMENT_ROOT'] . $svgFile) ) {
						break;
					}

					$newFileName = "/se_files/site_generated/svg-icons/final/{$groupName}.svgz";

					// Open file, compressed and save in new location
					$svgFileData = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $svgFile);
					$svgCompressed = \gzencode($svgFileData, 9);
					file_put_contents($_SERVER['DOCUMENT_ROOT'] . $newFileName, $svgCompressed);

					//
					$lastMod = filemtime($_SERVER['DOCUMENT_ROOT'] . $svgFile);
					$lastModTxt = date("ymd-His", $lastMod);

					// Save by mod data
					self::$files_end[$groupName]['files']['svgz'] = [
						"fName" => "/res/svg-icons/{$groupName}.{$lastModTxt}.svgz",
						"rfName" => $newFileName,
						"oName" => $svgFile,
						"mod" => $lastModTxt,
						"utime" => $lastMod
					];
					break;

				//
				case 'svgz':
					if ( empty($svgzFile) ) {
						break;
					}

					$newFileName = "/se_files/site_generated/svg-icons/final/{$groupName}.svgz";

					// Copy to target destination with target name
					copy($_SERVER['DOCUMENT_ROOT'] . $svgzFile, $_SERVER['DOCUMENT_ROOT'] . $newFileName);

					//
					$lastMod = filemtime($_SERVER['DOCUMENT_ROOT'] . $svgzFile);
					$lastModTxt = date("ymd-His", $lastMod);

					// Save by mod data
					self::$files_end[$groupName]['files']['svgz'] = [
						"fName" => "/res/svg-icons/{$groupName}.{$lastModTxt}.svgz",
						"rfName" => $newFileName,
						"oName" => $svgzFile,
						"mod" => $lastModTxt,
						"utime" => $lastMod
					];
					break;
			}
		}
	}

	//
	static function exec(&$response, $rebuild = false, $purge = false)
	{
		// Directories
		self::$rPathLen = strlen($_SERVER['DOCUMENT_ROOT']); // for cleaning final path
		self::$minFolder = '/se_files/site_generated/bundles/';
		self::$sPath = $_SERVER['DOCUMENT_ROOT'] . self::$minFolder;

		//
		if ( !file_exists(self::$sPath) ) {
			mkdir(self::$sPath, 0700, true);
		}

		//
		if ( !is_writable(self::$sPath) ) {
			\snkeng\core\engine\nav::killWithError('No se pueden agregar los archivos.', '');
			return;
		}

		// Delete all content of original compressed files
		if ( $rebuild ) {
			$files = glob(self::$sPath . '*'); // get all file names
			foreach ( $files as $file ) { // iterate files
				if ( is_file($file) ) {
					// echo "DELETING: {$file}.\n";
					unlink($file); // delete file
				}
			}
		}

		//
		$save_file = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles/files.json';
		self::$files_end = [];

		// Check if file exists and accesible
		if ( \file_exists($save_file) && !$purge ) {
			self::$files_end = \json_decode(file_get_contents($save_file), true);
		}

		//
		$debug = '';


		// Intentar leer posibles apps generales


		// Default folders (bundles in core and site)
		$finalFolders = [
			['site_core',   $_SERVER['DOCUMENT_ROOT'] . "/snkeng/site_core/res/bundles/"],
			['admin_core',  $_SERVER['DOCUMENT_ROOT'] . "/snkeng/admin_core/res/bundles/"],
			['engine_core', $_SERVER['DOCUMENT_ROOT'] . "/snkeng/core/res/bundles/"],
		];

		// App folders check (v2, allow creation of file groups by structure, subfolders in /res within apps and site / core)

		//
		$scanFolders = [
			$_SERVER['DOCUMENT_ROOT'] . "/snkeng/site_modules/",
			$_SERVER['DOCUMENT_ROOT'] . "/snkeng/admin_modules/",
		];

		// Scan for modules and add to final folders
		foreach ( $scanFolders as $cScanFolder ) {
			if ( !file_exists($cScanFolder) ) {
				continue;
			}

			// Add sub folder

			//
			foreach ( scandir($cScanFolder) as $cAppName ) {
				//
				$appFolder = $cScanFolder . $cAppName . "/res/bundles/";

				// Skip non folders
				if ( !is_dir($cScanFolder . $cAppName) || in_array($cAppName, ['.', '..']) || !file_exists($appFolder) ) {
					continue;
				}

				// Folder has sub folders
				$finalFolders[] = [$cAppName, $appFolder];
			}
		}

		// Process all candidate folders for bundles (are sub folders)
		foreach ( $finalFolders as $cRow ) {
			//
			$cAppName = $cRow[0];
			$cFolder = $cRow[1];

			//
			\snkeng\core\engine\serverSideEvent::sendMessage("Checking: {$cAppName} - {$cFolder}.");

			//
			if ( !is_dir($cFolder) ) {
				\snkeng\core\engine\serverSideEvent::sendMessage("INVALID FOLDER: {$cFolder}.");
				continue;
			}


			// Check res folders
			foreach ( scandir($cFolder) as $cGroupName ) {
				// Skip non folders
				if ( !is_dir($cFolder . $cGroupName) || in_array($cGroupName, ['.', '..']) ) {
					continue;
				}

				// Reset css/js counters
				$cssFiles = [];
				$jsFiles = [];
				$moduleFiles = [];
				$svgFile = '';
				$svgzFile = '';

				// Current group folder
				$groupFolder = $cFolder . $cGroupName . '/';

				// Read files in folder
				foreach ( scandir($groupFolder) as $cFile ) {
					// Skip non files
					if ( !is_file($groupFolder . $cFile) ) {
						continue;
					}

					// Current file location (full path)
					$cFileLoc = $groupFolder . $cFile;

					// Get file extention
					$array = explode('.', $cFile);
					$fileExt = strtolower(end($array));

					// Process file extention
					switch ( $fileExt ) {
						//
						case 'js':
							$jsFiles[] = $cFileLoc;
							break;
						//
						case 'css':
							$cssFiles[] = $cFileLoc;
							break;
						//
						case 'txt':
							switch ( $cFile ) {
								case 'css.txt':
									$cssFiles = array_merge(self::getFilesFromFile($cFileLoc), $cssFiles);
									break;
								case 'js.txt':
									$jsFiles = array_merge(self::getFilesFromFile($cFileLoc), $jsFiles);
									break;
							}
							break;
						//
						case 'svg':
							if ( !empty($svgFile) ) {
								break;
							}

							//
							$svgFile = $cFileLoc;
							break;
						//
						case 'svgz':
							if ( !empty($svgzFile) ) {
								break;
							}

							//
							$svgzFile = $cFileLoc;
							break;
					}
				}

				//
				$groupName = "{$cAppName}_{$cGroupName}";

				//
				$groupFolder = substr($groupFolder, self::$rPathLen);

				//
				self::processGroupFolder(true, $groupFolder, $groupName, $cssFiles, $jsFiles, $svgFile, $svgzFile);
			}
		}

		//
		// "min_files.json" files analysis (loose files)
		//

		// Programa
		$files = [];

		//
		$systemFileList = [
			'/snkeng/site_core/res/min_files.json',
			'/snkeng/admin_core/res/min_files.json',
			'/snkeng/core/res/ext_libraries/min_files.json'
		];


		//
		foreach ( $systemFileList as $cFile ) {
			if ( !file_exists($_SERVER['DOCUMENT_ROOT'] . $cFile) ) {
				continue;
			}

			$files[] = [
				$cFile,
				json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $cFile), true)
			];
		}

		// files empty?
		if ( $files == null ) {
			var_dump($files, "die:" . json_last_error_msg() . ". ");
			exit();
		}

		// Add detected min_files
		foreach ( $scanFolders as $cScanFolder ) {
			foreach ( glob($cScanFolder . "*/res/min_files.json") as $filename ) {
				//
				$files[] = [
					$filename,
					json_decode(file_get_contents($filename), true)
				];
			}
		}

		// Process files structure
		foreach ( $files as $fileInfo ) {
			foreach ( $fileInfo[1] as $groupName => $file ) {
				if ( !isset($file['content']) ) {
					echo "INVALID FILE STRUCURE (NO CONTENT FOUND).\n{$fileInfo[0]} - {$groupName}\n";
					continue;
				}

				//
				$newFiles['css'] = $file['content']['css'] ?? [];
				$newFiles['js'] = $file['content']['js'] ?? [];
				$moduleList = $file['content']['module'] ?? [];
				$file['content']['svg'] = $file['content']['svg'] ?? "";
				$file['content']['svgz'] = $file['content']['svgz'] ?? "";

				// Set full folders for sub folder
				foreach ( $newFiles as $fileType => &$cFiles ) {
					$cFiles = array_map(
						function ($fileName) {
							return $_SERVER['DOCUMENT_ROOT'] . $fileName;
						},
						$cFiles
					);
				}

				//
				self::processGroupFolder(false, $fileInfo[0], $groupName, $newFiles['css'], $newFiles['js'], $file['content']['svg'], $file['content']['svgz'], $moduleList);
			}
		}

		// Final savings
		$fileContents = \json_encode(self::$files_end, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

		$response['d']['ops'] = $debug;
		$response['d']['file'] = $fileContents;

		// Save
		\file_put_contents($save_file, $fileContents);

		if ( $rebuild ) {
			// echo "SISTEMA ACTUALIZADO (full refresh):\n\n";
			// \print_r(self::$files_end);
		}
	}
}
//
