<?php
//
namespace snkeng\admin_modules\admin_uber;

//
use MatthiasMullie\Minify;

//
class minify_modules
{
	//
	private static function folder_processing_modules(string $devRootFolder, string $targetLocation, string $moduleName, bool $checkDependencies)
	{
		global $rebuild;

		// Real development folder
		$realDevFolder = $_SERVER['DOCUMENT_ROOT'] . $devRootFolder;
		$realDevFolderLenght = strlen($realDevFolder);

		// Real production folder
		// Adjust if module present
		if ( $moduleName !== '' ) {
			$targetLocation = str_replace("!moduleName;", $moduleName, $targetLocation);
		}
		//
		$targetFolderLocation = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/modules' . $targetLocation . '/';

		// Check empty folder
		if ( !is_dir($realDevFolder) ) {
			return;
		}

		// Search files within dev folder
		$recursiveIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($realDevFolder));

		// Process each found file
		foreach ( $recursiveIterator as $file ) {
			// Ignore folders
			if ( $file->isDir() ) {
				continue;
			}

			// File name
			$cFile = $file->getPathname();

			// Short folder location (equal in both environments)
			$realFileName = substr($cFile, $realDevFolderLenght);

			//
			// echo "MODULE: {$moduleName}. FILE: {$realFileName}. ";

			// Check if file exists in production and if is newer (continue if true)
			if ( !$rebuild && file_exists($targetFolderLocation . $realFileName) && filemtime($cFile) < filemtime($targetFolderLocation . $realFileName) ) {
				// echo "Skipped.\n";
				continue;
			}

			// Check if folder exists
			$dirName = dirname($targetFolderLocation . $realFileName);
			if ( !is_dir($dirName) ) {
				mkdir($dirName, 0777, true);
			}

			// Analize file extention
			$ext = pathinfo($realFileName, PATHINFO_EXTENSION);
			switch ( $ext ) {
				//
				case 'mjs':
					$minifier = new Minify\JS();
					break;
				//
				case 'css':
					$minifier = new Minify\CSS();
					break;
				//
				default:
					// echo "Not valid type: {$ext}.\n";
					continue 2;
			}

			//
			if ( $checkDependencies ) {
				// Get file
				$fileContents = file_get_contents($cFile);

				// Update dependencies
				$fileContents = str_replace(
					['/snkeng/site_core/res/modules/', '/snkeng/core/res/modules/', '/snkeng/admin_core/res/modules/'],
					['/res/modules/site_core/', '/res/modules/core/', '/res/modules/admin_core/'],
					$fileContents
				);

				// Add edited CSS
				$minifier->add($fileContents);
			} else {
				$minifier->addFile($cFile);
			}

			// Save file
			file_put_contents($targetFolderLocation . $realFileName, $minifier->gzip());

			//
			unset($minifier);

			// echo "OK.\n";
		}
	}

	//
	static function exec(&$response, $rebuild = false)
	{
		$file_list = [];

		//
		$targetList = [
			'core' => [
				'dev' => '/snkeng/core/res/modules',
				'prod' => '/core',
				'module' => false,
				'dependencies' => false,
			],
			'se_site_main' => [
				'dev' => '/snkeng/site_core/res/modules',
				'prod' => '/site_core',
				'module' => false,
				'dependencies' => true,
			],
			'admin_core' => [
				'dev' => '/snkeng/admin_core/res/modules',
				'prod' => '/admin_core',
				'module' => false,
				'dependencies' => false,
			],
			'se_site_modules' => [
				'dev' => '/snkeng/site_modules/',
				'prod' => '/site_modules/!moduleName;',
				'module' => true,
				'dependencies' => true,
			],
			'admin_modules' => [
				'dev' => '/snkeng/admin_modules/',
				'prod' => '/admin_modules/!moduleName;',
				'module' => true,
				'dependencies' => true,
			],
		];

		// Delete previous content in folder
		if ( $rebuild ) {
			// echo "DELETING PREVIOUS FILES:\n";
			$dir = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/modules/';

			//
			if ( file_exists($dir) ) {
				$di = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS);
				$ri = new \RecursiveIteratorIterator($di, \RecursiveIteratorIterator::CHILD_FIRST);
				foreach ( $ri as $file ) {
					$file->isDir() ? rmdir($file) : unlink($file);
				}
			}
			// echo "DELETION COMPLETE\n";
		}

		// Second copy all files according to update
		foreach ( $targetList as $name => $props ) {
			// Get developer files
			// echo "--- INI: {$name} ---\n";

			// Is this a module (has sub folders)? or a main folder (these have no dependencies)
			if ( $props['module'] ) {
				// As a module structure it depends on actual module folders. Get and do operation.

				//
				foreach ( scandir($_SERVER['DOCUMENT_ROOT'] . $props['dev']) as $cAppName ) {
					//
					$appFolder = $props['dev'] . $cAppName . "/res/modules/";

					// Skip non folders
					if ( !is_dir($_SERVER['DOCUMENT_ROOT'] . $props['dev'] . '/' . $cAppName) || in_array($cAppName, ['.', '..']) || file_exists($appFolder) ) {
						continue;
					}

					//
					self::folder_processing_modules($appFolder, $props['prod'], $cAppName, true);
				}
			} else {
				self::folder_processing_modules($props['dev'], $props['prod'], '', $props['dependencies']);
			}

			// echo "--- END: {$name} ---\n";
		}
	}
}
//
