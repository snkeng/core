<?php
//
$nav = [
	'envvar' => [
		'sel' => "
		sv_id AS id, sv_var AS title, sv_value AS value, sv_type AS type
		",
		'from' => 'sa_sitevars',
		'lim' => 50,
		'where' => [
		],
		'order' => [
			['Name' => 'Orden', 'db' => 'sv_var', 'set' => 'ASC']
		]
	],
	'errors' => [
		'sel' => "elog_id AS id, elog_dtadd AS date, elog_type AS type, elog_key AS code",
		'from' => 'sb_errorlog',
		'lim' => 50,
		'where' => [
			'type' => ['name' => 'Tipo', 'db' => 'elog_type', 'vtype' => 'int', 'stype' => 'eq', 'set'=> 1]
		],
		'order' => [
			['Name' => 'ID', 'db' => 'elog_id', 'set' => 'ASC']
		]
	],
	'notifications' => [
		'sel' => "elog_id AS id, elog_dtadd AS date, elog_type AS type, elog_key AS code",
		'from' => 'sb_errorlog',
		'lim' => 50,
		'where' => [
			'type' => ['name' => 'Tipo', 'db' => 'elog_type', 'vtype' => 'int', 'stype' => 'eq', 'set'=> 2]
		],
		'order' => [
			['Name' => 'ID', 'db' => 'elog_id', 'set' => 'ASC']
		]
	],
];
return $nav;
