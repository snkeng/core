<?php
//
$nav = [
	'sel' => "mod_id AS id, mod_name AS title, mod_ver AS ver",
	'from' => 'sa_modules',
	'lim' => 20,
	'where' => [
	],
	'order' => [
		['Name' => 'Nombre', 'db' => 'mod_name', 'set' => 'ASC']
	]
];
return $nav;
