<?php
// Sistema de juntar icons
$icon_path = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final';
$icon_dir = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/min_files';

//
$response['d'] = '';

$dirs = [];
$dh  = opendir($icon_path);
while ( false !== ($dirName = readdir($dh)) ) {
	if ( is_dir($icon_path.'/'.$dirName) ) {
		$dirs[] = $dirName;
	}
}
$dirs = array_diff($dirs, ['.', '..']);

foreach ( $dirs as $cDir ) {
	$cDirFull = $icon_path.'/'.$cDir;
	$icon_file_full = $icon_dir.'/'.$cDir;
	//
	$fileCount = 0;
	$iconList = '';
	//
	$response['d'].= "FILE: {$cDir}.\n";
	$response['d'].= "FOLDER: {$cDirFull}.\n";
	//
	foreach ( glob($cDirFull."/*.svg") as $filename ) {
		$svg_title = basename($filename, '.svg');
		$svg_id = $svg_title; // simplify title
		$fileContent = file_get_contents($filename);
		// Icono
		preg_match("'viewBox=\"([^\"]*)\"'si", $fileContent, $m);
		$svg_viewBox = preg_replace('/\s+/', " ", $m[1]);
		// ViewSize
		preg_match("'<svg[^>]+>(.*?)</svg>'si", $fileContent, $m);
		$svg_content = trim(preg_replace('/\s+/', " ", $m[1]));
		//
		$iconList.= <<<HTML
<symbol id="{$svg_id}" viewBox="{$svg_viewBox}">
{$svg_content}
</symbol>\n
HTML;

		$fileCount++;
	}

	//
	if ( $fileCount ) {
		$fileContents = <<<HTML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none;">
{$iconList}</svg>
HTML;
		// Debug
		$response['d'].= "SUCCESS WITH {$fileCount} files.\n\n";
		// Save normal
		file_put_contents($icon_file_full.'.svg', $fileContents);
		// gZip
		$fileZip = gzencode($fileContents, 9);
		// Save Zip
		file_put_contents($icon_file_full.'.svgz', $fileZip);
	} else {
		$response['d'].= "FAIL: NO FILES.\n\n";
	}
}