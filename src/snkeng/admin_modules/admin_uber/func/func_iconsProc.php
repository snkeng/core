<?php


namespace admin_modules\admin_uber\func;
$dir_engine = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_icons';
$dir_page = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final';


// Icon management
class iconProc
{
	//

	//
	public static function faProcess()
	{
		// Origen FA
		$root = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/external/font-awesome';

		//

		// Sistema de juntar icons
		$fa_css = $root . '/css/font-awesome.css';
		$fa_svg = $root . '/fonts/fontawesome-webfont.svg';

		//
		$svg_final = $GLOBALS['dir_engine'] . "/font-awesome";

		//
		$css_rules = [];
		$json_list = [];

		// CSS
		$fileContent = file_get_contents($fa_css);
		preg_match_all('/.fa-[\w\-]*:before[^\}]*}/si', $fileContent, $m, PREG_PATTERN_ORDER);

		//
		foreach ($m[0] as $icon) {
			// echo($icon."\n");
			// Get index
			preg_match_all('/\"\\\\([\w0-9]+)\"/si', $icon, $contentData, PREG_SET_ORDER);
			$cName = $contentData[0][1];
			// Get names
			preg_match_all('/.fa-([\w0-9\-]+):before/si', $icon, $namesData, PREG_PATTERN_ORDER);
			// Save
			$css_rules[$cName]['names'] = $namesData[1];
		}

		// print_r($css_rules);

		// SVG
		$fileContent = file_get_contents($fa_svg);
		preg_match_all('/<glyph[^>]+>/si', $fileContent, $m, PREG_SET_ORDER);
		foreach ($m as $glyph) {
			// Data
			preg_match_all('/([A-z0-9\-]+)\=\"([^"]+)\"/si', $glyph[0], $cur, PREG_SET_ORDER);
			// Hacer el array (property=>value)
			$cChar = [];
			foreach ($cur as $cRow) {
				$cChar[$cRow[1]] = $cRow[2];
			}
			// Skip if it does not has a char
			if ( empty($cChar['d'])) {
				continue;
			}

			if ( empty($cChar['unicode']) ) {
				echo "SOMETHING IS MISSING HERE: skipping.\n";
				print_r($cChar);
				continue;
			}
			// print_r($cChar);
			// remove unicode init
			$cur_char = substr($cChar['unicode'], 3, 4);
			// Evaluate existance
			if (isset($css_rules[$cur_char])) {
				// Matriz de transformación 1: translate 0,-1536; 2: scale 1, -1
				$cChar['d'] = self::defApplyMatrix($cChar['d'], [1, 0, 0, -1, 0, 1536]);

				// Add to css rules
				$css_rules[$cur_char]['icon'] = [
					'd' => $cChar['d'],
					'l' => (empty($cChar['horiz-adv-x'])) ? 1536 : intval($cChar['horiz-adv-x'])
				];
			}
		}

		// Create SVG
		$iconList = '';
		$cheatList = '';

		// Números importantes: 256, 1792, 1536
		foreach ($css_rules as $icon ) {
			// Append to cheat list
			$cheatList.= implode(',', $icon['names']) . "\n";

			// Create SVG structure
			$iconList .= <<<XML
<symbol id="fa-{$icon['names'][0]}" viewBox="0,0,{$icon['icon']['l']},1792"><path d="{$icon['icon']['d']}"/></symbol>\n
XML;
			//
			$json_list['fa-' . $icon['names'][0]] = [
				'vb' => [0, 0, $icon['icon']['l'], 1792],
				'svg' => "<path d=\"{$icon['icon']['d']}\"/>"
			];
		}

		// CREAR SVG
		self::saveSVG($svg_final, $iconList);

		echo "done: {$svg_final}.\n";
		exit();


		// Save cheat sheet
		file_put_contents($svg_final . '.txt', $cheatList);

		// Save JSON Master
		file_put_contents($svg_final . '.json', json_encode($json_list, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

		// JSON Mix
		self::mergeJSON($GLOBALS['dir_engine'] . '/sec-icons', $json_list);

		// CREAR SVG
		self::saveSVG($svg_final, $iconList);

		// Response
		$response['d'] = $iconList;
	}

	//
	public static function iconAdd($rData)
	{
		$merge = false;
		$file = '';
		// Save directory
		switch ($rData['saveTo']) {
			case 'gen':
				$file = $GLOBALS['dir_engine'] . '/sec-icons.json';
				$merge = true;
				break;
			case 'page':
				$file = $GLOBALS['dir_page'] . '/fe-icons.json';
				break;
			default:
				\snkeng\core\engine\nav::killWithError('');
				break;
		}

		// Get ICONS
		if (file_exists($file)) {
			$iconData = json_decode(file_get_contents($file), true);
		} else {
			$iconData = [];
		}

		// Add
		$iconData[$rData['name']] = [
			'vb' => [
				$rData['xi'],
				$rData['yi'],
				$rData['xp'],
				$rData['yp'],
			],
			'svg' => $rData['svg']
		];

		// Save
		file_put_contents($file, json_encode($iconData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

		// Merge
		if ($merge) {
			self::mergeJSON($GLOBALS['dir_engine'] . '/font-awesome', $iconData);
		}
	}

	// TODO: REMOVE
	public static function iconSelect(&$response, $rData)
	{
		// Leer lista
		$iconPostList = explode(',', $rData['list']);
		if (empty($iconPostList)) {
			\snkeng\core\engine\nav::killWithError('ERROR, LISTA NO VÁLIDA');
		}
		// Directorio front end
		$saveLocation = $GLOBALS['dir_page'] . "/fe-icons";

		// Save picks
		file_put_contents($saveLocation . '.txt', $rData['list']);

		// Get ICONS
		$iconData = json_decode(file_get_contents($GLOBALS['dir_engine'] . '/se-icons.json'), true);

		//
		$iconList = '';
		//
		foreach ($iconPostList as $iconID) {
			if (!empty($iconData[$iconID])) {
				$data =& $iconData[$iconID];
				$iconList .= <<<XML
<symbol id="{$iconID}" viewBox="{$data['vb'][0]},{$data['vb'][1]},{$data['vb'][2]},{$data['vb'][3]}">{$data['svg']}</symbol>\n
XML;
			} else {
				\snkeng\core\engine\nav::killWithError('ERROR, ICONO NO VÁLIDO', $iconID);
			}
		}

		// Create SVG
		self::saveSVG($saveLocation, $iconList);

		// Response
		$response['d'] = $iconList;

		// Update Minify
		self::updateMinify('fe-icons');
	}

	//
	public static function updateMinify($name)
	{
		// Read
		$saved_file = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles/files.json';
		$saved_data = json_decode(file_get_contents($saved_file), true);

		$cFile =& $saved_data[$name];

		// Save
		if (!empty($cFile)) {
			$cTime = time();
			//
			$cFile['mod'] = date('c');
			$cFile['utime'] = $cTime;
		} else {
			\snkeng\core\engine\nav::killWithError('MINIFY ERROR');
		}

		// debugVariable($saved_data[$name]);

		// Save
		file_put_contents($saved_file, json_encode($saved_data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
	}

	//
	public static function mergeJSON($missingFile, $data)
	{
		$cFile = $missingFile . '.json';
		//
		$iconsMaster = $GLOBALS['dir_engine'] . '/se-icons';

		//
		if (file_exists($cFile)) {
			$iconData = json_decode(file_get_contents($cFile), true);
			$saveArray = array_merge($iconData, $data);
		} else {
			$saveArray =& $data;
		}

		// Save
		file_put_contents($iconsMaster . '.json', json_encode($saveArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

		//
		// Create SVG
		$iconList = '';
		//
		foreach ($saveArray as $iconID => $data) {
			$iconList .= <<<XML
<symbol id="{$iconID}" viewBox="{$data['vb'][0]},{$data['vb'][1]},{$data['vb'][2]},{$data['vb'][3]}">{$data['svg']}</symbol>\n
XML;
		}

		//
		self::saveSVG($iconsMaster, $iconList);

		//
		//// Update Minify
		self::updateMinify('se-icons');
	}

	//
	public static function saveSVG($loc, $data)
	{
		// SVG Structure
		$fileContents = <<<HTML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none;">
{$data}</svg>
HTML;

		// Save normal
		file_put_contents($loc . '.svg', $fileContents);
		// gZip
		$fileZip = gzencode($fileContents, 9);
		// Save Zip
		file_put_contents($loc . '.svgz', $fileZip);
	}

	// EASY SVG Transformation matrix
	public static function defApplyMatrix($def, $matrix)
	{
		// if there are several shapes in this definition, do the operation for each
		preg_match_all('/M[^zZ]*[zZ]/', $def, $shapes);
		$shapes = $shapes[0];
		if (count($shapes) > 1) {
			foreach ($shapes as &$shape) {
				$shape = self::defApplyMatrix($shape, $matrix);
			}
			return implode(' ', $shapes);
		}
		preg_match_all('/[a-zA-Z]+[^a-zA-Z]*/', $def, $instructions);
		$instructions = $instructions[0];
		$return = '';
		foreach ($instructions as &$instruction) {
			$i = preg_replace('/[^a-zA-Z]*/', '', $instruction);
			preg_match_all('/\-?[0-9\.]+/', $instruction, $coords);
			$coords = $coords[0];
			if (empty($coords)) {
				continue;
			}

			$new_coords = [];
			while (count($coords) > 0) {
				// do the matrix calculation stuff
				list($a, $b, $c, $d, $e, $f) = $matrix;
				// exception for relative instruction
				if (preg_match('/[a-z]/', $i)) {
					$e = 0;
					$f = 0;
				}
				// convert horizontal lineto (relative)
				if ($i == 'h') {
					$i = 'l';
					$x = floatval(array_shift($coords));
					$y = 0;
					// add new point's coordinates
					$current_point = [
						$a * $x + $c * $y + $e,
						$b * $x + $d * $y + $f,
					];
					$new_coords = array_merge($new_coords, $current_point);
				} // convert vertical lineto (relative)
				elseif ($i == 'v') {
					$i = 'l';
					$x = 0;
					$y = floatval(array_shift($coords));
					// add new point's coordinates
					$current_point = [
						$a * $x + $c * $y + $e,
						$b * $x + $d * $y + $f,
					];
					$new_coords = array_merge($new_coords, $current_point);
				} // convert quadratic bezier curve (relative)
				elseif ($i == 'q') {
					$x = floatval(array_shift($coords));
					$y = floatval(array_shift($coords));
					// add new point's coordinates
					$current_point = [
						$a * $x + $c * $y + $e,
						$b * $x + $d * $y + $f,
					];
					$new_coords = array_merge($new_coords, $current_point);
					// same for 2nd point
					$x = floatval(array_shift($coords));
					$y = floatval(array_shift($coords));
					// add new point's coordinates
					$current_point = [
						$a * $x + $c * $y + $e,
						$b * $x + $d * $y + $f,
					];
					$new_coords = array_merge($new_coords, $current_point);
				}
				// every other commands
				// @TODO: handle 'a,c,s' (elliptic arc curve) commands
				// cf. http://www.w3.org/TR/SVG/paths.html#PathDataCurveCommands
				else {
					$x = floatval(array_shift($coords));
					$y = floatval(array_shift($coords));

					// add new point's coordinates
					$current_point = [
						$a * $x + $c * $y + $e,
						$b * $x + $d * $y + $f,
					];
					$new_coords = array_merge($new_coords, $current_point);
				}
			}
			$instruction = $i . implode(',', $new_coords);
			// remove useless commas
			$instruction = preg_replace('/,\-/', '-', $instruction);
		}
		return implode('', $instructions);
	}
}
//
