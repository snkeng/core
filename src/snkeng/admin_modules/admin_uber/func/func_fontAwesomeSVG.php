<?php
$root = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/external/font-awesome';

// Sistema de juntar icons
$fa_css = $root . '/css/font-awesome.css';
$fa_svg = $root . '/fonts/fontawesome-webfont.svg';
$ver = file_get_contents($root . '/ver.txt');

$altVer = str_replace('.', '', $ver);

//
$finalFolder = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_icons';

$json_loc = $finalFolder . "/font-awesome-struct.{$altVer}.json";
$json_dynamic = $finalFolder . "/font-awesome.json";
$svg_final = $finalFolder . "/font-awesome.{$altVer}";
$svg_cheat = $finalFolder . "/font-awesome-cheat.{$altVer}.txt";

$css_rules = [];
$json_list = [];


// CSS
$fileContent = file_get_contents($fa_css);
preg_match_all('/.fa-[\w\-]*:before[^\}]*}/si', $fileContent, $m, PREG_PATTERN_ORDER);

foreach ( $m[0] as $icon ) {
	// echo($icon."\n");
	// Get index
	preg_match_all('/\"\\\\([\w0-9]+)\"/si', $icon, $contentData, PREG_SET_ORDER);
	$cName = $contentData[0][1];
	// Get names
	preg_match_all('/.fa-([\w0-9\-]+):before/si', $icon, $namesData, PREG_PATTERN_ORDER);
	// Save
	$css_rules[$cName]['names'] = $namesData[1];
}

// SVG
$fileContent = file_get_contents($fa_svg);
preg_match_all('/<glyph[^>]+>/si', $fileContent, $m, PREG_SET_ORDER);
foreach ( $m as $glyph ) {
	// Data
	preg_match_all('/([A-z0-9\-]+)\=\"([^"]+)\"/si', $glyph[0], $cur, PREG_SET_ORDER);
	// Hacer el array (property=>value)
	$cChar = [];
	foreach ( $cur as $cRow ) {
		$cChar[$cRow[1]] = $cRow[2];
	}
	// Get char
	if ( !empty($cChar['d']) ) {
		// remove unicode init
		$cur_char = substr($cChar['d'], 3, 4);
		// Evaluate existance
		if ( isset($css_rules[$cur_char]) ) {
			// Matriz de transformación 1: translate 0,-1536; 2: scale 1, -1
			$cChar['d'] = defApplyMatrix($cChar['d'], [1, 0, 0, -1, 0, 1536]);

			// Add to css rules
			$css_rules[$cur_char]['icon'] = [
				'd' => $cChar['d'],
				'l' => (empty($cChar['horiz-adv-x'])) ? 1536 : intval($cChar['horiz-adv-x'])
			];
		}

	}
}

// Save JSON Master
file_put_contents($json_loc, json_encode($css_rules, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

// Create SVG
$iconList = '';
$cheatList = '';

// Números importantes: 256, 1792, 1536
foreach ( $css_rules as $icon ) {
	$first = true;
	foreach ( $icon['names'] as $names ) {
		$cheatList .= ($first) ? '' : ",";
		$cheatList .= $names;
		$first = false;
	}
	$cheatList .= "\n";

	/*
	$iconList .= <<<XML
<symbol viewBox="0,0,{$icon['icon']['l']},1792" id="fa-{$icon['names'][0]}"><path transform="scale(1,-1) translate(0,-1536)" d="{$icon['icon']['d']}"/></symbol>\n
XML;
	*/
	$iconList .= <<<XML
<symbol id="fa-{$icon['names'][0]}" viewBox="0,0,{$icon['icon']['l']},1792"><path d="{$icon['icon']['d']}"/></symbol>\n
XML;
	//
	$json_list['fa-'.$icon['names'][0]] = [
		'vb' => [0,0,$icon['icon']['l'],1792],
		'path' => "<path d=\"{$icon['icon']['d']}\"/>"
	];
}

// Save JSON Master
file_put_contents($json_dynamic, json_encode($json_list, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

//
$fileContents = <<<HTML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none;">
{$iconList}</svg>
HTML;

// Save cheat sheet
file_put_contents($svg_cheat, $cheatList);

// Save normal
file_put_contents($svg_final . '.svg', $fileContents);
// gZip
$fileZip = gzencode($fileContents, 9);
// Save Zip
file_put_contents($svg_final . '.svgz', $fileZip);

// Response
$response['d'] = $fileContents;

// EASY SVG Transformation matrix
function defApplyMatrix($def, $matrix)
{
	// if there are several shapes in this definition, do the operation for each
	preg_match_all('/M[^zZ]*[zZ]/', $def, $shapes);
	$shapes = $shapes[0];
	if ( count($shapes) > 1 ) {
		foreach ( $shapes as &$shape ) {
			$shape = defApplyMatrix($shape, $matrix);
		}
		return implode(' ', $shapes);
	}
	preg_match_all('/[a-zA-Z]+[^a-zA-Z]*/', $def, $instructions);
	$instructions = $instructions[0];
	$return = '';
	foreach ( $instructions as &$instruction ) {
		$i = preg_replace('/[^a-zA-Z]*/', '', $instruction);
		preg_match_all('/\-?[0-9\.]+/', $instruction, $coords);
		$coords = $coords[0];
		if ( empty($coords) ) {
			continue;
		}

		$new_coords = [];
		while ( count($coords) > 0 ) {
			// do the matrix calculation stuff
			list($a, $b, $c, $d, $e, $f) = $matrix;
			// exception for relative instruction
			if ( preg_match('/[a-z]/', $i) ) {
				$e = 0;
				$f = 0;
			}
			// convert horizontal lineto (relative)
			if ( $i == 'h' ) {
				$i = 'l';
				$x = floatval(array_shift($coords));
				$y = 0;
				// add new point's coordinates
				$current_point = [
					$a * $x + $c * $y + $e,
					$b * $x + $d * $y + $f,
				];
				$new_coords = array_merge($new_coords, $current_point);
			} // convert vertical lineto (relative)
			elseif ( $i == 'v' ) {
				$i = 'l';
				$x = 0;
				$y = floatval(array_shift($coords));
				// add new point's coordinates
				$current_point = [
					$a * $x + $c * $y + $e,
					$b * $x + $d * $y + $f,
				];
				$new_coords = array_merge($new_coords, $current_point);
			} // convert quadratic bezier curve (relative)
			elseif ( $i == 'q' ) {
				$x = floatval(array_shift($coords));
				$y = floatval(array_shift($coords));
				// add new point's coordinates
				$current_point = [
					$a * $x + $c * $y + $e,
					$b * $x + $d * $y + $f,
				];
				$new_coords = array_merge($new_coords, $current_point);
				// same for 2nd point
				$x = floatval(array_shift($coords));
				$y = floatval(array_shift($coords));
				// add new point's coordinates
				$current_point = [
					$a * $x + $c * $y + $e,
					$b * $x + $d * $y + $f,
				];
				$new_coords = array_merge($new_coords, $current_point);
			}
			// every other commands
			// @TODO: handle 'a,c,s' (elliptic arc curve) commands
			// cf. http://www.w3.org/TR/SVG/paths.html#PathDataCurveCommands
			else {
				$x = floatval(array_shift($coords));
				$y = floatval(array_shift($coords));

				// add new point's coordinates
				$current_point = [
					$a * $x + $c * $y + $e,
					$b * $x + $d * $y + $f,
				];
				$new_coords = array_merge($new_coords, $current_point);
			}
		}
		$instruction = $i . implode(',', $new_coords);
		// remove useless commas
		$instruction = preg_replace('/,\-/', '-', $instruction);
	}
	return implode('', $instructions);
}