<?php

//
use admin_modules\admin_uber\func\iconProc;

switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'groups':
		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			//
			case 'add':
				break;
			//
			case 'delete':
				break;
			//
			case 'single':
				//
				switch ( \snkeng\core\engine\nav::next() )
				{
					//
					case 'select':
						$rData = \snkeng\core\general\saveData::postParsing(
							$response,
							[
								'file' => ['name' => 'Contenido', 'type' => 'text', 'validate' => true],
								'list' => ['name' => 'Contenido', 'type' => 'json', 'validate' => true],
							]
						);

						//
						\snkeng\admin_modules\admin_uber\svg_icons\group::select($rData['file'], $rData['list']);

						break;
					//
					default:
						\snkeng\core\engine\nav::invalidPage();
						break;
				}
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
