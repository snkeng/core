<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'download':
		\snkeng\admin_modules\admin_uber\sql_structure::tableReturn($response);
		break;
	//
	case 'upload':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'content' => ['name' => 'Contenido',   'type' => 'json', 'validate' => true],
			]
		);

		\snkeng\admin_modules\admin_uber\sql_structure::tableUpload($response, $rData['content']);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//