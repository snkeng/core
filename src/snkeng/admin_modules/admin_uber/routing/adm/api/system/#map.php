<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	case 'console':
		require __DIR__ . '/console.php';
		break;

	//
	case 'dbstruct':
		require __DIR__ . '/dbstruct.php';
		break;

	//
	case 'coreupdate':
		require __DIR__ . '/coreupdate.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
