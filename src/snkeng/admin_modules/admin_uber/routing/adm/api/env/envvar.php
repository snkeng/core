<?php
//
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case '':
		case null:
			switch ( $_SERVER['REQUEST_METHOD'] ) {
				//
				case 'GET':
					// Preparar estructura
					\snkeng\core\general\asyncDataJson::printJson(
						[
							'sel' => "sv_id AS id, sv_var AS title, sv_value AS value, sv_type AS type",
							'from' => 'sa_sitevars',
							'lim' => 50,
							'where' => [
							],
							'order' => [
								'var' => ['Name' => 'Orden', 'db' => 'sv_var']
							],
							'default_order' => [
								['var', 'ASC']
							]
						]
					);
					break;

				//
				case 'POST':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceAddSingle(
						[
							'title' => [
								'name' => 'Title',
								'type' => 'text',
								'required' => true,
								'validation' => [
									'minlength' => 0,
									'maxlength' => 30,
									'filter' => 'simpleTitle'
								]
							],
							'value' => [
								'name' => 'Title',
								'type' => 'text',
								'required' => true,
								'validation' => [
									'minlength' => 0,
									'maxlength' => 150,
								]
							],
							'type' => [
								'name' => 'Tipo',
								'type' => 'int',
								'required' => true,
								'validation' => [
									'min' => 0,
									'max' => 10
								]
							]
						],
						[
							'elems' => [
								'sv_var' => 'title',
								'sv_value' => 'value',
								'sv_type' => 'type',
							],
							'tName' => 'sa_sitevars',
							'tId' => ['nm' => 'id', 'db' => 'sv_id']
						]
					);
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidMethod();
					break;
			}
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
else {
	$params['vars']['envVarId'] = intval(\snkeng\core\engine\nav::next());

	//
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case '':
		case null:
			$tableStructure = [
				'elems' => [
					'sv_var' => 'title',
					'sv_value' => 'value',
					'sv_type' => 'type',
				],
				'tName' => 'sa_sitevars',
				'tId' => ['nm' => 'id', 'db' => 'sv_id', 'value' => $params['vars']['envVarId']]
			];

			switch ( $_SERVER['REQUEST_METHOD'] ) {
				//
				case 'GET':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceReadSingle(
						$tableStructure
					);
					break;

				//
				case 'POST':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceUpdateSingle(
						[
							'title' => [
								'name' => 'Title',
								'type' => 'text', 'required' => true,
								'validation' => [
									'minlength' => 0,
									'maxlength' => 30,
									'filter' => 'simpleTitle'
								]
							],
							'value' => [
								'name' => 'Value',
								'type' => 'text', 'required' => true,
								'validation' => [
									'minlength' => 0,
									'maxlength' => 150,
								]
							],
							'type' => [
								'name' => 'Tipo',
								'type' => 'int', 'required' => true,
								'validation' => [
									'min' => 0,
									'max' => 10
								]
							]
						],
						$tableStructure
					);
					break;

				//
				case 'DELETE':
					\snkeng\core\engine\restfulOps::resourceDeleteSingle($tableStructure);
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidMethod();
					break;
			}
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
