<?php

//
use admin_modules\admin_uber\func\iconProc;

switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'patches':
		switch ( \snkeng\core\engine\nav::next() ) {
			//
			case 'exec':
				$file = $_POST['file'];
				$exec = ( isset($_POST['exec']) && $_POST['exec']) ? true : false;
				$file = $_SERVER['DOCUMENT_ROOT'] . '/zz_patches/'.$file.'.php';
				if ( is_file($file) ) {
					require $file;
				} else {
					\snkeng\core\engine\nav::killWithError('Archivo no disponible');
				}
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;
	//
	case 'minify':
		$rebuild = $params['isFix'] ?? false;

		//
		\snkeng\admin_modules\admin_uber\minify_bundles::exec($response, $rebuild);
		\snkeng\admin_modules\admin_uber\minify_modules::exec($response, $rebuild);
		break;

	//
	case 'icons':
		require __DIR__ . '/../../../../func/func_iconsProc.php';

		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			//
			case 'add':
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'name' => ['name' => 'Nombre',   'type' => 'str','validate' => true],
						'svg' => ['name' => 'SVG',   'type' => 'str', 'validate' => true],
						'xi' => ['name' => 'XI',   'type' => 'int', 'validate' => false],
						'yi' => ['name' => 'YI',   'type' => 'int', 'validate' => false],
						'xp' => ['name' => 'XP',   'type' => 'int', 'validate' => false],
						'yp' => ['name' => 'YP',   'type' => 'int', 'validate' => false],
						'saveTo' => ['name' => 'Salvar en',   'type' => 'str','validate' => true],
					]
				);

				//
				iconProc::iconAdd($rData);
				break;
			//
			case 'select':
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'list' => ['name' => 'list',   'type' => 'str','validate' => true],
					]
				);

				//
				iconProc::iconSelect($response, $rData);

				$response['d'] = 'Guardados';
				break;
			//
			case 'fa-proc':
				iconProc::faProcess();
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
