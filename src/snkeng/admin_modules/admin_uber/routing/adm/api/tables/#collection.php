<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case '':
	case null:
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			case 'GET':
				// Preparar estructura
				\snkeng\core\general\asyncDataJson::printJson(
					[
						'sel' => "t.t_id AS id, t.t_name AS title,
							t.t_dbname AS dbName, t.t_dbnick AS dbNick,
							m.mod_name AS modTitle,
							IF (gt.gt_id IS NOT NULL && gt_dt_mod>= t.t_dt_mod, 1, 0) AS upToDate
					",
						'from' => 'sa_table_object AS t
							INNER JOIN sa_modules AS m ON t.mod_id=m.mod_id
							LEFT OUTER JOIN sb_ghost_table_object AS gt ON gt.t_id=t.t_id',
						'lim' => 50,
						'where' => [
							'modTitle' => ['name' => 'Mod Nombre', 'db' => 'm.mod_name', 'vtype' => 'str', 'stype' => 'like'],
							'title' => ['name' => 'Nombre', 'db' => 't.t_name', 'vtype' => 'str', 'stype' => 'like'],
							'dbName' => ['name' => 'DB Nombre', 'db' => 't.t_dbname', 'vtype' => 'str', 'stype' => 'like'],
							'dbNick' => ['name' => 'DB Nick', 'db' => 't.t_dbnick', 'vtype' => 'str', 'stype' => 'like']
						],
						'order' => [
							'modName' => ['Name' => 'Módulo', 'db' => 'mod_name'],
							'tabName' => ['Name' => 'Nombre', 'db' => 't_name']
						],
						'default_order' => [
							['modName', 'ASC'],
							['tabName', 'ASC'],
						]
					]
				);
				break;
			//
			case 'POST':
				// Uses old method since it returns $rData
				$rData = \snkeng\core\general\saveData::sql_complex_rowAdd(
					$response,
					[
						'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
						'modId' => ['name' => 'ID Modulo', 'type' => 'int', 'process' => false, 'validate' => true, 'notNull' => true],
						'dbName' => ['name' => 'Nombre DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
						'dbNick' => ['name' => 'Nick DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
						'dbType' => ['name' => 'Tipo DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
						'addId' => ['name' => 'Add Id', 'type' => 'bool'],
						'addDtAdd' => ['name' => 'Add dt add', 'type' => 'bool'],
						'addDtMod' => ['name' => 'Add dt mod', 'type' => 'bool'],
						'addTitle' => ['name' => 'ADD title', 'type' => 'bool'],
					],
					[
						'elems' => [
							't_name' => 'title',
							'mod_id' => 'modId',
							't_dbname' => 'dbName',
							't_dbnick' => 'dbNick',
							't_dbtype' => 'dbType',
						],
						'tName' => 'sa_table_object',
						'tId' => ['nm' => 'id', 'db' => 't_id'],
						'dtAdd' => 't_dt_add'
					]
				);

				// Add extra properties
				$tableId = \snkeng\core\engine\mysql::getLastId();
				$add_qry = "";

				// Add id?
				if ( $rData['addId'] ) {
					$add_qry .= "INSERT INTO sa_table_column (t_id, te_dt_add, te_name, te_pos, te_db_name, te_d_nick, te_db_type, te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_d_special) VALUES ({$tableId}, NOW(), 'ID', 1, '{$rData['dbNick']}_id', 'id', 'INT', 1, 1, 1, 1, 2);\n";
				}

				//
				if ( $rData['addDtAdd'] ) {
					$add_qry .= "INSERT INTO sa_table_column (t_id, te_dt_add, te_name, te_pos, te_db_name, te_d_nick, te_db_type, te_db_notnull, te_db_default, te_d_special) VALUES ({$tableId}, NOW(), 'Dt Add', 11, '{$rData['dbNick']}_dt_add', 'dtAdd', 'TIMESTAMP', 1, 'CURRENT_TIMESTAMP', 3);\n";
				}

				//
				if ( $rData['addDtMod'] ) {
					$add_qry .= "INSERT INTO sa_table_column (t_id, te_dt_add, te_name, te_pos, te_db_name, te_d_nick, te_db_type, te_db_notnull, te_db_default, te_d_special) VALUES ({$tableId}, NOW(), 'Dt Mod', 12, '{$rData['dbNick']}_dt_mod', 'dtMod', 'TIMESTAMP', 1, 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP', 4);\n";
				}

				//
				if ( $rData['addTitle'] ) {
					$add_qry .= "INSERT INTO sa_table_column (t_id, te_dt_add, te_name, te_pos, te_db_name, te_d_nick, te_db_type, te_db_typedesc, te_db_notnull, te_d_special) VALUES ({$tableId}, NOW(), 'Title', 21, '{$rData['dbNick']}_title', 'title', 'VARCHAR', '100', 1, 11);\n";
				}

				if ( !empty($add_qry) ) {
					\snkeng\core\engine\mysql::submitMultiQuery($add_qry);
				}
				break;

			//
			default:
				\snkeng\core\engine\nav::killWithError('Invalid operation', "Recieved header: {$_SERVER['HTTP_REQUEST_METHOD']}");
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

