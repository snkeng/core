<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'queryExec':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'elems' => ['name' => 'Elements', 'type' => 'straightStr','validate' => true],
				'from' => ['name' => 'From', 'type' => 'str', 'validate' => true],
				'where' => ['name' => 'Where', 'type' => 'straightStr', 'validate' => false],
				'order' => ['name' => 'Order', 'type' => 'str','validate' => false],
				'limit' => ['name' => 'Limit', 'type' => 'int', 'validate' => true],
			]
		);

		//
		$sel_qry = "SELECT {$rData['elems']}\n";
		$sel_qry.= "FROM {$rData['from']}\n";
		$sel_qry.= ( $rData['where'] ) ? "WHERE {$rData['where']}\n" : '';
		$sel_qry.= ( $rData['order'] ) ? "ORDER BY {$rData['order']}\n" : '';
		$sel_qry.= "LIMIT {$rData['limit']};\n";

		//
		$response['d'] = \snkeng\core\engine\mysql::returnArray($sel_qry);
		break;

	//
	case 'properties':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['tableId'],
			[
				'elems' => [
					't_name' => 'title',
					'mod_id' => 'modId',
					't_desc' => 'description',
					't_dbname' => 'dbName',
					't_dbnick' => 'dbNick',
					't_dbtype' => 'dbType',
				],
				'tName' => 'sa_table_object',
				'tId' => ['nm' => 'id', 'db' => 't_id'],
				'dtAdd' => 't_dt_add',
				'dtMod' => 't_dt_mod'
			],
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'modId' => ['name' => 'ID Modulo', 'type' => 'int', 'process' => false, 'validate' => true, 'notNull' => true],
				'dbName' => ['name' => 'Nombre DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
				'description' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 255, 'filter' => 'urlTitles', 'process' => false, 'validate' => false],
				'dbNick' => ['name' => 'Nick DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
				'dbType' => ['name' => 'Tipo DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
			]
		);
		break;

	//
	case 'updateBulk':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'elems' => ['name' => 'Elements', 'type' => 'straightStr','validate' => true],
				'from' => ['name' => 'From', 'type' => 'str', 'validate' => true],
				'where' => ['name' => 'Where', 'type' => 'straightStr', 'validate' => true],
				'order' => ['name' => 'Order', 'type' => 'str','validate' => false],
				'limit' => ['name' => 'Limit', 'type' => 'int', 'validate' => true],
			]
		);

		//
		$upd_qry = "UPDATE {$rData['from']} SET {$rData['elems']} WHERE {$rData['where']} LIMIT {$rData['limit']};";
		\snkeng\core\engine\mysql::submitQuery(
			$upd_qry,
			[
				'errorKey' => 'UAdminTablesContentUpdateSingle',
				'errorDesc' => 'No fue posible actualizar el valor.'
			]
		);
		break;

	//
	case 'updateSimple':
		//
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'idValue' => ['name' => 'ID Value', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'tName' => ['name' => 'Table Name', 'type'=>'str', 'process'=>false, 'validate'=>true],
				'idName' => ['name' => 'Table ID', 'type'=>'str', 'process'=>false, 'validate'=>true],
				'data' => ['name' => 'Sent data', 'type'=>'json', 'process'=>false, 'validate'=>true],
			]
		);

		//
		$dataStr = '';

		//
		$first = true;
		foreach ( $rData['data'] AS $name => $data ) {
			$dataStr.= ( $first ) ? '' : ', ';
			$dataStr.= $name.'=\'';
			//
			switch ( $data['type'] )
			{
				case 'bool':
				case 'int':
					$dataStr.= intval($data['value']);
					break;
				case 'float':
					$dataStr.= floatval($data['value']);
					break;
				default:
					$dataStr.= \snkeng\core\engine\mysql::real_escape_string($data['value']);
					break;
			}
			$dataStr.= '\'';
			//
			$first = false;
		}

		//
		$upd_qry = "UPDATE {$rData['tName']} SET {$dataStr} WHERE {$rData['idName']}={$rData['idValue']} LIMIT 1;";
		\snkeng\core\engine\mysql::submitQuery(
			$upd_qry,
			[
				'errorKey' => 'UAdminTablesContentUpdateSingle',
				'errorDesc' => 'No fue posible actualizar el valor.'
			]
		);
		break;

	//
	case 'delete':
		//
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'idValue' => ['name' => 'ID Value', 'type'=>'int', 'process'=>false, 'validate'=>true],
				'tName' => ['name' => 'Table Name', 'type'=>'str', 'process'=>false, 'validate'=>true],
				'idName' => ['name' => 'Table ID', 'type'=>'str', 'process'=>false, 'validate'=>true],
			]
		);

		//
		$upd_qry = "DELETE FROM {$rData['tName']} WHERE {$rData['idName']}={$rData['idValue']} LIMIT 1;";

		\snkeng\core\engine\mysql::submitQuery(
			$upd_qry,
			[
				'errorKey' => 'UAdminTablesContentDeleteSingle',
				'errorDesc' => 'No fue posible borrar el valor de la base de datos.'
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
