<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'direct':
		$query = strval($_POST['query']);

		//
		if ( empty($query) ) {
			\snkeng\core\engine\nav::killWithError('No hay query.', '');
		}

		//
		if ( \snkeng\core\engine\mysql::multiQuery($query) ) {

			$queryCounter = 1;
			$result = '';
			do {
				$result.= "<h4>Query $queryCounter</h4>\n";
				if ( !empty(\snkeng\core\engine\mysql::$result) ) {
					
					if ( \snkeng\core\engine\mysql::$result->num_rows !== 0 ) {
						// debugVariable(\snkeng\core\engine\mysql::$result);
						$result .= "<div class=\"qTableContainer\">\n";
						$result .= "<table class=\"qTable\">\n";
						$result .= "<thead>\n";
						$finfo = \snkeng\core\engine\mysql::$result->fetch_fields();
						foreach ( $finfo as $x ) {
							$result .= "\t<th>" . $x->name . "</th>\n";
						}
						$result .= "</thead>\n<tbody>\n";
						while ( $data = \snkeng\core\engine\mysql::$result->fetch_row() ) {
							$ammount = sizeof($data);
							$result .= "<tr>\n";
							for ( $i = 0; $i < $ammount; $i++ ) {
								$cValue = ( $data[$i] ) ? wordwrap(htmlspecialchars($data[$i]), 80, "<br />") : '';
								$result .= "\t<td>" . $cValue . "</td>\n";
							}
							$result .= "</tr>\n";
						}

						$result .= "</table>\n</tbody>\n</div>\n";
						/* print divider */
						if ( \snkeng\core\engine\mysql::$moreResults ) {
							$result .= "-----------------<br />\n";
						}
						// debugVariable($result);
					} else {
						$result.= "No hay resultados.";
					}
				} else {
					$result.= "Comando ejecutado.";
				}
				$queryCounter++;
			} while ( \snkeng\core\engine\mysql::mqNextResult() );

			// Reportar
			$response['s']['d'] = 'Query ejecutado';
			//
			$response['d'] = $result;
		} else {
			\snkeng\core\engine\mysql::killIfError('Query de actualización no válido.', '');
		}
		break;

	//
	case 'tables':
		require __DIR__ . '/../tables/#map.php';
		break;

	//
	case 'table_check':
		$alpha = new \snkeng\admin_modules\admin_uber\snkEng_update();
		$alpha->tableCheck($response);
		break;

	//
	case 'gtable_clean':
		$alpha = new \snkeng\admin_modules\admin_uber\snkEng_update();
		$alpha->gTableClean($response);
		break;

	//
	case 'structure':
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->structureFull();
		$alpha->saveData("");
		break;

	//
	case 'backup':
		require __DIR__ . '/backup.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
