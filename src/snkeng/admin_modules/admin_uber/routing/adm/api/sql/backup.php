<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'download_all':
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->fullBackup();
		$alpha->saveData("");
		break;

	//
	case 'download_struct':
		$alpha = new \snkeng\admin_modules\admin_uber\dbExtractor();
		$alpha->structure();
		$alpha->saveData("");
		break;

	//
	case 'upload':
		if ( empty($_FILES['sql_file']) ) {
			\snkeng\core\engine\nav::killWithError('Archivo no encontrado.');
		}

		//
		$result = '';
		$sql = file_get_contents($_FILES['sql_file']['tmp_name']);
		if ( \snkeng\core\engine\mysql::submitMultiQuery($sql) ) {
			$result.= "La base de datos ha sido cargada con éxito.<br />\n";
		} else {
			$response['s']['d'] = "ERROR: No se pudo subir la base de datos al sistema.";
			if ( \snkeng\core\engine\mysql::isError ) {
				$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', 'full');
			} else {
				$response['s']['ex'] = "Contenido Vacío.<br />\n";
			}
		}
		// Reportar
		$response['s']['d'] = 'Query ejecutado';
		$response['s']['ex'] = $result;
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
