<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'install':
		require __DIR__ . '/install.php';
		break;

	// Update
	case 'update':
		require __DIR__ . '/update.php';
		break;

	// Query
	case 'sql':
		$engUpdater = new \snkeng\core\engine\fileUpd();
		$engUpdater->sql_send($response, $_POST['site'], $_POST['query']);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
