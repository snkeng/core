<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxPath = \snkeng\core\engine\nav::navPathGet('ajax');

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!fkName;</td>
	<td>!tableName;</td>
	<td class="actions">
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $ajaxPath.'/readAll',
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID'],
		['name' => 'Nombre', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Tabla', 'filter' => 1, 'fName' => 'dbName'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva",
			"save_url":"{$ajaxPath}/add",
			"actName":"Agregar",
			"elems":[
				{"ftype":"text", "vname":"title", "fname":"Nombre", "fdesc":"", "dtype":"str", "regexp":"simpleTitle", "lMin":3, "lMax":60, "requiered":true}
			]
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$ajaxPath}/{id}/edit",
			"save_url":"{$ajaxPath}/{id}/edit",
			"defaults":{},
			"actName":"Guardar",
			"elems":[
				{"ftype":"text", "vname":"title", "fname":"Nombre", "fdesc":"", "dtype":"str", "regexp":"simpleTitle", "lMin":3, "lMax":60, "requiered":true}
			]
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$ajaxPath}/del"}
}
JSON
];

// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Foreign Keys</div>

{$tableStructure}
HTML;
//
