<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxPath = \snkeng\core\engine\nav::navPathGet('ajax');

//
// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!title;</span>
	<span><input type="text" name="dbName" value="!dbName;" minlength="3" maxlength="50" pattern="[A-z_]+" size="30"/></span>
	<span><select name="dbType" se-fix="!dbType;">
			<option value=""> - - </option>
			<option value="BOOLEAN">BOOLEAN</option>
			<option value="TINYINT">TINYINT (255)</option>
			<option value="SMALLINT">SMALLINT (65535)</option>
			<option value="MEDIUMINT">MEDIUMINT (16777215)</option>
			<option value="INT">INT (4294967295)</option>
			<option value="BIGINT">BIGINT (+1.84E19)</option>
			<option value="FLOAT">FLOAT (+4.0E44)</option>
			<option value="DOUBLE">DOUBLE (+2E308)</option>
			<option value="DECIMAL">DECIMAL</option>
			<option value="CHAR">CHAR (255)</option>
			<option value="VARCHAR">VARCHAR (255)</option>
			<option value="BINARY">BINARY (255)</option>
			<option value="VARBINARY">VARBINARY (255)</option>
			<option value="TINYTEXT">TINYTEXT (255)</option>
			<option value="TEXT">TEXT (65535)</option>
			<option value="MEDIUMTEXT">MEDIUMTEXT (16777215)</option>
			<option value="LONGTEXT">LONGTEXT (4294967295)</option>
			<option value="JSON">JSON</option>
			<option value="DATETIME">DATETIME</option>
			<option value="DATE">DATE</option>
			<option value="YEAR">YEAR</option>
			<option value="TIME">TIME</option>
			<option value="TIMESTAMP">TIMESTAMP</option>
	</select>
	</span>
	<span><input type="text" name="dbTypeDesc" value="!dbTypeDesc;" size="5" /></span>
	<span><input type="checkbox" name="dbPrikey" value="1" se-fix="!dbPrikey;" /></span>
	<span><input type="checkbox" name="dbAutoInc" value="1" se-fix="!dbAutoInc;" /></span>
	<span><input type="checkbox" name="dbUnsigned" value="1" se-fix="!dbUnsigned;" /></span>
	<span><input type="checkbox" name="dbNotNull" value="1" se-fix="!dbNotNull;" /></span>
	<span><input type="text" name="dbDefault" value="!dbDefault;" size="50" /></span>
	<span><input type="checkbox" name="dbFullText" value="1" se-fix="!dbFullText;" /></span>
</form>
HTML;
//
$exData = [
	'js_url' => $ajaxPath,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'Nombre'],
		['name' => 'Nombre DB'],
		['name' => 'Tipo'],
		['name' => 'Desc'],
		['name' => 'P.Key'],
		['name' => 'Auto Inc'],
		['name' => 'Unsigned'],
		['name' => 'Not Null'],
		['name' => 'Default'],
		['name' => 'FullText']
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"updAll":
	{
		"ajax-action":"edit",
		"action":"saveAll",
		"menu":{"title":"Guardar todos", "icon":"fa-save"},
		"save_url":"{$ajaxPath}/sql"
	}
}
JSON
];

// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Elementos - SQL</div>

{$tableStructure}
HTML;
//