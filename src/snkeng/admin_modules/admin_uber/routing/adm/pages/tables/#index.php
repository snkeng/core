<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxUrl = \snkeng\core\engine\nav::navPathGet('ajax');

// Módulos disponibles
$modOps = '';
$sql_qry = "SELECT mod_id, mod_name
			FROM sa_modules
			ORDER BY mod_name ASC;";
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	$first = true;
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
		$modOps .= ($first) ? '' : ', ';
		$modOps .= "\"{$datos['mod_id']}\":\"{$datos['mod_name']}\"";
		$first = false;
	}
}
// Tipo base de datos

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!modTitle;</td>
	<td>!title;</td>
	<td>!dbName;</td>
	<td>!dbNick;</td>
	<td><span class="table_status s!upToDate;"></span></td>
	<td class="actions">
		<a class="btn small" se-nav="app_content" href="./!id;/columns-basic/"><svg class="icon inline"><use xlink:href="#fa-pencil"/></svg> Elementos</a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $ajaxUrl,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID'],
		['name' => 'Modulo', 'filter' => 1, 'fName' => 'modTitle'],
		['name' => 'Nombre', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Nombre DB', 'filter' => 1, 'fName' => 'dbName'],
		['name' => 'Nick DB', 'filter' => 1, 'fName' => 'dbNick'],
		['name' => 'Vigente'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva",
			"save_url":"{$ajaxUrl}",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 60,
						"data-regexp": "simpleTitle"
					}
				},
				"dbName": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre DB",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 60,
						"data-regexp": "urlnames"
					}
				},
				"dbNick": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nick DB",
					"info_description": "",
					"attributes": {
						"minlength": 1,
						"maxlength": 10,
						"data-regexp": "urlnames"
					}
				},
				"modId": {
					"field_type": "list",
					"data_type": "int",
					"required": false,
					"info_name": "Módulo",
					"info_description": "",
					"attributes": {
						"options": {{$modOps}}
					}
				},
				"dbType": {
					"field_type": "list",
					"data_type": "string",
					"default_value": "",
					"required": false,
					"info_name": "Tipo base de datos",
					"info_description": "",
					"attributes": {
						"options": {"InnoDB":"InnoDB", "MyISAM":"MyISAM"}
					}
				},
				"addId": {
					"field_type": "input",
					"data_type": "bool",
					"info_name": "Agregar ID automático",
					"info_description": "",
					"attributes": {
						"type": "checkbox",
						"value": 1
					}
				},
				"addDtAdd": {
					"field_type": "input",
					"data_type": "bool",
					"info_name": "Agregar Fecha de agregado",
					"info_description": "",
					"attributes": {
						"type": "checkbox",
						"value": 1
					}
				},
				"addDtMod": {
					"field_type": "input",
					"data_type": "bool",
					"info_name": "Agregado Fecha de modificación",
					"info_description": "",
					"attributes": {
						"type": "checkbox",
						"value": 1
					}
				},
				"addTitle": {
					"field_type": "input",
					"data_type": "bool",
					"info_name": "Agregar Título",
					"info_description": "",
					"attributes": {
						"type": "checkbox",
						"value": 1
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"$ajaxUrl/{id}/",
			"save_url":"$ajaxUrl/{id}/",
			"defaults":{},
			"actName":"Guardar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 60,
						"data-regexp": "simpleTitle"
					}
				},
				"dbName": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre DB",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 60,
						"data-regexp": "urlnames"
					}
				},
				"dbNick": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nick DB",
					"info_description": "",
					"attributes": {
						"minlength": 1,
						"maxlength": 10,
						"data-regexp": "urlnames"
					}
				},
				"modId": {
					"field_type": "list",
					"data_type": "int",
					"required": false,
					"info_name": "Módulo",
					"info_description": "",
					"attributes": {
						"options": {{$modOps}}
					}
				},
				"dbType": {
					"field_type": "list",
					"data_type": "string",
					"default_value": "",
					"required": false,
					"info_name": "Tipo base de datos",
					"info_description": "",
					"attributes": {
						"options": {"InnoDB":"InnoDB", "MyISAM":"MyISAM"}
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"$ajaxUrl/"}
}
JSON
];

// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] .= <<<HTML
<style>
.table_status { display:block; margin:0 auto; border-radius:50%; height:15px; width:15px; }
.table_status.s0 { background-color:#F00; }
.table_status.s1 { background-color:#0F0; }
</style>

<div class="pageTitle">Tablas</div>

{$tableStructure}
HTML;
//
