<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxPath = \snkeng\core\engine\nav::navPathGet('ajax');

//
$page['head']['title'] = "Tablas";

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span><input type="checkbox" /></span>
	<span>!id;</span>
	<span><input type="number" name="cOrder" value="!cOrder;" size="5"/></span>
	<span><input type="text" name="title" value="!title;" size="30" minlength="2" maxlength="50" pattern="[A-z \-]"/></span>
	<span><input type="text" name="dbName" value="!dbName;" size="30" minlength="3" maxlength="50" pattern="[a-z_]"/></span>
	<span><input type="text" name="dNick" value="!dNick;" size="30" minlength="2" maxlength="50" pattern="[A-z_]"/></span>
	<span class="actions">
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" type="button" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></button>
	</span>
</form>
HTML;


//
$exData = [
	'js_url' => $ajaxPath,

	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'Sel'],
		['name' => 'ID'],
		['name' => 'Orden'],
		['name' => 'Nombre', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Nombre DB', 'filter' => 1, 'fName' => 'dbName'],
		['name' => 'Nick', 'filter' => 1, 'fName' => 'dbName'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSOBJ
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo",
			"save_url":"{$ajaxPath}",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 2,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"dbName": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre DB",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "urlnames"
					}
				}
			}
		}
	},
	"updAll":
	{
		"ajax-action":"edit",
		"action":"saveAll",
		"menu":{"title":"Guardar todos", "icon":"fa-save"},
		"save_url":"{$ajaxPath}/basic"
	},
	"del_many":
	{
		"action":"function",
		"ajax-action":"none",
		"ajax-elem":"sel",
		"menu":{"title":"Borrar seleccionados", "icon":"fa-trash-o"},
		"function":(content, pDefaults) => {
			console.warn(content, pDefaults);
			//
			var elIds = [];
			content.se_each((id, cEl) => {
				elIds.push(cEl.se_data("objid"));
			});
			
			//
			se.ajax.json("{$ajaxPath}",
				se.object.merge(pDefaults, { "act":"truncate", "elems":elIds.join() }),
				{
					onSuccess:(msg) => {
						$("#pagElements").ajaxSearch.reload();
					}
				}
			);
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$ajaxPath}/{id}"}
}
JSOBJ
];
// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] .= <<<HTML
<div class="pageSubTitle">Propiedades</div>

{$tableStructure}
HTML;
//
