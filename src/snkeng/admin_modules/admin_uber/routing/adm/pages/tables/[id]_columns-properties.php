<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxPath = \snkeng\core\engine\nav::navPathGet('ajax');

//
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/variable/regexp.php';

//
$filters = '<option value="">None</option>';
foreach( $regexpVarsSimple as $value => $data ) {
	$filters.= "<option value='{$value}'>{$value}</option>";
}

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!title;</span>
	<span><select name="sysEsp" se-fix="!sysEsp;">
		<option value=""> - - </option>
		<option value="0">No</option>
		<option value="1">Único</option>
		<option value="2" selected="selected">Primary Key</option>
		<option value="10">Foreign Key</option>
		<option value="3">dtAdd</option>
		<option value="4">dtMod</option>
		<option value="11">Título</option>
		<option value="5">Pw. Seed</option>
		<option value="6">f. Nombre</option>
		<option value="7">f. Tamaño</option>
		<option value="8">f. Tipo</option>
		<option value="9">f. Loc</option>
	</select></span>
	<span><input type="number" name="sysParent" value="!sysParent;" /></span>
	<span><input type="checkbox" name="sysOpc" value="1" se-fix="!sysOpc;" /></span>
	<span><select name="fType" se-fix="!fType;">{$filters}</select></span>
	<span><input type="number" class="small" name="fMinl" value="!fMinl;" /></span>
	<span><input type="number" name="fMaxl" value="!fMaxl;" /></span>
	<span><input type="number" name="fMaxlim" value="!fMaxlim;" /></span>
</form>
HTML;
//
$exData = [
	'js_url' => $ajaxPath,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'Nombre'],
		['name' => 'Especial'],
		['name' => 'Id Ext.'],
		['name' => 'Opcional'],
		['name' => 'Filtro'],
		['name' => 'Min L'],
		['name' => 'Max L'],
		['name' => 'Max Lim'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"updAll":
	{
		"ajax-action":"edit",
		"action":"saveAll",
		"menu":{"title":"Guardar todos", "icon":"fa-save"},
		"save_url":"{$ajaxPath}/properties"
	}
}
JSON
];

// debugVariable($an_qry, '', true);

$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] .= <<<HTML
<div class="pageSubTitle">Elementos - Sistema</div>

{$tableStructure}
HTML;
//
