<?php
//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Contenido');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/icons_add', 'Iconos Agregar'],
	[$params['page']['main'].'/icons_process', 'Iconos Process'],
	[$params['page']['main'].'/icons_pick', 'Iconos Seleccionar'],
	[$params['page']['main'].'/minify', 'Minify'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'icons_add':
		require __DIR__ . '/icons_add.php';
		break;
	//
	case 'icons_process':
		require __DIR__ . '/icons_process.php';
		break;
	//
	case 'icons_pick':
		require __DIR__ . '/icons_pick.php';
		break;
	//
	case 'minify':
		require __DIR__ . '/minify.php';
		break;
	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">Contenido</div>

{$nav}

{$page['body']}\n
HTML;
//
