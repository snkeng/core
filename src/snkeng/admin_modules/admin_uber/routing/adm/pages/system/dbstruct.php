<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-system-db-struct.mjs');

//
$params['page']['ajax'].= "/dbstruct";

//
$page['head']['title'] = "Estructura Base de datos";

//
$page['body'] = <<<HTML
<div class="pageSubTitle">DB Actualización</div>

<uber-adm-pages-system-db-struct data-apiurl="{$params['page']['ajax']}">

	<div class="grid">

		<div class="gr_sz04">
			<h2>Descargar estructura del sistema</h2>
			<div class="verticalListObjects">
				<button class="btn wide blue" data-action="download"><svg class="icon inline mr"><use xlink:href="#fa-download" /></svg>Descargar</button>
				<button class="btn wide blue" data-action="copyDownload"><svg class="icon inline mr"><use xlink:href="#fa-copy" /></svg>Copiar</button>
				<textarea data-element="download" style="width:100%; height:400px;"></textarea>
			</div>
		</div>

		<div class="gr_sz04">
			<h2>Subir escructura de comparación</h2>
			<form class="se_form" data-element="structUpload" method="post" is="se-async-form">
				<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-upload" /></svg>Subir</button>
				<output></output>
				<div class="separator required"><label>
					<div class="cont"><span class="title">Patch</span><span class="desc"></span></div>
					<textarea name="content" style="height:400px;"></textarea>
				</label></div>
			</form>
		</div>

		<div class="gr_sz04">
			<h2>Resultado de la comparación</h2>
			<div class="verticalListObjects">
			<button class="btn wide blue" data-action="copyEnd"><svg class="icon inline mr"><use xlink:href="#fa-copy" /></svg>Copiar</button>
			<textarea data-element="result" style="width:100%; height:400px;"></textarea>
			</div>
		</div>

	</div>
	
</uber-adm-pages-system-db-struct>
HTML;
//
