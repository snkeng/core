<?php
//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'SQL');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/scripts', 'Scripts'],
	[$params['page']['main'].'/dbstruct', 'DB Struct'],
	[$params['page']['main'].'/coreupdate', 'Core Update'],
	[$params['page']['main'].'/console', 'Consola'],
	[$appData['url'].'/system/phpinfo', 'Backup'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'scripts':
		require __DIR__ . '/scripts.php';
		break;
	//
	case 'dbstruct':
		require __DIR__ . '/dbstruct.php';
		break;
	//
	case 'coreupdate':
		require __DIR__ . '/coreupdate.php';
		break;
	//
	case 'console':
		require __DIR__ . '/console.php';
		break;
	//
	case 'phpinfo':
		require __DIR__ . '/phpinfo.php';
		break;
	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">Sistema</div>

<div class="nav4">
	<div class="normal">
		<a se-nav="app_content" href="{$params['page']['main']}/scripts">Scripts</a>
		<a se-nav="app_content" href="{$params['page']['main']}/dbstruct">DB Struct</a>
		<a se-nav="app_content" href="{$params['page']['main']}/coreupdate">Core Update</a>
		<a se-nav="app_content" href="{$params['page']['main']}/console">Consola</a>
		<a target="_blank" href="{$params['page']['main']}/phpinfo">PHP INFO</a>
	</div>
</div>

{$page['body']}\n
HTML;
//