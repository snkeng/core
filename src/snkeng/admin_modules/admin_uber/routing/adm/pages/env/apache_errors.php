<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Apache Errors";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-add.mjs');


//
$params['page']['ajax'].= '/apache_errors';

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Apache Errores</div>

<div se-plugin="uber_admin_adm_env_page_apache_error" data-ajaxUrl="{$params['page']['ajax']}">
	<div>
		
	</div>

	<div class="grid" >
		<div class="gr_sz03">
			<h4>Lista</h4>
			<form class="se_form" se-elem="query">
				<label class="separator required">
					<span class="title">Archivo</span>
					<select name="file"></select>
				</label>
				<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-sign-in"/></svg>Login</button>
				<output se-elem="response"></output>
			</form>
		</div>
		<div class="gr_sz06">
			<h4>Contenido</h4>
			<table se-elem="list" class="se_table border alternate hover">
				<template>
					<tr>
						<td>!time;</td>
						<td>!type;</td>
						<td>!process;</td>
						<td>!client;</td>
						<td>!data;</td>
					</tr>
				</template>
				<thead>
					<tr class="titles">
						<th>Time</th>
						<th>Type</th>
						<th>Process</th>
						<th>Client</th>
						<th>Data</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
HTML;
//