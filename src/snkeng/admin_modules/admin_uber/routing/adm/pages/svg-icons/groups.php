<?php
// Add files
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-svg-icons-groups.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-svg-icons-groups.mjs');

//
$page['head']['title'] = "SVG Icons - Groups";

// Search for available groups
$filenames = glob( $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/groups/*.json');

$fileList = '';

foreach ($filenames as $fileLocation) {
	$fileName = pathinfo($fileLocation)['filename'];
	$dtMod = date ("F d Y H:i:s.", filemtime($fileLocation));

	//
	$fileList.= <<<HTML
<tr>
<td>{$fileName}</td>
<td>{$dtMod}</td>
<td><a class="btn blue" data-target="se_middle" href="./$fileName/">Edit</a></td>
</tr>
HTML;

}

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Available groups</div>

<uber-adm-pages-svg-icons-groups data-apiurl="{$params['page']['ajax']}">

	<table>
		<thead>
			<tr>
				<th>Group name</th>
				<th>Last Mod</th>
				<th>Actions</th>
</tr>	
		</thead>
		<tbody>
			{$fileList}
		</tbody>
	</table>

</uber-adm-pages-svg-icons-groups>
HTML;
//
