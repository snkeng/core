<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "SQL - External Direct";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-external-sql-direct-query.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-direct-query.css');


// Server
$opts = '';
$struct = <<<HTML
<label class="radio"><input type="radio" name="site" value="%s" required><span class="title">%s - %s</span></label>\n
HTML;

//
$opts .= sprintf($struct, $siteVars['site']['url'], 'Live', $siteVars['site']['url']);

// Buscar en servidores de prueba
$serverParams = ( require($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_server_config.php') );
foreach ( $serverParams['locations'] as $loc => $params['vars'] ) {
	if ( isset($params['vars']['local']) && !$params['vars']['local'] ) {
		$serverUrl = "http://".$loc;
		$type = ( $params['vars']['test'] ) ? 'Test' : 'Live';
		$opts .= sprintf($struct, $serverUrl, $type, $serverUrl);
	}
}

//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Query</div>

<uber-adm-pages-external-sql-direct-query>
<form class="se_form" method="post" action="{$params['page']['ajax']}/sql" is="se-async-form">
	<div class="cont requiered">
		<span class="title">Destino</span><span class="desc"> </span>
		<div>
		{$opts}
		</div>
	</div>
	<label class="cont requiered">
		<span class="title">SQL</span><span class="desc"> </span>
		<textarea name="query" rows="15" style="width:100%; box-sizing:border-box;"></textarea>
	</label>
	<button type="submit">Enviar</button>
	<h3>Resultado</h3>
	<div se-elem="response"></div>
	<div se-elem="query"></div>
</form>
</uber-adm-pages-external-sql-direct-query>
HTML;
//
