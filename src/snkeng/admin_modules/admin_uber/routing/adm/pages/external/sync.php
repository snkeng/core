<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Sync";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-external-sync.mjs');

//
$opts = '';
$struct = <<<HTML
<label class="radio"><input type="radio" name="site" value="%s" required><span class="title">%s</span></label>\n
HTML;
//
$serverParams = ( require($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_server_config.php') );
// debugVariable($serverParams);
foreach ( $serverParams['locations'] as $loc => $params['vars'] ) {
	if ( isset($params['vars']['local']) && !$params['vars']['local'] ) {
		$opts .= sprintf($struct, $loc, $loc);
	}
}

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Syncronización</div>

<style>
	#opList { max-height: 500px; overflow-y:auto; }
	#opList li { list-style-type:none; }
	#opList li.error { background-color:#f33 }
</style>

<uber-adm-pages-external-sync class="grid" data-apiurl="{$params['page']['ajax']}">
	<div class="gr_sz04 gr_ps02">
		<h3>Subir</h3>
		<form class="se_form" data-element="update" action="{$params['page']['ajax']}/update" method="post">
			<label class="separator required">
				<span class="title">Destino</span><span class="desc"></span>
				<div>
				{$opts}
				</div>
			</label>
			<label class="separator required">
				<span class="title">Operación</span><span class="desc"> </span>
				<div>
					<label class="radio"><input type="radio" name="op" value="files_update" required><span class="title">Archivos - Actualizar</span></label>
					<label class="radio"><input type="radio" name="op" value="files_reinstall" required><span class="title">Archivos - Reinstalar</span></label>
					<label class="radio"><input type="radio" name="op" value="sql_structure" required><span class="title">SQL - Estructura</span></label>
					<br />
					<label class="radio"><input type="radio" name="op" value="sql_all" required><span class="title">*SQL - Todo</span></label>
					<label class="radio"><input type="radio" name="op" value="files_uploads" required><span class="title">*Archivos subidos</span></label>
				</div>
			</label>
			<div class="separator">
				<span class="title">Contraseña</span><span class="desc">Para operaciones de riesgo.</span>
				<input type="text" placeholder="replace" name="security" >
			</div>
			<label class="checkbox"><input type="checkbox" name="isFix" value="1" /><span>Reconstruir</span></label>
			<button type="submit" class="btn big blue"><svg class="icon inline mr"><use xlink:href="#fa-upload"/></svg>Actualizar</button>
			<output se-elem="response"></output>
		</form>
		<div id="opList"></div>
	</div>

	<div class="gr_sz04">
		<h3>Descargar</h3>
		<form class="se_form" data-element="sync" action="{$params['page']['ajax']}/sync" method="post" is="se-async-form">
			<div class="separator required">
				<span class="title">Destino</span><span class="desc"></span>
				<div>
					{$opts}
				</div>
			</div>
			<div class="separator required">
				<span class="title">Operación</span><span class="desc"></span>
				<div>
				<label class="radio"><input type="radio" name="op" value="files_update" required><span class="title">SQL - Respaldar</span></label>
				<label class="radio"><input type="radio" name="op" value="files_reinstall" required><span class="title">Archivos - Respaldar</span></label>
				</div>
			</div>
			<div class="separator">
				<span class="title">Contraseña</span><span class="desc">Para operaciones de riesgo.</span>
				<input type="text" placeholder="replace" name="security" >
			</div>
			<button type="submit" class="btn big blue"><svg class="icon inline mr"><use xlink:href="#fa-download"/></svg>Descargar</button>
			<output se-elem="response"></output>
		</form>
	</div>
</uber-adm-pages-external-sync>
HTML;
//
