<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "SQL - Tablas";

//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td><input type="checkbox" /></td>
	<td>!id;</td>
	<td>!modTitle;</td>
	<td>!title;</td>
	<td>!dbName;</td>
	<td>!dbNick;</td>
	<td><span class="table_status s!upToDate;"></span></td>
</tr>
HTML;
//

//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'] . '',
	'printStruct' => true,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'Sel'],
		['name' => 'ID'],
		['name' => 'Modulo', 'filter' => 1, 'fName' => 'modTitle'],
		['name' => 'Nombre', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Nombre DB', 'filter' => 1, 'fName' => 'dbName'],
		['name' => 'Nick DB', 'filter' => 1, 'fName' => 'dbNick'],
		['name' => 'Vigente']
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"update":
	{
		"action":"function",
		"ajax-action":"none",
		"menu":{"title":"Actualizar", "icon":"fa-clock-o"},
		"ajax-elem":"sel",
		"function":function(content, pDefaults) {
			var elIds = [];
			content.se_each((id, cEl) => {
				elIds.push(cEl.se_data("objid"));
			});
			if ( confirm("Actualizar las tablas seleccionadas.") ) {
				se.ajax.json("{$params["page"]["ajax"]}/table_multi/update",
					se.object.merge(pDefaults, { "act":"truncate", "elems":elIds.join() }), {
						onSuccess:function (msg) {
							$("#pagElements").ajaxSearch.reload();
						}
					}
				);
			}
		}
	},
	"truncate":
	{
		"action":"function",
		"ajax-action":"none",
		"menu":{"title":"Truncar", "icon":"fa-trash-o"},
		"ajax-elem":"sel",
		"function":(content, pDefaults) => {
			var elIds = [];
			content.se_each(function(id, cEl){
				elIds.push(cEl.se_data("objid"));
			});
			se.ajax.json("{$params["page"]["ajax"]}/table_multi/truncate",
				se.object.merge(pDefaults, { "act":"truncate", "elems":elIds.join() }), {
					onSuccess:function (msg) {
						console.log(msg.d.qry);
					}
				}
			);
		}
	},
	"delete":
	{
		"action":"function",
		"ajax-action":"none",
		"menu":{"title":"Borrar (drop y gtable)", "icon":"fa-trash-o"},
		"ajax-elem":"sel",
		"function":(content, pDefaults) => {
			var elIds = [];
			content.se_each(function(id, cEl){
				elIds.push(cEl.se_data("objid"));
			});
			se.ajax.json("{$params["page"]["ajax"]}/table_multi/drop",
				se.object.merge(pDefaults, { "act":"truncate", "elems":elIds.join() }), {
					onSuccess:function (msg) {
						console.log(msg.d.qry);
					}
				}
			);
		}
	},
	"deleteAll":
	{
		"action":"function",
		"ajax-action":"none",
		"menu":{"title":"Borrar (drop, gtable, sa_table_object)", "icon":"fa-trash-o"},
		"ajax-elem":"sel",
		"function":(content, pDefaults) => {
			var elIds = [];
			content.se_each(function(id, cEl){
				elIds.push(cEl.se_data("objid"));
			});
			se.ajax.json("{$params["page"]["ajax"]}/table_multi/system",
				se.object.merge(pDefaults, { "act":"truncate", "elems":elIds.join() }), {
					onSuccess:function (msg) {
						console.log(msg.d.qry);
					}
				}
			);
		}
	}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<style>
	.table_status { display:block; margin:0 auto; border-radius:50%; height:15px; width:15px; }
	.table_status.s0 { background-color:#F00; }
	.table_status.s1 { background-color:#0F0; }
</style>

<div class="pageSubTitle">Tablas</div>

{$tableStructure}
HTML;
//
