<?php

//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'SQL');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/direct', 'Query Directo'],
	[$params['page']['main'].'/ops', 'Operaciones'],
	[$params['page']['main'].'/structure', 'Estructura'],
	[$params['page']['main'].'/tables', 'Tablas Borrar'],
	[$params['page']['main'].'/backup', 'Backup'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'direct':
		require __DIR__ . '/direct.php';
		break;
	//
	case 'ops':
		require __DIR__ . '/ops.php';
		break;
	//
	case 'structure':
		require __DIR__ . '/structure.php';
		break;
	//
	case 'tables':
		require __DIR__ . '/tables.php';
		break;
	//
	case 'backup':
		require __DIR__ . '/backup.php';
		break;
	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">SQL</div>

{$nav}

{$page['body']}\n
HTML;
//