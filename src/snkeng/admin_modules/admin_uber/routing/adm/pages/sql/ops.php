<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Operaciones de limpieza";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-cleanup-ops.mjs');

//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Operaciones de limpieza</div>

<uber-adm-pages-sql-cleanup-ops class="grid" data-ajaxurl="{$params['page']['ajax']}">
	<div class="gr_sz04 verticalListObjects">
		<button data-action="clearGTables" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-pencil"/></svg> Limpiar.</button>
		<button data-action="showTables" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-trash-o"/></svg> Devolver tablas invisibles.</button>
	</div>
	<div class="gr_sz08">
		<pre id="sql_output"></pre>
	</div>
</uber-adm-pages-sql-cleanup-ops>
HTML;
//
