<?php

//
\snkeng\core\engine\login::kill_loginLevel('uadmin');

//
\snkeng\core\engine\nav::navPathSet('main', $appData['url']);
\snkeng\core\engine\nav::navPathSet('ajax', $appData['url_json']);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'modules':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/modules/modules.php';
		break;

	//
	case 'tables':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/tables/#map.php';
		break;

	//
	case 'env':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/env/#map.php';
		break;

	//
	case 'content':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/content/#map.php';
		break;

	//
	case 'svg-icons':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/svg-icons/#map.php';
		break;

	//
	case 'external':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/external/#map.php';
		break;

	//
	case 'sql':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/sql/#map.php';
		break;

	//
	case 'system':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

		require __DIR__ . '/system/#map.php';
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
