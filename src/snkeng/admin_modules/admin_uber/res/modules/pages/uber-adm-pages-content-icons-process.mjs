import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-content-icons-process', class extends HTMLElement {

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.ajaxUrl = this.dataset['ajaxurl'];

		//
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_faProc(e, cEl) {
		cEl.innerText = "Cargando...";

		//
		formActions.postRequest(
			this.ajaxUrl + '/icons/fa-proc',
			{},
			{
				onSuccess:function(msg){
					cEl.innerText = msg.d;
				},
				onFail:function(msg){
					cEl.innerText = msg.title;
				}
			}
		);
	}

	//
	onClick_svgMerge(e, cEl) {
		cEl.innerText = "Cargando...";

		//
		formActions.postRequest(
			this.ajaxUrl + '/svg_merge',
			{},
			{
				onSuccess:function(msg){
					cEl.innerText = msg.d;
				},
				onFail:function(msg){
					cEl.innerText = msg.title;
				}
			}
		);
	}
});