import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';

//
customElements.define('uber-adm-pages-tables-content', class extends HTMLElement {
	//
	elemListGeneralHead = this.querySelector('table[se-elem="elemListGeneral"] thead');
	elemListGeneralBody = this.querySelector('table[se-elem="elemListGeneral"] tbody');
	selectQueryForm = this.querySelector('form[se-elem="selectQueryForm"]');
	updateQueryForm = this.querySelector('form[se-elem="updateQueryForm"]');
	elemTitles = this.querySelector('div[se-elem="elemTitles"]');
	elemContent = this.querySelector('div[se-elem="elemContent"]');

	//
	mainUrl = this.dataset['ajaxurl'];
	tableName = this.dataset['tablename'];
	tableId = this.dataset['tableid'];

	//
	dataTableRow = '';
	tableElems = [];

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Adjust forms
		this.selectQueryForm.action = this.mainUrl + "/queryExec";
		this.updateQueryForm.action = this.mainUrl + "/updateBulk";
		this.selectQueryForm.method = "post";
		this.updateQueryForm.method = "post";

		// Bind forms
		this.selectQueryForm.addEventListener('submit', this.selectQueryFormSubmit.bind(this));
		this.updateQueryForm.addEventListener('submit', this.updateQueryFormSubmit.bind(this));


		this.elemContent.se_on('click', 'button[se-act]', this.elemAction.bind(this));
		this.elemListGeneralHead.se_on('change', ['input', 'select'], this.tableFilter.bind(this));
		this.elemListGeneralBody.se_on('change', ['input', 'select'], this.qryBuildGeneral.bind(this));
		//
		this.elemContent.se_on('change', ['input', 'checkbox', 'textarea'], this.rowSelect.bind(this));

		//
		this.qryBuildGeneral();
	}

	//
	selectQueryFormSubmit(e) {
		e.preventDefault();
		formActions.formSubmit(
			this.selectQueryForm,
			{
				onRequest: this.contentRequest.bind(this),
				onSuccess: this.contentSuccess.bind(this)
			}
		);
	}

	//
	updateQueryFormSubmit(e) {
		e.preventDefault();
		formActions.formSubmit(
			this.updateQueryForm,
			{
				onComplete: (msg) => {
					this.querySelector('output[se-elem="response"]').innerText = msg.d;
				}
			}
		);
	}

	//
	contentRequest(ev) {
		let tHead = '';
		//
		tHead += '<span>Actions</span>';
		tHead += '<span>ID</span>';
		//
		this.dataTableRow = '<form data-id="!' + this.tableId + ';"><span>';
		this.dataTableRow += '<button class="btn" type="submit" se-act="save"><svg class="icon"><use xlink:href="#fa-save"/></svg></button>';
		this.dataTableRow += '<button class="btn" type="button" se-act="delete"><svg class="icon"><use xlink:href="#fa-trash-o"/></svg></button></span>';
		this.dataTableRow += '<span>!' + this.tableId + ';</span>';

		//
		for ( let cEl of this.tableElems ) {
			let isReq = ( cEl.notnull ) ? ' required' : '',
				xtra;

			//
			tHead += '<span>' + cEl.title + '</span>';

			// Row create
			this.dataTableRow += '<span>';
			switch ( cEl.type ) {
				case 'BOOLEAN':
					this.dataTableRow += '<input type="checkbox" name="' + cEl.dbName + '" data-type="bool" value="1" se-fix="!' + cEl.dbName + ';" />';
					break;
				case 'TINYINT':
					xtra = (cEl.unsigned) ? ' min="0" max="255"' : ' min="-128" max="127"';
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" data-type="int"' + xtra + '' + isReq + '/>';
					break;
				case 'SMALLINT':
					xtra = (cEl.unsigned) ? ' min="0" max="65535"' : ' min="-32768" max="32767"';
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" data-type="int"' + xtra + '' + isReq + '/>';
					break;
				case 'MEDIUMINT':
					xtra = (cEl.unsigned) ? ' min="0" max="16777215"' : ' min="-8388608" max="8388607"';
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" data-type="int"' + xtra + '' + isReq + '/>';
					break;
				case 'INT':
					xtra = (cEl.unsigned) ? ' min="0" max="4294967295"' : ' min="-2147483648" max="2147483647"';
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" data-type="int"' + xtra + '' + isReq + '/>';
					break;
				case 'BIGINT':
					xtra = (cEl.unsigned) ? ' min="0" max="18446744073709551615"' : ' min="-9223372036854775808" max=" 9223372036854775807"';
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" data-type="int"' + xtra + '' + isReq + '/>';
					break;
				case 'FLOAT':
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" step="any" data-type="float"' + isReq + '/>';
					break;
				case 'DOUBLE':
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" step="any" data-type="float"' + isReq + '/>';
					break;
				case 'DECIMAL':
					this.dataTableRow += '<input type="number" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" step="any" data-type="float"' + isReq + '/>';
					break;
				case 'CHAR':
				case 'VARCHAR':
					this.dataTableRow += '<input type="text" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" maxlength="' + cEl.typedesc + '"' + isReq + '/>';
					break;
				case 'TINYTEXT':
					this.dataTableRow += '<textarea name="' + cEl.dbName + '" maxlength="255"' + isReq + '>!' + cEl.dbName + ';</textarea>';
					break;
				case 'TEXT':
					this.dataTableRow += '<textarea name="' + cEl.dbName + '" maxlength="65535"' + isReq + '>!' + cEl.dbName + ';</textarea>';
					break;
				case 'MEDIUMTEXT':
					this.dataTableRow += '<textarea name="' + cEl.dbName + '" maxlength="16777215"' + isReq + '>!' + cEl.dbName + ';</textarea>';
					break;
				case 'LONGTEXT':
					this.dataTableRow += '<textarea name="' + cEl.dbName + '" maxlength="4294967295"' + isReq + '>!' + cEl.dbName + ';</textarea>';
					break;
				case 'DATETIMEz':
					this.dataTableRow += '<input type="datetime-local" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';"' + isReq + '/>';
					break;
				case 'DATETIME':
					this.dataTableRow += '<input type="text" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';" pattern="([0-9]{4})-([0-1][0-9])-([0-3][0-9])\s?(?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?"' + isReq + '/>';
					break;
				case 'DATE':
					this.dataTableRow += '<input type="date" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';"' + isReq + '/>';
					break;
				case 'YEAR':
					this.dataTableRow += '<input type="number" min="1970" maxlength="2100" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';"' + isReq + '/>';
					break;
				case 'TIME':
					this.dataTableRow += '<input type="time" name="' + cEl.dbName + '" value="!' + cEl.dbName + ';"' + isReq + '/>';
					break;
				case 'TIMESTAMP':
					this.dataTableRow += '!' + cEl.dbName + ';';
					break;
				default:
					console.log("typo no definido:", cEl);
					break;
			}
			this.dataTableRow += '</span>';
		}

		//
		this.dataTableRow += '</form>';

		//
		this.elemTitles.innerHTML = tHead;
	}

	contentSuccess(msg) {
		// Clear
		this.elemContent.innerHTML = '';

		//
		domManipulator.insertContentFromTemplate(
			this.elemContent,
			'beforeend',
			this.dataTableRow,
			msg.d
		);

		//
		domManipulator.fixInputValues(this.elemContent);
	}

	//
	elemAction(ev) {
		ev.preventDefault();
		let cBtn = ev.target,
			cEl = cBtn.closest('form');
		switch ( cBtn.getAttribute('se-act') ) {
			case 'save':
				this.rowSave(cEl);
				break;
			case 'delete':
				this.rowDelete(cEl);
				break;
		}
	}

	//
	rowSave(cForm) {
		//
		let elements = cForm.querySelectorAll('input, select, textarea'),
			data = {}, len = elements.length, cEl;
		if ( elements ) {
			for ( cEl of elements ) {
				if ( cEl.name ) {
					data[cEl.name] = {
						type: cEl.dataset['type'],
						value: ''
					};
					//
					if ( cEl.type !== 'checkbox' ) {
						data[cEl.name].value = cForm.elements.namedItem(cEl.name).value;
					} else if ( cEl.type === 'checkbox' ) {
						data[cEl.name].value = cEl.checked ? 1 : 0;
					}
				}
			}

			console.log("Guardar", data);
			formActions.postRequest(
				this.mainUrl + '/updateSimple',
				{
					tName: this.tableName,
					idName: this.tableId,
					idValue: cForm.dataset['id'],
					data: JSON.stringify(data)
				},
				{
					onSuccess: function (msg) {
						cForm.classList.remove('mod');
						console.log("Actualizado.");
					}
				}
			);
		} else {
			console.log('WHAAAT?');
			return false;
		}
	}

	rowDelete(cForm) {
		if ( !confirm('Borrar elemento') ) {
			return;
		}

		//
		formActions.postRequest(
			this.mainUrl + '/delete',
			{
				tName: this.tableName,
				idName: this.tableId,
				idValue: cForm.dataset['id'],
			},
			{
				onSuccess: (msg) => {
					cForm.se_remove();
				}
			}
		);
	}

	rowSelect(ev) {
		let cEl = ev.target,
			cRow = cEl.closest('form');

		//
		cRow.classList.add('mod');
	}

	qryBuildGeneral() {
		// Update
		let cRows = this.elemListGeneralBody.querySelectorAll('tr'),
			rowParams = {
				select: [],
				update: [],
				where: [],
				order: []
			},
			elements = [];

		//
		this.tableElems = [];

		//
		cRows.forEach((cRow) => {
			let cTabName = cRow.dataset['name'],
				cInputSelectVal = cRow.querySelector('input[name="select"]').checked,
				cInputUpdateVal = cRow.querySelector('input[name="update"]').value.trim(),
				cInputWhereValue = cRow.querySelector('input[name="where_value"]').value.trim(),
				cInputWhereType = cRow.querySelector('select[name="where_type"]').value.trim(),
				cInputOrderPriority = cRow.querySelector('input[name="order_priority"]').value.trim(),
				cInputOrderType = cRow.querySelector('select[name="order_type"]').value.trim();

			// Select
			if ( cInputSelectVal ) {
				let selString = '';
				//
				switch ( cRow.dataset['dbtype'] ) {
					//
					case 'DATETIME':
						selString = "DATE_FORMAT(" + cTabName + ", '%Y-%m-%dT%T') AS " + cTabName;
						break;
					//
					default:
						selString = cTabName;
						break;
				}

				let eProps = JSON.parse(cRow.dataset['props']);
				this.tableElems.push(eProps);

				//
				rowParams.select.push(selString);
			}

			// Update
			if ( cInputUpdateVal ) {
				rowParams.update.push(`${cTabName}='${cInputUpdateVal}'`);
			}

			// Where
			if ( cInputWhereValue ) {
				//
				switch ( cInputWhereType ) {
					//
					case 'default':
						rowParams.where.push(`${cTabName}='${cInputWhereValue}'`);
						break;
					//
					case 'like':
						rowParams.where.push(`${cTabName} LIKE '%${cInputWhereValue}%'`);
						break;
					//
					case 'inInt':
						cInputWhereValue = cInputWhereValue.replace(/[\s,]+/g, ',').replace(/[^\d,]/g, '').replace(/,+/g, ',');
						let cMod = cInputWhereValue.split(',');
						cMod = cMod.filter((item, index) => {
							return cMod.indexOf(item) === index;
						});
						cMod.sort((a, b) => {
							return a - b;
						});
						//
						cInputWhereValue = cMod.join(',');
						//
						cRow.querySelector('input[name="value"]').value = cInputWhereValue;

						rowParams.where.push(`${cTabName} IN (${cInputWhereValue})`);
						break;
					//
					default:
						console.error("caso no programado", cInputWhereType);
						break;
				}
			}

			// Order
			if ( cInputOrderType ) {
				elements.push({
					name: cTabName,
					order: intVal(cInputOrderPriority),
					sort: cInputOrderType
				});
			}
		});
		//

		//
		elements.sort((a, b) => {
			return a.order - b.order;
		});

		//
		elements.forEach((cEl) => {
			rowParams.order.push(`${cEl.name} ${cEl.sort}`);
		});

		//
		this.selectQueryForm.se_formElVal('elems', rowParams.select.join(', '));
		//
		this.updateQueryForm.se_formElVal('elems', rowParams.update.join(', '));
		//
		this.selectQueryForm.se_formElVal('where', rowParams.where.join(' AND '));
		this.updateQueryForm.se_formElVal('where', rowParams.where.join(' AND '));
		//
		this.selectQueryForm.se_formElVal('order', rowParams.order.join(', '));
		this.updateQueryForm.se_formElVal('order', rowParams.order.join(', '));
	}

	tableFilter(e) {
		e.preventDefault();

		// Get filters
		let cFiltersInput = this.elemListGeneralHead.querySelectorAll('input'),
			cFiltersValue = [];

		// Create filters
		cFiltersInput.forEach((cInput) => {
			if ( cInput.value ) {
				cFiltersValue.push({
					index: cInput.parentElement.se_index(),
					value: cInput.value.toLowerCase()
				});
			}
		});

		// No filters, reset all
		let bodyRows = this.elemListGeneralBody.querySelectorAll('tr');
		if ( cFiltersValue.length === 0 ) {
			bodyRows.setAttribute('aria-hidden', 'false');
			return;
		}

		// Check for filters
		bodyRows.forEach((cRow) => {
			let cHidden = 'false',
				cTD = cRow.querySelectorAll('td');

			console.warn("check row", cTD);
			for ( let cFilter of cFiltersValue ) {
				let cText = cTD[cFilter.index].textContent || cTD[cFilter.index].innerText;
				if ( !cText.includes(cFilter.value) ) {
					cHidden = 'true';
					break;
				}
			}
			cRow.setAttribute('aria-hidden', cHidden);
		});
	}
});