import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('uber-adm-pages-system-console', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.structureForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess:(msg) => {
							let console = document.getElementById('adm_console'),
								content = console.querySelector('div.content'),
								input = console.querySelector('input'),
								command = input.value;

							//
							input.value = '';
							content.innerHTML = content.innerHTML + "\\n---\\n" + command + "\\n" + msg.d;
						}
					},
				}),
			);
		});


		//
		this.structureForm.addEventListener('submit', this.submitForm.bind(this));
	}
});