import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('uber-adm-pages-sql-structure', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.structureForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: (msg) => {
							this.querySelector('output[se-elem="response"]').innerText = msg.d;
						}
					},
				}),
			);
		});
	}
});