import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('uber-adm-pages-system-db-struct', class extends HTMLElement {
	downloadResponse = this.querySelector('textarea[data-element="download"]');
	structUploadForm = this.querySelector('form[data-element="structUpload"]');
	endResultResponse = this.querySelector('textarea[data-element="result"]');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.mainAjax = this.dataset['apiurl'];

		//
		this.structUploadForm.action = this.mainAjax + '/upload';

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.structUploadForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess:(msg) => {
							this.endResultResponse.value = msg.d;
						}
					},
				}),
			);
		});

		//
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_copyDownload(ev) {
		ev.preventDefault();

		/* Select the text field */
		this.downloadResponse.select();
		this.downloadResponse.setSelectionRange(0, 99999); /* For mobile devices */

		document.execCommand("copy");
		alert("copiado descarga del sistema");
	}

	//
	onClick_copyEnd(ev, cBtn) {
		ev.preventDefault();

		/* Select the text field */
		this.endResultResponse.select();
		this.endResultResponse.setSelectionRange(0, 99999); /* For mobile devices */

		document.execCommand("copy");
		alert("copiado resultado final");
	}

	//
	onClick_download(ev) {
		ev.preventDefault();

		//
		formActions.postRequest(
			this.mainAjax + '/download',
			null,
			{
				method:'get',
				onSuccess:(msg) => {
					this.downloadResponse.value = msg.d;
				},
				onFail:(msg) => {
					this.downloadResponse.value = msg.title;
				}
			}
		);
	}
});