import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';

//
customElements.define('uber-adm-pages-content-icons-pick', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.apiUrl = this.dataset['apiurl'];

		// Convert file information to usable object
		const scriptElement = this.querySelector('script[data-element="fileData"]');
		this.fileData = JSON.parse(scriptElement.textContent);

		// Bindings
		this.querySelector('#iconList').se_on('click', '.iconSelector', this.iconSelect.bind(this));
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		this.querySelector('select').addEventListener('change', this.getNewSVG.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-iconpick-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['iconpickAction'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	getNewSVG(e) {
		e.preventDefault();
		let cSVGLibrary = e.target.value;

		if ( !cSVGLibrary ) {
			return;
		}

		this.currentLibrary = cSVGLibrary;

		let targetUrl = '/res/svg-icons-ops/libraries/' + cSVGLibrary + '.svg';

		let svgContainer = this.querySelector('.svgContainer'),
			iconListTemplateString = this.querySelector('template').innerHTML,
			iconListContent = this.querySelector('#iconList'),
			currentElements = this.fileData[cSVGLibrary] ?? [];

		// Clear element list
		iconListContent.innerHTML = '';

		// Get file

		// Create a request object
		const request = new XMLHttpRequest();

		// Open request with GET method and handle response
		request.open("GET", targetUrl, true);
		request.responseType = "text"; // Set response type to text to get SVG code

		//
		request.onload = function() {
			if (request.status === 200) {
				const svgData = request.responseText;

				// Parse the SVG code using DOMParser
				const parser = new DOMParser();
				const svgDoc = parser.parseFromString(svgData, "image/svg+xml");

				// Append the SVG element to the body ?
				// svgContainer.appendChild(svgDoc.documentElement);

				// Get all elements and apply them to template
				let symbolList = svgDoc.querySelectorAll('symbol');

				//
				for ( let cSymbol of symbolList ) {
					//
					domManipulator.insertContentFromTemplate(
						iconListContent,
						'beforeend',
						iconListTemplateString,
						{
							'isSelected': ( currentElements.includes(cSymbol.id) ) ? 'true' : 'false',
							'symbolViewBox': cSymbol.getAttribute('viewBox'),
							'symbolId': cSymbol.id,
							'symbolContents': cSymbol.innerHTML,
						}
					);
				}

			} else {
				console.error("Failed to download SVG:", request.statusText);
				return;
			}
		};

		// Send the request to download the SVG
		request.send();

	}

	//
	onClick_upload(e, cBtn) {
		//
		formActions.postRequest(
			this.apiUrl + '/select',
			{
				'list': JSON.stringify(this.fileData),
				'file': this.dataset['groupname']
			},
			{
				response: this.querySelector('output[data-type="upload"]')
			}
		);
	}


	//
	iconSelect(ev, cEl) {
		let currentState = cEl.getAttribute('aria-selected'),
			symbolId = cEl.dataset['id'],
			newState;

		//
		if ( currentState === 'true' ) {
			newState = 'false';
			// Remove from list
			let index = this.fileData[this.currentLibrary].indexOf(symbolId);
			if ( index !== -1 ) {
				this.fileData[this.currentLibrary].splice(index, 1);
			}
		} else {
			newState = 'true';
			// Safety check
			if ( !this.fileData.hasOwnProperty(this.currentLibrary) ) {
				this.fileData[this.currentLibrary] = [];
			}

			this.fileData[this.currentLibrary].push(symbolId);
		}

		// Modify state
		cEl.setAttribute('aria-selected', newState);
	}

	//
	onClick_filter(e, cBtn) {
		$('#iconList').dataset['status'] = cBtn.dataset['status'];
	}
});