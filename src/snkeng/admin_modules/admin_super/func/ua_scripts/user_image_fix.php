<?php

function purifyFileName($fileName) {
	$fileName = str_replace(['.jpg', '.png', '.webp', '.avif'], '', $fileName);

	return $fileName;
}

//
$upd_qry = '';

//
$sql_qry = <<<SQL
SELECT
	a_id AS id, a_img_full AS imgFull, a_img_crop AS imgCrop, a_banner_full AS bannerFull, a_banner_crop as bannerCrop
FROM sb_objects_obj;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		$datos['imgFull'] = purifyFileName($datos['imgFull']);
		$datos['imgCrop'] = purifyFileName($datos['imgCrop']);
		$datos['bannerFull'] = purifyFileName($datos['bannerFull']);
		$datos['bannerCrop'] = purifyFileName($datos['bannerCrop']);

		//
		$upd_qry.= <<<SQL
UPDATE sb_objects_obj SET a_img_full='{$datos['imgFull']}', a_img_crop='{$datos['imgCrop']}', a_banner_full='{$datos['bannerFull']}', a_banner_crop='{$datos['bannerCrop']}' WHERE a_id='{$datos['id']}';\n
SQL;
	}
}

//
$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;
