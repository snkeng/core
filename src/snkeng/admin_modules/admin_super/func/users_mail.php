<?php
//
$nav = [
	'sel' => "admail.am_id AS id, admail.am_mail AS email,
				admail.am_status AS status, admail.am_type AS type, admail.am_dt_add AS dtAdd",
	'from' => 'sb_users_mail AS admail',
	'lim' => 20,
	'where' => [
		'id' => ['name' => 'ID Usuario', 'db' => 'admail.a_id', 'vtype' => 'int', 'stype' => 'eq'],
	],
	'order' => [
		'am_id' => ['Name' => 'ID', 'db' => 'admail.am_id', 'set' => 'DESC']
	]
];
return $nav;
