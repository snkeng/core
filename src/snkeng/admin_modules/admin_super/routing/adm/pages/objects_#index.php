<?php
// Cache check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td>!fullName;</td>
	<td>!urlNameFull;</td>
	<td>!urlNameSimple;</td>
	<td>!dtAdd;</td>
	<td>!type;</td>
	<td class="actions">
		<a class="btn small" se-nav="app_content" href="{$appData['url']}/objects/!id;/" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye"/></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="name" title="Nombres"><svg class="icon inline"><use xlink:href="#fa-pencil"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="url" title="URL"><svg class="icon inline"><use xlink:href="#fa-chain"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o"/></svg></button>
	</td>
</tr>
HTML;
//

//
$an_empty = "No hay elementos que mostrar.";

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	
	'printType'=>'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'name'],
		['name' => 'Nombre URL', 'filter' => 1, 'fName' => 'urlname'],
		['name' => 'Nombre URL Simple', 'filter' => 1, 'fName' => 'urlnamesimple'],
		['name' => 'Creado'],
		['name' => 'Tipo', 'filter' => 1, 'fName' => 'type'],
		['name' => 'Acciones'],
	],
	'settings'=>['nav'=>true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"name": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "names"
					}
				},
				"type": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Tipo",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "names"
					}
				}
			}
		}
	},
	"name":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Nombre",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/nameRead",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/nameUpd",
			"actName":"Guardar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int",
					"default_value": ""
				},
				"uName": {
					"field_type": "input",
					"data_type": "string",
					"default_value": "",
					"required": false,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				}
			}
		}
	},
	"url":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"URL",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/urlRead",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/urlUpd",
			"actName":"Guardar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int",
					"default_value": ""
				},
				"url": {
					"field_type": "input",
					"data_type": "string",
					"default_value": "",
					"required": false,
					"info_name": "URL",
					"info_description": "",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "url":"{$params["page"]["ajax"]}/{id}/del"}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);


//
$page['head']['title'] = 'Objetos en lista';

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Objetos</div>

{$tableStructure}
HTML;
//
