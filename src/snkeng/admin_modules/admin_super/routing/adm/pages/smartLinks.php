<?php
//
$params['page']['ajax'].= '/smartLinks';

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!appName;</td>
	<td>!actName;</td>
	<td>!actId;</td>
	<td><span class="fakeCheck" data-value="!isActive;"></span></td>
	<td><time datetime="!dtAddFull;" title="!dtAddFull;">!dtAdd;</time></td>
	<td><time datetime="!dtEndFull;" title="!dtEndFull;">!dtEnd;</time></td>
	<td><span class="fakeCheck" data-value="!status;"></span></td>
	<td class="actions">
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printType'=>'table',
	'tableWide' => true,
	'printContent' => false,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'App Name', 'filter' => 1, 'fName' => 'appName'],
		['name' => 'Act Title', 'filter' => 1, 'fName' => 'actName'],
		['name' => 'Act ID'],
		['name' => 'Activo'],
		['name' => 'DT Add'],
		['name' => 'Dt End'],
		['name' => 'Status'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Editar Smart Links",
			"load_url":"{$params["page"]["ajax"]}/statusRead",
			"save_url":"{$params["page"]["ajax"]}/statusUpd",
			"defaults":{},
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				},
				"ver": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Versión",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				},
				"content": {
					"field_type": "textarea",
					"data_type": "str",
					"info_name": "Descripción"
				}
			}
		}
	}
}
JSON
];
//

//
$an_qry = (require __DIR__ . '/../../../navs/smartLinks.php');

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Sitio General
$page['head']['title'] = "Smart Links";

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Smart Links</div>

{$tableStructure}
HTML;
//
