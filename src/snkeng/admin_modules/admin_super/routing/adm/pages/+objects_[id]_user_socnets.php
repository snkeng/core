<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();


// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td>!snType;</td>
	<td>!snId;</td>
	<td>!dtAdd;</td>
</tr>
HTML;
//
$exData = [
	'js_url' => $params['page']['ajax'] . '/readSingle',
	
	'printType'=>'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Red social'],
		['name' => 'Status'],
		['name' => 'Creado'],
	],
	'settings'=>['nav'=>true],
	'actions' => <<<JSON
{
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);


// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Redes sociales</div>

{$tableStructure}
HTML;
//
