<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case '':
	case null:
		require __DIR__ . '/+objects_#index.php';
		break;

	//
	case 'properties':
		\snkeng\core\engine\nav::pageFileGroupAdd(['objects']);
		require __DIR__ . '/+objects_properties.php';
		break;

	//
	case 'notifications':
		require __DIR__ . '/+objects_notifications.php';
		break;

	//
	case 'settings':
		require __DIR__ . '/+objects_settings.php';
		break;

	//
	case 'relationships_in':
		require __DIR__ . '/+objects_relationships_in.php';
		break;

	//
	case 'relationships_out':
		require __DIR__ . '/+objects_relationships_out.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Single
$page['body'] = <<<HTML
<div class="pageTitle">Objetos</div>

<div class="nav4">
	<div class="breadcrumbs">
		<a se-nav="app_content" href="{$appData['url']}/objects">Objetos</a>
	</div>
	<div class="normal">
		<a se-nav="app_content" href="{$appData['url']}/objects/{$params['vars']['objId']}/properties">Propiedades</a>
		<a se-nav="app_content" href="{$appData['url']}/objects/{$params['vars']['objId']}/notifications">Notificaciones</a>
		<a se-nav="app_content" href="{$appData['url']}/objects/{$params['vars']['objId']}/properties">Configuración</a>
		<a se-nav="app_content" href="{$appData['url']}/objects/{$params['vars']['objId']}/relationships_in">Relaciones Internas</a>
		<a se-nav="app_content" href="{$appData['url']}/objects/{$params['vars']['objId']}/relationships_out">Relaciones Externas</a>
	</div>
</div>

{$page['body']}
HTML;
//