<?php
//
$params['page']['ajax'].= '/scripts';

//
$optList = '';
$scripts = glob(__DIR__.'/../../func/ua_scripts/*');
foreach ( $scripts as $script ) {
	//
	$scriptName = basename ($script, '.php');

	//
	$optList.= <<<HTML
<option value="{$scriptName}">{$scriptName}</option>\n
HTML;

}

// Estructura
$page['body'] = <<<HTML
<div class="pageSubTitle">Operaciones</div>
<div class="grid" se-plugin="se_uAdminScript" data-ajaxpage="{$params['page']['ajax']}">
<div class="gr_sz02">
	<form class="tabLessForm" se-elem="ua_scripts">
		<select class="elem" name="script">{$optList}</select>
		<input type="text" name="params" placeholder="extra parameters, coma separated" />
		<label class="elem">
			<input type="checkbox" name="exec" value="1" /><span>Ejecutar</span>
		</label>
		<button type="submit" class="btn wide blude"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Ejecutar</button>
	</form>
</div>
<div class="gr_sz10">
	<output se-elem="response" class="container_simple"></output>
</div>
</div>
HTML;
//
