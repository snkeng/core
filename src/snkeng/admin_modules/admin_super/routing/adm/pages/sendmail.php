<?php
//
$params['page']['ajax'].= '/sendmail';

// Sitio General
$page['head']['title'] = "Enviar correo";

// Página
$page['body'] = <<<HTML
<style></style>
<div class="pageTitle">Enviar correo</div>
<!-- INI:BLOG -->
<div class="wpContent grid">
<!-- INI:POSTS -->
<div class="gr_sz04 gr_ps04">
	<form class="se_form" method="post" action="{$params['page']['ajax']}/send" id="examBatch" autocomplete="off" se-plugin="simpleForm">
		<div class="separator required"><label>
			<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
			<input type="text" name="fName" required />
		</label></div>
		<div class="separator required"><label>
			<div class="cont"><span class="title">Apellido</span><span class="desc"></span></div>
			<input type="text" name="lName" required />
		</label></div>
		<div class="separator required"><label>
			<div class="cont"><span class="title">Correo</span><span class="desc"></span></div>
			<input type="email" name="eMail" required />
		</label></div>
		<div class="separator required"><label>
			<div class="cont"><span class="title">Título</span><span class="desc"></span></div>
			<input type="text" name="title" required />
		</label></div>
		<div class="separator required"><label>
			<div class="cont"><span class="title">Mensaje</span><span class="desc"></span></div>
			<textarea name="content"></textarea>
		</label></div>
		<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-mail-forward" /></svg>Enviar</button>
		<output se-elem="response"></output>
	</form>
</div>
<!-- END:BLOG -->
</div>\n
HTML;
//
