<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

\snkeng\core\engine\nav::pageFileGroupAdd(['jsoneditor']);

$user_perms_file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_permits.php';
if ( !file_exists($user_perms_file) ) {
	\snkeng\core\engine\nav::killWithError('Archivo no encontrado.', 'No se encontró el archivo de configuración de permisos.', 500);
}

//
$usr_perms_data = (require $user_perms_file);
$usr_perms_json = json_encode($usr_perms_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

// Page
$page['body'] = <<<HTML
<h1>Permisos para administradores</h1>
<pre>{$usr_perms_json}</pre>


<div id="jsoneditor" style="width: 400px; height: 400px;"></div>
HTML;
//

//
$page['mt'].= <<<JS
// create the editor
const container = document.getElementById("jsoneditor")
const options = {}
const editor = new JSONEditor(container, options)

// set json
const initialJson = {$usr_perms_json};
editor.set(initialJson)

// get json
const updatedJson = editor.get();
JS;
//
