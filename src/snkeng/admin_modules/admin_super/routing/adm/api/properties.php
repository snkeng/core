<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "oProp.op_id AS id, oProp.a_id AS objId, 
			      oProp.op_dt_add AS dtAdd, oProp.op_dt_mod AS dtMod,
			      oProp.app_name AS name, oProp.app_type AS type",
				'from' => 'sb_objects_properties AS oProp',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Relación', 'db' => 'oProp.op_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objId' => ['name' => 'ID Usuario', 'db' => 'oProp.a_id', 'vtype' => 'int', 'stype' => 'eq'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'oProp.op_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	// Leer todas
	case 'readSingle':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "oProp.op_id AS id, oProp.a_id AS objId, 
			      oProp.op_dt_add AS dtAdd, oProp.op_dt_mod AS dtMod,
			      oProp.app_name AS appName, oProp.app_type AS appType",
				'from' => 'sb_objects_properties AS oProp',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Relación', 'db' => 'oProp.op_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objId' => ['name' => 'ID Usuario', 'db' => 'oProp.a_id', 'vtype' => 'int', 'stype' => 'eq'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'oProp.op_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
