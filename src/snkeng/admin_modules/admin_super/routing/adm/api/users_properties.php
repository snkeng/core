<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Editar
	case 'nameUpd':
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'fName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
				'lName' => ['name' => 'Apellido', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
				'uName' => ['name' => 'Nombre Completo', 'type' => 'str', 'lMin' => 7, 'lMax' => 50, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
			]
		);

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_fname='{$rData['fName']}', a_lname='{$rData['lName']}', a_obj_name='{$rData['uName']}'
WHERE a_id='{$params['vars']['objId']}'
LIMIT 1;
SQL;
		$file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_user_edit.php';
		if ( file_exists($file) ) {
			require $file;
		}
		\snkeng\core\engine\mysql::submitMultiQuery(
			$sql_upd,
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);

		//
		$response['d'] = [
			'id' => $params['vars']['objId'],
			'name' => $rData['uName'],
			'dtadd' => '',
			'email' => ''
		];
		break;

	// Leer
	case 'nameRead':
		\snkeng\core\general\saveData::sql_table_readRow(
			$response,
			[
				'elems' => [
					'a_fname' => 'fName',
					'a_lname' => 'lName',
					'a_obj_name' => 'uName',
					'a_url_name_full' => 'url',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			$params['vars']['objId']
		);
		break;

	// Editar
	case 'urlUpd':
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'url' => ['name' => 'URL', 'type' => 'str', 'lMin' => 4, 'lMax' => 50, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
			]
		);

		//
		\snkeng\core\user\management::objectUrlUpdate($response, $params['vars']['objId'], $rData['url']);
		break;

	// Leer
	case 'urlRead':
		\snkeng\core\general\saveData::sql_table_readRow($response,
			[
				'elems' => [
					'a_url_name_full' => 'url',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			$params['vars']['objId']
		);
		break;

	// Editar
	case 'permitsUpd':
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'level' => ['name' => 'Nivel', 'type' => 'int', 'process' => false, 'validate' => true],
				'sublevel' => ['name' => 'Subnivel', 'type' => 'str', 'process' => false, 'validate' => false],
				'status' => ['name' => 'Status', 'type' => 'int', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'a_adm_level' => 'level',
					'a_adm_type' => 'sublevel',
					'a_status' => 'status',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			[
				'setData' => [
					'id' => $params['vars']['objId']
				]
			]
		);
		break;

	// Leer
	case 'permitsRead':
		\snkeng\core\general\saveData::sql_table_readRow(
			$response,
			[
				'elems' => [
					'a_adm_level' => 'level',
					'a_adm_type' => 'sublevel',
					'a_status' => 'status',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			$params['vars']['objId']
		);
		break;

	// Cambiar contraseña
	case 'password':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'password' => ['name' => 'Contraseña', 'type' => 'str', 'lMin' => 4, 'lMax' => 32, 'filter' => 'password', 'process' => false, 'validate' => true],
				'sendmail' => ['name' => 'Enviar correo', 'type' => 'int', 'process' => false, 'validate' => false],
			]
		);

		//
		\snkeng\core\user\management::passRecoveryUpdate($params['vars']['objId'], $rData['password'], true);

		// Mail
		if ( $rData['sendmail'] ) {
			$sql_qry = "SELECT am_mail FROM sb_users_mail WHERE a_id={$params['vars']['objId']} AND am_status=1 LIMIT 1;";
			$email = \snkeng\core\engine\mysql::singleValue($sql_qry);
			$sql_qry = "SELECT a_obj_name FROM sb_objects_obj WHERE a_id={$params['vars']['objId']} LIMIT 1;";
			$name = \snkeng\core\engine\mysql::singleValue($sql_qry);
			//
			// Datos
			$to = "{$name} <$email>";
			$title = 'Cambio de contraseña en '.$siteVars['site']['name'];
			$message = <<<HTML
<p>Se ha actualizado la contraseña de su cuenta.</p>
<p>Para iniciar sesión use los siguientes datos.</p>
<br />
<p>Usuario: {$email}</p>
<p>Contraseña: {$rData['password']}</p>
HTML;
			//

			//
			$mail = \snkeng\core\general\mail::sendHTML($to, $title, $message, '/snkeng/site_core/templates/mail.php');
			$response['debug']['mail'] = $mail['debug']['mail'];
		}
		break;

	//
	case 'image':
		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			// Picture
			case 'profile_upload':
				\snkeng\core\socnet\object_picture::picture_upload($response, $params['vars']['objId']);
				break;

			//
			case 'profile_crop':
				\snkeng\core\socnet\object_picture::picture_crop($response, $params['vars']['objId']);
				break;

			// Banner
			case 'banner_upload':
				\snkeng\core\socnet\object_picture::banner_upload($response, $params['vars']['objId']);
				break;

			//
			case 'banner_crop':
				\snkeng\core\socnet\object_picture::banner_crop($response, $params['vars']['objId']);
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
