<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'objId_target' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
				'objId_from' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
			]
		);

		//
		$sql_qry = <<<SQL
SELECT
	rel.orel_id AS id
FROM sb_objects_relationships AS rel
WHERE rel.obj_id_target='{$rData['objId_target']}' AND rel.obj_id_from='{$rData['objId_from']}'
LIMIT 1;
SQL;
		if ( \snkeng\core\engine\mysql::notNullQuery($sql_qry) ) {
			\snkeng\core\engine\nav::killWithError('Relación ya existente.', '');
		}

		// Determinar si usuario existe
		$sql_qry = <<<SQL
SELECT
	obj.a_obj_name AS objName
FROM sb_objects_obj AS obj
WHERE obj.a_id='{$rData['objId_from']}'
LIMIT 1;
SQL;
		$objData = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			[],
			[
				'errorKey' => 'coreSuperObjRelAddUsrRead',
				'errorDesc' => 'No fue posible leer el estado del usuario.',
				'errorEmpty' => 'No existe el ID del objeto'
			]
		);

		//
		$rData['status'] = 0;
		$rData['isBlocked'] = 0;
		$rData['isSubscribed'] = 0;
		$rData['isCommend'] = 0;
		$rData['admLevel'] = 0;
		$rData['admType'] = '';

		//
		\snkeng\core\general\saveData::sql_table_addRow(
			$response,
			[
				'elems' => [
					'obj_id_target' => 'objId_target',
					'obj_id_from' => 'objId_from',
					'orel_status' => 'status',
					'orel_blocked' => 'isBlocked',
					'orel_commend' => 'isCommend',
					'orel_subscribed' => 'isSubscribed',
					'orel_admin_level' => 'admLevel',
					'orel_admin_type' => 'admType',
				],
				'tName' => 'sb_objects_relationships',
				'tId' => ['nm' => 'id', 'db' => 'orel_id']
			],
			$rData
		);

		//
		$response['d']['objName'] = $objData['objName'];

		break;

	//
	case 'readSingle':
		//
		\snkeng\core\general\saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'orel_status' => 'status',
					'orel_blocked' => 'isBlocked',
					'orel_commend' => 'isCommend',
					'orel_subscribed' => 'isSubscribed',
					'orel_admin_level' => 'admLevel',
					'orel_admin_type' => 'admType',
				],
				'tName' => 'sb_objects_relationships',
				'tId' => ['nm' => 'id', 'db' => 'orel_id']
			],
			'id'
		);
		break;

	//
	case 'upd':
		// Validar
		\snkeng\core\general\saveData::sql_complex_rowUpd($response,
			[
				'id' => ['name' => 'ID', 'type' => 'int', 'validate' => true],
				'status' => ['name' => 'Status', 'type' => 'int', 'validate' => true],
				'isBlocked' => ['name' => 'Blocked', 'type' => 'bool', 'validate' => true],
				'isCommend' => ['name' => 'Blocked', 'type' => 'bool', 'validate' => true],
				'isSubscribed' => ['name' => 'Blocked', 'type' => 'bool', 'validate' => true],
				'admLevel' => ['name' => 'Admin Level', 'type' => 'int', 'validate' => true],
				'admType' => ['name' => 'Admin Type', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleTitle', 'process' => false, 'validate' => false],
			],
			[
				'elems' => [
					'orel_status' => 'status',
					'orel_blocked' => 'isBlocked',
					'orel_commend' => 'isCommend',
					'orel_subscribed' => 'isSubscribed',
					'orel_admin_level' => 'admLevel',
					'orel_admin_type' => 'admType',
				],
				'tName' => 'sb_objects_relationships',
				'tId' => ['nm' => 'id', 'db' => 'orel_id']
			]
		);
		break;

	//
	case 'del':
		\snkeng\core\general\saveData::sql_complex_rowDel(
			$response,
			[
				'tName' => 'sb_objects_relationships',
				'tId' => ['nm' => 'id', 'db' => 'orel_id']
			]
		);
		break;


	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "asn.asn_id AS id, asn_type AS snType, asn.asn_sn_id AS snId, asn.asn_dt_add AS dtAdd,
				user.a_id AS userId, user.a_obj_name AS userName, user.a_url_name_full AS userUrlName,
				user.a_adm_level AS userLevel, user.a_adm_type AS userType",
				'from' => 'sb_users_socnet AS asn
					INNER JOIN sb_objects_obj AS user ON user.a_id=asn.a_id',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Usuario', 'db' => 'asn.asn_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objId' => ['name' => 'ID Usuario', 'db' => 'asn.a_id', 'vtype' => 'int', 'stype' => 'eq'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'asn.asn_id']
				],
				'default_order' => [
					['id', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'snType' => [
							'fb' => 'Facebook',
							'tw' => 'Twitter',
							'gp' => 'Google',
						]
					]
				]
			]
		);
		break;

	// Leer todas
	case 'readSingleIn':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "orel.orel_id AS id, orel.obj_id_target AS objIdTarget, orel.obj_id_from AS objIdFrom,
					DATE(orel.orel_dt_add) AS dtAdd, DATE(orel.orel_dt_mod) AS dtMod,
					orel.orel_status AS status, orel.orel_blocked AS isBlocked, orel.orel_subscribed AS isSubscribed, orel.orel_commend AS isCommended,
					orel.orel_admin_level AS admLevel, orel.orel_admin_type AS admType,
					obj.a_obj_name AS objName, obj.a_url_name_full AS objUrl",
				'from' => 'sb_objects_relationships AS orel
					INNER JOIN sb_objects_obj AS obj ON obj.a_id=orel.obj_id_from',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Relación', 'db' => 'orel.orel_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objIdTarget' => ['name' => 'ID Objeto receptor', 'db' => 'orel.obj_id_target', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['objId']],
					'objIdFrom' => ['name' => 'ID Objeto origen', 'db' => 'orel.obj_id_from', 'vtype' => 'int', 'stype' => 'eq'],
					'objName' => ['name' => 'Nombre', 'db' => 'obj.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'orel.orel_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	// Leer todas
	case 'readSingleOut':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "orel.orel_id AS id, orel.obj_id_target AS objIdTarget, orel.obj_id_from AS objIdFrom,
					DATE(orel.orel_dt_add) AS dtAdd, DATE(orel.orel_dt_mod) AS dtMod,
					orel.orel_status AS status, orel.orel_blocked AS isBlocked, orel.orel_subscribed AS isSubscribed, orel.orel_commend AS isCommended,
					orel.orel_admin_level AS admLevel, orel.orel_admin_type AS admType,
					obj.a_obj_name AS objName, obj.a_url_name_full AS objUrl",
				'from' => 'sb_objects_relationships AS orel
					INNER JOIN sb_objects_obj AS obj ON obj.a_id=orel.obj_id_target',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Relación', 'db' => 'orel.orel_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objIdTarget' => ['name' => 'ID Objeto receptor', 'db' => 'orel.obj_id_target', 'vtype' => 'int', 'stype' => 'eq'],
					'objIdFrom' => ['name' => 'ID Objeto origen', 'db' => 'orel.obj_id_from', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['objId']],
					'objName' => ['name' => 'Nombre', 'db' => 'obj.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'orel.orel_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
