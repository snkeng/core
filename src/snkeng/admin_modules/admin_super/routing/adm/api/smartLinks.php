<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "sl.sl_id AS id,
					sl.sl_dt_add AS dtAddFull, sl.sl_dt_mod AS dtMod, sl.sl_dt_end AS dtEnd,
					DATE(sl.sl_dt_add) AS dtAdd, DATE(sl.sl_dt_mod) AS dtMod, DATE(sl.sl_dt_end) AS dtEnd,
					sl.sl_app AS appName, sl.sl_act_name AS actName, sl.sl_act_id AS actId,
					sl.sl_key AS slKey,
					IF(NOW()<sl.sl_dt_end AND sl.sl_status=1, 1, 0) AS isActive",
				'from' => 'sc_smartlinks AS sl',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'sl.sl_id', 'vtype' => 'int', 'stype' => 'eq'],
					'appName' => ['name' => 'Nombre APP', 'db' => 'sl.sl_app', 'vtype' => 'str', 'stype' => 'like'],
					'actName' => ['name' => 'Nombre Acción', 'db' => 'sl.sl_act_name', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'sl.sl_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
