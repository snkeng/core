<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson([
			'sel' => "asn.asn_id AS id, asn_type AS snType, asn.asn_sn_id AS snId, asn.asn_dt_add AS dtAdd,
				user.a_id AS userId, user.a_obj_name AS userName, user.a_url_name_full AS userUrlName,
				user.a_adm_level AS userLevel, user.a_adm_type AS userType",
			'from' => 'sb_users_socnet AS asn
					INNER JOIN sb_objects_obj AS user ON user.a_id=asn.a_id',
			'lim' => 20,
			'where' => [
				'id' => ['name' => 'ID Usuario', 'db' => 'asn.asn_id', 'vtype' => 'int', 'stype' => 'eq'],
				'objId' => ['name' => 'ID Usuario', 'db' => 'asn.a_id', 'vtype' => 'int', 'stype' => 'eq'],
				'usrName' => ['name' => 'Usr Name', 'db' => 'user.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
				'usrUrl' => ['name' => 'Usr Url', 'db' => 'user.a_url_name_full', 'vtype' => 'str', 'stype' => 'like'],
				'usrAdmLevel' => ['name' => 'Nivel Administración', 'db' => 'user.a_adm_level', 'vtype' => 'int', 'stype' => 'eq'],
				'usrAdmSubLevel' => ['name' => 'Sub nivel administración', 'db' => 'user.a_adm_type', 'vtype' => 'str', 'stype' => 'eq'],
			],
			'order' => [
				'id' => ['Name' => 'ID', 'db' => 'asn.asn_id']
			],
			'default_order' => [
				['id', 'DESC']
			],
			'toSimple' => [
				'replace' => [
					'snType' => [
						'fb' => 'facebook-f',
						'tw' => 'twitter',
						'gp' => 'google',
					],
					'userLevel' => [
						'0' => 'U. Admin',
						'1' => 'S. Admin',
						'2' => 'Admin',
						'3' => 'Usuario',
						'4' => 'Objeto',
					]
				]
			]
		]);
		break;

	// Leer todas
	case 'readSingle':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "asn.asn_id AS id, asn_type AS snType, asn.asn_sn_id AS snId, asn.asn_dt_add AS dtAdd",
				'from' => 'sb_users_socnet AS asn',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID Usuario', 'db' => 'asn.asn_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objId' => ['name' => 'ID Usuario', 'db' => 'asn.a_id', 'vtype' => 'int', 'stype' => 'eq'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'asn.asn_id']
				],
				'default_order' => [
					['id', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'snType' => [
							'fb' => 'Facebook',
							'tw' => 'Twitter',
							'gp' => 'Google',
						]
					]
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
