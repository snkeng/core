<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		\snkeng\core\general\saveData::sql_complex_rowAdd(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 30, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'ver' => ['name' => 'Versión', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true]
			],
			[
				'elems' => [
					'mod_name' => 'title',
					'mod_ver' => 'ver'
				],
				'tName' => 'sa_modules',
				'tId' => ['nm' => 'id', 'db' => 'mod_id']
			]
		);
		break;

	// Actualizar
	case 'upd':
		// Validar
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 30, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'ver' => ['name' => 'Versión', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'content' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 0, 'filter' => 'simpleText', 'process' => false, 'validate' => false],
			],
			[
				'elems' => [
					'mod_name' => 'title',
					'mod_ver' => 'ver',
					'mod_desc' => 'content'
				],
				'tName' => 'sa_modules',
				'tId' => ['nm' => 'id', 'db' => 'mod_id']
			]
		);
		break;

	// Borrar
	case 'del':
		$tData = [
			'tName' => 'sa_modules',
			'tId' => ['db' => 'pbas_id']
		];
		// \snkeng\core\general\saveData::sql_complex_rowDel($response, $tData, 'id');
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "adm.a_id AS id,
				adm.a_obj_name AS fullName, adm.a_url_name_full AS urlName,
				adm.a_adm_level AS level, adm.a_adm_type AS sublevel, adm.a_status AS status,
				DATE(adm.a_dt_add) as dtAdd, DATE(adm.a_dt_mod) as dtMod",
				'from' => 'sb_objects_obj AS adm',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'adm.a_id', 'vtype' => 'int', 'stype' => 'eq'],
					'name' => ['name' => 'Nombre', 'db' => 'adm.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
					'urlname' => ['name' => 'Nombre URL', 'db' => 'adm.a_url_name_full', 'vtype' => 'str', 'stype' => 'like'],
					'type' => ['name' => 'Tipo', 'db' => 'adm.a_type', 'vtype' => 'str', 'stype' => 'eq', 'set'=> 'user'],
					'admLevel' => ['name' => 'Nivel Administración', 'db' => 'adm.a_adm_level', 'vtype' => 'int', 'stype' => 'eq'],
					'admSubLevel' => ['name' => 'Sub nivel administración', 'db' => 'adm.a_adm_type', 'vtype' => 'str', 'stype' => 'eq'],
					'status' => ['name' => 'Status', 'db' => 'adm.a_status', 'vtype' => 'str', 'stype' => 'eq']
				],
				'order' => [
					's_users' => ['Name' => 'ID', 'db' => 'adm.a_id']
				],
				'default_order' => [
					['s_users', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'level' => [
							'0' => 'U. Admin',
							'1' => 'S. Admin',
							'2' => 'Admin',
							'3' => 'Usuario',
							'4' => 'Objeto',
						]
					]
				]
			]
		);
		break;

	// Leer uno
	case 'readSingle':
		\snkeng\core\general\saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'mod_name' => 'title',
					'mod_ver' => 'ver',
					'mod_desc' => 'content'
				],
				'tName' => 'sa_modules',
				'tId' => ['nm' => 'id', 'db' => 'mod_id']
			],
			'id'
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
