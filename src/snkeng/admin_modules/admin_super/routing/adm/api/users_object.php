<?php
//

switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'fname' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
				'lname' => ['name' => 'Apellido', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
				'email' => ['name' => 'Email', 'type' => 'str', 'lMin' => 7, 'lMax' => 75, 'filter' => 'email', 'process' => false, 'validate' => true],
				'password' => ['name' => 'Contraseña', 'type' => 'str', 'lMin' => 4, 'lMax' => 32, 'filter' => 'password', 'process' => false, 'validate' => true],
				'level' => ['name' => 'Nivel', 'type' => 'int', 'process' => false, 'validate' => false],
				'sendmail' => ['name' => 'Enviar correo', 'type' => 'int', 'process' => false, 'validate' => false],
			]
		);

		// Verificación inicial
		if ( !\snkeng\core\engine\login::check_loginLevel('sadmin') ) {
			\snkeng\core\engine\nav::killWithError("El usuario actual no puede crear usuarios.");
		}

		// Revisar que no cree uber administradores
		if ( $rData['level'] === 0 && !\snkeng\core\engine\login::check_loginLevel('uadmin')) {
			\snkeng\core\engine\nav::killWithError('No se puede crear un usuario con estos privilegios.');
		}

		//
		\snkeng\core\user\management::userCreate($response, $rData['fname'], $rData['lname'], $rData['level'], 'mail', [
			'eMail' => $rData['email'],
			'pass' => $rData['password'],
			'mailStatus' => 1
		]);

		// Enviar correo?
		if ( $rData['sendmail'] ) {
			//
			// Datos
			$to = "{$rData['fullName']} <{$rData['email']}>";
			$title = 'Nueva cuenta en '.$siteVars['site']['name'];
			$message = <<<EOD
<p>Se ha creado una cuenta en {$siteVars['site']['name']}, directamente en el administrador.</p>
<br />
<p>Usuario: {$rData['email']}</p>
<p>Contraseña: {$rData['password']}</p>
EOD;
			//

			//
			$mail = \snkeng\core\general\mail::sendHTML($to, $title, $message, '/snkeng/site_core/templates/mail.php');
			$response['debug']['mail'] = $mail['debug']['mail'];
		}
		break;

	//
	case 'emails':
		require __DIR__ . '/emails.php';
		break;

	//
	case 'socnets':
		require __DIR__ . '/socnets.php';
		break;

	//
	case 'relationships':
		require __DIR__ . '/relationships.php';
		break;


	// Leer Uno
	case 'readSingle':
		$objId = intval($_POST['id']);
		if ( $objId === 0 ) {
			\snkeng\core\engine\nav::killWithError("No se obtuvo el id del objeto");
		}

		//
		elementReturnSingle($objId);
		break;

	// Borrar
	case 'del':
		\snkeng\core\general\saveData::sql_table_delRow(
			$response,
			[
				'tName' => 'st_site_textedit',
				'tId' => ['db' => 'ste_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[

				'sel' => "adm.a_id AS id,
					adm.a_obj_name AS fullName, adm.a_url_name_full AS urlName,
					adm.a_adm_level AS level, adm.a_adm_type AS sublevel, adm.a_status AS status,
					DATE(adm.a_dt_add) as dtAdd, DATE(adm.a_dt_mod) as dtMod",
				'from' => 'sb_objects_obj AS adm',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'adm.a_id', 'vtype' => 'int', 'stype' => 'eq'],
					'name' => ['name' => 'Nombre', 'db' => 'adm.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
					'urlname' => ['name' => 'Nombre URL', 'db' => 'adm.a_url_name_full', 'vtype' => 'str', 'stype' => 'like'],
					'type' => ['name' => 'Tipo', 'db' => 'adm.a_type', 'vtype' => 'str', 'stype' => 'eq', 'set'=> 'user'],
					'admLevel' => ['name' => 'Nivel Administración', 'db' => 'adm.a_adm_level', 'vtype' => 'int', 'stype' => 'eq'],
					'admSubLevel' => ['name' => 'Sub nivel administración', 'db' => 'adm.a_adm_type', 'vtype' => 'str', 'stype' => 'eq'],
					'status' => ['name' => 'Status', 'db' => 'adm.a_status', 'vtype' => 'str', 'stype' => 'eq']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'adm.a_id']
				],
				'default_order' => [
					['id', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'level' => [
							'0' => 'U. Admin',
							'1' => 'S. Admin',
							'2' => 'Admin',
							'3' => 'Usuario',
							'4' => 'Objeto',
						]
					]
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

//
// INIT: jsonSelection
//	Operaciones que regresan json
function elementReturnSingle($objId)
{
	$tData = [
		'elems' => [
			'ste_title' => 'title',
		],
		'tName' => 'st_site_textedit',
		'tId' => ['nm' => 'id', 'db' => 'ste_id'],
		'dtAdd' => 'ste_dtadd',
		'dtMod' => 'ste_dtmod',
	];
	return sql_table_readRow($tData, $objId);
}
// END: jsonSelection
