#!/usr/bin/env bash


echo -n "Site URL: "
read -r SITE_URL

echo -n "Short Name: "
read -r SHORT_NAME

#SITE_URL=$1
#SHORT_NAME=$2

#
if [[ -z "$SITE_URL" ]]; then
  echo "NO SITE DETECTED (FORMAT domain.bigdomain)"
  exit 1
fi
#
if [[ -z "$SHORT_NAME" ]]; then
  echo "NO DB NAME DETECTED (FORMAT domain.bigdomain)"
  exit 1
fi


# server folder
mkdir -p /var/www/$SITE_URL/public_html
chmod -R 777 /var/www/$SITE_URL/public_html

# log folder
mkdir /var/log/apache2/$SITE_URL
chmod -R 777 /var/log/apache2/$SITE_URL

# Create file reference
cat > /etc/apache2/sites-available/$SITE_URL.conf << EOF
<VirtualHost *:80>
	ServerName $SITE_URL
	ServerAlias www.$SITE_URL

	ServerAdmin webmaster@mktred.mx

	DocumentRoot /var/www/$SITE_URL/public_html

	ErrorLog \${APACHE_LOG_DIR}/$SITE_URL/error.log
	CustomLog \${APACHE_LOG_DIR}/$SITE_URL/access.log combined
</VirtualHost>
EOF


# building begin
DB_NAME="db_$SHORT_NAME"
#
DB_ADM_NM="se_${SHORT_NAME}_adm"
DB_USR_NM="se_${SHORT_NAME}_usr"
# bash generate random 48 character alphanumeric string (upper and lowercase) and
DB_ADM_PW=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 48 | head -n 1)
DB_USR_PW=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 48 | head -n 1)

COOKIE_RAND=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 48 | head -n 1)

# Enable site
a2ensite $SITE_URL

# Restart apache
service apache2 reload

# config file
cat > /var/www/$SITE_URL/config.txt << EOF
MS DB: $DB_NAME
MS ADM NM: $DB_ADM_NM
MS ADM PW: $DB_ADM_PW
MS USR NM: $DB_USR_NM
MS USR PW: $DB_USR_PW

#

CREATE SCHEMA IF NOT EXISTS `$DB_NAME` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER $DB_ADM_NM@localhost IDENTIFIED WITH mysql_native_password BY "$DB_ADM_PW";
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, DROP, EVENT, TRIGGER, INDEX ON $DB_NAME.* TO $DB_ADM_NM@localhost;

CREATE USER $DB_USR_NM@localhost IDENTIFIED WITH mysql_native_password BY "$DB_USR_PW";
GRANT SELECT, INSERT, UPDATE, DELETE ON $DB_NAME.* TO $DB_USR_NM@localhost;

FLUSH PRIVILEGES;

EOF

# config file
cat > /var/www/$SITE_URL/.env << EOF
MYSQL_SERVER="localhost"
MYSQL_DB_NAME="$DB_NAME"
MYSQL_USR_USR="$DB_USR_NM"
MYSQL_USR_PWD="$DB_USR_PW"
MYSQL_ADM_USR="$DB_ADM_NM"
MYSQL_ADM_PWD="$DB_ADM_PW"
# SE variables, all required
SE_TYPE="dev"
# Must be literaly "true" for it to be a debuging environment.
SE_DEBUG="false"
SE_SYNC_CODE="PLACE_SE_SYNC_CODE"
SE_COOKIE="$COOKIE_RAND"
SE_MAIL_CONTACT="PLACE_EMAIL"
# CUSTOM PER SITE
GOOGLE_ANALYTICS_V3="TESTENV"
GOOGLE_CAPTCHA_SECRET="TESTENV"
GOOGLE_CAPTCHA_KEY="TESTENV"
EOF


# Mysql operations
mysql -uroot -p -e <<EOFMYSQL
CREATE SCHEMA IF NOT EXISTS $DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER $DB_ADM_NM@localhost IDENTIFIED WITH mysql_native_password BY "$DB_ADM_PW";
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, DROP, EVENT, TRIGGER, INDEX ON $DB_NAME.* TO $DB_ADM_NM@localhost;

CREATE USER $DB_USR_NM@localhost IDENTIFIED WITH mysql_native_password BY "$DB_USR_PW";
GRANT SELECT, INSERT, UPDATE, DELETE ON $DB_NAME.* TO $DB_USR_NM@localhost;

FLUSH PRIVILEGES;

EOFMYSQL

#

